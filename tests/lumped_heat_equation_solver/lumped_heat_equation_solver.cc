/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <time.h>
#include <meteoio/MeteoIO.h>
#include <streamflow/libstreamflow.h>

using namespace sflow;


//------------------------------------------------------------------------------
// Values of the variables used in the test (they need to be defined outside of
// the main function since they are used in various classes defined below)
//------------------------------------------------------------------------------

struct Config {
	Config(const size_t& nTimeSteps, const double& timeStep, const size_t& nSpaceSteps,
			const double& spaceStep) : start_date(2010, 1, 1, 0, 0, 0),
					end_date(start_date), dx(spaceStep), dt(timeStep),
					nx(nSpaceSteps), nt(nTimeSteps), oscillation_period(3600.),
					inlet_amplitude(1.5), inlet_offset(283.15),
					source_amplitude(5e-3), source_offset(2e-4), source_tau(2e6),
					velocity_factor(1./(nx*dx)), velocity_offset(0.5),
					const_velocity(1.),
					rho_cp(SFConstants::WATER_MASS_DENSITY*SFConstants::WATER_HEAT_CAPACITY)
			{end_date += nt*dt/86400.;}
	mio::Date start_date;      //< automatically computed
	mio::Date end_date;        //< automatically computed based on dt and nt
	double dx, dt;             //< space step [m] and time step [s]
	size_t nx, nt;             //< number of space cells and time steps
	double oscillation_period; //< [s]   period of oscillation of the heat forcing
	double inlet_amplitude;    //< [K]   amplitude of the inlet temperature oscillations
	double inlet_offset;       //< [K]   time average of the inlet temperature oscillations
	double source_amplitude;   //< [K/s] amplitude of the heat forcing oscillations
	double source_offset;      //< [K/s] time average of the heat forcing oscillations
	double source_tau;         //< [s]   characteristic time of the heat forcing increase
	double velocity_factor;    //< [1/s] factor controlling the increase rate of velocity in space
	double velocity_offset;    //< [m/s] value of velocity at the inlet point
	double const_velocity;     //< [m/s] value of velocity when assumed constant
	const double rho_cp;       //< [J/(kg*K)]
};

double inletTemp(const double& time, const Config& cfg); //< forward declaration


//------------------------------------------------------------------------------
// Useful classes
//------------------------------------------------------------------------------

class TestSubwatershed : public LumpedSubwatershedInterface {
	public:
		TestSubwatershed(const Clock& clk) : LumpedSubwatershedInterface(clk) {};
		virtual ParameterSet getParams() const {throw std::runtime_error("Not implemented!");};
		virtual void setParams(const ParameterSet&) {/*do nothing*/};
		virtual bool isHeadSubwatershed() const {throw std::runtime_error("Not implemented!");};
		virtual void advanceInTime() {/*do nothing*/};
		virtual void reset() {/*do nothing*/};
		virtual void setDisplayInfo(const bool&) {/*do nothing*/};
		virtual bool isSet() const {throw std::runtime_error("Not implemented!");};
		virtual bool contains(const StreamReach&, const bool&) const {
			throw std::runtime_error("Not implemented!");
		};
		virtual void setStreamReach(const StreamReach&) {/*do nothing*/};
		virtual const StreamReach& getStreamReach() const {
			throw std::runtime_error("Not implemented!");
		};
		virtual const mio::Grid2DObject& getMask() const {
			throw std::runtime_error("Not implemented!");
		};
		virtual size_t getIndex() const {throw std::runtime_error("Not implemented!");};
		virtual double getArea() const {throw std::runtime_error("Not implemented!");};
		virtual double get(const SubwatershedVariable&) const {return 0.;};
		virtual void getStreamLateralInflow(CellInflow& inflow) const {
			inflow.discharge = 0.;
		}
		virtual void getStreamLateralInflow(std::vector<CellInflow>&) const {
			throw std::runtime_error("Not implemented!");
		};
		virtual void updateBoundaryConditions() {/*do nothing*/};
		virtual ~TestSubwatershed() {};
};


class TestReach: public InstantaneousRoutingReach {
	public:
		TestReach(const std::vector<StreamCell>& cells,
				StreamWidthModel& widthModel, Clock& clk,
				StreamHeightModel& heightModel,
				const Config& testConfig, const double& x,
				const std::string& type = "Lowney") :
					InstantaneousRoutingReach(cells, widthModel, clk, heightModel),
					_config(testConfig) {
			if(mio::IOUtils::strToLower(type) == "lowney") {
				_discharge = 1.;
				_inlet_bc.discharge = 1.;
			} else {
				_discharge = testConfig.velocity_factor*x + testConfig.velocity_offset;
				_inlet_bc.discharge = _discharge - testConfig.velocity_factor*_length/2.;
			}
			_water_height = 1.;
			_inlet_bc.temperature = testConfig.inlet_offset;
			_width = widthModel.getWidth(*this);
		};
		virtual void updateInletBoundaryConditions() override {
			if(this->isHeadReach()) {
				const double elapsedTime(round(86400.*(_clock.getCurrTime().getJulian() -
						_clock.getStartTime().getJulian()))); //< [s]
				_inlet_bc.temperature = inletTemp(elapsedTime, _config);
			} else {
				const double tmp(_inlet_bc.discharge);
				InstantaneousRoutingReach::updateInletBoundaryConditions();
				_inlet_bc.discharge = tmp;
			}
		}
		virtual void advanceInTime() override {/*do nothing*/};
		virtual ~TestReach() {};

	protected:
		const Config& _config;
		virtual void setInitialConditions() override {/*do nothing*/};
};


class SimpleHeatFluxModel : public ReachHeatFluxModel {
	public:
		SimpleHeatFluxModel(const mio::Config& uselessCfg, const Config& testCfg) :
			ReachHeatFluxModel(uselessCfg), _config(testCfg) {};
		virtual double getAbsorbedSWRadiation(const StreamCell&,
				const mio::Date&, const mio::MeteoData&) const override {return 0.;};
		virtual double getAbsorbedLWRadiation(const mio::MeteoData&) const override {return 0.;};
		virtual double getEmittedLWRadiation(const double& streamTemp) const override {
			return _config.rho_cp*streamTemp/_config.source_tau;
		}
		virtual double getLatentHeatFlux(const StreamCell&,
				const mio::MeteoData&, const double&) const override {return 0.;};
		virtual double getSensibleHeatFlux(const StreamCell&,
				const mio::MeteoData&, const double&) const override {return 0.;};
		virtual double getBedConductionHeatFlux(const double&,
				const double&) const override {return 0.;};
		virtual double getBedFrictionHeatFlux(const double&, const double&,
				const double&) const override {return 0.;};
		virtual ~SimpleHeatFluxModel() {};

	protected:
		const Config& _config;
};


class LowneyHeatFluxModel : public SimpleHeatFluxModel {
	public:
		LowneyHeatFluxModel(const mio::Config& uselessCfg, const Config& testCfg) :
			SimpleHeatFluxModel(uselessCfg, testCfg) {};
		virtual double getAbsorbedSWRadiation(const StreamCell&,
				const mio::Date& date, const mio::MeteoData&) const override {
			const double elapsedTime(round(86400.*(date.getJulian() -
					_config.start_date.getJulian()))); //< [s]
			const double omega(2*SFConstants::PI/_config.oscillation_period);
			return _config.rho_cp*(_config.source_amplitude*sin(omega*elapsedTime) +
					_config.source_offset);
		}
		virtual ~LowneyHeatFluxModel() {};
};


class DummyInputReader : public ReachInputReader {
	using ReachInputReader::getLumpedMeteoData;
	using ReachInputReader::getMeteoData;
	public:
		DummyInputReader() : ReachInputReader() {};
		virtual double getLumpedMeteoData(const mio::Date&,
				const std::string&, const StreamReach&) {return 0.;};
		virtual void getLumpedMeteoData(const mio::Date&,
				const StreamReach&, mio::MeteoData& data) {data.addParameter("TSOIL");};
		virtual std::vector<double> getMeteoData(const mio::Date&,
				const std::string&, const StreamReach&) {return {};};
		virtual void getMeteoData(const mio::Date&, const StreamReach&,
				std::vector<mio::MeteoData>&) {};
		virtual ~DummyInputReader() {};
};


//------------------------------------------------------------------------------
// Global functions
//------------------------------------------------------------------------------

double inletTemp(const double& time, const Config& cfg) {
	return cfg.inlet_offset + cfg.inlet_amplitude*sin(2*SFConstants::PI*time/cfg.oscillation_period);
}

double analyticalSolution(const double& x, const double& t,	const Config& cfg,
		std::string type = "Lowney") {
	double output(0.);
	type = mio::IOUtils::strToLower(type);
	if(type == "lowney") {
		const double t0(t - x/cfg.const_velocity);
		const double e(exp(x/(cfg.const_velocity*cfg.source_tau)));
		const double omega(2*SFConstants::PI/cfg.oscillation_period);
		const double factor(cfg.source_amplitude*cfg.source_tau/
				(1 + mio::Optim::pow2(omega*cfg.source_tau)));
		output += inletTemp(t0, cfg)*e + cfg.source_offset*cfg.source_tau*(e - 1.);
		output -= factor*(sin(omega*t) + cfg.source_tau*omega*cos(omega*t));
		output += factor*e*(sin(omega*t0) + cfg.source_tau*omega*cos(omega*t0));
	} else if(type == "simple") {
		const double factor(cfg.velocity_factor/cfg.velocity_offset*x + 1.);
		output += inletTemp(t - log(factor)/cfg.velocity_factor, cfg)*
				pow(factor, 1./(cfg.velocity_factor*cfg.source_tau));
	} else {
		throw std::runtime_error("Cannot compute analytical solution for case '"
				+ type + "': unknown case");
	}
	return output;
}

double modelRMSE(const std::vector<double>& simuValues, const Config& cfg,
		const std::string& type = "Lowney") {
	double sse(0.);
	for(size_t iCell(0); iCell < cfg.nx; ++iCell) {
		const double x((iCell + 0.5)*cfg.dx);
		const double err(simuValues[iCell] -
				analyticalSolution(x, cfg.nt*cfg.dt, cfg, type));
		sse += err*err;
	}
	return sqrt(sse/cfg.nx);
}

void writeResultsInFile(const std::string& filename, const std::vector<double>& simuValues,
		const Config& cfg, const std::string& type = "Lowney") {
	std::ofstream f(filename);
	if(f.fail()) throw mio::AccessException(filename, AT);
	f << "SMET 1.1 ASCII" << std::endl;
	f << "[HEADER]" << std::endl;
	f << "station_id   = "              << type << "_case" << std::endl;
	f << "station_name = subwatershed_" << type << "_case" << std::endl;
	f << "latitude     = -999" << std::endl;
	f << "longitude    = -999" << std::endl;
	f << "altitude     = -999" << std::endl;
	f << std::right << std::fixed << std::setprecision(1);
	f << "nodata       = " << mio::IOUtils::nodata << std::endl;
	f << "fields       = position analytical_solution simulated_solution\n";
	f << "units        = m K K" << std::endl;
	f << "[DATA]" << std::endl;
	f << std::right << std::fixed << std::setprecision(3);
	for(size_t iCell(0); iCell < cfg.nx; ++iCell) {
		const double x((iCell + 0.5)*cfg.dx);
		f << std::setw(10) << x;
		f << std::setw(10) << analyticalSolution(x, cfg.nt*cfg.dt, cfg, type);
		f << std::setw(10) << simuValues[iCell] << std::endl;
	}
	f.close();
}

int runSimulation(const mio::Config& stdCfg, const Config& testCfg,
		const double& precision, std::string type = "Lowney") {
	//Define some useful variables
	Clock clk(stdCfg, testCfg.start_date, testCfg.end_date);
	DischargePowerHeightModel heightModel(stdCfg);
	UniformWidth widthModel(stdCfg);
	DummyInputReader inputReader;
	TestSubwatershed subw(clk);
	int status(EXIT_SUCCESS);
	type = mio::IOUtils::strToLower(type);
	ReachHeatFluxModel *heatFluxModel(nullptr);
	if(type == "lowney")
		heatFluxModel = new LowneyHeatFluxModel(stdCfg, testCfg);
	else
		heatFluxModel = new SimpleHeatFluxModel(stdCfg, testCfg);

	//Create the stream reaches
	std::vector<LumpedReachInterface*> reaches;
	reaches.reserve(testCfg.nx);
	mio::Coords pt("CH1903");
	for(size_t iReach(0); iReach < testCfg.nx; ++iReach) {
		const double x((iReach + 0.5)*testCfg.dx);
		pt.setXY(x, 0., 0.);
		const StreamCell cell(pt, 0., 0., testCfg.dx, 0.);
		LumpedReachInterface* tmp(new TestReach({cell}, widthModel, clk,
				heightModel, testCfg, x, type));
		tmp->setSubwatershed(subw);
		if(iReach > 0) tmp->setUpstreamReach(*reaches[iReach - 1]);
		LumpedReachInterface* reach(new LumpedReachWithEB(tmp, *heatFluxModel,
				inputReader));
		reaches.push_back(reach);
	}

	//Run simulation for the Lowney case
	std::cout << "[i] Running model for the " << type << " case with a space "
	          << "step of " << std::fixed << std::setprecision(0) << testCfg.dx
	          << "m and a time step of " << clk.getTimeStep()
	          << "s..." << std::endl;
	while(clk.getCurrTime() < clk.getEndTime()) {
		for(StreamReach* reach : reaches) {
			reach->updateInletBoundaryConditions();
			reach->advanceInTime();
		}
		clk.advanceInTime();
	}

	//Evaluate the simulation and write results in output file
	std::vector<double> temp;
	temp.reserve(testCfg.nx);
	for(LumpedReachInterface* reach : reaches) {
		temp.push_back(reach->get(StreamVariable::T));
	}
	const double rmse (modelRMSE(temp, testCfg, type));
	std::cout << std::scientific << std::setprecision(3);
	if(rmse <= precision) {
		std::cout << "[OK] Simulation RMSE (" << rmse << " degC) is lower than "
				<< "the target value (" << precision << " degC)" << std::endl;
	} else {
		std::cout << "[FAIL] Simulation RMSE (" << rmse << " degC) is larger than "
				<< "the target value (" << precision << " degC)" << std::endl;
		status = EXIT_FAILURE;
	}

	delete heatFluxModel;
	for(StreamReach *reach : reaches)
		delete reach;
	return status;
}


//------------------------------------------------------------------------------
// Main function
//------------------------------------------------------------------------------

int main(int argc, char *argv[])
{
	//Get the target precision
	if(argc < 2)
		throw std::invalid_argument("Program expects the target precision (in deg C) as an argument");
	double precision;
	mio::IOUtils::convertString(precision, std::string(argv[1]));

	//Define the variable containing the test configuration
	const size_t simulationDuration(8*3600); //< [s]
	const size_t domainLength(12800);        //< [m]
	const size_t dt(1);                      //< [s]
	const size_t dx(16);                     //< [m]
	const Config testCfg(simulationDuration/dt, dt, domainLength/dx, dx);

	//Define some additional useful variables
	StreamflowConfig stdCfg;
	std::ostringstream os;
	os << std::fixed << std::setprecision(10) << dt/60.;
	stdCfg.addKey("TIME_STEP", SFConstants::CONFIG_SECTION_NAME, os.str());
	stdCfg.addKey("TIME_STEP::LIMITS", SFConstants::CONFIG_SECTION_NAME, "0 " + os.str());
	stdCfg.addKey("UNIFORM_WIDTH", SFConstants::CONFIG_SECTION_NAME, "1");
	stdCfg.addKey("HEIGHT_MODEL_FACTOR", SFConstants::CONFIG_SECTION_NAME, "5e-2");
	stdCfg.addKey("HEIGHT_MODEL_POWER", SFConstants::CONFIG_SECTION_NAME, "0.6");
	stdCfg.addKey("WIND_FUNCTION_FACTOR", SFConstants::CONFIG_SECTION_NAME, "2e-3");
	stdCfg.addKey("WIND_FUNCTION_OFFSET", SFConstants::CONFIG_SECTION_NAME, "2e-3");
	stdCfg.addKey("BED_HEAT_TRANSFER_COEFF", SFConstants::CONFIG_SECTION_NAME, "10");

	//Run simulation for the Lowney case
	int status1(runSimulation(stdCfg, testCfg, precision, "Lowney"));
	std::cout << std::endl;

	//Run simulation for the simple case
	int status2(runSimulation(stdCfg, testCfg, precision, "Simple"));

	return std::max(status1, status2);
}
