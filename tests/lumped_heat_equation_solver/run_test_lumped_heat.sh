#!/bin/bash
# This script is used to test the lumped heat equation solver

# Run the test
PRECISION="0.50" # [K]
./lumped_heat_equation_solver ${PRECISION}
