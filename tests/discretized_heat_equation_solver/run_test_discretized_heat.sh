#!/bin/bash
# This script is used to test the discretized heat equation solver

# Run the test
PRECISION="0.13" # [K]
./discretized_heat_equation_solver ${PRECISION}
