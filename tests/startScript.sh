#!/bin/bash
#warning: the order IS important (they depend on each other)

log_echo() {
        msg=$1
        me=${USER}
        datum=$(date "+%Y-%m-%dT%H:%M:%S")
        printf "[${datum}] [${me}] ${msg}\n"
}

#get expanded path to STREAMFLOW's root, ie one level above this script
tmp=`dirname $0`
tmp="${tmp}/../"
cd ${tmp}
SF_ROOT=`pwd`
cd -

#run the tests
log_echo "Starting STREAMFLOW testing with SF_ROOT=${SF_ROOT}"
cd ${SF_ROOT}

make distclean
/usr/bin/ctest -S ${SF_ROOT}/tests/startScriptCoverage.cmake -V > ${SF_ROOT}/tests/startScriptCoverage.log 2>&1

make distclean
/usr/bin/ctest -S ${SF_ROOT}/tests/startScriptValgrind.cmake -V > ${SF_ROOT}/tests/startScriptValgrind.log 2>&1

make distclean
/usr/bin/ctest -S ${SF_ROOT}/tests/startScriptNightly.cmake -V > ${SF_ROOT}/tests/startScriptNightly.log 2>&1

log_echo "STREAMFLOW testing done!"
