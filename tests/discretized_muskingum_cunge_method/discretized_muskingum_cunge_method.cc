/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <time.h>
#include <meteoio/MeteoIO.h>
#include <streamflow/libstreamflow.h>

using namespace sflow;


class TestReach : public DiscrMuskingumCungeReach {
	public:
		TestReach(const std::vector<StreamCell>& cells,
				StreamWidthModel& widthModel, Clock& clk,
				const StreamBedFrictionModel& fricModel) :
					DiscrMuskingumCungeReach(cells, widthModel, clk, fricModel) {
			_water_height.assign(_n_cells, 1.);
			_discharge.assign(_n_cells, 10.);
			_inlet_bc.discharge = 10.;
			_width = _width_model.getWidth(*this);
		};
		virtual void updateInletBoundaryConditions() override {
			_inlet_discharge_old = _inlet_bc.discharge;
			const double t(round(86400.*(_clock.getCurrTime().getJulian() -
					_clock.getStartTime().getJulian())) + 1); //< add 1 second to avoid the case t = 0 (which creates problems in the next line)
			_inlet_bc.discharge = 10. + 1e5/t*exp(-3.*mio::Optim::pow2(log(t/10.) - 7.6)); //< log-normal distribution
		}
		virtual ~TestReach() {};

	protected:
		virtual void setInitialConditions() override {/*do nothing*/};
};



std::ofstream& initializeOutputFile(const std::string& filename) {
	std::ofstream *tmp(new std::ofstream(filename));
	std::ofstream& f(*tmp);
	if(f.fail()) throw mio::AccessException(filename, AT);
	f << "SMET 1.1 ASCII" << std::endl;
	f << "[HEADER]" << std::endl;
	f << "station_id   = test_discretized_muskingum_cunge" << std::endl;
	f << "station_name = test_discretized_muskingum_cunge" << std::endl;
	f << "latitude     = -999" << std::endl;
	f << "longitude    = -999" << std::endl;
	f << "altitude     = -999" << std::endl;
	f << std::right << std::fixed << std::setprecision(1);
	f << "nodata       = " << mio::IOUtils::nodata << std::endl;
	f << "fields       = timestamp water_depth_inlet water_depth_outlet "
	  << "discharge_inlet discharge_outlet" << std::endl;
	f << "units        = - m m m^3/s m^3/s" << std::endl;
	f << "[DATA]" << std::endl;
	f << std::right << std::fixed << std::setprecision(3);
	return f;
}



//------------------------------------------------------------------------------
// Main function
//------------------------------------------------------------------------------
int main()
{
	//Define the test configuration
	const size_t simulationDuration(86400); //< [s]
	const size_t domainLength(20000);       //< [m]
	const size_t dt(5);                     //< [s]
	const size_t dx(100);                   //< [m]
	const size_t nx(domainLength/dx);
	const mio::Date startDate(2010, 1, 1, 0, 0, 0);
	const double bedSlope(0.0003);

	//Define some additional useful variables
	StreamflowConfig stdCfg;
	std::ostringstream os;
	os << std::fixed << std::setprecision(10) << dt/60.;
	stdCfg.addKey("TIME_STEP", SFConstants::CONFIG_SECTION_NAME, os.str());
	stdCfg.addKey("TIME_STEP::LIMITS", SFConstants::CONFIG_SECTION_NAME, "0 " + os.str());
	stdCfg.addKey("UNIFORM_WIDTH", SFConstants::CONFIG_SECTION_NAME, "15");
	stdCfg.addKey("MANNING_COEFF", SFConstants::CONFIG_SECTION_NAME, "0.06");
	Clock clk(stdCfg, startDate, startDate + simulationDuration/86400.);
	ManningFrictionModel fricModel(stdCfg);
	UniformWidth widthModel(stdCfg);

	//Create the vector of stream cells
	std::vector<StreamCell> cells;
	cells.reserve(nx);
	mio::Coords pt("CH1903");
	for(size_t iCell(0); iCell < nx; ++iCell) {
		pt.setXY((iCell + 0.5)*dx, 0., (nx - 0.5 - iCell)*dx*bedSlope);
		StreamCell cell(pt, 0., 0., dx, bedSlope);
		cells.push_back(cell);
	}

	//Run simulation
	std::ofstream& f(initializeOutputFile("output.smet"));
	TestReach reach(cells, widthModel, clk, fricModel);
	while(clk.getCurrTime() < clk.getEndTime()) {
		reach.updateInletBoundaryConditions();
		reach.advanceInTime();
		clk.advanceInTime();
		f << clk.getCurrTime().toString(mio::Date::ISO);
		f << std::setw(10) << reach.get(StreamVariable::H, 0);
		f << std::setw(10) << reach.get(StreamVariable::H, nx - 1);
		f << std::setw(10) << reach.get(StreamVariable::Q, 0);
		f << std::setw(10) << reach.get(StreamVariable::Q, nx - 1) << std::endl;
	}

	f.close();
	delete &f;
	return 0;
}
