#!/bin/bash
# This script is used to test the input reader

# Print a special line to prevent CTest from truncating the test output
echo "CTEST_FULL_OUTPUT (line required by CTest to avoid output truncation)"
echo ""

# Perform the test
PRECISION="1e-4"
./input_reader ${PRECISION}
