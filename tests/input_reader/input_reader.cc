/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <ctime>
#include <streamflow/libstreamflow.h>
#include <meteoio/MeteoIO.h>

using namespace sflow;


//------------------------------------------------------------------------------
// Useful classes
//------------------------------------------------------------------------------

class TestSubwatershed : public LumpedSubwatershed {
	public:
		TestSubwatershed(const mio::Grid2DObject& grid, const size_t& index,
				const Clock& clk) : LumpedSubwatershed(grid, index, clk) {};
		virtual void updateBoundaryConditions() {/*do nothing*/};
		virtual void advanceInTime() {/*do nothing*/};
		virtual void setDisplayInfo(const bool&) {/*do nothing*/};
		virtual ParameterSet getParams() const {throw std::runtime_error("Not implemented!");};
		virtual void setParams(const ParameterSet&) {/*do nothing*/};
		virtual void reset() {/*do nothing*/};
		virtual ~TestSubwatershed() {};
};

class DummyWidthModel : public StreamWidthModel {
	public:
		DummyWidthModel() {};
		virtual double getWidth(const StreamReach&) {throw std::runtime_error("Not implemented!");};
		virtual ParameterSet getParams() const {throw std::runtime_error("Not implemented!");};
		virtual void setParams(const ParameterSet&) {/*do nothing*/};
		virtual ~DummyWidthModel() {};
};

class TestReach : public LumpedStreamReach {
	public:
		TestReach(const std::vector<StreamCell>& cells,
				StreamWidthModel& widthModel, const Clock& clk) :
					LumpedStreamReach(cells, widthModel, clk) {};
		virtual ParameterSet getParams() const {throw std::runtime_error("Not implemented!");};
		virtual void setParams(const ParameterSet&) {/*do nothing*/};
		virtual void advanceInTime() {/*do nothing*/};
		virtual void setDisplayInfo(const bool&) {/*do nothing*/};
		virtual ~TestReach() {};
};


//------------------------------------------------------------------------------
// Global functions
//------------------------------------------------------------------------------

double maxRelativeDifference(const std::vector<double>& vec, const std::vector<double>& ref) {
	double output(0.);
	if(vec.size() != ref.size())
		throw std::invalid_argument("The two vectors must have the same length");

	for(size_t ie(0); ie < vec.size(); ++ie)
		output = fmax(output, fabs((vec[ie] - ref[ie])/ref[ie]));

	return output;
}


double maxRelativeDifference(const mio::MeteoData& data, const mio::MeteoData& ref) {
	double output(0.);

	for(size_t iVar(0); iVar < ref.getNrOfParameters(); ++iVar) {
		const std::string& paramName(ref.getNameForParameter(iVar));
		if(!data.param_exists(paramName)) {
			throw std::invalid_argument("The two MeteoData instances do not hold "
					"the same variables (variable " + paramName + " missing)");
		}
		output = fmax(output, fabs((data(paramName) - ref(paramName))/ref(paramName)));
	}

	return output;
}


bool testSubwDataReading(InputReader& reader, const TestSubwatershed& subw,
		const std::vector<mio::Grid2DObject>& expectedGrids,
		const std::vector<double>& expectedMeanValues, const double& precision) {
	//Define useful variables
	const mio::Date testDate(2013, 10, 1, 13, 0, 1);
	const mio::Date startDate(2013, 10, 1, 4, 0, 1);
	const mio::Date endDate(2013, 10, 1, 22, 0, 1);
	bool status(true);
	mio::Grid2DObject returnedGrid, relDiffGrid;
	double returnedValue, relDiff;
	std::ostringstream os;
	const std::vector<std::string> variables({"ROT", "TA", "ISWR"});
	const size_t lineLength(72);

	//Read grid values over the subwatershed on a particular date
	std::cout << std::left << std::setfill('.');
	for(size_t iVar(0); iVar < variables.size(); ++iVar) {
		const std::string& var(variables[iVar]);
		os.str("");
		os << "Reading " << var << " grid values over subwatershed "
		   << subw.getIndex() << " on " << testDate.toString(mio::Date::ISO)
		   << " ";
		std::cout << std::setw(lineLength) << os.str();
		returnedGrid = reader.getMeteoData(testDate, var, subw);
		relDiffGrid  = (returnedGrid - expectedGrids[iVar])/expectedGrids[iVar];
		if(relDiffGrid.grid2D.getAbs().getMax() <= precision) {
			std::cout << " [OK]" << std::endl;
		} else {
			std::cout << " [FAIL]" << std::endl;
			status = false;
		}
	}

	//Read ROT grid values over the subwatershed, averaged over 24h
	os.str("");
	os << "    " << endDate.toString(mio::Date::ISO) << " over subwatershed "
	   << subw.getIndex() << " ";
	std::cout << "Reading ROT grid values averaged between "
	          << startDate.toString(mio::Date::ISO) << " and" << std::endl
	          << std::setw(lineLength) << os.str();
	returnedGrid = reader.getTimeAveragedMeteoData(startDate, endDate, "ROT", subw);
	relDiffGrid  = (returnedGrid - expectedGrids.back())/expectedGrids.back();
	if(relDiffGrid.grid2D.getAbs().getMax() <= precision) {
		std::cout << " [OK]" << std::endl;
	} else {
		std::cout << " [FAIL]" << std::endl;
		status = false;
	}

	//Read mean grid values averaged in space over the subwatershed
	for(size_t iVar(0); iVar < variables.size(); ++iVar) {
		const std::string& var(variables[iVar]);
		os.str("");
		os << "Reading mean " << var << " value over subwatershed "
		   << subw.getIndex() << " on " << testDate.toString(mio::Date::ISO)
		   << " ";
		std::cout << std::setw(lineLength) << os.str();
		returnedValue = reader.getLumpedMeteoData(testDate, var, subw);
		relDiff = (returnedValue - expectedMeanValues[iVar])/expectedMeanValues[iVar];
		if(fabs(relDiff) < precision) {
			std::cout << " [OK]" << std::endl;
		} else {
			std::cout << " [FAIL]" << std::endl;
			status = false;
		}
	}

	//Read mean ROT value over the subwatershed, averaged in time over 24h
	os.str("");
	os << "    " << startDate.toString(mio::Date::ISO) << " and "
	   << endDate.toString(mio::Date::ISO) << " ";
	std::cout << "Reading mean ROT value over subwatershed " << subw.getIndex()
	          << ", averaged in time between" << std::endl
			  << std::setw(lineLength) << os.str();
	returnedValue = reader.getLumpedTimeAveragedMeteoData(startDate, endDate,
			"ROT", subw);
	relDiff = (returnedValue - expectedMeanValues.back())/expectedMeanValues.back();
	if(fabs(relDiff) < precision) {
		std::cout << " [OK]" << std::endl;
	} else {
		std::cout << " [FAIL]" << std::endl;
		status = false;
	}

	return status;
}


bool testReachDataReading(InputReader& reader, const TestReach& reach,
		const std::vector<std::vector<double>>& expectedVectors,
		const mio::MeteoData& expectedMeanData, const double& precision) {
	//Define useful variables
	const mio::Date testDate(2013, 10, 1, 12, 0, 1);
	bool status(true);
	std::ostringstream os;
	mio::MeteoData returnedMeanData;
	returnedMeanData.addParameter("TSOIL");
	const std::vector<std::string> variables({"TSOIL", "TA", "ISWR"});
	const size_t lineLength(72);

	//Read the values of specified meteo variables over the given stream cells
	std::cout << std::left << std::setfill('.');
	for(size_t iVar(0); iVar < variables.size(); ++iVar) {
		const std::string& var(variables[iVar]);
		os.str("");
		os << "Reading " << var << " values over the stream reach cells "
		   << "on " << testDate.toString(mio::Date::ISO) << " ";
		std::cout << std::setw(lineLength) << os.str();
		const std::vector<double> returnedVector(reader.getMeteoData(testDate,
				var, reach));
		if(maxRelativeDifference(returnedVector, expectedVectors[iVar]) <= precision) {
			std::cout << " [OK]" << std::endl;
		} else {
			std::cout << " [FAIL]" << std::endl;
			status = false;
		}
	}

	//Read the space-average of all meteo variables over the given stream cells
	os.str("");
	os << "the stream reach cells on " << testDate.toString(mio::Date::ISO) << " ";
	std::cout << "Reading the space-averaged values of all meteorological variables "
	          << "over" << std::endl << std::setw(lineLength) << os.str();
	reader.getLumpedMeteoData(testDate, reach, returnedMeanData);
	if(maxRelativeDifference(returnedMeanData, expectedMeanData) <= precision) {
		std::cout << " [OK]" << std::endl;
	} else {
		std::cout << " [FAIL]" << std::endl;
		status = false;
	}

	return status;
}


void getExpectedGridValuesOverSubw1(std::vector<mio::Grid2DObject>& expectedGrids,
		std::vector<double>& expectedMeanValues) {
	expectedGrids.resize(4);
	const double& nodata(mio::IOUtils::nodata);
	mio::Coords pt("CH1903");
	pt.setXY(782550, 171750, nodata);
	mio::Grid2DObject grid(5, 5, 100., pt);

	//ROT grid on 2013-10-1T13:00:00+1
	grid(0,4) = nodata;  grid(1,4) = 0.09500; grid(2,4) = 0.26700; grid(3,4) = 0.63375; grid(4,4) = 0.81525;
	grid(0,3) = 0.09525; grid(1,3) = 0.27825; grid(2,3) = 0.64775; grid(3,3) = 0.82175; grid(4,3) = 0.81475;
	grid(0,2) = 0.47000; grid(1,2) = 0.64650; grid(2,2) = 0.82975; grid(3,2) = 0.82075; grid(4,2) = 0.81550;
	grid(0,1) = 0.84825; grid(1,1) = 0.83550; grid(2,1) = 0.82750; grid(3,1) = 0.82175; grid(4,1) = nodata;
	grid(0,0) = 0.65400; grid(1,0) = 0.82850; grid(2,0) = 0.82525; grid(3,0) = 0.82275; grid(4,0) = nodata;
	expectedGrids[0] = grid;

	//TA grid on 2013-10-1T13:00:00+1
	grid(0,4) = nodata;    grid(1,4) = 283.55875; grid(2,4) = 283.67875; grid(3,4) = 283.77250; grid(4,4) = 283.84625;
	grid(0,3) = 283.54325; grid(1,3) = 283.65125; grid(2,3) = 283.74500; grid(3,3) = 283.81425; grid(4,3) = 283.86200;
	grid(0,2) = 283.67725; grid(1,2) = 283.74675; grid(2,2) = 283.80525; grid(3,2) = 283.85275; grid(4,2) = 283.88800;
	grid(0,1) = 283.80175; grid(1,1) = 283.83725; grid(2,1) = 283.86500; grid(3,1) = 283.89425; grid(4,1) = nodata;
	grid(0,0) = 283.89550; grid(1,0) = 283.91250; grid(2,0) = 283.92475; grid(3,0) = 283.94500; grid(4,0) = nodata;
	expectedGrids[1] = grid;

	//ISWR grid on 2013-10-1T13:00:00+1
	grid(0,4) = nodata;    grid(1,4) = 475.62100; grid(2,4) = 468.70500; grid(3,4) = 460.63950; grid(4,4) = 453.68775;
	grid(0,3) = 481.45575; grid(1,3) = 475.96975; grid(2,3) = 468.28225; grid(3,3) = 461.11025; grid(4,3) = 455.61075;
	grid(0,2) = 483.25650; grid(1,2) = 476.28950; grid(2,2) = 468.49025; grid(3,2) = 462.03975; grid(4,2) = 457.42925;
	grid(0,1) = 481.39125; grid(1,1) = 473.66950; grid(2,1) = 467.98525; grid(3,1) = 463.39725; grid(4,1) = nodata;
	grid(0,0) = 474.01200; grid(1,0) = 468.60625; grid(2,0) = 465.70300; grid(3,0) = 463.43150; grid(4,0) = nodata;
	expectedGrids[2] = grid;

	//ROT grid averaged in time between 2013-10-1T04:00:00+1 and 2013-10-1T22:00:00+1
	grid(0,4) = nodata;  grid(1,4) = 0.07729; grid(2,4) = 0.22854; grid(3,4) = 0.53457; grid(4,4) = 0.68643;
	grid(0,3) = 0.07732; grid(1,3) = 0.23175; grid(2,3) = 0.53950; grid(3,3) = 0.68971; grid(4,3) = 0.68718;
	grid(0,2) = 0.39371; grid(1,2) = 0.54436; grid(2,2) = 0.69589; grid(3,2) = 0.69164; grid(4,2) = 0.68961;
	grid(0,1) = 0.71146; grid(1,1) = 0.70304; grid(2,1) = 0.69811; grid(3,1) = 0.69529; grid(4,1) = nodata;
	grid(0,0) = 0.55043; grid(1,0) = 0.70186; grid(2,0) = 0.69971; grid(3,0) = 0.69896; grid(4,0) = nodata;
	expectedGrids[3] = grid;

	//Space-averaged values of variables ROT, TA and ISWR over the subwatershed
	//on 2013-10-01T13:00:00+1, and mean spaced-averaged value of ROT over the
	//subwatershed between 2013-10-01T04:00:00+1 and 2013-10-01T22:00:00+1
	expectedMeanValues = {0.04032, 283.79627, 468.49015, 0.03396};
}


void getExpectedFileValuesOverSubw1(std::vector<mio::Grid2DObject>& expectedGrids,
		std::vector<double>& expectedMeanValues) {
	//Space-averaged values of variables ROT, TA and ISWR over the subwatershed
	//on 2013-10-01T13:00:00+1, and mean spaced-averaged value of ROT over the
	//subwatershed between 2013-10-01T04:00:00+1 and 2013-10-01T22:00:00+1
	expectedMeanValues = {4.3869e-3, 277.01, 208.46, 4.1441e-03};

	//Define useful variables
	expectedGrids.resize(4);
	const double& nodata(mio::IOUtils::nodata);
	mio::Coords pt("CH1903");
	pt.setXY(782550, 171750, nodata);
	mio::Grid2DObject grid;

	//ROT grid on 2013-10-1T13:00:00+1
	grid.set(5, 5, 100., pt, expectedMeanValues[0]*1000.*3600./220000.); //< convert from m^3/s to mm/h
	expectedGrids[0] = grid;

	//TA grid on 2013-10-1T13:00:00+1
	grid(0,4) = nodata;    grid(1,4) = 276.90867; grid(2,4) = 277.19267; grid(3,4) = 277.15867; grid(4,4) = 277.03067;
	grid(0,3) = 275.99467; grid(1,3) = 276.65067; grid(2,3) = 277.01667; grid(3,3) = 277.06467; grid(4,3) = 277.01467;
	grid(0,2) = 275.73067; grid(1,2) = 276.41267; grid(2,2) = 276.82067; grid(3,2) = 276.94067; grid(4,2) = 276.95867;
	grid(0,1) = 275.64667; grid(1,1) = 276.20867; grid(2,1) = 276.58867; grid(3,1) = 276.76267; grid(4,1) = nodata;
	grid(0,0) = 275.62267; grid(1,0) = 276.03267; grid(2,0) = 276.36667; grid(3,0) = 276.57867; grid(4,0) = nodata;
	expectedGrids[1] = grid;

	//ISWR grid on 2013-10-1T13:00:00+1
	grid(0,4) = nodata;    grid(1,4) = 211.76770; grid(2,4) = 211.07190; grid(3,4) = 211.15520; grid(4,4) = 211.46880;
	grid(0,3) = 214.00700; grid(1,3) = 212.39980; grid(2,3) = 211.50310; grid(3,3) = 211.38550; grid(4,3) = 211.50800;
	grid(0,2) = 214.65380; grid(1,2) = 212.98290; grid(2,2) = 211.98330; grid(3,2) = 211.68930; grid(4,2) = 211.64520;
	grid(0,1) = 214.85960; grid(1,1) = 213.48270; grid(2,1) = 212.55170; grid(3,1) = 212.12540; grid(4,1) = nodata;
	grid(0,0) = 214.91840; grid(1,0) = 213.91390; grid(2,0) = 213.09560; grid(3,0) = 212.57620; grid(4,0) = nodata;
	expectedGrids[2] = grid;

	//ROT grid averaged in time between 2013-10-1T04:00:00+1 and 2013-10-1T22:00:00+1
	grid.set(5, 5, 100., pt, expectedMeanValues[3]*1000.*3600./220000.); //< convert from m^3/s to mm/h
	expectedGrids[3] = grid;
}


void getExpectedGridValuesOverSubw2(std::vector<mio::Grid2DObject>& expectedGrids,
		std::vector<double>& expectedMeanValues) {
	expectedGrids.resize(4);
	const double& nodata(mio::IOUtils::nodata);
	mio::Coords pt("CH1903");
	pt.setXY(782525, 171725, nodata);
	mio::Grid2DObject grid(5, 5, 25., pt);

	//ROT grid on 2013-10-1T13:00:00+1
    grid(0,4) = 0.8530; grid(1,4) = 0.8530; grid(2,4) = nodata; grid(3,4) = nodata; grid(4,4) = 0.8340;
    grid(0,3) = 0.8530; grid(1,3) = 0.8530; grid(2,3) = 0.8530; grid(3,3) = nodata; grid(4,3) = 0.8340;
    grid(0,2) = 0.1020; grid(1,2) = 0.1020; grid(2,2) = 0.1020; grid(3,2) = 0.8270; grid(4,2) = 0.8270;
    grid(0,1) = 0.1020; grid(1,1) = 0.1020; grid(2,1) = 0.1020; grid(3,1) = 0.8270; grid(4,1) = 0.8270;
    grid(0,0) = 0.1020; grid(1,0) = 0.1020; grid(2,0) = 0.1020; grid(3,0) = 0.8270; grid(4,0) = 0.8270;
    expectedGrids[0] = grid;

    //TA grid on 2013-10-1T13:00:00+1
    grid(0,4) = 283.8430; grid(1,4) = 283.8430; grid(2,4) = nodata;   grid(3,4) = nodata;   grid(4,4) = 283.8740;
    grid(0,3) = 283.8430; grid(1,3) = 283.8430; grid(2,3) = 283.8430; grid(3,3) = nodata;   grid(4,3) = 283.8740;
    grid(0,2) = 283.9230; grid(1,2) = 283.9230; grid(2,2) = 283.9230; grid(3,2) = 283.9420; grid(4,2) = 283.9420;
    grid(0,1) = 283.9230; grid(1,1) = 283.9230; grid(2,1) = 283.9230; grid(3,1) = 283.9420; grid(4,1) = 283.9420;
    grid(0,0) = 283.9230; grid(1,0) = 283.9230; grid(2,0) = 283.9230; grid(3,0) = 283.9420; grid(4,0) = 283.9420;
    expectedGrids[1] = grid;

	//ISWR grid on 2013-10-1T13:00:00+1
	grid(0,4) = 483.6150; grid(1,4) = 483.6150; grid(2,4) = nodata;   grid(3,4) = nodata;   grid(4,4) = 474.1580;
	grid(0,3) = 483.6150; grid(1,3) = 483.6150; grid(2,3) = 483.6150; grid(3,3) = nodata;   grid(4,3) = 474.1580;
	grid(0,2) = 471.3940; grid(1,2) = 471.3940; grid(2,2) = 471.3940; grid(3,2) = 466.8810; grid(4,2) = 466.8810;
	grid(0,1) = 471.3940; grid(1,1) = 471.3940; grid(2,1) = 471.3940; grid(3,1) = 466.8810; grid(4,1) = 466.8810;
	grid(0,0) = 471.3940; grid(1,0) = 471.3940; grid(2,0) = 471.3940; grid(3,0) = 466.8810; grid(4,0) = 466.8810;
	expectedGrids[2] = grid;

	//ROT grid averaged in time between 2013-10-1T04:00:00+1 and 2013-10-1T22:00:00+1
    grid(0,4) = 0.7176; grid(1,4) = 0.7176; grid(2,4) = nodata; grid(3,4) = nodata; grid(4,4) = 0.7050;
    grid(0,3) = 0.7176; grid(1,3) = 0.7176; grid(2,3) = 0.7176; grid(3,3) = nodata; grid(4,3) = 0.7050;
    grid(0,2) = 0.0770; grid(1,2) = 0.0770; grid(2,2) = 0.0770; grid(3,2) = 0.7021; grid(4,2) = 0.7021;
    grid(0,1) = 0.0770; grid(1,1) = 0.0770; grid(2,1) = 0.0770; grid(3,1) = 0.7021; grid(4,1) = 0.7021;
    grid(0,0) = 0.0770; grid(1,0) = 0.0770; grid(2,0) = 0.0770; grid(3,0) = 0.7021; grid(4,0) = 0.7021;
    expectedGrids[3] = grid;

	//Space-averaged values of variables ROT, TA and ISWR over the subwatershed
	//on 2013-10-01T13:00:00+1, and mean spaced-averaged value of ROT over the
	//subwatershed between 2013-10-01T04:00:00+1 and 2013-10-01T22:00:00+1
	expectedMeanValues = {2.0509e-03, 283.9055, 473.1920, 1.7194e-03};
}


void getExpectedFileValuesOverSubw2(std::vector<mio::Grid2DObject>& expectedGrids,
		std::vector<double>& expectedMeanValues) {
	//Space-averaged values of variables ROT, TA and ISWR over the subwatershed
	//on 2013-10-01T13:00:00+1, and mean spaced-averaged value of ROT over the
	//subwatershed between 2013-10-01T04:00:00+1 and 2013-10-01T22:00:00+1
	expectedMeanValues = {2.0033e-3, 275.84, 209.87, 2.1203e-03};

	//Define useful variables
	expectedGrids.resize(4);
	const double& nodata(mio::IOUtils::nodata);
	mio::Coords pt("CH1903");
	pt.setXY(782525, 171725, nodata);
	mio::Grid2DObject grid;

	//ROT grid on 2013-10-1T13:00:00+1
	grid.set(5, 5, 25., pt, expectedMeanValues[0]*1000.*3600./13750.); //< convert from m^3/s to mm/h
	expectedGrids[0] = grid;

	//TA grid on 2013-10-1T13:00:00+1
	grid(0,4) = 275.39867; grid(1,4) = 275.39867; grid(2,4) = nodata;    grid(3,4) = nodata;    grid(4,4) = 275.88667;
	grid(0,3) = 275.39867; grid(1,3) = 275.39867; grid(2,3) = 275.39867; grid(3,3) = nodata;    grid(4,3) = 275.88667;
	grid(0,2) = 275.43067; grid(1,2) = 275.43067; grid(2,2) = 275.43067; grid(3,2) = 275.77467; grid(4,2) = 275.77467;
	grid(0,1) = 275.43067; grid(1,1) = 275.43067; grid(2,1) = 275.43067; grid(3,1) = 275.77467; grid(4,1) = 275.77467;
	grid(0,0) = 275.43067; grid(1,0) = 275.43067; grid(2,0) = 275.43067; grid(3,0) = 275.77467; grid(4,0) = 275.77467;
	expectedGrids[1] = grid;

	//ISWR grid on 2013-10-1T13:00:00+1
	grid(0,4) = 215.46720; grid(1,4) = 215.46720; grid(2,4) = nodata;    grid(3,4) = nodata;    grid(4,4) = 214.27160;
	grid(0,3) = 215.46720; grid(1,3) = 215.46720; grid(2,3) = 215.46720; grid(3,3) = nodata;    grid(4,3) = 214.27160;
	grid(0,2) = 215.38880; grid(1,2) = 215.38880; grid(2,2) = 215.38880; grid(3,2) = 214.54600; grid(4,2) = 214.54600;
	grid(0,1) = 215.38880; grid(1,1) = 215.38880; grid(2,1) = 215.38880; grid(3,1) = 214.54600; grid(4,1) = 214.54600;
	grid(0,0) = 215.38880; grid(1,0) = 215.38880; grid(2,0) = 215.38880; grid(3,0) = 214.54600; grid(4,0) = 214.54600;
	expectedGrids[2] = grid;

	//ROT grid averaged in time between 2013-10-1T04:00:00+1 and 2013-10-1T22:00:00+1
	grid.set(5, 5, 25., pt, expectedMeanValues[3]*1000.*3600./13750.); //< convert from m^3/s to mm/h
	expectedGrids[3] = grid;
}


void getExpectedGridValuesOverReach(std::vector<std::vector<double>>& expectedVectors,
		mio::MeteoData& expectedMeanData) {
	//Values of selected meteo variables over the stream reach cells on
	//2013-10-01T12:00:00+1
	expectedVectors.resize(3);
	expectedVectors[0] = {290.036,   289.919,   289.791,   289.624,   289.428};   //< TSOIL values
	expectedVectors[1] = {276.55867, 276.77467, 276.98267, 277.10267, 277.18267}; //< TA values
	expectedVectors[2] = {161.56127, 161.03207, 160.52247, 160.22847, 160.03247}; //< ISWR values

	//Space-averaged values of all meteorological variables over the stream
	//reach cells on 2013-10-01T12:00:00+1
	expectedMeanData.reset();
	expectedMeanData.addParameter("TSOIL");
	expectedMeanData("P")       = 76311.95990;
	expectedMeanData("TA")      = 276.92027;
	expectedMeanData("RH")      = 0.93533;
	expectedMeanData("TSG")     = 283.15;
	expectedMeanData("TSS")     = 278.15;
	expectedMeanData("HS")      = 0.0075;
	expectedMeanData("VW")      = 2.237;
	expectedMeanData("DW")      = 221.28333;
	expectedMeanData("VW_MAX")  = 4.17;
	expectedMeanData("RSWR")    = 120.0;
	expectedMeanData("ISWR")    = 160.67535;
	expectedMeanData("ILWR")    = 338.97835;
	expectedMeanData("TAU_CLD") = 0.5;
	expectedMeanData("PSUM")    = 0.04433;
	expectedMeanData("PSUM_PH") = 1.0;
	expectedMeanData("TSOIL")   = 289.75960;
}


void getExpectedFileValuesOverReach(std::vector<std::vector<double>>& expectedVectors,
		mio::MeteoData& expectedMeanData) {
	//Values of selected meteo variables over the stream reach cells on
	//2013-10-01T12:00:00+1
	expectedVectors.resize(3);
	expectedVectors[0] = {277.98, 274.25, 277.59, 277.70, 277.61}; //< TSOIL values
	expectedVectors[1] = {276.97, 277.14, 277.31, 277.40, 277.47}; //< TA values
	expectedVectors[2] = {90.,    89.,    102.,   115.,   118.};   //< ISWR values

	//Space-averaged values of all meteorological variables over the stream
	//reach cells on 2013-10-01T12:00:00+1
	expectedMeanData.reset();
	expectedMeanData.addParameter("TSOIL");
	expectedMeanData("TA")      = 277.258;
	expectedMeanData("TSS")     = 279.496;
	expectedMeanData("TSG")     = 279.496;
	expectedMeanData("VW")      = 2.300;
	expectedMeanData("DW")      = 0.000;
	expectedMeanData("VW_MAX")  = 0.000;
	expectedMeanData("ISWR")    = 102.800;
	expectedMeanData("RSWR")    = 27.400;
	expectedMeanData("ILWR")    = 325.9708;
	expectedMeanData("PSUM")    = 0.000;
	expectedMeanData("PSUM_PH") = 1.000;
	expectedMeanData("HS")      = 0.000;
	expectedMeanData("RH")      = 0.938;
	expectedMeanData("TSOIL")   = 277.026;
}


//------------------------------------------------------------------------------
// Main function
//------------------------------------------------------------------------------

int main(int argc, char *argv[])
{
	//Get target precision
	if(argc < 2)
		throw std::invalid_argument("Program expects the target relative precision as input argument");
	double precision;
	if(!mio::IOUtils::convertString(precision, std::string(argv[1])))
		throw std::invalid_argument("Invalid program argument: unknown format for the target relative precision");

	//Create two TestSubwatershed instances
	mio::Config cfg("io.ini");
	mio::IOManager io(cfg);
	Clock dummyClock(cfg, {}, {});
	mio::Grid2DObject mask;
	io.read2DGrid(mask, "subwatershed1.asc");
	TestSubwatershed subw1(mask, 1, dummyClock);
	io.read2DGrid(mask, "subwatershed2.asc");
	TestSubwatershed subw2(mask, 2, dummyClock);

	//Create a TestReach instance
	DummyWidthModel dummyWidth;
	std::vector<std::pair<double, double>> coords({{782750,172050}, {782750,172150}, {782750,172250}, {782750,172350}, {782750,172450}});
	std::vector<StreamCell> cells;
	cells.reserve(coords.size());
	mio::Coords pt("CH1903");
	for(const std::pair<double, double>& coord : coords) {
		pt.setXY(coord.first, coord.second, 0.);
		const StreamCell cell(pt);
		cells.push_back(cell);
	}
	TestReach reach(cells, dummyWidth, dummyClock);

	bool success(true);
	const size_t lineWidth(77);

	//Create two InputReader instances: one reading the requested values from the
	//SMET files generated by Alpine3D for each subwatershed and each stream cell,
	//and one reading the requested values from the grids generated by Alpine3D
	std::cout << std::left << std::setfill('=');
	std::cout << std::setw(lineWidth) << "==== [INITIALIZATION] " << std::endl;
	InputReader gridReader(cfg, {2013, 10, 1, 13, 0, 1});
	cfg.addKey("CATCHMENTS_PATH", "InputFromA3D", "input/runoff_files");
	cfg.addKey("STREAM_DATA_SOURCE", "InputFromA3D", "POI");
	cfg.deleteKeys("GRID2D", "InputFromA3D", true);
	InputReader fileReader(cfg, {2013, 10, 1, 13, 0, 1});
	std::cout << std::setw(lineWidth) << "==== [DONE] " << std::endl << std::endl;

	//Test grid reading over the two subwatersheds created above
	std::cout << std::setw(lineWidth) << "==== [TEST GRID READING OVER TWO SUBWATERSHED MASKS] "
	          << std::endl;
	std::vector<mio::Grid2DObject> expectedGrids;
	std::vector<double> expectedMeanValues;
	getExpectedGridValuesOverSubw1(expectedGrids, expectedMeanValues);
	success &= testSubwDataReading(gridReader, subw1, expectedGrids,
			expectedMeanValues, precision);
	std::cout << std::endl;
	getExpectedGridValuesOverSubw2(expectedGrids, expectedMeanValues);
	success &= testSubwDataReading(gridReader, subw2, expectedGrids,
			expectedMeanValues, precision);
	std::cout << std::left << std::setfill('=');
	std::cout << std::setw(lineWidth) << "==== [DONE] " << std::endl << std::endl;

	//Test SMET file reading over the two subwatersheds created above
	std::cout << std::setw(lineWidth) << "==== [TEST SUBWATERSHED SMET FILE READING] "
	          << std::endl;
	getExpectedFileValuesOverSubw1(expectedGrids, expectedMeanValues);
	success &= testSubwDataReading(fileReader, subw1, expectedGrids,
			expectedMeanValues, precision);
	std::cout << std::endl;
	getExpectedFileValuesOverSubw2(expectedGrids, expectedMeanValues);
	success &= testSubwDataReading(fileReader, subw2, expectedGrids,
			expectedMeanValues, precision);
	std::cout << std::left << std::setfill('=');
	std::cout << std::setw(lineWidth) << "==== [DONE] " << std::endl << std::endl;

	//Test grid reading over the stream reach cells
	std::cout << std::setw(lineWidth) << "==== [TEST GRID READING OVER THE STREAM REACH CELLS] "
	          << std::endl;
	std::vector<std::vector<double>> expectedVectors;
	mio::MeteoData expectedMeanData;
	getExpectedGridValuesOverReach(expectedVectors, expectedMeanData);
	success &= testReachDataReading(gridReader, reach, expectedVectors,
			expectedMeanData, precision);
	std::cout << std::left << std::setfill('=');
	std::cout << std::setw(lineWidth) << "==== [DONE] " << std::endl << std::endl;

	//Test SMET file reading over the stream reach cells
	std::cout << std::setw(lineWidth) << "==== [TEST STREAM CELL SMET FILE READING] "
	          << std::endl;
	getExpectedFileValuesOverReach(expectedVectors, expectedMeanData);
	success &= testReachDataReading(fileReader, reach, expectedVectors,
			expectedMeanData, precision);
	std::cout << std::left << std::setfill('=');
	std::cout << std::setw(lineWidth) << "==== [DONE] " << std::endl << std::endl;

	const int exitFlag(success ? EXIT_SUCCESS : EXIT_FAILURE);
	return exitFlag;
}
