/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <meteoio/MeteoIO.h>
#include <streamflow/libstreamflow.h>

using namespace sflow;

int main(int /*argc*/, char **/*argv[]*/)
{
	bool status = true;
	//Define the precision with which the roots should be found
	const double precision(1e-10);

	std::cout << std::fixed << std::left << std::setprecision(10);
	//Find the minimum value of various functions
	double x0 = RootFindingAlgorithms::brentMethod([](const double& x){
		return x;
		}, -1, 1, precision);
	std::cout << std::setw(40) << "Root of f(x) = x:";
	if (!mio::IOUtils::checkEpsilonEquality(x0, 0., precision)) {
		status = false;
		std::cout << "[found " << x0 << " instead of " << 0. <<  "]\n";
	} else
	std::cout << "[OK]\n";
	
	x0 = RootFindingAlgorithms::brentMethod([](const double& x){
		return x*x*x;
		}, -1, 1, precision);
	std::cout << std::setw(40) << "Root of f(x) = x^3:";
	if (!mio::IOUtils::checkEpsilonEquality(x0, 0., precision)) {
		status = false;
		std::cout << "[found " << x0 << " instead of " << 0. <<  "]\n";
	} else
	std::cout << "[OK]\n";
	
	x0 = RootFindingAlgorithms::brentMethod([](const double& x){
		return 2*x*x*x - 4*x*x + 3*x;
		}, -1, 1, precision);
	std::cout << std::setw(40) << "Root of f(x) = 2x^3 - 4x^2 + 3x:";
	if (!mio::IOUtils::checkEpsilonEquality(x0, 0., precision)) {
		status = false;
		std::cout << "[found " << x0 << " instead of " << 0. <<  "]\n";
	} else
	std::cout << "[OK]\n";
	
	x0 = RootFindingAlgorithms::brentMethod([](const double& x){
		return cos(x) - x*x*x;
		}, -10, 10, precision);
	std::cout << std::setw(40) << "Root of f(x) = cos(x) - x^3:";
	if (!mio::IOUtils::checkEpsilonEquality(x0, 0.865474033102, precision)) {
		status = false;
		std::cout << "[found " << x0 << " instead of " << 0.865474033102 <<  "]\n";
	} else
	std::cout << "[OK]\n";
	
	x0 = RootFindingAlgorithms::brentMethod([](const double& x){
		return x <= 2./3. ? sqrt(2./3. - x) : -sqrt(x - 2./3.);
		}, -10, 10, precision);
	std::cout << std::setw(40) << "Root of f(x) = sqrt(x - 2/3):";
	if (!mio::IOUtils::checkEpsilonEquality(x0, 2./3., precision)) {
		status = false;
		std::cout << "[found " << x0 << " instead of " << 2./3. <<  "]\n";
	} else
	std::cout << "[OK]\n";

	if (status==false) std::cout << "Root finding error";
	return 0;
}
