
## Test find_root
# generate executable
ADD_EXECUTABLE(find_root find_root.cc)
TARGET_LINK_LIBRARIES(find_root ${LIBRARIES})

# add the tests
ADD_TEST(find_root.smoke find_root)
SET_TESTS_PROPERTIES(find_root.smoke PROPERTIES LABELS smoke FAIL_REGULAR_EXPRESSION "error|differ|fail")



