/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <time.h>
#include <meteoio/MeteoIO.h>
#include <streamflow/libstreamflow.h>

using namespace sflow;


//------------------------------------------------------------------------------
// Useful classes
//------------------------------------------------------------------------------

class TestSubwatershed : public LumpedSubwatershedInterface {
	public:
		TestSubwatershed(const Clock& clk) : LumpedSubwatershedInterface(clk) {};
		virtual ParameterSet getParams() const {throw std::runtime_error("Not implemented!");};
		virtual void setParams(const ParameterSet&) {/*do nothing*/};
		virtual bool isHeadSubwatershed() const {throw std::runtime_error("Not implemented!");};
		virtual void advanceInTime() {/*do nothing*/};
		virtual void reset() {/*do nothing*/};
		virtual void setDisplayInfo(const bool&) {/*do nothing*/};
		virtual bool isSet() const {throw std::runtime_error("Not implemented!");};
		virtual bool contains(const StreamReach&, const bool&) const {
			throw std::runtime_error("Not implemented!");
		};
		virtual void setStreamReach(const StreamReach&) {/*do nothing*/};
		virtual const StreamReach& getStreamReach() const {
			throw std::runtime_error("Not implemented!");
		};
		virtual const mio::Grid2DObject& getMask() const {
			throw std::runtime_error("Not implemented!");
		};
		virtual size_t getIndex() const {throw std::runtime_error("Not implemented!");};
		virtual double getArea() const {throw std::runtime_error("Not implemented!");};
		virtual double get(const SubwatershedVariable&) const {return 0.;};
		virtual void getStreamLateralInflow(CellInflow& inflow) const {
			inflow.discharge = 0.;
		}
		virtual void getStreamLateralInflow(std::vector<CellInflow>&) const {
			throw std::runtime_error("Not implemented!");
		};
		virtual void updateBoundaryConditions() {/*do nothing*/};
		virtual ~TestSubwatershed() {};
};


class TestReach : public LumpedMuskingumCungeReach {
	public:
		TestReach(const std::vector<StreamCell>& cells,
				StreamWidthModel& widthModel, Clock& clk,
				const StreamBedFrictionModel& fricModel) :
					LumpedMuskingumCungeReach(cells, widthModel, clk, fricModel) {
			_water_height = 1.;
			_discharge    = 10.;
			_inlet_bc.discharge = 10.;
			_width = _width_model.getWidth(*this);
		};
		virtual void updateInletBoundaryConditions() override {
			if(this->isHeadReach()) {
				_inlet_discharge_old = _inlet_bc.discharge;
				const double t(round(86400.*(_clock.getCurrTime().getJulian() -
						_clock.getStartTime().getJulian())) + 1); //< add 1 second to avoid the case t = 0 (which creates problems in the next line)
				_inlet_bc.discharge = 10. + 1e5/t*exp(-3.*mio::Optim::pow2(log(t/10.) - 7.6)); //< log-normal distribution
			} else {
				LumpedMuskingumCungeReach::updateInletBoundaryConditions();
			}
		}
		virtual ~TestReach() {};

	protected:
		virtual void setInitialConditions() override {/*do nothing*/};
};


//------------------------------------------------------------------------------
// Global function
//------------------------------------------------------------------------------

std::ofstream& initializeOutputFile(const std::string& filename) {
	std::ofstream *tmp(new std::ofstream(filename));
	std::ofstream& f(*tmp);
	if(f.fail()) throw mio::AccessException(filename, AT);
	f << "SMET 1.1 ASCII" << std::endl;
	f << "[HEADER]" << std::endl;
	f << "station_id   = test_discretized_muskingum_cunge" << std::endl;
	f << "station_name = test_discretized_muskingum_cunge" << std::endl;
	f << "latitude     = -999" << std::endl;
	f << "longitude    = -999" << std::endl;
	f << "altitude     = -999" << std::endl;
	f << std::right << std::fixed << std::setprecision(1);
	f << "nodata       = " << mio::IOUtils::nodata << std::endl;
	f << "fields       = timestamp water_depth_inlet water_depth_outlet "
	  << "discharge_inlet discharge_outlet" << std::endl;
	f << "units        = - m m m^3/s m^3/s" << std::endl;
	f << "[DATA]" << std::endl;
	f << std::right << std::fixed << std::setprecision(3);
	return f;
}


//------------------------------------------------------------------------------
// Main function
//------------------------------------------------------------------------------
int main()
{
	//Define the test configuration
	const size_t simulationDuration(86400); //< [s]
	const size_t domainLength(20000);       //< [m]
	const size_t dt(5);                     //< [s]
	const size_t dx(100);                   //< [m]
	const size_t nx(domainLength/dx);
	const mio::Date startDate(2010, 1, 1, 0, 0, 0);
	const double bedSlope(0.0003);

	//Define some additional useful variables
	StreamflowConfig stdCfg;
	std::ostringstream os;
	os << std::fixed << std::setprecision(10) << dt/60.;
	stdCfg.addKey("TIME_STEP", SFConstants::CONFIG_SECTION_NAME, os.str());
	stdCfg.addKey("TIME_STEP::LIMITS", SFConstants::CONFIG_SECTION_NAME, "0 " + os.str());
	stdCfg.addKey("UNIFORM_WIDTH", SFConstants::CONFIG_SECTION_NAME, "15");
	stdCfg.addKey("MANNING_COEFF", SFConstants::CONFIG_SECTION_NAME, "0.06");
	Clock clk(stdCfg, startDate, startDate + simulationDuration/86400.);
	ManningFrictionModel fricModel(stdCfg);
	UniformWidth widthModel(stdCfg);
	TestSubwatershed subw(clk);

	//Create the vector of stream reaches
	std::vector<LumpedReachInterface*> reaches;
	reaches.reserve(nx);
	mio::Coords pt("CH1903");
	for(size_t iReach(0); iReach < nx; ++iReach) {
		pt.setXY((iReach + 0.5)*dx, 0., (nx - 0.5 - iReach)*dx*bedSlope);
		const StreamCell cell(pt, 0., 0., dx, bedSlope);
		LumpedReachInterface *reach(new TestReach({cell}, widthModel, clk, fricModel));
		reach->setSubwatershed(subw);
		if(iReach > 0) reach->setUpstreamReach(*reaches[iReach - 1]);
		reaches.push_back(reach);
	}

	//Run simulation
	std::ofstream& f(initializeOutputFile("output.smet"));
	while(clk.getCurrTime() < clk.getEndTime()) {
		for(StreamReach *reach : reaches) {
			reach->updateInletBoundaryConditions();
			reach->advanceInTime();
		}
		clk.advanceInTime();
		f << clk.getCurrTime().toString(mio::Date::ISO);
		f << std::setw(10) << reaches[0]->get(StreamVariable::H);
		f << std::setw(10) << reaches[nx - 1]->get(StreamVariable::H);
		f << std::setw(10) << reaches[0]->get(StreamVariable::Q);
		f << std::setw(10) << reaches[nx - 1]->get(StreamVariable::Q) << std::endl;
	}

	f.close();
	delete &f;
	return 0;
}
