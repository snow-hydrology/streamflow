#!/bin/bash
# This script is used to test the lumped Muskingum-Cunge routing technique

./lumped_muskingum_cunge_method

PRECISION="1e-3"
numdiff -r ${PRECISION} ref_output.smet output.smet | grep "+++"
rm -f output.smet
