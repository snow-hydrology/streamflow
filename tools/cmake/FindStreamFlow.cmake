INCLUDE(LibFindMacros)

# Finally the library itself
GET_FILENAME_COMPONENT(SRC_DIR ${CMAKE_SOURCE_DIR} PATH) #ie goes up one level
STRING(REPLACE " " "\\ " SRC_DIR ${SRC_DIR})

IF(WIN32)
	GET_FILENAME_COMPONENT(STREAMFLOW_ROOT1 "[HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\StreamFlow;UninstallString]" PATH CACHE INTERNAL)
	GET_FILENAME_COMPONENT(STREAMFLOW_ROOT2 "[HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\StreamFlow;UninstallString]" PATH CACHE INTERNAL)
	GET_FILENAME_COMPONENT(STREAMFLOW_ROOT3 "[HKEY_LOCAL_MACHINE\\SOFTWARE\\EPFL CRYOS Lab\\StreamFlow]" ABSOLUTE CACHE INTERNAL)
	GET_FILENAME_COMPONENT(STREAMFLOW_ROOT4 "C:/Progra~1/StreamFlow*" ABSOLUTE CACHE INTERNAL)
	SET(SEARCH_PATH
		ENV LIB
		${SRC_DIR}/streamflow/lib
		${SRC_DIR}/../../streamflow/lib
		${STREAMFLOW_ROOT1}/lib
		${STREAMFLOW_ROOT2}/lib
		${STREAMFLOW_ROOT3}/lib
		${STREAMFLOW_ROOT4}/lib)
	IF(MSVC)
		FIND_LIBRARY(STREAMFLOW_LIBRARY
			NAMES libstreamflow.lib
			PATHS ${SEARCH_PATH}
			DOC "Location of the libstreamflow, like c:/Program Files/StreamFlow-1.0.0/lib/libstreamflow.lib"
			)
	ELSE(MSVC)
		FIND_LIBRARY(STREAMFLOW_LIBRARY
			NAMES libstreamflow.dll.a
			PATHS ${SEARCH_PATH}
			DOC "Location of the libstreamflow, like c:/Program Files/StreamFlow-1.0.0/lib/libstreamflow.dll.a"
			)
	ENDIF(MSVC)
ELSE(WIN32)
	IF(APPLE)
		FIND_LIBRARY(STREAMFLOW_LIBRARY
		NAMES streamflow
		PATHS
			ENV LD_LIBRARY_PATH
			ENV DYLD_FALLBACK_LIBRARY_PATH
			${SRC_DIR}/streamflow/lib
			${SRC_DIR}/../../streamflow/lib
			"~/usr/lib"
			"/Applications/StreamFlow/lib"
			"/usr/local/lib"
			"/usr/lib"
			"/opt/lib"
		DOC "Location of the libstreamflow, like /usr/lib/libstreamflow.dylib"
		)
	ELSE(APPLE)
		FIND_LIBRARY(STREAMFLOW_LIBRARY
		NAMES streamflow
		PATHS
			ENV LD_LIBRARY_PATH
			${SRC_DIR}/streamflow/lib
			${SRC_DIR}/../../streamflow/lib
			"~/usr/lib"
			"/usr/local/lib"
			"/usr/lib"
			"/opt/lib"
		DOC "Location of the libstreamflow, like /usr/lib/libstreamflow.so"
		)
	ENDIF(APPLE)
ENDIF(WIN32)

#build STREAMFLOW_ROOT so we can provide a hint for searching for the header file
GET_FILENAME_COMPONENT(streamflow_libs_root ${STREAMFLOW_LIBRARY} PATH)
IF(${CMAKE_VERSION} VERSION_GREATER "2.8.11")
	GET_FILENAME_COMPONENT(STREAMFLOW_ROOT ${streamflow_libs_root} DIRECTORY)
ELSE(${CMAKE_VERSION} VERSION_GREATER "2.8.11")
	SET(STREAMFLOW_ROOT "${streamflow_libs_root}/../")
	STRING(REPLACE  " " "\\ " STREAMFLOW_ROOT ${STREAMFLOW_ROOT})
ENDIF(${CMAKE_VERSION} VERSION_GREATER "2.8.11")

# locate main header file
FIND_PATH(STREAMFLOW_INCLUDE_DIR
  NAMES streamflow/libstreamflow.h
  #HINTS ${STREAMFLOW_ROOT}/include
  PATHS
	"${STREAMFLOW_ROOT}/include"
	"${STREAMFLOW_ROOT}"
	"~/usr/include"
	"/usr/local/include"
	"/usr/include"
	"/opt/include"
  DOC "Location of the streamflow headers, like /usr/include"
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
SET(STREAMFLOW_PROCESS_INCLUDES STREAMFLOW_INCLUDE_DIR)
SET(STREAMFLOW_PROCESS_LIBS STREAMFLOW_LIBRARY)
libfind_process(STREAMFLOW)
