import Tkinter as tk
import tkFileDialog as tkd
import tkMessageBox as tkm
import os.path
from math import floor
from operator import itemgetter


class FileDialog(tk.Frame):
    """
    This class creates a GUI in order to get the names of the files containing 
    the DEM, the POIs and the reach cell coordinates
    """
    
    def __init__(self, mainWindow):
        """
        Constructor of class FileDialog, taking the root Tkinter object as 
        input argument.
        """
        tk.Frame.__init__(self, mainWindow)
        self._parent = mainWindow
        currDir = os.path.dirname(os.path.realpath(__file__))
        
        labelOpts = {'sticky': tk.W, 'padx': 2}
        tk.Label(self, text="File containing the coordinates of the stream " +
                 "cells:").grid(row=0, **labelOpts)
        tk.Label(self, text="DEM file:").grid(row=1, **labelOpts)
        tk.Label(self, text="POI file:").grid(row=2, **labelOpts)
        
        entryOpts = {'column': 1, 'pady': 2}
        self._cellFile = tk.Entry(self, width=40)
        self._demFile  = tk.Entry(self, width=40)
        self._poiFile  = tk.Entry(self, width=40)
        self._cellFile.grid(row=0, **entryOpts)
        self._demFile.grid(row=1,  **entryOpts)
        self._poiFile.grid(row=2,  **entryOpts)
        
        buttonOpts = {'column': 2, 'padx': 2}
        iconPath   = os.path.join(currDir, "..", "doc", "images", "open.gif")
        self._buttonIcon = tk.PhotoImage(file=iconPath)
        cellButton = tk.Button(self, image=self._buttonIcon, 
                  command=lambda: self._get_file_name(self._cellFile, "File Containing the Stream Cell Coordinates"))
        demButton = tk.Button(self, image=self._buttonIcon, 
                  command=lambda: self._get_file_name(self._demFile, "DEM File"))
        poiButton = tk.Button(self, image=self._buttonIcon,
                  command=lambda: self._get_file_name(self._poiFile, "POI File"))
        cellButton.grid(row=0, **buttonOpts)
        demButton.grid(row=1, **buttonOpts)
        poiButton.grid(row=2, **buttonOpts)
        
        buttonOpts = {'padx': 2, 'pady': 2}
        buttonFrame = tk.Frame(self)
        buttonFrame.grid(row=4, columnspan=3, sticky=tk.E)
        okButton     = tk.Button(buttonFrame, text="OK", command=self._add_points)
        cancelButton = tk.Button(buttonFrame, text="Cancel", command=mainWindow.destroy)
        okButton.pack(side=tk.LEFT, **buttonOpts)
        cancelButton.pack(side=tk.LEFT, **buttonOpts)
        
        self._last_path = os.path.join(currDir, "..")

        
    def _get_file_name(self, entryButton, fileType):
        """
        Prints the name of the file selected by the user in the specified Entry
        widget
        """
        fileOpts = {'initialdir': self._last_path, 'parent': self, 
                    'title': 'Open ' + fileType}
        fileName = tkd.askopenfilename(**fileOpts)
        if fileName:
            entryButton.delete(0, tk.END)
            entryButton.insert(0, fileName)
            self._last_path = os.path.dirname(fileName)
       
            
    def _add_points(self):
        """
        Calls global function add_stream_points_in_poi_file (see below)
        """
        fileNames = (self._cellFile.get(), self._demFile.get(), self._poiFile.get())
        fileTypes = ("file containing the stream cell coordinates", "DEM file", "POI file")
        for name, type in zip(fileNames, fileTypes):
            if name == "":
                tkm.showerror("Empty File Name", "Please specify the name of the " + type)
                return
            elif not os.path.exists(name):
                tkm.showerror("Invalid File", name + " does not exist or is not a valid file name")
                return
        
        add_stream_points_in_poi_file(*fileNames)
        tkm.showinfo("Operation successful", "Stream cell indices were successfully added to the POI file.\n"
                     "Do not forget to also set key PROF_DAYS_BETWEEN to 0.416667 in section [OUTPUT] of "
                     "the configuration file!")
        self._parent.destroy()



class GridInfo:
    """
    This class contains the geolocalization of an ARC grid and can be used to
    identify the indices of given grid points.
    """
    
    def __init__(self, fileName):
        """
        Class constructor. The full name of the grid should be passed as 
        argument.
        """
        self._nrows     = None
        self._ncols     = None
        self._xllcorner = None
        self._yllcorner = None
        self._cellsize  = None 
        with open(fileName, 'r') as f:
            for iLine, line in enumerate(f):
                if line =="":
                    continue
                
                words = line.split()
                key = value = None
                try:
                    key   = words[0]
                    value = float(words[1])
                except:
                    raise ValueError("Error parsing line {0} of file {1}".format(iLine, fileName))
                
                if key == "ncols":
                    self._ncols = value
                elif key == "nrows":
                    self._nrows = value
                elif key == "xllcorner":
                    self._xllcorner = value
                elif key == "yllcorner":
                    self._yllcorner = value
                elif key == "cellsize":
                    self._cellsize = value
                    
                if(self._nrows     != None and self._ncols     != None and 
                   self._xllcorner != None and self._yllcorner != None and
                   self._cellsize  != None):
                    return;
        
        raise ValueError("File {0} does not comply with the ARC format".format(fileName))


    def gridify_point(self, point):
        """
        This method returns the grid indices of the point whose coordinates
        are passed as input argument.
        """
        ix = iy = None
        try:
            ix = int(floor((point[0] - self._xllcorner)/self._cellsize))
            iy = int(floor((point[1] - self._yllcorner)/self._cellsize))
        except:
            raise ValueError("Method gridify() expects a 1x2 tuple as input argument")
        
        if(ix < 0 or ix >= self._nrows or iy < 0 or iy >= self._ncols):
            raise ValueError("Point " + point.str() + " is located outside of the grid")
        
        return (ix, iy)
        
        
    def gridify_points(self, points):
        """
        This method returns the grid indices of the points whose coordinates
        are passed as input arguments.
        """
        coords = [self.gridify_point(point) for point in points]
        return sorted(set(coords), key = itemgetter(0, 1))
        


def read_stream_points(fileName):
    """
    This function extracts the coordinates of the stream cells from the TauDEM
    file whose full name is passed as input argument.
    """
    coords = []
    with open(fileName, 'r') as f:
        for iLine, line in enumerate(f):
            if line == "":
                continue
            
            words = line.split()
            try:
                coords.append( (float(words[0]), float(words[1])) )
            except:
                raise ValueError("Error parsing line {0} of file {1}".format(iLine, fileName))
            
    return set(coords)

        

def add_stream_points_in_poi_file(cellFile, demFile, poiFile):
    """
    This function extracts the stream cell coordinates from the TauDEM file
    (given as first input argument), computes their respective x- and y-indices
    in the DEM grid (passed as second input argument), and writes these indices
    in the POI file (passed as thrid input argument)
    """
    dem = GridInfo(demFile)
    pts = dem.gridify_points(read_stream_points(cellFile))
    
    comment  = "\n# Indices of the stream cells (required by the hydrological model)\n"
    contents = ""
    with open(poiFile, 'r') as f:
        contents = f.read() + "\n"
        
    idx = contents.find(comment)
    if idx != -1:
        contents = contents[:idx]

    with open(poiFile, 'w') as f:
        f.write(contents)
        f.write(comment)
        [f.write("{:5.0f} {:5.0f}\n".format(p[0], p[1])) for p in pts]
    


if __name__ == '__main__':
    mainWindow = tk.Tk()
    mainWindow.title("Add stream cells to the POI file")
    FileDialog(mainWindow).pack()
    try:
        mainWindow.mainloop()
    except BaseException as e:
        tkm.showerror("Error", str(e))