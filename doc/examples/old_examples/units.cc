/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>
#include <streamflow/libstreamflow.h>

using namespace sflow;



// Converts a Units::Quantity to a string.
std::string toString(Units::Quantity q)
{
	switch(q) {
	case Units::Quantity::Acceleration: return "Acceleration"; break;
	case Units::Quantity::Angle:        return "Angle"; break;
	case Units::Quantity::Energy:       return "Energy"; break;
	case Units::Quantity::Force:        return "Force"; break;
	case Units::Quantity::Length:       return "Force"; break;
	case Units::Quantity::Mass:         return "Mass"; break;
	case Units::Quantity::Number:       return "Number"; break;
	case Units::Quantity::Power:        return "Power"; break;
	case Units::Quantity::Pressure:     return "Pressure"; break;
	case Units::Quantity::Speed:        return "Speed"; break;
	case Units::Quantity::Surface:      return "Surface"; break;
	case Units::Quantity::Temperature:  return "Temperature"; break;
	case Units::Quantity::Time:         return "Time"; break;
	case Units::Quantity::Volume:       return "Volume"; break;
	default:                            return "Unknown";
	}
}


// Converts a Units::BasicQuantity to a string.
std::string toString(Units::Dimensions dims)
{
	std::ostringstream os;
	os << "     Length:\t" << dims.length      << "\n";
	os << "     Mass:\t"   << dims.mass        << "\n";
	os << "     Time:\t"   << dims.time        << "\n";
	os << "     Temp.:\t"  << dims.temperature << "\n";
	return os.str();
}


/**
 * @brief Main function.
 */
int main(int argc, char *argv[])
{
	// Create two speed units
	Units mps(Units::m/Units::s);   //< [m/s]
	Units kmph(Units::km/Units::h); //< [km/h]

	// Compute the multiplicative factor to convert the 1st units into the 2nd ones
	double factor(mps.getConversionFunctionTo(kmph).getFactor());
	std::cout << ">> To convert " << mps << " to " << kmph
	          << ", you need to multiply by: " << factor << std::endl;

	// Compute the offset between degC and degK
	double offset(Units::degC.getConversionFunctionTo(Units::degK).getOffset());
	std::cout << ">> To convert " << Units::degC << " to " << Units::degK
	          << ", you need to add: " << offset << std::endl;

	// Check that temperature units are handled properly in composite units
	Units heatCapacity1(Units::J/(Units::kg*Units::degK)); //< uses degK
	Units heatCapacity2(Units::J/(Units::kg*Units::degC)); //< uses degC
	if(heatCapacity1 == heatCapacity2) {
		std::cout << ">> " <<  heatCapacity1 << " and " << heatCapacity2
		          << " correspond to the same units" << std::endl;
	} else {
		std::cout << ">> " << heatCapacity1 << " and " << heatCapacity2
		          << " are different units" << std::endl;
	}

	// Display the physical quantity corresponding to units [kg*m^2/s^2]
	Units units(Units::s.pow(-1));
	std::cout << ">> " << units << " correspond to units of "
	          << toString(units.getQuantity()) << std::endl;


	// Display the dimensions associated with units [J]
	Units::Dimensions dims(Units::J.getDimensions());
	std::cout << ">> Joules have the following dimensions:" << std::endl;
	std::cout << toString(dims) << std::endl;

	//Test function strToUnits()
	std::string energy("kg*m^2/s^2");
	std::cout << "String \"" << energy << "\" is interpreted as: "
	          << Units::parseString(energy) << std::endl;
	std::string complex("((kg * m)^2 / J)^(-2) / (min * bar)");
	std::cout << "String \"" << complex << "\" is interpreted as: "
	          << Units::parseString(complex) << std::endl;
	std::istringstream is("mm/m");
	is >> units;
	std::cout << "String \"mm/m\" is interpreted as: " << units << std::endl;

	//Test scientific print out
	Units gasConstant(Units::J/(Units::mol*Units::degK));
	std::cout << "The perfect gas constant is expressed in " << gasConstant
	          << ", which can also be written: "
			  << gasConstant.toString(Units::Format::SCI) << std::endl;

	return 0;
}
