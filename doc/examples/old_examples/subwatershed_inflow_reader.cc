/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <streamflow/libstreamflow.h>
#include <meteoio/MeteoIO.h>

using namespace sflow;

/**
 * @brief Main function. Executable should be called as:
 * 		[executable] [subwatershed index] [date 1] [date 2] ... [date n]
 * where [subwatershed index] corresponds to the index of the subwatershed
 * whose total inflow should be read at dates [date 1], [date 2], ..., [date n]
 */
int main(int argc, char *argv[])
{
	//Register start time
	clock_t tStart = clock();

	//Get input argument(s)
	unsigned int subwId(1);
	std::vector<mio::Date> dates;
	if(argc > 1)
		subwId = std::atoi(argv[1]);
	if(argc > 2) {
		mio::Date d;
		for(size_t ix = 2; ix < argc; ++ix) {
			mio::IOUtils::convertString(d, argv[ix], 1);
			dates.push_back(d);
		}
	} else {
		mio::Date d;
		d.setDate(2011, 10, 1, 1, 0, 1);
		dates.push_back(d);
		d.setDate(2011, 10, 1, 4, 0, 1);
		dates.push_back(d);
		d.setDate(2011, 10, 1, 8, 0, 1);
		dates.push_back(d);
	}

	//Create a WatershedInflowReader
	StreamflowConfig cfg("cfgfiles/io_test.ini");
	SubwInputFileReader reader(cfg);

	//Display the meteo values at the specified dates
	mio::MeteoData data(reader.getMeteoData(dates.front(), subwId));
	std::cout << "Meteo values for subwatershed " << subwId << ":\n";
	std::cout << "Time\t\t";
	for(size_t iParam(0); iParam < data.getNrOfParameters(); ++iParam)
		std::cout << "\t" << data.getNameForParameter(iParam);
	std::cout << std::endl;
	for(const mio::Date& date : dates) {
		data = reader.getMeteoData(date, subwId);
		std::cout << date.toString(mio::Date::ISO);
		for(size_t iParam(0); iParam < data.getNrOfParameters(); ++iParam)
			std::cout << "\t" << data(iParam);
		std::cout << std::endl;
	}
	std::cout << std::endl;

	//Display the inflow values at the specified dates
	double inflow;
	std::cout << "Inflow values for subwatershed " << subwId << ":\n";
	for(const mio::Date& date : dates) {
		inflow = reader.getMeteoData(date, "TOTAL_RO", subwId);
		std::cout << date.toString(mio::Date::ISO) << "\t" << inflow << " m^3/s\n";
	}
	std::cout << std::endl;

	//Display the soil temperature values at the specified dates
	double temp;
	std::cout << "Soil temperature values for subwatershed " << subwId << ":\n";
	for(const mio::Date& date : dates) {
		temp = reader.getMeteoData(date, "TSOIL", subwId);
		std::cout << date.toString(mio::Date::ISO) << "\t" << temp << " K\n";
	}
	std::cout << std::endl;

	//Display the average soil temperature value
	std::cout << "Average soil temperature values between "
	          << dates.front().toString(mio::Date::ISO) << " and "
	          << dates.back().toString(mio::Date::ISO) << ": "
	          << reader.getTimeAveragedMeteoData(dates.front(), dates.back(),
	        		  "TSOIL", subwId)
	          << std::endl;

	//Print execution time
	printf("\nTime taken: %.3fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	return 0;
}
