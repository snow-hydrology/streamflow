/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <set>
#include <sstream>
#include <time.h>
#include <streamflow/libstreamflow.h>
#include <meteoio/MeteoIO.h>

namespace sflow {

class TestDecorator: public SimpleSubwatershedWithEB {
	public:
		TestDecorator(LumpedSubwatershedInterface *wrappedObject,
				Parameters& diffTime) :
					SimpleSubwatershedWithEB(wrappedObject, diffTime) {}
		virtual void reset() {
			SimpleSubwatershedWithEB::reset();
			TwoVerticalLinearReservoirs *subwatershed = dynamic_cast<TwoVerticalLinearReservoirs*>(_wrapped_object);
			subwatershed->_upper_res.old_inflow  = 0.;
			subwatershed->_upper_res.curr_inflow = 0.;
			subwatershed->_upper_res.outflow     = 0.;
			subwatershed->_lower_res.old_inflow  = 0.;
			subwatershed->_lower_res.curr_inflow = 0.;
			subwatershed->_lower_res.outflow     = 0.;
		};
		virtual ~TestDecorator() {};
};

};

using namespace sflow;

/**
 * @brief Main function.
 */
int main(int argc, char *argv[])
{
	//Register start time
	clock_t tStart = clock();

	//Get the name of the file in which the model results should be written, the
	//time step and the HSPF parameter values
	std::string fname("output/temp_advection_reach_simulation.txt");
	double timeStep(1.), smoothingFactor(5.556e-7), tempDiff(-2.);
	if(argc > 1)
		fname = std::string(argv[1]);
	if(argc > 2)
		timeStep = atoi(argv[2]);
	if(argc > 3)
		smoothingFactor = atoi(argv[3]);
	if(argc > 4)
		tempDiff = atoi(argv[4]);

	//Useful variables
	StreamflowConfig cfg("cfgfiles/io_test.ini");
	std::ostringstream os;
	os << smoothingFactor;
	cfg.addKey("HSPF_SMOOTHING_FACTOR", SFConstants::CONFIG_SECTION_NAME, os.str());
	cfg.addKey("HSPF_SMOOTHING_FACTOR::LIMITS", SFConstants::CONFIG_SECTION_NAME, "0 1");
	os.str("");
	os.clear();
	os << tempDiff;
	cfg.addKey("HSPF_TEMPERATURE_DIFF", SFConstants::CONFIG_SECTION_NAME, os.str());
	cfg.addKey("HSPF_TEMPERATURE_DIFF::LIMITS", SFConstants::CONFIG_SECTION_NAME, "-10 10");
	os.str("");
	os.clear();
	os << std::fixed << std::setprecision(5) << timeStep/60.;
	cfg.addKey("TIME_STEP", SFConstants::CONFIG_SECTION_NAME, os.str());
	os.str("");
	os.clear();
	os << std::fixed << std::setprecision(5) << 1./60. << " " << timeStep/60.;
	cfg.addKey("TIME_STEP::LIMITS", SFConstants::CONFIG_SECTION_NAME, os.str());
	mio::IOManager io(cfg);
	mio::Grid2DObject grid;
	io.read2DGrid(grid, "dischmaw.asc");
	const mio::Date startDate(2013, 10, 1, 0, 0, 1);
	const mio::Date endDate(2014, 9, 28, 0, 0, 1);
	Clock clk(cfg, startDate, endDate);
	const double watershedArea = 40.0;
	TwoVerticalLinearReservoirs::Parameters reservoirs(cfg, watershedArea);
	SubwatershedWithHSPFOutflowTemp::Parameters params(cfg);
	InputReader reader(cfg, startDate);

	//Test the HSPF decorator
	SubwatershedWithHSPFOutflowTemp subw(new TwoVerticalLinearReservoirs(grid, 1, clk, reader,
			reservoirs), params, reader);
	clk.updateTimeStep();

	//Initialize the output file
	std::ofstream f(fname);
	if(f.fail()) throw mio::AccessException(fname, AT);
	f << clk.getCurrTime().toString(mio::Date::ISO) << " "
	  << subw.get(SubwatershedVariable::T_outlet) << std::endl;

	//Evaluate the model
	while(clk.getCurrTime() < clk.getNextTime()) {
		subw.updateBoundaryConditions();
		subw.advanceInTime();
		clk.advanceInTime();
		f << clk.getCurrTime().toString(mio::Date::ISO) << " "
		  << subw.get(SubwatershedVariable::T_outlet) << std::endl;
	}
	f.close();

	//Print execution time
	printf("\nTime taken: %.3fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	return 0;
}
