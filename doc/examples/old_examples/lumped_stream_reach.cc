/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <time.h>
#include <meteoio/MeteoIO.h>
#include <streamflow/libstreamflow.h>

using namespace sflow;

/**
 * @brief Main function.
 */
int main(int argc, char *argv[])
{
	//Register start time
	clock_t tStart = clock();

	//Useful variables
	StreamflowConfig cfg("cfgfiles/io_test.ini");
	mio::IOManager io(cfg);
	mio::Grid2DObject grid;
	io.read2DGrid(grid, "dischmaw.asc");
	Clock clk(cfg, {2011, 10, 1, 1, 0, 1}, {2011, 11, 1, 1, 0, 1});
	InputReader reader(cfg, clk.getStartTime());

	//Create a simple watershed working with the Forward Euler scheme
	TwoVerticalLinearReservoirs::Parameters reservoirs(cfg, 40);
	TwoVerticalLinearReservoirs subwatershed(grid, 1, clk, reader, reservoirs);

	//Create a vector of stream cells
	const size_t nCells = 11;
	std::vector<StreamCell> cells;
	cells.reserve(nCells);
	mio::Coords pt("CH1903");
	for(size_t ix = 0; ix < nCells; ++ix) {
		pt.setXY(7e4+1e3*ix, 1e5, 10.0);
		StreamCell cell(pt, 0.0, 0.0, 1e3);
		cells.push_back(cell);
	}

	//Create a simple stream reach working with the Muskingum method
	LumpedMuskingumReach::Parameters params(cfg);
	AreaPowerWidthModel width(cfg);
	LumpedMuskingumReach reachMusk(cells, width, clk, params);
	reachMusk.setSubwatershed(subwatershed);

	//Evaluate the model error as a function of the time step
	const int duration = 2816; //< about 2 days, expressed in minutes
	const double k(cfg.get("MUSKINGUM_K", SFConstants::CONFIG_SECTION_NAME));
	const double x(cfg.get("MUSKINGUM_X", SFConstants::CONFIG_SECTION_NAME));
	const double heightBeg = reachMusk.get(StreamVariable::H);
	CellInflow latInflow;
	subwatershed.getStreamLateralInflow(latInflow);
	const double totInflow = (subwatershed.get(SubwatershedVariable::Q_source) +
			latInflow.discharge)/(reachMusk.getLength()*reachMusk.getWidth());
	const double heightEnd = k*totInflow + (heightBeg - k*totInflow)*exp(duration*60/(k*(x-1)));
	std::cout << "dt [s]\terror [m^3/s]" << std::endl;
	for(unsigned int dt = 1; dt < 256; dt *= 2) {
		clk.setTimeStep(dt*60);
		for(int it = 0; it < duration/dt; ++it) {
			reachMusk.advanceInTime();
		}
		std::cout << dt << "\t" << reachMusk.get(StreamVariable::H) - heightEnd
				<< std::endl;
		reachMusk.reset();
	}

	//Print execution time
	printf("\nTime taken: %.3fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	return 0;
}
