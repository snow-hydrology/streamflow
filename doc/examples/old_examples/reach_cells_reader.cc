/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <meteoio/MeteoIO.h>
#include <streamflow/libstreamflow.h>

using namespace sflow;

/**
 * @brief Main function.
 */
int main(int argc, char *argv[])
{
	//Register start time
	clock_t tStart = clock();

	//Get input argument
	unsigned int reachNumber(1);
	if(argc > 1)
		reachNumber = std::atoi(argv[1]);

	//Useful variables
	mio::Config cfg("cfgfiles/io_test.ini");

	//Create a ReachCellsReader
	TaudemReachFilesReader reader(cfg);
	std::vector<StreamCell> cells(reader.getReachCells(reachNumber));
	std::vector<size_t> usWatersheds(reader.getUpstreamReaches(reachNumber));

	//Print the cells properties
	std::cout << "Easting\tNorthing\tAltitude [m]\tSpec. drainage area [m^2]\t";
	std::cout << "Length [m]\tSlope [-]" << std::endl;
	for(size_t ix = 0; ix < cells.size(); ++ix) {
		std::cout << cells[ix].coords.getEasting() << "\t" << cells[ix].coords.getNorthing() <<"\t";
		std::cout << cells[ix].coords.getAltitude() << "\t" << cells[ix].spec_drainage_area << "\t";
		std::cout << cells[ix].length << "\t" << cells[ix].slope << std::endl;
	}

	//Print execution time
	printf("\nTime taken: %.3fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	return 0;
}
