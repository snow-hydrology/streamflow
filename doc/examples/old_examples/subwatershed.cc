/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <set>
#include <sstream>
#include <time.h>
#include <streamflow/libstreamflow.h>
#include <meteoio/MeteoIO.h>

using namespace sflow;

/**
 * @brief Main function.
 */
int main(int argc, char *argv[])
{
	//Register start time
	clock_t tStart = clock();

	//Useful variables
	StreamflowConfig cfg("cfgfiles/io_test.ini");
	cfg.addKey("LOWER_RESERVOIR_TAU", SFConstants::CONFIG_SECTION_NAME, "1"); //< in days
	cfg.addKey("LOWER_RESERVOIR_TAU::limits", SFConstants::CONFIG_SECTION_NAME, "0 2"); //< in days
	mio::IOManager io(cfg);
	mio::Grid2DObject grid;
	io.read2DGrid(grid, "subwatersheds_25m_m.asc");
	cfg.deleteKeys("TIME_STEP", SFConstants::CONFIG_SECTION_NAME); //< set time step to 1h
	Clock clk(cfg, {2011, 10, 1, 1, 0, 1}, {2011, 11, 1, 1, 0, 1});
	const double watershedArea = 40.0;
	TwoVerticalLinearReservoirs::Parameters reservoirs(cfg, watershedArea);
	InputReader reader(cfg, clk.getStartTime());

	//Print each subwatershed's mask
	std::set<size_t> watershedIndices;
	for(size_t iCell = 0; iCell < grid.getNx()*grid.getNy(); ++iCell) {
		if(grid(iCell) != mio::IOUtils::nodata) {
			size_t currIndex((size_t)grid(iCell));
			if(watershedIndices.find(currIndex) == watershedIndices.end()) {
				watershedIndices.insert(currIndex);
				const TwoVerticalLinearReservoirs tmp(grid, currIndex, clk, reader, reservoirs);
				std::ostringstream os;
				os << "mask_subwatershed_" << currIndex << ".asc";
				io.write2DGrid(tmp.getMask(), os.str());
			}
		}
	}

	//Create a simple subwatershed
	std::cout << "Discharge model parameters:\n" << reservoirs.getParams() << std::endl;
	TwoVerticalLinearReservoirs subw(grid, 1, clk, reader, reservoirs);
	std::cout << "\nSubwatershed index: " << subw.getIndex() << std::endl;
	std::cout << "Subwatershed area:  " << subw.getArea() << " m^2\n" << std::endl;

	//Evaluate the model error as a function of the time step
	const int duration     = 2816; //< about 2 days, in minutes
	const double areaRatio = subw.getArea()/watershedArea*1e-6;
	const double tau = cfg.get("LOWER_RESERVOIR_TAU", SFConstants::CONFIG_SECTION_NAME); //< in days
	const double inflow = reader.getLumpedMeteoData(clk.getStartTime(), "ROT", subw);
	const double Qend = inflow + (subw.get(SubwatershedVariable::Q_outlet) - inflow)*
			exp(-duration/(24*60*tau*pow(areaRatio, 1.0/3.0)));
	std::cout << "dt [s]\terror [m^3/s]" << std::endl;
	for(unsigned int dt = 1; dt < 256; dt *= 2) {
		clk.setTimeStep(dt*60);
		subw.reset(); //< force inflow to zero
		for(int it = 0; it < duration/dt; ++it) {
			subw.advanceInTime();
		}
		std::cout << dt << "\t\t" << subw.get(SubwatershedVariable::Q_outlet) - Qend
				<< std::endl;
	}

	//Print execution time
	printf("\nTime taken: %.3fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	return 0;
}
