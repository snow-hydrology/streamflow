/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <meteoio/MeteoIO.h>
#include <streamflow/libstreamflow.h>

using namespace sflow;

/**
 * @brief Main function.
 */
int main(int argc, char *argv[])
{
	//Define the precision with which the roots should be found
	const double precision(1e-10);

    //Find the minimum value of various functions
	double x0 = RootFindingAlgorithms::brentMethod([](const double& x){
		return x;
		}, -1, 1, precision);
	std::cout << "Root of f(x) = x: " << x0 << std::endl;
	x0 = RootFindingAlgorithms::brentMethod([](const double& x){
		return x*x*x;
		}, -1, 1, precision);
	std::cout << "Root of f(x) = x^3: " << x0 << std::endl;
	x0 = RootFindingAlgorithms::brentMethod([](const double& x){
		return 2*x*x*x - 4*x*x + 3*x;
		}, -1, 1, precision);
	std::cout << "Root of f(x) = 2x^3 - 4x^2 + 3x: " << x0 << std::endl;
	x0 = RootFindingAlgorithms::brentMethod([](const double& x){
		return cos(x) - x*x*x;
		}, -10, 10, precision);
	std::cout << "Root of f(x) = cos(x) - x^3: " << x0 << std::endl;
	x0 = RootFindingAlgorithms::brentMethod([](const double& x){
		return x <= 2./3. ? sqrt(2./3. - x) : -sqrt(x - 2./3.);
		}, -10, 10, precision);
	std::cout << "Root of f(x) = sqrt(x - 2/3): " << x0 << std::endl;

	return 0;
}
