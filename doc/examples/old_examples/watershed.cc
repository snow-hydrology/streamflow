/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <time.h>
#include <streamflow/libstreamflow.h>
#include <meteoio/MeteoIO.h>

using namespace sflow;

const std::string printWatersheds(Watershed& basin, const std::vector<size_t>& indices);

/**
 * @brief Main function.
 */
int main(int argc, char *argv[])
{
	//Register start time
	clock_t tStart = clock();

	//Useful variables
	StreamflowConfig cfg("cfgfiles/io_test.ini");
	mio::Date startTime(2011, 10, 1, 0, 0, 1);
	mio::Date endTime(2015, 10, 1, 0, 0, 1);
	cfg.deleteKeys("TIME_STEP", SFConstants::CONFIG_SECTION_NAME); //< set time step to 1h
	Clock clk(cfg, startTime, endTime);
	InputReader reader(cfg, startTime);
	StreamNetwork network(cfg, clk, reader);

	//Create a simple basin
	Watershed simpBasin(cfg, clk, reader);
	std::cout << "Basin area: " << simpBasin.getArea() << " km^2" << std::endl;
	std::cout << "Number of watersheds: " << simpBasin.getNumSubwatersheds() << std::endl;
	std::cout << "Basin parameters:\n" << simpBasin.getParams() << std::endl;
	std::cout << std::endl;

	//Print the watershed areas
	std::cout << "Watershed areas: ";
	for(const Subwatershed& sd : simpBasin) {
		std::cout << sd.getArea() << " ";
	}
	std::cout << std::endl;

	//Evolve the basin in time
	simpBasin.setStreamNetwork(network);
	const std::vector<size_t> w_indices = simpBasin.getSubwatershedIndices();
	std::cout << "\ntime [s]";
	for(size_t index : w_indices) {
		std::cout << "\tQ_" << index << " [m^3/s]";
	}
	std::cout << "\n" << clk.getCurrTime().toString(mio::Date::ISO) <<
			"\t" << printWatersheds(simpBasin, w_indices) << "\n";
	for(size_t ix = 0; ix < 24*2; ++ix, clk.advanceInTime()) {
		simpBasin.advanceInTime();
		std::cout << clk.getCurrTime().toString(mio::Date::ISO) << "\t" <<
				printWatersheds(simpBasin, w_indices) << std::endl;
	}

	//Print the downstream reach cell of each stream reach contained in each
	//subwatershed
	std::cout << "\nEasting\tNorthing\n";
	for(const Subwatershed& subwatershed : simpBasin) {
		const StreamCell& cell(subwatershed.getStreamReach().getCells().back());
		std::cout << cell.getEasting() << "\t" << cell.getNorthing() << "\n";
	}

	//Print execution time
	printf("\nTime taken: %.3fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	return 0;
}


const std::string printWatersheds(Watershed& basin, const std::vector<size_t>& indices) {
	Watershed::const_iterator it(basin.begin());
	std::ostringstream os;
	os << std::setprecision(2);
	os << static_cast<const LumpedSubwatershed&>(*it).get(SubwatershedVariable::Q_outlet);
	for(++it; it != basin.end(); ++it) {
		os << "\t";
		os << static_cast<const LumpedSubwatershed&>(*it).get(SubwatershedVariable::Q_outlet);
	}
	return os.str();
}
