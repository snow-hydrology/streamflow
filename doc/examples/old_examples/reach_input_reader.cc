/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <ctime>
#include <streamflow/libstreamflow.h>
#include <meteoio/MeteoIO.h>

using namespace sflow;

/**
 * @brief Main function. Executable should be called as:
 * 		[executable] [date 1] [date 2] ... [date n]
 * where [date 1], [date 2], ..., [date n] denote the depths at which the meteo
 * and soil temperature data should be returned
 */
int main(int argc, char *argv[])
{
	//Register start time
	clock_t tStart = clock();

	//Get input argument(s)
	std::vector<mio::Date> dates;
	if(argc > 1) {
		mio::Date d;
		for(size_t ix = 2; ix < argc; ++ix) {
			mio::IOUtils::convertString(d, argv[ix], 1);
			dates.push_back(d);
		}
	} else {
		mio::Date d(2014, 10, 2, 4, 0, 1);
		dates.push_back(d);
	}

	//Load the DEM
	mio::DEMObject dem;
	StreamflowConfig cfg("cfgfiles/io_best.ini");
	mio::IOManager io(cfg);
	io.readDEM(dem);

	//Create an array of points
	std::vector<mio::Coords> cells;
	std::vector<std::pair<size_t, size_t>> indices = {{19, 43}, {35, 78},
			{35, 79}, {35, 86}, {35, 87}};
	for(const std::pair<size_t, size_t>& index : indices) {
		mio::Coords coords("CH1903");
		coords.setGridIndex(index.first, index.second, mio::IOUtils::inodata);
		dem.gridify(coords);
		cells.push_back(coords);
	}

	//Create a ReachInputFileReader
	ReachInputFileReader reader(cfg);

	//Display which variables are contained in the SMET files, as well as the
	//period over which data is available
	mio::MeteoData tmp;
	tmp.addParameter("TSOIL");
	std::cout << "The following variables are contained in the SMET files:" << std::endl;
	for(size_t iParam(0); iParam < tmp.getNrOfParameters(); ++iParam) {
		const std::string& paramName(tmp.getNameForParameter(iParam));
		std::cout << "\t" << paramName << ": "
		          << (reader.provides(paramName) ? "true" : "false")
				  << std::endl;
	}
	std::cout << std::endl;

	mio::Date startDate, endDate;
	reader.getMeasurementDateRange(startDate, endDate);
	std::cout << "Data is available between " << startDate.toString(mio::Date::ISO)
	          << " and " << endDate.toString(mio::Date::ISO)
			  << std::endl << std::endl;

	//Display the meteo and soil temperature values at the specified dates
	std::vector<mio::MeteoData> meteo;
	for(const mio::Date& date : dates) {
		std::cout << "Meteo and soil temperature on "
		          << date.toString(mio::Date::ISO) << ":\n";
		std::cout << "Cell index";
		for(size_t iParam = 0; iParam < mio::MeteoData::nrOfParameters; ++iParam)
			std::cout << "\t" << mio::MeteoData::getParameterName(iParam);
		std::cout << "\tTSOIL\n";
		std::string info;
		try {
			reader.getDataOverPoints(date, cells, meteo, info);
		} catch(const std::exception& e) {
			throw std::runtime_error("Cannot read meteo and soil temperature "
					"data on " + date.toString(mio::Date::ISO) + ": " + e.what());
		}
		for(size_t iCell(0); iCell < cells.size(); ++iCell) {
			std::cout << std::setw(3) << indices.at(iCell).first
			          << std::setw(3) << indices.at(iCell).second << "\t";
			for(size_t iParam = 0; iParam < mio::MeteoData::nrOfParameters; ++iParam)
				std::cout << "\t" << meteo.at(iCell)(iParam);
			std::cout << "\t" << meteo.at(iCell)("TSOIL") << std::endl;
		}
		std::cout << std::endl;
	}

	//Print execution time
	printf("\nTime taken: %.3fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	return 0;
}
