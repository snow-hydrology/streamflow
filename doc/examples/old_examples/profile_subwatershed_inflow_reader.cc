/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <streamflow/libstreamflow.h>
#include <meteoio/MeteoIO.h>

using namespace sflow;

/**
 * @brief Main function.
 */
int main(int argc, char *argv[])
{
	//Useful variables
	mio::Config cfg("cfgfiles/io_profile_inflow_reader.ini");
	mio::Date startTime(2014, 10, 1, 1, 0, 1);
	mio::Date endTime(2014, 10, 31, 23, 0, 1);
	cfg.deleteKeys("TIME_STEP", SFConstants::CONFIG_SECTION_NAME);
	Clock clk(cfg, startTime, endTime);

	//Construct a SimpleWatershed object to easily access the single Subwatersheds
	InputReader tmpReader(cfg, startTime);
	Watershed watershed(cfg, clk, tmpReader);
	std::vector<size_t> indices(watershed.getSubwatershedIndices());

	//Create two SubwatershedInflowReaders: one reading data from the grids,
	//and the other one from the SMET files
	SubwInputGridReader gridReader(cfg);
	SubwInputFileReader fileReader(cfg, false, false);

	//Read inflow, at each time step and for each subwatershed, using the SMET
	//file reader
	clock_t tStart = clock();
	for( ; clk.getCurrTime() <= endTime; clk.advanceInTime()) {
		for(const Subwatershed& subw : watershed) {
			fileReader.getMeanTotalInflow(clk.getCurrTime(), subw);
		}
	}
	printf("\nTime taken by the SMETInflowReader instance: %.3fs\n",
			(double)(clock() - tStart)/CLOCKS_PER_SEC);

	//Read inflow, at each time step and for each subwatershed, using the grid
	//reader
	clk.reset();
	tStart = clock();
	for( ; clk.getCurrTime() <= endTime; clk.advanceInTime()) {
		for(const Subwatershed& subw : watershed) {
			gridReader.getMeanTotalInflow(clk.getCurrTime(), subw);
		}
	}
	printf("\nTime taken by the GridInflowReader instance: %.3fs\n",
			(double)(clock() - tStart)/CLOCKS_PER_SEC);

	return 0;
}
