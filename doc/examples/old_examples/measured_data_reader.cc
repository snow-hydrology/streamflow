/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <numeric>
#include <streamflow/libstreamflow.h>
#include <meteoio/MeteoIO.h>

using namespace sflow;

/**
 * @brief Main function. Executable should be called as:
 * 		[executable] [date start] [date end]
 * where [date start] corresponds to the date at which the measurement reading
 * should start, and [date end] the date at which the measurement reading
 * should end
 */
int main(int argc, char *argv[])
{
	//Register start time
	clock_t tStart = clock();

	//Get input argument(s)
	const StreamVariable *var;
	mio::Date startDate, endDate;
	if(argc == 4) {
		var = &StreamVariable::getVariable(argv[1]);
		mio::IOUtils::convertString(startDate, argv[2], 1);
		mio::IOUtils::convertString(endDate, argv[3], 1);
	} else {
		var = &StreamVariable::H;
		startDate.setDate(2011, 10, 1, 4, 0, 1);
		endDate.setDate(2011, 10, 2, 4, 0, 1);
	}

	//Create a MeasuredDataReader
	mio::Config cfg("cfgfiles/io_test.ini");
	MeasuredDataReader reader(cfg);

	//Display the measurement points
	std::vector<size_t> ptIds(reader.getNumMeasPoints());
	std::iota(ptIds.begin(), ptIds.end(), 1);
	std::vector<std::map<mio::Date, double>> values;
	reader.getData(startDate, endDate, *var, ptIds, values);
	for(size_t iPoint = 0; iPoint < values.size(); ++iPoint) {
		std::cout << "Measured values at point " << ptIds.at(iPoint) << std::endl;
		std::cout << "Time\t\t\t" << var->toString() << std::endl;
		for(const std::pair<mio::Date, double>& timePt : values.at(iPoint)) {
			std::cout << timePt.first.toString(mio::Date::ISO) << "\t"
			          << timePt.second << std::endl;
		}
		std::cout << std::endl;
	}

	//Do the same as above, but time step by time step
	const double timeStep = 1./24.; //< in days
	std::cout << std::endl;
	double val;
	for(size_t iPoint = 0; iPoint < values.size(); ++iPoint) {
		std::cout << "Measured values at point " << ptIds.at(iPoint) << std::endl;
		std::cout << "Time\t\t\t" << var->toString() << std::endl;
		for(mio::Date currDate = startDate; currDate < endDate; currDate += timeStep) {
			reader.getData(currDate, *var, ptIds.at(iPoint), val);
			std::cout << currDate.toString(mio::Date::ISO) << "\t"
			          << val << std::endl;
		}
		std::cout << std::endl;
	}

	//Print execution time
	printf("\nTime taken: %.3fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	return 0;
}
