/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <meteoio/MeteoIO.h>
#include <streamflow/libstreamflow.h>

using namespace sflow;

/**
 * @brief Main function. This program aims at testing the interpolation
 * capabilities of the IOManager on data which is not read from files but
 * has been previously pushed into the buffer manually.
 */
int main(int argc, char *argv[])
{
	//Register start time
	clock_t tStart = clock();

	//Get input argument(s)
	std::vector<mio::Date> dates;
	if(argc > 1) {
		mio::Date d;
		for(size_t ix = 1; ix < argc; ++ix) {
			mio::IOUtils::convertString(d, argv[ix], 1);
			dates.push_back(d);
		}
	} else {
		mio::Date d(2011, 10, 1, 12, 0, 1);
		dates.push_back(d);
	}

	//Create a IOManager instance
	const std::string variable("DISCHARGE");
	mio::Config cfg("./cfgfiles/io_test.ini");
	mio::IOManager io(cfg);
	//io.setProcessingLevel(mio::IOUtils::raw | mio::IOUtils::resampled);

	//Create data to be pushed into the IOManager
	mio::Date dateStart(2011, 10, 1, 0, 0, 1);
	double valueStart = 0.;
	mio::METEO_SET set;
	set.reserve(10);
	for(size_t ii = 0; ii < 10; ++ii) {
		mio::MeteoData data(dateStart + ii);
		data(mio::MeteoData::TA) = valueStart + double(ii);
		set.push_back(data);
	}

	//Display contents of the data to be pushed in the IOManager
	std::cout << "Time\tPushed value" << std::endl;
	for(const mio::MeteoData& d : set) {
		std::cout << d.date.toString(mio::Date::ISO) << "\t"
		          << d(mio::MeteoData::TA) << std::endl;
	}
	std::cout << std::endl;

	//Push data in the IOManager and attempt at reading interpolated values
	std::vector<mio::METEO_SET> dataSet = {set};
	io.push_meteo_data(mio::IOUtils::filtered, dateStart, dateStart+9, dataSet);
	std::cout << "Time\tInterpolated value" << std::endl;
	for(const mio::Date& date : dates) {
		io.getMeteoData(date, set);
		std::cout << date.toString(mio::Date::ISO) << "\t"
		          << set.front()(mio::MeteoData::TA) << std::endl;
	}

	//Print execution time
	printf("\nTime taken: %.3fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	return 0;
}
