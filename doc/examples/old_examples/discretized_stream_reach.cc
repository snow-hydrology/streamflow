/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <time.h>
#include <sstream>
#include <functional>
#include <meteoio/MeteoIO.h>
#include <streamflow/libstreamflow.h>

using namespace sflow;


class TestReach: public DiscrMuskingumCungeReach {
	public:
		TestReach(const std::vector<StreamCell>& cells,
				StreamWidthModel& widthModel, Clock& clk,
				const StreamBedFrictionModel& fricModel, const double& iniDischarge);
		virtual void updateInletBoundaryConditions() override;
		virtual ~TestReach();
};


/**
 * @brief Main function.
 */
int main(int argc, char *argv[])
{
	//Register start time
	clock_t tStart = clock();
	srand((unsigned int)time(0));

	//Get the name of the file in which the model results should be written and
	//the time step to be used
	std::string fname("output/muskingum_cunge_reach_simulation.txt");
	double timeStep(3600.); //< [s]
	if(argc > 1)
		fname = std::string(argv[1]);
	if(argc > 2)
		timeStep = atoi(argv[2]);

	//Other parameters
	const size_t nCells(100);
	const double slope(0.01);        //< [-] slope of each reach cell
	const double width(3.0);         //< [m] width of the stream reach
	const double manningCoeff(0.3);  //< [-] Manning coefficient
	const double iniDischarge(1.);   //< [m^3/s] initial discharge value
	//const std::vector<double> cellLengths(nCells, 10.); //< [m] vector containing the length of each cell
	std::vector<double> cellLengths(nCells); //< [m] vector containing the length of each cell
	double totLength(0.);
	for(size_t iCell(0); iCell < nCells; ++iCell) {
		const double length(fmax(double(rand())/double(RAND_MAX), 0.1));
		cellLengths[iCell] = length;
		totLength += length;
	}
	const double lengthFactor(10.*nCells/totLength);
	for(size_t iCell(0); iCell < nCells; ++iCell) {
		cellLengths[iCell] *= lengthFactor;
	}
	const mio::Date startDate(2011, 10, 1, 1, 0, 1);
	const mio::Date endDate(2011, 10, 2, 1, 0, 1);

	//Useful variables
	mio::Config cfg;
	std::ostringstream os;
	os << width;
	cfg.addKey("UNIFORM_WIDTH", SFConstants::CONFIG_SECTION_NAME, os.str());
	os.str("");
	os.clear();
	os << manningCoeff;
	cfg.addKey("MANNING_COEFF", SFConstants::CONFIG_SECTION_NAME, os.str());
	os.str("");
	os.clear();
	os << 0.9*manningCoeff << " " << 1.1*manningCoeff;
	cfg.addKey("MANNING_COEFF::LIMITS", SFConstants::CONFIG_SECTION_NAME, os.str());
	os.str("");
	os.clear();
	os << std::fixed << std::setprecision(5) << timeStep/60.;
	cfg.addKey("TIME_STEP", SFConstants::CONFIG_SECTION_NAME, os.str());
	os.str("");
	os.clear();
	os << std::fixed << std::setprecision(5) << 1./60 << " " << timeStep/60.;
	cfg.addKey("TIME_STEP::LIMITS", SFConstants::CONFIG_SECTION_NAME, os.str());
	Clock clk(cfg, startDate, endDate);

	//Create a vector of stream cells
	std::vector<StreamCell> cells;
	cells.reserve(nCells);
	mio::Coords pt("CH1903");
	for(size_t iCell(0); iCell < nCells; ++iCell) {
		pt.setXY(0., 0., 0.);
		StreamCell cell(pt, 0.0, 0.0, cellLengths.at(iCell), slope);
		cells.push_back(cell);
	}

	//Create a simple stream reach working with the Muskingum-Cunge method
	ManningFrictionModel fricModel(cfg);
	UniformWidth widthModel(cfg);
	TestReach reach(cells, widthModel, clk, fricModel, iniDischarge);
	clk.updateTimeStep();

	//Initialize the output file
	std::ofstream f(fname);
	if(f.fail()) throw mio::AccessException(fname, AT);
	f << clk.getCurrTime().toString(mio::Date::ISO) << " ";
	for(const double& len : cellLengths) {
		f << std::fixed << std::setw(10) << std::setprecision(2) << len;
	}
	f << std::endl;
	f << clk.getCurrTime().toString(mio::Date::ISO) << " ";
	for(const double& q : reach.get(StreamVariable::Q)) {
		f << std::fixed << std::setw(10) << std::setprecision(4) << q;
	}
	f << std::endl;

	//Evaluate the model error as a function of the time step
	while(clk.getCurrTime() < clk.getEndTime()) {
		reach.updateInletBoundaryConditions();
		reach.advanceInTime();
		clk.advanceInTime();
		f << clk.getCurrTime().toString(mio::Date::ISO) << " ";
		for(const double& q : reach.get(StreamVariable::Q)) {
			f << std::fixed << std::setw(10) << std::setprecision(4) << q;
		}
		f << std::endl;
	}
	f.close();

	//Print execution time
	printf("\nTime taken: %.3fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	return 0;
}



TestReach::TestReach(const std::vector<StreamCell>& cells,
		StreamWidthModel& widthModel, Clock& clk,
		const StreamBedFrictionModel& fricModel, const double& iniDischarge) :
				DiscrMuskingumCungeReach(cells, widthModel, clk, fricModel)
{
	_discharge.assign(_n_cells, iniDischarge);
	_inlet_bc.discharge = iniDischarge;
	_width = widthModel.getWidth(*this);
}

void TestReach::updateInletBoundaryConditions() {
	_inlet_discharge_old = _inlet_bc.discharge;
	_inlet_bc.discharge  = 0.2*sin(2.*SFConstants::PI*(_clock.getNextTime().getJulian() -
			_clock.getStartTime().getJulian())) + 1.;
}

TestReach::~TestReach() {}

