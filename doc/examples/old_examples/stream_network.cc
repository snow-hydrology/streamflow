/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <time.h>
#include <streamflow/libstreamflow.h>
#include <meteoio/MeteoIO.h>

using namespace sflow;

const std::string printReaches(StreamNetwork& network);

/**
 * @brief Main function.
 */
int main(int argc, char *argv[])
{
	//Register start time
	clock_t tStart = clock();

	//Useful variables
	StreamflowConfig cfg("cfgfiles/io_test.ini");
	mio::IOManager io(cfg);
	mio::Date startTime(2011, 10, 1, 0, 0, 1);
	mio::Date endTime(2015, 10, 1, 0, 0, 1);
	cfg.deleteKeys("TIME_STEP", SFConstants::CONFIG_SECTION_NAME);
	Clock clk(cfg, startTime, endTime);

	//Create a stream network
	InputReader reader(cfg, startTime);
	Watershed simpBasin(cfg, clk, reader);
	StreamNetwork network(cfg, clk, reader);
	network.setWatershed(simpBasin);
	std::cout << "Network parameters:\n" << network.getParams() << std::endl;
	std::ostringstream os;
	std::cout << "Easting\tNorthing\n";
	size_t index(0);
	for(const StreamReach& reach : network) {
		std::cout << reach.getCell(0).getEasting() << "\t" <<
				reach.getCell(0).getNorthing() << "\n";
		os << "mask_reach_" << index << ".asc";
		io.write2DGrid(reach.getSubwatershed().getMask(), os.str());
		os.str("");
		os.clear();
		++index;
	}
	std::cout << std::endl;

	//Print info about the last cell
	const StreamReach& lastReach(network.getReach(54));
	const size_t nCells(lastReach.getNumCells());
	const StreamCell& lastCell(lastReach.getCell(nCells-1));
	std::cout << "Last stream cell's slope: " << std::setprecision(4) << lastCell.slope << std::endl;
	std::cout << "Last stream cell's width: " << lastReach.getWidth() << std::endl;

	//Print the reaches lengths
	std::cout << "Reach lengths: ";
	for(const StreamReach& reach : network) {
		std::cout << reach.getLength() << " ";
	}
	std::cout << std::endl;

	//Evolve the network in time
	std::cout << "\ntime [s]";
	for(const StreamReach& reach : network) {
		std::cout << "\tQ_" << reach.getIndex() << " [m^3/s]";
	}
	std::cout << "\n" << clk.getCurrTime().toString(mio::Date::ISO) << "\t" <<
			printReaches(network) << "\n";
	for(size_t ix = 0; ix < 24*10; ++ix, clk.advanceInTime()) {
		network.advanceInTime();
		std::cout << clk.getCurrTime().toString(mio::Date::ISO) << "\t" <<
				printReaches(network) << std::endl;
	}
	network.advanceInTime();

	//Output the lateral inflow discharge of each stream reach
	std::cout << "Reach index\tLateral inflow [m^3/s]\n";
	for(const StreamReach& reach : network) {
		std::cout << reach.getIndex() << "\t";
		std::cout << reach.getSubwatershed().get(SubwatershedVariable::Q_outlet) << "\n";
	}

	//Print execution time
	printf("\nTime taken: %.3fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	return 0;
}


const std::string printReaches(StreamNetwork& network) {
	StreamNetwork::const_iterator it(network.begin());
	std::ostringstream os;
	os << std::setprecision(3);
	os << static_cast<const LumpedStreamReach&>(*it).get(StreamVariable::Q);
	for(++it; it != network.end(); ++it) {
		os << "\t";
		os << static_cast<const LumpedStreamReach&>(*it).get(StreamVariable::Q);
	}
	return os.str();
}
