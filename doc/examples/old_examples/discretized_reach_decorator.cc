/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <time.h>
#include <meteoio/MeteoIO.h>
#include <streamflow/libstreamflow.h>

using namespace sflow;

class TestReach: public DiscrMuskingumCungeReach {
	public:
		TestReach(const std::vector<StreamCell>& cells,
				StreamWidthModel& widthModel, Clock& clk,
				const StreamBedFrictionModel& fricModel, const double& discharge,
				const double& waterHeight, const double& inputTemp);
		virtual void advanceInTime() override;
		virtual ~TestReach();
};


class TestDecorator : public DiscretizedReachWithEB {
	public:
		TestDecorator(DiscretizedReachInterface *decoratedObject,
				const ReachHeatFluxModel& model, InputReader& reader,
				const StreamBedFrictionModel& fricModel,
				const std::vector<double>& iniTemp, Clock& clock);
		virtual ~TestDecorator();
};


class DummyHeatFluxModel : public ReachHeatFluxModel {
	public:
		DummyHeatFluxModel(const mio::Config& cfg);
		virtual double getAbsorbedSWRadiation(const StreamCell& cell,
				const mio::Date& date, const mio::MeteoData& meteo) const override;
		virtual double getAbsorbedLWRadiation(const mio::MeteoData& meteo) const override;
		virtual double getEmittedLWRadiation(const double& streamTemp) const override;
		virtual double getLatentHeatFlux(const StreamCell& cell,
				const mio::MeteoData& meteo, const double& streamTemp) const override;
		virtual double getSensibleHeatFlux(const StreamCell& cell,
				const mio::MeteoData& meteo, const double& streamTemp) const override;
		virtual double getBedConductionHeatFlux(const double& streamTemp,
				const double& bedTemp) const override;
		virtual double getBedFrictionHeatFlux(const double& slope,
				const double& discharge, const double& width) const override;
		virtual ~DummyHeatFluxModel();
};



/**
 * @brief Main function.
 */
int main(int argc, char *argv[])
{
	//Register start time
	clock_t tStart = clock();

	//Get the name of the file in which the model results should be written and
	//the time step to be used
	std::string fname("output/temp_advection_reach_simulation.txt");
	double timeStep(1.); //< [s]
	if(argc > 1)
		fname = std::string(argv[1]);
	if(argc > 2)
		timeStep = atoi(argv[2]);

	//Other parameters
	const size_t nCells(100);
	const double slope(0.01);        //< [-] slope of each reach cell
	const double width(2.0);         //< [m] width of the stream reach
	const double manningCoeff(0.3);  //< [-] Manning coefficient
	const double discharge(1.);      //< [m^3/s] uniform discharge value
	const double waterHeight(0.5);   //< [m] uniform water height value
	const double inputTemp(293.15);  //< [K] input temperature
	std::vector<double> temperature(nCells, 278.15);
	//const std::vector<double> cellLengths(nCells, 10.); //< [m] vector containing the length of each cell
	std::vector<double> cellLengths(nCells); //< [m] vector containing the length of each cell
	double totLength(0.);
	for(size_t iCell(0); iCell < nCells; ++iCell) {
		const double length(fmax(double(rand())/double(RAND_MAX), 0.1));
		cellLengths[iCell] = length;
		totLength += length;
	}
	const double lengthFactor(10.*nCells/totLength);
	for(size_t iCell(0); iCell < nCells; ++iCell) {
		cellLengths[iCell] *= lengthFactor;
	}
	const mio::Date startDate(2011, 10, 1, 1, 0, 1);
	const mio::Date endDate(2011, 10, 1, 11, 0, 1);

	//Useful variables
	StreamflowConfig cfg("cfgfiles/io_best.ini");
	std::ostringstream os;
	os << width;
	cfg.addKey("UNIFORM_WIDTH", SFConstants::CONFIG_SECTION_NAME, os.str());
	os.str("");
	os.clear();
	os << manningCoeff;
	cfg.addKey("MANNING_COEFF", SFConstants::CONFIG_SECTION_NAME, os.str());
	os.str("");
	os.clear();
	os << 0.9*manningCoeff << " " << 1.1*manningCoeff;
	cfg.addKey("MANNING_COEFF::LIMITS", SFConstants::CONFIG_SECTION_NAME, os.str());
	os.str("");
	os.clear();
	os << std::fixed << std::setprecision(5) << timeStep/60.;
	cfg.addKey("TIME_STEP", SFConstants::CONFIG_SECTION_NAME, os.str());
	os.str("");
	os.clear();
	os << std::fixed << std::setprecision(5) << 1./60. << " " << timeStep/60.;
	cfg.addKey("TIME_STEP::LIMITS", SFConstants::CONFIG_SECTION_NAME, os.str());
	Clock clk(cfg, startDate, endDate);

	//Create a vector of stream cells
	std::vector<StreamCell> cells;
	cells.reserve(nCells);
	mio::Coords pt("CH1903");
	for(size_t iCell(0); iCell < nCells; ++iCell) {
		pt.setXY(0., 0., 0.);
		StreamCell cell(pt, 0.0, 0.0, cellLengths.at(iCell), slope);
		cells.push_back(cell);
	}

	//Create a simple stream reach able to compute stream temperature
	ManningFrictionModel fricModel(cfg);
	UniformWidth widthModel(cfg);
	DummyHeatFluxModel fluxModel(cfg);
	InputReader inputReader(cfg, startDate);
	TestReach *tmp = new TestReach(cells, widthModel, clk, fricModel,
			discharge, waterHeight, inputTemp);
	TestDecorator *reach = new TestDecorator(tmp, fluxModel, inputReader,
			fricModel, temperature, clk);
	clk.updateTimeStep();

	//Initialize the output file
	std::ofstream f(fname);
	if(f.fail()) throw mio::AccessException(fname, AT);
	f << clk.getCurrTime().toString(mio::Date::ISO) << " ";
	for(const double& len : cellLengths) {
		f << std::fixed << std::setw(10) << std::setprecision(2) << len;
	}
	f << std::endl;
	f << clk.getCurrTime().toString(mio::Date::ISO) << " ";
	for(const double& t : reach->get(StreamVariable::T)) {
		f << std::fixed << std::setw(10) << std::setprecision(4) << t;
	}
	f << std::endl;

	//Evaluate the model error as a function of the time step
	while(clk.getCurrTime() < clk.getEndTime()) {
		reach->advanceInTime();
		clk.advanceInTime();
		f << clk.getCurrTime().toString(mio::Date::ISO) << " ";
		for(const double& t : reach->get(StreamVariable::T)) {
			f << std::fixed << std::setw(10) << std::setprecision(4) << t;
		}
		f << std::endl;
	}
	f.close();
	delete reach;

	//Print execution time
	printf("\nTime taken: %.3fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	return 0;
}



TestReach::TestReach(const std::vector<StreamCell>& cells,
		StreamWidthModel& widthModel, Clock& clk,
		const StreamBedFrictionModel& fricModel, const double& discharge,
		const double& waterHeight, const double& inputTemp) :
				DiscrMuskingumCungeReach(cells, widthModel, clk, fricModel)
{
	_discharge.assign(_n_cells, discharge);
	_water_height.assign(_n_cells, waterHeight);
	_inlet_bc.discharge   = discharge;
	_inlet_bc.temperature = inputTemp;
	_width = widthModel.getWidth(*this);
}

void TestReach::advanceInTime() {
	//do nothing
}

TestReach::~TestReach() {}


TestDecorator::TestDecorator(DiscretizedReachInterface *decoratedObject,
		const ReachHeatFluxModel& model, InputReader& reader,
		const StreamBedFrictionModel& fricModel, const std::vector<double>& iniTemp,
		Clock& clock) :
				DiscretizedReachWithEB(decoratedObject, model, reader, fricModel, clock)
{
	if(iniTemp.size() != this->getNumCells())
		throw std::invalid_argument("Initial stream temperature vector needs to "
				"have the same number of elements as the stream reach");
	_temperature = iniTemp;
	_inlet_temperature_old = decoratedObject->getInletBoundaryConditions().temperature;
}

TestDecorator::~TestDecorator() {}


DummyHeatFluxModel::DummyHeatFluxModel(const mio::Config& cfg) :
		ReachHeatFluxModel(cfg) {}

double DummyHeatFluxModel::getAbsorbedSWRadiation(const StreamCell&,
		const mio::Date&, const mio::MeteoData&) const {
	return 2903.472;
}

double DummyHeatFluxModel::getAbsorbedLWRadiation(const mio::MeteoData&) const {
	return 0.;
}

double DummyHeatFluxModel::getEmittedLWRadiation(const double& streamTemp) const {
	return 0.;
}

double DummyHeatFluxModel::getLatentHeatFlux(const StreamCell&,
		const mio::MeteoData&, const double&) const {
	return 0.;
}

double DummyHeatFluxModel::getSensibleHeatFlux(const StreamCell&,
		const mio::MeteoData&, const double&) const {
	return 0.;
}

double DummyHeatFluxModel::getBedConductionHeatFlux(const double&, const double&) const {
	return 0.;
}

double DummyHeatFluxModel::getBedFrictionHeatFlux(const double&, const double&,
		const double&) const {
	return 0.;
}

DummyHeatFluxModel::~DummyHeatFluxModel() {}
