/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <time.h>
#include <meteoio/MeteoIO.h>
#include <streamflow/libstreamflow.h>

using namespace sflow;


/**
 * @brief Main function.
 */
int main(int argc, char *argv[])
{
	//Register start time
	clock_t tStart = clock();

	//Get the name of the file in which the model results should be written and
	//the time step to be used
	std::string fname("output/subw0_discharge_simulation_dt_1s.txt");
	double timeStep(1.); //< [s]
	if(argc > 1)
		fname = std::string(argv[1]);
	if(argc > 2)
		timeStep = atoi(argv[2]);

	//Useful variables
	StreamflowConfig cfg("cfgfiles/io_test.ini");
	cfg.addKey("MAX_INFILTRATION_RATE", SFConstants::CONFIG_SECTION_NAME, "8.55"); //< in mm/day
	cfg.addKey("MAX_INFILTRATION_RATE::limits", SFConstants::CONFIG_SECTION_NAME, "0 10"); //< in mm/day
	cfg.addKey("UPPER_RESERVOIR_TAU", SFConstants::CONFIG_SECTION_NAME, "18.07"); //< in days
	cfg.addKey("UPPER_RESERVOIR_TAU::limits", SFConstants::CONFIG_SECTION_NAME, "0 100"); //< in days
	cfg.addKey("LOWER_RESERVOIR_TAU", SFConstants::CONFIG_SECTION_NAME, "308"); //< in days
	cfg.addKey("LOWER_RESERVOIR_TAU::limits", SFConstants::CONFIG_SECTION_NAME, "1 600"); //< in days
	std::ostringstream os;
	os << std::fixed << std::setprecision(5) << timeStep/60.;
	cfg.addKey("TIME_STEP", SFConstants::CONFIG_SECTION_NAME, os.str());
	os.str("");
	os.clear();
	os << std::fixed << std::setprecision(5) << 1./60. << " " << timeStep/60.;
	cfg.addKey("TIME_STEP::LIMITS", SFConstants::CONFIG_SECTION_NAME, os.str());
	const mio::Date startDate(2013, 10, 1, 0, 0, 1);
	const mio::Date endDate(2014, 9, 28, 10, 0, 1);
	Clock clk(cfg, startDate, endDate);
	mio::IOManager io(cfg);
	mio::Grid2DObject grid;
	io.read2DGrid(grid, "dischmaw.asc");

	//Create a simple subwatershed
	const double watershedArea = 43.28;
	TwoVerticalLinearReservoirs::Parameters reservoirs(cfg, watershedArea);
	InputReader reader(cfg, startDate);
	TwoVerticalLinearReservoirs subw(grid, 0, clk, reader, reservoirs);
	subw.updateBoundaryConditions();

	//Initialize the output file
	std::ofstream f(fname);
	if(f.fail()) throw mio::AccessException(fname, AT);
	f << "SMET 1.1 ASCII\n";
	f << "[HEADER]\n";
	f << "station_id   = 0"              << subw.getIndex() << "\n";
	f << "station_name = subwatershed_" << subw.getIndex() << "\n";
	f << std::left << std::fixed << std::setprecision(8);
	const mio::Coords& llCorner(subw.getMask().llcorner);
	f << "latitude     = " << llCorner.getLat() << "\n";
	f << "longitude    = " << llCorner.getLon() << "\n";
	f << std::setprecision(2);
	f << "altitude     = " << llCorner.getAltitude() << "\n";
	f << "area         = " << subw.getArea()*1e-6 << "\n";
	f << "comment      = area is in km^2\n";
	f << std::right << std::fixed << std::setprecision(1);
	f << "nodata       = " << mio::IOUtils::nodata << "\n";
	f << "tz           = " << 1. << "\n";
	f << "fields       = timestamp outlet_discharge\n";
	f << "units        = none m^3/s\n";
	f << "[DATA]\n";
	f << clk.getCurrTime().toString(mio::Date::ISO) << " " << std::setprecision(4)
	  << subw.get(SubwatershedVariable::Q_outlet) << std::endl;

	//Advance the subwatershed state in time and print the discharge values in
	//the output file
	while(clk.getCurrTime() < clk.getEndTime()) {
		subw.updateBoundaryConditions();
		subw.advanceInTime();
		clk.advanceInTime();
		f << clk.getCurrTime().toString(mio::Date::ISO) << " " << std::setprecision(4)
		  << subw.get(SubwatershedVariable::Q_outlet) << std::endl;
	}
	f.close();

	return 0;
}
