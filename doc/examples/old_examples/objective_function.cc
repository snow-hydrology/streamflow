/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <meteoio/MeteoIO.h>
#include <streamflow/libstreamflow.h>

using namespace sflow;

/**
 * @brief Main function.
 */
int main(int argc, char *argv[])
{
	//Register start time
	clock_t tStart = clock();

	//Create an ObjectiveFunction object
	mio::Config cfg("./cfgfiles/io_test.ini");
	ObjectiveFunction fun(cfg);

	//Test object
	std::cout << "Objective function value: " << fun.getValue() << std::endl;
	std::cout << "Performance measures (1st method):\n";
	const std::vector<std::string>& names(fun.getPerfMeasureNames());
	std::vector<double> values(fun.getPerfMeasureValues());
	for(size_t iMeas = 0; iMeas < names.size(); ++iMeas) {
		std::cout << std::setw(10) << names.at(iMeas) << ": " << values.at(iMeas) << std::endl;
	}
	std::cout << "\nPerformance measures (2nd method):\n";
	for(const std::pair<std::string,double>& measure : fun.getPerfMeasures()) {
		std::cout << std::setw(10) << measure.first << ": " << measure.second << std::endl;
	}

	//Print execution time
	printf("\nTime taken: %.3fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	return 0;
}
