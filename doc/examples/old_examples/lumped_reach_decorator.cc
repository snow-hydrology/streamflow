/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <time.h>
#include <meteoio/MeteoIO.h>
#include <streamflow/libstreamflow.h>

using namespace sflow;

/**
 * @brief Main function.
 */
int main(int argc, char *argv[])
{
	//Register start time
	clock_t tStart = clock();

	//Useful variables
	StreamflowConfig cfg("cfgfiles/io_best.ini");
	mio::IOManager io(cfg);
	mio::Grid2DObject grid;
	io.read2DGrid(grid, "dischmaw.asc");
	Clock clk(cfg, {2011, 10, 1, 1, 0, 1}, {2011, 11, 1, 1, 0, 1});
	InputReader inputReader(cfg, clk.getStartTime());
	TaudemReachFilesReader cellsReader(cfg);

	//Create a simple subwatershed working with the Forward Euler scheme
	const size_t reachIndex(1);
	TwoVerticalLinearReservoirs::Parameters reservoirs(cfg, 43.28);
	LumpedSubwatershedInterface *subwatershed(nullptr);
	subwatershed = new TwoVerticalLinearReservoirs(grid, reachIndex, clk,
			inputReader, reservoirs);
	SimpleSubwatershedWithEB::Parameters diffTime(cfg);
	subwatershed = new SimpleSubwatershedWithEB(subwatershed, diffTime);

	//Create a simple stream reach
	const std::vector<StreamCell>& cells(cellsReader.getReachCells(reachIndex));
	AreaPowerWidthModel widthModel(cfg);
	DischargePowerHeightModel heightModel(cfg);
	LumpedReachInterface *reach(nullptr);
	reach = new InstantaneousRoutingReach(cells, widthModel, clk, heightModel);
	reach->setSubwatershed(*subwatershed);
	subwatershed->setStreamReach(*reach);

	//Decorate the stream reach with an energy-balance decorator
	ReachHeatFluxModel heatFluxes(cfg);
	reach = new LumpedReachWithEB(reach, heatFluxes, inputReader);

	//Advance a few time steps
	const size_t nSteps(10);
	std::cout << "Time step\tDischarge [m^3/s]\tTemperature [K]\n";
	std::cout << "0\t\t" << std::setw(10) << reach->get(StreamVariable::Q) << "\t\t"
			<< reach->get(StreamVariable::T) << std::endl;
	for(size_t iStep(0); iStep < nSteps; ++iStep) {
		subwatershed->updateBoundaryConditions();
		subwatershed->advanceInTime();
		reach->updateInletBoundaryConditions();
		reach->updateLateralBoundaryConditions();
		reach->advanceInTime();
		clk.advanceInTime();
		std::cout << iStep + 1 << "\t\t" << std::setw(10)
				<< reach->get(StreamVariable::Q) << "\t\t"
				<< reach->get(StreamVariable::T) << std::endl;
	}
	delete subwatershed;
	delete reach;

	//Print execution time
	printf("\nTime taken: %.3fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	return 0;
}
