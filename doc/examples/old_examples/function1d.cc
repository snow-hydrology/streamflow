/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <streamflow/libstreamflow.h>

using namespace sflow;

/**
 * @brief Main function.
 */
int main(int argc, char *argv[])
{
	//Create a simple piecewise linear function
	std::vector<double> x = {0, 1, 3, 4};
	std::vector<double> y = {0, 1, -1, 0};
	PiecewiseLinear1D linfun(x, y);

	//Evaluate the function in different points
	std::vector<double> xeval = {-1, 0.5, 1.5, 1.7, 2.5, 4, 5};
	std::cout << ">> Piecewise linear function" << std::endl;
	for(size_t ix = 0; ix < xeval.size(); ++ix) {
		std::cout << "\tf(" << xeval[ix] << ") = " << linfun(xeval[ix]) << std::endl;
	}

	//Evaluate the function integral between different limits
	std::vector<std::vector<double>> limits = {{-1, 5}, {0, 2}, {2, 4}, {1, 3}};
	for(size_t ix = 0; ix < limits.size(); ++ix) {
		std::cout << "\tIntegral of f over [" <<
				limits[ix][0] << "," << limits[ix][1] << "] = " <<
				linfun.getIntegral(limits[ix][0], limits[ix][1]) << std::endl;
	}

	return 0;
}
