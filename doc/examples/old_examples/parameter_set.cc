/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <time.h>
#include <meteoio/MeteoIO.h>
#include <streamflow/libstreamflow.h>

using namespace sflow;

/**
 * @brief Main function.
 */
int main(int argc, char *argv[])
{
	//Register start time
	clock_t tStart = clock();
	mio::Config cfg("cfgfiles/io_best.ini");

	//Create some parameters
	Parameter param1("1st_param", 1.5, Units::s, {0, 2}, true, "blank");
	Parameter param2("2nd_param", 3.0, Units::m/Units::s, {0, 10}, false, "nothing");
	Parameter param3("3rd_param", 0.01, Units::m, {0, 0.1}, false, "still noting");
	Parameter param4("3rd_param_bis", 0.4, Units::m, {0, 0.5}, true, "");
	Parameter param5(cfg, "MAX_INFILTRATION_RATE", "HYDROMODEL", Units::mm/Units::day,
			{0, 100}, "Maximum infiltration rate");

	//Create a parameter set and display it
	ParameterSet set;
	set.add(param1);
	set.add(param2);
	set.add(param3);
	set.add(param4);
	set.add(param5);
	std::set<std::string> names(set.getParameterNames());
	std::cout << "Parameter set names: ";
	for(auto it = names.begin(); it != names.end(); ++it)
		std::cout << *it << " ";
	std::cout << std::endl;
	std::cout << "Parameter set contents:\n" << set << std::endl;

	//Test some of the object functionalities
	std::cout << "\nParameters starting with \"3\":\n" << set.getSubset("3") << std::endl;
	set.prependNames("prefix_");
	std::cout << "\nParameters with prefix \"prefix_\":\n" << set << std::endl;
	set.ltrimNames("prefix_");
	std::cout << "\nParameters without prefix \"prefix_\":\n" << set << std::endl;
	set.remove("3rd_param");
	std::cout << "\nParameters without \"3rd_param\":\n" << set << std::endl;
	set.removeFixedParameters();
	std::cout << "\nOnly the parameters to calibrate:\n" << set << std::endl;

	//Create a scored parameter set and test it
	std::map<std::string, double> measures = {{"RMSE", 0.3}, {"NSE", 1.5}};
	ScoredParameterSet sset;
	sset.set(set, 100, measures);
	std::cout << "\nParameter set contents:\n" << sset << std::endl;
	std::cout << "Parameter set score: " << sset.getScore() << std::endl;
	std::cout << "Performance measures:\n";
	for(const std::pair<std::string, double>& measure : sset.getPerfMeasures())
		std::cout << "\t" << measure.first << " = " << measure.second << "\n";

	//Test some functionalities
	if(sset == set) std::cout << "\n'sset' and 'set' are equal\n";
	else std::cout << "'sset' and 'set' are different\n";
	ScoredParameterSet sset2(sset);
	sset2.setScore(10.);
	if(sset == sset2) std::cout << "'sset' and 'sset2' are equal\n";
	else std::cout << "'sset' and 'sset2' are different\n";
	if(sset2 == set) std::cout << "'sset2' and 'set' are equal\n";
	else std::cout << "'sset2' and 'set' are different\n";


	//Print execution time
	printf("\nTime taken: %.3fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	return 0;
}
