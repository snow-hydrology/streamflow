—— STREAMFLOW EXAMPLES —-

1. NOTES

These examples show the calibration and the run of streamflow.
The input are taken from A3D generated runoff and meet set files 
(in the temperature examples) or from grids (in the discharge examples).
To reduce the input files size, the input is from A3D over the Dischma 
catchment with a coarse 500m resolution grids, so the output are not aimed 
to be good.


2. ADDITIONAL HELP

To have details about the model and parameters, please read:

Gallice, A. et al. (2016), StreamFlow 1.0: An extension to the spatially
distributed snow model Alpine3D for hydrological modelling and deterministic
stream temperature prediction, Geoscientific Model Development, 9, 4491-4519.

and:

Comola, F. Wt al. (2015), Thermodynamics in the hydrologic response: Travel 
time formulation and application to Alpine catchments, Water Resour. Res., 
51, 1671–1687.  
To have details about how to run the model, please go to 
https://models.slf.ch/p/streamflow/page/Running-StreamFlow/
Note that the sections "Section[INPUT]" and "Section[INPUTFROMA3D] are
Currently not up to date. Please refer to the comments in the examples
ini files to see which keys are required.


3. RUNNING THE EXAMPLES

	1. Extract the input tar.gz file
	2. Compile streamflow (see https://models.slf.ch/p/streamflow/page/Installing-StreamFlow/)
	3. Make the sh files in the setup folder executable (from setup dir: chmod +x *.sh)
	4. Run the various examples
