#!/bin/bash
#This is the launching script for StreamFlow. Please make sure the user section matches your needs!
########################### PBS directives. Edit for Sun Grid Engine runs
#$ -m aes
#$ -M adrien.michel@epfl.ch
#$ -N Streamflow_calibration_q_example
#$ -S /bin/bash
#$ -cwd
#$ -j y
#$ -l mem_free=12G ##nodes must have at least this much free memory
##$ -pe threaded 2 ##at least 2 free cores per node
#$ -pe smp 16
##$ -pe orte 8 ##for MPI simulations

########################### USER CONFIGURATION
##EITHER OPENMP OR NONE
PARALLEL=OPENMP

##when using openmp but NOT relying on Sun Grid Engine for job management, how many cores to use (otherwise it will be ignored)
NCORES=2

N_EB=1
N_SN=1
if [ "${PARALLEL}" = "OPENMP" ]; then
    export OMP_NUM_THREADS=${NSLOTS:=$NCORES}
    N_EB=$OMP_NUM_THREADS
    N_SN=$OMP_NUM_THREADS
    echo "Running with OPENMP on ${N_SN} core(s)"
fi

## Add path to the libraries to the search path
export LD_LIBRARY_PATH=../bin:${LD_LIBRARY_PATH}

## StreamFlow executable
EXE="streamflow"

## Configuration file
CONFIG="io_calib_t.ini"

## Should the model be calibrated?
CALIBRATE=true

## Define the simulation warmup start and end dates
WARMUP_START="2014-08-01T00:00:00"
WARMUP_END="2015-08-01T00:00:00"
## Define the simulation warmup start and end dates
SIMU_START="2014-08-01T00:00:00"
SIMU_END="2015-10-01T00:00:00"

## Create command
CMD="${EXE} -i=${CONFIG}"
if [ -n "${WARMUP_START}" ]; then
    CMD="${CMD} --warmup-startdate=${WARMUP_START} --warmup-enddate=${WARMUP_END}"
fi
if [ -n "${SIMU_START}" ]; then
    CMD="${CMD} -a=${SIMU_START}"
fi
if [ -n "${SIMU_END}" ]; then
    CMD="${CMD} -z=${SIMU_END}"
fi
if [ "${CALIBRATE}" = true ]; then
    CMD="${CMD} -c"
fi

## Launch command
${CMD} > stdouterr_calib_t.log 2>&1 $*
echo "StreamFlow simulation completed"
