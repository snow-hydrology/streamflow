/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINPAGE_H_
#define MAINPAGE_H_

/**
 * @brief The namespace sflow contains all the classes and utility functions
 * related with the hydrological model StreamFlow.
 */
namespace sflow {

//-----------------------------------------------------------------------------
// Definition of the modules (groups of logically related classes)
//-----------------------------------------------------------------------------

/**
 * @defgroup calibration Calibration
 * This module regroups the classes used to calibrate the hydrological model.
 * On top of various calibration algorithms, it contains different
 * performance measures to be used when defining the objective function (i.e. the
 * function whose value is to be minimized by the calibration algorithm).
 */

/**
 * @defgroup streamReach Stream Reach
 * This module contains the classes related to the definition of a stream reach,
 * i.e. to the fraction of stream contained between two confluences.
 * There are two main types of stream reaches in StreamFlow: the lumped ones
 * (treated as if a single point) and the discretized ones (split into smaller
 * spatial entities called "cells").
 */

/**
 * @defgroup subwatershed Subwatershed
 * This module regroups the classes related to the definition of a subwatershed.
 * Only lumped subwatersheds (modeled as if a single point) are available so far
 * in StreamFlow, but discretized ones (taking spatial information into account)
 * might become available in the future.
 */

/**
 * @defgroup io Input/Output
 * This module regroups all the classes used for reading input data or writing
 * output files.
 */

} /* namespace sflow */


#endif /* MAINPAGE_H_ */
