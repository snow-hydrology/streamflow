/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYDROLOGICALMODEL_H_
#define HYDROLOGICALMODEL_H_

#include <iostream>
#include <ctime>
#include <sstream>
#include <string>
#include <streamflow/Watershed.h>
#include <streamflow/StreamNetwork.h>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/utils/StreamflowExceptions.h>
#include <streamflow/io/DataContainer.h>
#include <meteoio/MeteoIO.h>

namespace sflow {

/**
 * @class HydrologicalModel
 * @brief This class implements a hydrological model, composed of a watershed
 * and its associated stream network.
 * @author Aurelien Gallice
 * @date 2015-09-10
 */
class HydrologicalModel : public ObservableModel {
	public:
		HydrologicalModel(const mio::Config& cfg, const mio::Date& startTime,
				const mio::Date& endTime, const mio::Date& startData, const mio::Date& endData);
		HydrologicalModel(const mio::Config& cfg, const mio::Date& startTime,
				const mio::Date& endTime, const mio::Date& warmupStartTime,
				const mio::Date& warmupEndTime, const mio::Date& startData, const mio::Date& endData);
		HydrologicalModel(const HydrologicalModel&);

		HydrologicalModel& operator=(const HydrologicalModel&) = delete;
		void setWarmupPhase(const mio::Date& startTime, const mio::Date& endTime);
		void setDisplayInfo(const bool& value) override;
		void setParams(const ParameterSet& params) override;
		ParameterSet getParams() const override;
		Clock& getClock();
		const Clock& getClock() const;
		void run() override;
		void reset() override;
		~HydrologicalModel() override;

	protected:
		const mio::Config _cfg;
		Clock _clock;
		std::shared_ptr<DataContainer> dataContainer;
		Watershed _watershed;
		StreamNetwork _network;
		mio::Date _warmup_start_date;
		mio::Date _warmup_end_date;
		const mio::Date _simulation_start_date;
		const mio::Date _simulation_end_date;
		bool _simulation_completed; //< flag to avoid method "run" to be called several times in a row
		bool _display_info;         //< flag specifying whether information regarding model progress should be printed on the screen

		void initialize();
		void runWarmupPhase();
		void runSimulation();
};

} /* namespace sflow */

#endif /* HYDROLOGICALMODEL_H_ */
