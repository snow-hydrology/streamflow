/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/HydrologicalModel.h>

namespace sflow {

/**
 * @brief Constructor of class HydrologicalModel initializing the model without
 * a warmup phase
 * @param cfg Reference to the Config object holding the simulation parameters
 * @param startTime Simulation start date
 * @param endTime Simulation end date
 */
HydrologicalModel::HydrologicalModel(const mio::Config& cfg, const mio::Date& startTime,
		const mio::Date& endTime, const mio::Date& startData, const mio::Date& endData) : ObservableModel(),
				_cfg(cfg), _clock(cfg, startTime, endTime),
				 dataContainer(std::make_shared<DataContainer>(_cfg, startData, endData,_watershed,_network)),
				_watershed(cfg, _clock,dataContainer),_network(cfg, _clock,dataContainer),
				_warmup_start_date(),_warmup_end_date(), _simulation_start_date(startTime),
				_simulation_end_date(endTime), _simulation_completed(false),
				_display_info(true)
{
	this->initialize();

}


/**
 * @brief Constructor of class HydrologicalModel initializing the model with a
 * warmup phase
 * @param cfg Reference to the Config object holding the simulation parameters
 * @param startTime Simulation start date
 * @param endTime Simulation end date
 * @param warmupStartTime Start of the warmup phase
 * @param warmupEndTime End of the warmup phase
 */
HydrologicalModel::HydrologicalModel(const mio::Config& cfg, const mio::Date& startTime,
		const mio::Date& endTime, const mio::Date& warmupStartTime,
		const mio::Date& warmupEndTime, const mio::Date& startData, const mio::Date& endData) : ObservableModel(),
				_cfg(cfg), _clock(cfg, startTime, endTime),
				dataContainer(std::make_shared<DataContainer>(_cfg,startData, endData,_watershed,_network)), _watershed(cfg, _clock,dataContainer),
				_network(cfg, _clock,dataContainer), _warmup_start_date(),
				_warmup_end_date(),	_simulation_start_date(startTime),
				_simulation_end_date(endTime), _simulation_completed(false),
				_display_info(true)
{
	this->initialize();
	this->setWarmupPhase(warmupStartTime, warmupEndTime);
}


/**
 * @brief Copy constructor of the Hydrological model class. The Data container is not copied,
 * a shared pointer to the same instance is created. All the other components are copied.
 * This copy is aimed to be used for parallel calibration.
 # @param model HydrologicalModel class to be copied
 */
HydrologicalModel::HydrologicalModel(const HydrologicalModel& model) : ObservableModel(),
				_cfg(model._cfg), _clock(model._clock),
				dataContainer(model.dataContainer),
				_watershed(model._watershed, _cfg, _clock, dataContainer),_network(_cfg, _clock, dataContainer),
				_warmup_start_date(model._warmup_start_date),
				_warmup_end_date(model._warmup_end_date),	_simulation_start_date(model._simulation_start_date),
				_simulation_end_date(model._simulation_end_date), _simulation_completed(model._simulation_completed),
				_display_info(model._display_info)
{
	_watershed.setStreamNetwork(_network);
	_network.setWatershed(_watershed);
	_clock.updateTimeStep();
	_watershed.initialize();
	_network.initialize();
}

/**
 * @brief Sets the model warmup phase (during which the model is run but no
 * output is produced)
 * @param startTime Start date of the warmup phase
 * @param endTime End date of the warmup phase
 */
void HydrologicalModel::setWarmupPhase(const mio::Date& startTime, const mio::Date& endTime) {
	_warmup_start_date = startTime;
	_warmup_end_date   = endTime;
}


/**
 * @brief Sets whether information should be displayed on the standard output
 * flux regarding the simulation progress. Disabling information display is
 * particularly useful when calibrating the model.
 */
void HydrologicalModel::setDisplayInfo(const bool& value) {
	_display_info = value;
	_watershed.setDisplayInfo(value);
	_network.setDisplayInfo(value);


}


/**
 * @brief Returns the model parameters
 * @return Model parameters
 */
ParameterSet HydrologicalModel::getParams() const {
	ParameterSet params;
	params.add(_watershed.getParams());
	params.add(_network.getParams());
	return params;
}


/**
 * @brief Sets the model parameters. Unknown parameters will be ignored, and
 * not all parameters need to be set at the same time (i.e. the list of
 * parameters passed as function input argument can be incomplete)
 * @param params Model parameters
 */
void HydrologicalModel::setParams(const ParameterSet& params) {
	_watershed.setParams(params);
	_network.setParams(params);
}


/**
 * @brief Returns a reference to the internal Clock instance
 * @return Clock object
 */
Clock& HydrologicalModel::getClock() {
	return _clock;
}


/**
 * @brief Returns a const reference to the internal Clock instance
 * @return Clock object
 */
const Clock& HydrologicalModel::getClock() const {
	return _clock;
}


/**
 * @brief Runs a hydrological simulation. This method evolves the subwatersheds
 * and their associated stream reaches in time, and writes the states of the
 * watershed and the stream network in output files if required.
 */
void HydrologicalModel::run() {
	if(_simulation_completed) return;
	if(!_warmup_start_date.isUndef() && !_warmup_end_date.isUndef()) {
		_clock.reset(_warmup_start_date, _warmup_end_date);
		this->reset();
		this->runWarmupPhase();
	}
	_clock.reset(_simulation_start_date, _simulation_end_date);
	this->runSimulation();
	_simulation_completed = true;
}


/**
 * @brief Method used to reset the hydrological model to its inital state. This
 * method is particularly useful when attempting at calibrating the hydrological
 * model, since it avoid destroying the model and constructing a new one in
 * order to launch a new simulation
 */
void HydrologicalModel::reset() {
	ObservableModel::reset();
	_clock.reset();
	_watershed.reset();
	_network.reset();
	_clock.updateTimeStep();
	_simulation_completed = false;
}


/**
 * @brief Destructor of class HydrologicalModel
 */
HydrologicalModel::~HydrologicalModel() {}


/**
 * @brief Method initializing the hydrological model. This method is preferred
 * here over the constructor delegation procedure (introduced in c++11), since
 * the latter is not supported by all compilers
 */
void HydrologicalModel::initialize() {

	_watershed.setStreamNetwork(_network);

	_network.setWatershed(_watershed);

	_clock.updateTimeStep();

	dataContainer->loadData();

	_watershed.initialize();
	_network.initialize();
}


/**
 * @brief This method runs the model warmup phase
 */
void HydrologicalModel::runWarmupPhase() {

	const clock_t tStart = clock();
	if(_display_info)
		std::cout << "[i] Starting model warmup phase\n";


	const double startJulian(_clock.getStartTime().getJulian());
	const double warmupDuration(_clock.getEndTime().getJulian() - startJulian);
	int oldCompletion(-1), currCompletion(0);

	while(_clock.getCurrTime() < _clock.getEndTime()) {
		const double elapsedTime(_clock.getCurrTime().getJulian() - startJulian);
		currCompletion = int(elapsedTime/warmupDuration*100.);
		_watershed.advanceInTime();
		_network.advanceInTime();
		_clock.advanceInTime(); //< clock is incremented last since it holds the current time
		if(_display_info && currCompletion != oldCompletion) {
			oldCompletion = currCompletion;
			std::cout << "[i] " << std::setw(3) << std::setprecision(0)
			          << currCompletion << "% completed\n";
		}
	}

	if(_display_info) {
		std::cout << "[i] Model warmup phase completed\n";
		std::cout << "[i] Execution time of the warmup phase: "
		          << SFUtils::timeToString(double(clock() - tStart)/CLOCKS_PER_SEC)
		          << std::endl;
	}
}


/**
 * @brief This method runs the model simulation
 */
void HydrologicalModel::runSimulation() {
	const clock_t tStart = clock();

	if(_display_info)
		std::cout << "[i] Starting model simulation\n";

	this->updateObservers(_clock.getCurrTime(), _watershed, _network); //< initial conditions

	const double startJulian(_clock.getStartTime().getJulian());
	const double simulationDuration(_clock.getEndTime().getJulian() - startJulian);
	int oldCompletion(-1), currCompletion(0);

	while(_clock.getCurrTime() < _clock.getEndTime()) {
		const double elapsedTime(_clock.getCurrTime().getJulian() - startJulian);
		currCompletion = int(elapsedTime/simulationDuration*100.);

		_watershed.advanceInTime();
		_network.advanceInTime();
		_clock.advanceInTime(); //< clock is incremented last since it holds the current time

		this->updateObservers(_clock.getCurrTime(), _watershed, _network);

		if(_display_info && currCompletion != oldCompletion) {
			oldCompletion = currCompletion;
			std::cout << "[i] " << std::setw(3) << std::setprecision(0)
			          << currCompletion << "% completed";
			const double elapsedRealTime(double(clock() - tStart)/CLOCKS_PER_SEC);
			if(currCompletion > 0 && elapsedRealTime > 60.) {
				const double remainingTime((100./double(currCompletion) - 1.)*elapsedRealTime);
				std:: cout << " (estimated completion in "
				           << SFUtils::timeToString(remainingTime) << ")";
			}
			std::cout << std::endl;
		}
	}

	if(_display_info) {
		std::cout << "[i] Model simulation completed\n";
		std::cout << "[i] Execution time of the simulation: "
		          << SFUtils::timeToString(double(clock() - tStart)/CLOCKS_PER_SEC)
		          << std::endl;
	}
}

} /* namespace sflow */
