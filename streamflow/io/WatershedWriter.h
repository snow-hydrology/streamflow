/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WATERSHEDWRITER_H_
#define WATERSHEDWRITER_H_

#include <streamflow/Watershed.h>
#include <streamflow/utils/HydroVariable.h>
#include <streamflow/utils/StreamflowExceptions.h>
#include <streamflow/subwatershed/Subwatershed.h>
#include <meteoio/MeteoIO.h>

namespace sflow {

/**
 * @class WatershedWriter
 * @brief This class is used by class Watershed to write its current state
 * in output file(s).
 * @ingroup io
 * @author Aurelien Gallice
 * @date 2015-09-11
 */
class WatershedWriter {
  public:
    WatershedWriter(const mio::Config& cfg, const Watershed& watershed);
    virtual void writeState(const mio::Date& currTime,
        const Watershed& watershed) const = 0;
    virtual ~WatershedWriter();

  protected:
    const std::vector<size_t> _output_subwatershed_ids;
    const std::vector<const SubwatershedVariable*> _output_variables;

    static std::vector<size_t> getOutputSubwatershedIds(const mio::Config& cfg,
        const Watershed& watershed);
    static std::vector<const SubwatershedVariable*> getOutputVariables(const mio::Config& cfg);
};


/**
 * @class LumpedWatershedSMETWriter
 * @brief This class implements the interface WatershedWriter for
 * LumpedWatershed objects. It writes the current state of a LumpedWatershed
 * object in SMET files, one for each of the subwatersheds composing the
 * watershed. The files are named subwatershed[i].smet, where [i] denotes
 * the ID number of the subwatershed.
 * @author Aurelien Gallice
 * @date 2015-09-11
 */
class LumpedWatershedSMETWriter : public WatershedWriter {
  public:
    LumpedWatershedSMETWriter(const mio::Config& cfg, const Watershed& watershed);
    virtual void writeState(const mio::Date& currTime,
        const Watershed& watershed) const;
    virtual ~LumpedWatershedSMETWriter();

  protected:
    const std::string _output_path;

    static const std::string _FILE_NAME_PREFIX;
    static const std::string _FILE_NAME_EXTENSION;

    virtual void initializeFile(const Subwatershed& sd, const double& timeZone) const;
    virtual void updateFile(const mio::Date& currTime, const Subwatershed& sd) const;
    virtual std::string getFileName(const Subwatershed& sd) const;

    static std::string getOutputPath(const mio::Config& cfg);
};

} /* namespace sflow */

#endif /* WATERSHEDWRITER_H_ */
