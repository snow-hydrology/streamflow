
/*****************************************************************************/
/* Copyright 2017 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/io/DataContainer.h>
#include <streamflow/Watershed.h>
#include <streamflow/StreamNetwork.h>
//#include <streamflow/subwatershed/Subwatershed.h>
//#include <streamflow/streamReach/StreamReach.h>
#include <streamflow/io/TSGrids.h>
#include <streamflow/io/TSReaches.h>
#include <streamflow/io/TSWatersheds.h>

#ifdef _OPENMP
  #include <omp.h>
#endif

//#include "TSContainer.h"


namespace sflow {

/**
 * @brief Destructor of class DataContainer
 */

DataContainer::~DataContainer() {}

/**
 * @brief Constructor of class DataContainer
 * @param cfg Reference to the Config instance holding the model parameters
 * @param clk Simulation clock object
 * @param wshed Watershed object (not necessarily initialized yet)
 * @param snetwork StreamNetwork object (not necessarily initialized yet)
 */

DataContainer::DataContainer(const mio::Config& cfg_imp, const mio::Date _startData, const mio::Date _endData, const Watershed& wshed, const StreamNetwork& snetwork  ) :
    cfg(cfg_imp), startData(_startData), endData(_endData),  watershed(wshed), streamnetwork(snetwork), io_container(cfg_imp, startData, endData, wshed, snetwork),
    loadInstructions(),ts_grids(nullptr), ts_watersheds(nullptr), ts_reaches(nullptr), masks(), dem_grid(), wshed_continuous_id(), wshed_stream_cells()
{
  io_container.io_grid_manager->readDEM(dem_grid);
}

/**
 * @brief Add a variable to the list of variables to be loaded.
 * @param var Variable to be loaded, nammed as in mio::MeteoGrids::Parameters
 * @param type of data required, nammed as in streamflow::IOContainer::DataType
 */

void DataContainer::addLoadInstruction(Variables var, DataType type){

  if(loadInstructions[var].find(type)==loadInstructions[var].end()){
    loadInstructions[var].insert(type);
  }
}


void DataContainer::printInstructions(){

  for (auto const& var : loadInstructions)
  {
    std::cout << "For variable: " << mio::MeteoGrids::getParameterName(var.first)
          << " the required arguments are: "<< std::endl;
    for(auto const& type : var.second) {
      std::cout << "\t" << type << std::endl;
    }
  }
}

/**
 * @brief Determines the data availability and the fastest way to load it depending on the availability and on the required type.
 * Must be called only afet watershed and streamNetwork object given as reference to the constructor have been initialized.
 */

void DataContainer::loadData(){


  std::cout << "[i] Loading and resizing masks" << std::endl;
  prepareData();
  loadMasks();

  std::cout << "[i] Loading data" << std::endl;
  this->ts_grids = std::unique_ptr<TSGrids>(new TSGrids(cfg, io_container, startData, endData, masks, wshed_continuous_id, wshed_stream_cells));
  this->ts_reaches = std::unique_ptr<TSReaches>(new TSReaches(cfg, io_container, startData, endData, masks, wshed_continuous_id, wshed_stream_cells,*ts_grids));
  this->ts_watersheds = std::unique_ptr<TSWatersheds>(new TSWatersheds(cfg, io_container, startData, endData, masks, wshed_continuous_id, wshed_stream_cells,*ts_grids));

  parseLoadInstructions();

  for (auto const& var : loadInstructions){

    ts_grids->prepareMemory(var.first);
    ts_grids->loadData(var.first);

    ts_watersheds->prepareMemory(var.first);
    ts_watersheds->loadData(var.first);

    ts_reaches->prepareMemory(var.first);
    ts_reaches->loadData(var.first);

    ts_grids->clearTmpData(var.first);
  }

}




void DataContainer::prepareData(){
  size_t id = 0;
  for(const auto i : watershed.getSubwatershedIndices()){
    masks.insert(std::make_pair(i, mio::Grid2DObject()));
    wshed_continuous_id.insert(std::make_pair(i,id));
    for(StreamCell cell : streamnetwork.getReach(i).getCells()){
      mio::Coords coord(cell.getCoords());
      wshed_stream_cells[i].push_back(std::make_pair(coord.getGridI(), coord.getGridJ()));
    }
    ++id;
  }
}

void DataContainer::loadMasks(){
  size_t size = watershed.getSubwatershedIndices().size();
  #ifdef _OPENMP
  #pragma omp parallel for
  #endif
  for(size_t id = 0; id< size; id++){
    size_t i = watershed.getSubwatershedIndices()[id];
    masks[i] = GridOperations::crop2(dem_grid,watershed.getSubwatershed(i).getMask());
  }
}


void DataContainer::parseLoadInstructions(){
  for (auto const& var : loadInstructions){
    for(auto const& type : var.second) {
      if(type == DataType::GRID_POINTS){
          if(io_container.isAvailable(var.first, DataType::GRID_POINTS)){
            std::cout << "[i] GRID for var " << mio::MeteoGrids::getParameterName(var.first) << " will be loaded from ASC file" << std::endl;
            ts_grids->addData(var.first, type, DataSource::GRID);
          }
          else{
            std::ostringstream out;
            out << "Cannot load var " << mio::MeteoGrids::getParameterName(var.first) <<  " from grids as requested. Terminating now";
            throw std::runtime_error(out.str());
          }
      }
      else if(type == DataType::NETWORK_POINTS){
        if(io_container.isAvailable(var.first,type))
        {
          std::cout << "[i] MAP for var " << mio::MeteoGrids::getParameterName(var.first) << " will be loaded from SMET" << std::endl;
          ts_reaches->addData(var.first, type, DataSource::SMET);
        }
        else if(var.second.find(GRID_POINTS) != var.second.end())
        {
          std::cout << "[i] MAP for var " << mio::MeteoGrids::getParameterName(var.first) << " will be computed from GRIDS" << std::endl;
          ts_reaches->addData(var.first,type, DataSource::GRID);
        }
        else if(io_container.isAvailable(var.first,DataType::GRID_POINTS))
        {
          std::cout << "[i] MAP for var " << mio::MeteoGrids::getParameterName(var.first) << " will be computed from TMP GRIDS" << std::endl;
          ts_grids->addTmpData(var.first, DataType::GRID_POINTS, DataSource::GRID);
          ts_reaches->addData(var.first,type, DataSource::GRID);
        }
        else
        {
          std::ostringstream out;
          out << "Cannot load var " << mio::MeteoGrids::getParameterName(var.first) <<  " as requested. Terminating now";
          throw std::runtime_error(out.str());
        }
      }
      else{

        if(io_container.isAvailable(var.first,type))
        {
          std::cout << "[i] VECT for var " << mio::MeteoGrids::getParameterName(var.first) << " will be loaded from SMET" << std::endl;
          ts_watersheds->addData(var.first, type, DataSource::SMET);
        }
        else if(var.second.find(GRID_POINTS) != var.second.end())
        {
          std::cout << "[i] VECT for var " << mio::MeteoGrids::getParameterName(var.first) << " will be computed from GRIDS" << std::endl;
          ts_watersheds->addData(var.first,type, DataSource::GRID);
        }
        else if(io_container.isAvailable(var.first,DataType::GRID_POINTS))
        {
          std::cout << "[i] VECT for var " << mio::MeteoGrids::getParameterName(var.first) << " will be computed from TMP GRIDS" << std::endl;
          ts_grids->addTmpData(var.first, DataType::GRID_POINTS, DataSource::GRID);
          ts_watersheds->addData(var.first,type,DataSource::GRID);
        }
        else{
          std::ostringstream out;
          out << "Cannot load var " << mio::MeteoGrids::getParameterName(var.first) <<  " as requested. Terminating now";
          throw std::runtime_error(out.str());
        }
      }
    }
  }
}



/**
 * @brief Returns the value of the requested variable, averaged over the
 * specified subwatershed at the given date
 * @param date Date at which the variable value should be returned
 * @param variable Name of the variable to be returned. Subwatershed infiltration
 * rate is called "ROT", soil temperature "TSOIL", and all other meteorological
 * variables are named as in class mio::MeteoGrids::Parameters (e.g. "TA" for air temperature,
 * "RH" for relative humidity, etc.)
 * @param subw Subwatershed over which mean data should be returned
 * @return Mean value of the requested variable over the specified subwatershed
 */

double DataContainer::getLumpedMeteoData(const mio::Date& date,
    const std::string& variable, const Subwatershed& subw)
{
  return ts_watersheds->getMeanVal(date.getJulian(), subw.getIndex(), static_cast<Variables>(mio::MeteoGrids::getParameterIndex(variable)));
}

/**
 * @brief Returns the value of the requested variable, averaged over the
 * specified stream reach at the given date
 * @param date Date at which the variable value should be returned
 * @param variable Name of the variable to be returned. Subwatershed infiltration
 * rate is called "ROT", soil temperature "TSOIL", and all other meteorological
 * variables are named as in class mio::MeteoGrids::Parameters (e.g. "TA" for air temperature,
 * "RH" for relative humidity, etc.)
 * @param subw Subwatershed over which mean data should be returned
 * @return Mean value of the requested variable over the specified subwatershed
 */

double DataContainer::getLumpedMeteoData(const mio::Date& date,
    const std::string& variable, const StreamReach& reach)
{
  return ts_reaches->getMeanVal(date.getJulian(), reach.getSubwatershed().getIndex(), static_cast<Variables>(mio::MeteoGrids::getParameterIndex(variable)));
}

/**
 * @brief Returns the value of the standard meteo variables, averaged over the
 * specified subwatershed at the given date
 * @param[in] date Date at which the variable value should be returned
 * @param[in] subw Subwatershed over which mean data should be returned
 * @param[in,out] data Variable in which the averaged meteo values are returned.
 * All the variables loaded as WATERSHED_AVG or WATERSHED_SUM are returned.
 */

void DataContainer::getLumpedMeteoData(const mio::Date& date, const Subwatershed& subw,
    mio::MeteoData& data)
{
  ts_watersheds->getMeanMeteo(date.getJulian(), subw.getIndex(), data);
}

/**
 * @brief Returns the value of the standard meteo variables, averaged over the
 * specified stream reach at the given date
 * @param[in] date Date at which the variable value should be returned
 * @param[in] reach Stream reach over which mean meteo data should be returned
 * @param[in,out] data Variable in which the averaged meteo values are returned.
 * All the variables loaded as NETWORK_POINTS are returned.
 */
void DataContainer::getLumpedMeteoData(const mio::Date& date,
    const StreamReach& reach, mio::MeteoData& data)
{
  ts_reaches->getMeanMeteo(date.getJulian(), reach.getSubwatershed().getIndex(), data);
}


/**
 * @brief Returns a grid containing the values of the specified variable over
 * the given subwatershed.
 * @param date Date at which the grid should be returned
 * @param variable Name of the variable to be returned. Subwatershed infiltration
 * rate is called "ROT", soil temperature "TSOIL", and all other meteorological
 * variables are named as in class mio::MeteoGrids::Parameters (e.g. "TA" for air temperature,
 * "RH" for relative humidity, etc.)
 * @param subw Subwatershed over which mean data should be returned
 * @return Grid of the requested variable over the specified subwatershed
 */

mio::Grid2DObject DataContainer::getMeteoData(const mio::Date& date,
    const std::string& variable, const Subwatershed& subw)
{
  //NOT IMPLEMENTED YET
  (void)date;
  (void)variable;
  (void)subw;
  mio::Grid2DObject grid;
  throw NotImplementedException("getMeteoData for grids "
      "has not been implemented yet!", AT);
  //return grid;
}

/**
 * @brief Returns the values of the specified variable over the cells of the
 * given stream reach.
 * @param date Date at which the variable values should be returned
 * @param variable Name of the variable to be returned. Subwatershed infiltration
 * rate is called "ROT", soil temperature "TSOIL", and all other meteorological
 * variables are named as in class mio::MeteoGrids::Parameters (e.g. "TA" for air temperature,
 * "RH" for relative humidity, etc.)
 * @param reach Stream reach over which data should be returned
 * @return Values of the requested variable over the cells of the specified
 * stream reach
 */

std::vector<double> DataContainer::getMeteoData(const mio::Date& date,
    const std::string& variable, const StreamReach& reach)
{
  return ts_reaches->getVectVal(date.getJulian(), reach.getSubwatershed().getIndex(), static_cast<Variables>(mio::MeteoGrids::getParameterIndex(variable)));

}


/**
 * @brief TODO
 * @param[in] reach Stream reach over which data should be returned
 * @param[in,out] data vector of mio::MeteoData objects in which the meteo values
 * are returned. All the variables loaded as NETWORK_POINTS are returned.
 */

void DataContainer::initializeMeteo(const StreamReach& reach, std::vector<mio::MeteoData>& data)
{
  ts_reaches->initializeMeteo(reach.getSubwatershed().getIndex(), data);
}


/**
 * @brief Returns the values of standard meteorological variables over the
 * cells of the given stream reach.
 * @param[in] date Date at which the variable values should be returned
 * @param[in] reach Stream reach over which data should be returned
 * @param[in,out] data vector of mio::MeteoData objects in which the meteo values
 * are returned. All the variables loaded as NETWORK_POINTS are returned.
 */

void DataContainer::getMeteoData(const mio::Date& date, const StreamReach& reach,
    std::vector<mio::MeteoData>& data)
{
  ts_reaches->getMeteo(date.getJulian(), reach.getSubwatershed().getIndex(), data);
}


/**
 * @brief Returns the time averaged value of the requested variable over the
 * specified subwatershed as a grid.
 * @param startDate Beginning of the time period over which the variable should
 * be averaged
 * @param endDate End of the time period over which the variable should be averaged
 * @param variable Name of the variable to be returned. Subwatershed infiltration
 * rate is called "ROT", soil temperature "TSOIL", and all other meteorological
 * variables are named as in class mio::MeteoGrids::Parameters (e.g. "TA" for air temperature,
 * "RH" for relative humidity, etc.)
 * @param subw Subwatershed over which data should be returned
 * @return Grid of the requested time-averaged variable over the specified
 * subwatershed
 */

mio::Grid2DObject DataContainer::getTimeAveragedMeteoData(const mio::Date& startDate,
    const mio::Date& endDate, const std::string& variable,
    const Subwatershed& subw)
{
  //NOT IMPLEMENTED YET
  //NOT REQUIRED
  (void)startDate;
  (void)endDate;
  (void)variable;
  (void)subw;
  mio::Grid2DObject grid;
  throw NotImplementedException("getTimeAveragedMeteoData for grids "
      "has not been implemented yet!", AT);
  //return grid;
}

/**
 * @brief Returns the value of the requested variable, averaged in space over the
 * specified subwatershed and in time over the given period
 * @param startDate Beginning of the time period over which the variable should
 * be averaged
 * @param endDate End of the time period over which the variable should be averaged
 * @param variable Name of the variable to be returned. Subwatershed infiltration
 * rate is called "ROT", soil temperature "TSOIL", and all other meteorological
 * variables are named as in class mio::MeteoGrids::Parameters (e.g. "TA" for air temperature,
 * "RH" for relative humidity, etc.)
 * @param subw Subwatershed over which mean data should be returned
 * @return Time averaged value of the requested variable over the specified
 * subwatershed
 */

double DataContainer::getLumpedTimeAveragedMeteoData(const mio::Date& startDate,
    const mio::Date& endDate, const std::string& variable,
    const Subwatershed& subw)
{
  return ts_watersheds->getTimeAveragedMeanVal(startDate.getJulian(), endDate.getJulian(), subw.getIndex(), static_cast<Variables>(mio::MeteoGrids::getParameterIndex(variable)));
}


//// TO BE REMOVED //////
void DataContainer::getVal(const mio::Date& date, Variables var, size_t nx, size_t ny){
  ts_grids->getVal(date.getJulian(),nx,ny,var);
}

double DataContainer::getAvgVal(const mio::Date& date, Variables var, size_t subw_ind){
  return ts_watersheds->getMeanVal(date.getJulian(),subw_ind, var);
}

double DataContainer::getNetworkVal(const mio::Date& date, Variables var, size_t subw_ind){
  return ts_reaches->getMeanVal(date.getJulian(),subw_ind, var);
}

}
