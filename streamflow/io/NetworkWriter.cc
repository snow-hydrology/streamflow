/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/io/NetworkWriter.h>

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class NetworkWriter
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class NetworkWriter
 * @param cfg Reference to the Config object holding the simulation parameters
 * @param network Reference to the StreamNetwork instance whose state should be
 * written using the current object
 */
NetworkWriter::NetworkWriter(const mio::Config& cfg, const StreamNetwork& network) :
    _output_reach_ids(getOutputReachIds(cfg, network)),
    _output_variables(getOutputVariables(cfg)) {}


/**
 * @brief Destructor of class NetworkWriter
 */
NetworkWriter::~NetworkWriter() {}


/**
 * @brief Returns the indices of the stream reaches whose state variables should
 * be written in the output files.
 * @param cfg Reference to the Config object holding the simulation parameters
 * @param network Reference to the StreamNetwork instance whose state should be
 * written using the current object
 * @return Indices of the stream reaches contained in the stream network and
 * whose state variables should be written out
 */
std::vector<size_t> NetworkWriter::getOutputReachIds(const mio::Config& cfg,
    const StreamNetwork& network) {
  std::string indices;
  cfg.getValue("STREAM::INDICES", "OUTPUT", indices, mio::IOUtils::nothrow);

  std::vector<size_t> output;
  if(indices.empty() || mio::IOUtils::strToLower(indices) == "all") {
    output = network.getReachIndices();
  } else {
    std::vector<double> tmp;
    mio::IOUtils::readLineToVec(indices, tmp);
    output.reserve(tmp.size());
    for(const double& index : tmp) {
      if(!network.containsReach(size_t(index))) {
        std::ostringstream os;
        os << "Index " << size_t(index) << " does not exist."
           << "Please modify the value of key STREAM::INDICES in section "
           << "[OUTPUT] of the config file";
        throw InvalidConfigKeyException(os.str(), AT);
      }
      output.push_back(size_t(index));
    }
  }

  return output;
}


/**
 * @brief Returns the variables which should be written in the output file(s)
 * @param cfg Reference to the Config object holding the simulation parameters
 * @return Variables to write in the output file(s)
 */
std::vector<const StreamVariable*> NetworkWriter::getOutputVariables(const mio::Config& cfg) {
  std::vector<std::string> variables;
  cfg.getValue("STREAM::VARIABLES", "OUTPUT", variables, mio::IOUtils::nothrow);

  std::vector<const StreamVariable*> output;
  if(variables.empty()) {
    output = {&StreamVariable::H, &StreamVariable::Q, &StreamVariable::T};
  } else {
    output.reserve(variables.size());
    for(const std::string& name : variables)
      output.push_back(&StreamVariable::getVariable(name));
  }

  return output;
}


//-----------------------------------------------------------------------------
// Implementation of class LumpedNetworkSMETWriter
//-----------------------------------------------------------------------------

const std::string LumpedNetworkSMETWriter::_FILE_NAME_PREFIX("reach");
const std::string LumpedNetworkSMETWriter::_FILE_NAME_EXTENSION(".smet");

/**
 * @brief Constructor of class LumpedNetworkSMETWriter
 * @param cfg Reference to the Config object holding the simulation parameters
 * @param network Reference to the StreamNetwork instance whose state
 * should be written using the current object
 */
LumpedNetworkSMETWriter::LumpedNetworkSMETWriter(const mio::Config& cfg,
    const StreamNetwork& network) : NetworkWriter(cfg, network),
        _output_path(getOutputPath(cfg))
{
  const double timeZone = cfg.get("TIME_ZONE", "OUTPUT");
  for(const size_t& reachId : _output_reach_ids)
    this->initializeFile(network.getReach(reachId), timeZone);
}


/**
 * @brief Writes the current state (water depth, discharge and temperature) of
 * each stream reach belonging to the network into its corresponding SMET file
 * @param currTime Current time
 * @param network Reference to the StreamNetwork object containing the stream
 * reaches whose state is to be written out in the SMET files
 */
void LumpedNetworkSMETWriter::writeState(const mio::Date& currTime,
    const StreamNetwork& network) const {
  if(network.getDiscretization() != DiscretizedObject::Discretization::Lumped)
    throw mio::InvalidArgumentException("Class LumpedNetworkSMETWriter can "
        "only be used to write the state of lumped StreamNetwork objects", AT);

  for(const size_t& reachId : _output_reach_ids)
    this->updateFile(currTime, network.getReach(reachId));
}


/**
 * @brief Destructor of class LumpedNetworkSMETWriter
 */
LumpedNetworkSMETWriter::~LumpedNetworkSMETWriter() {}


/**
 * @brief This method writes the header of the output SMET file associated with
 * the stream reach passed as input argument
 * @param reach Stream reach whose associated output file should be initialized
 * @param timeZone Time zone of the time values written in the output file
 */
void LumpedNetworkSMETWriter::initializeFile(const StreamReach& reach,
    const double& timeZone) {
  const std::string filename(getFileName(reach));
  std::ofstream smetOut(filename);
  if(smetOut.fail())
    throw mio::AccessException(filename.c_str(), AT);

  smetOut << "SMET 1.1 ASCII" << std::endl;
  smetOut << "[HEADER]" << std::endl;
  smetOut << "station_id   = "              << reach.getIndex() << std::endl;
  smetOut << "station_name = stream_reach_" << reach.getIndex() << std::endl;
  smetOut << std::left << std::fixed << std::setprecision(6);
  smetOut << "latitude     =";
  for(const StreamCell& cell : reach) smetOut << " " << std::setw(11) << cell.getLat();
  smetOut << std::endl << "longitude    =";
  for(const StreamCell& cell : reach) smetOut << " " << std::setw(11) << cell.getLon();
  smetOut << std::endl << std::fixed << std::setprecision(1);
  const short int epsg(reach.getCell(0).coords.getEPSG());
  if(epsg != -1) {
    smetOut << "epsg         = " << epsg << std::endl;
    smetOut << "easting      =";
    for(const StreamCell& cell : reach) smetOut << " " << std::setw(10) << cell.getEasting();
    smetOut << std::endl << "northing     =";
    for(const StreamCell& cell : reach) smetOut << " " << std::setw(10) << cell.getNorthing();
    smetOut << std::endl;
  }
  smetOut << "altitude     =";
  for(const StreamCell& cell : reach) smetOut << " " << std::setw(7) << cell.getAltitude();
  smetOut << std::endl << "length       = " << reach.getLength()  << std::endl;
  smetOut << "comment      = the above coordinates correspond to those of "
          << "the points defining the stream reach" << std::endl;
  smetOut << "comment      = reach length is in m" << std::endl;
  smetOut << "nodata       = " << mio::IOUtils::nodata << std::endl;
  smetOut << "tz           = " << timeZone << std::endl;
  smetOut << "fields       = timestamp width";
  for(const StreamVariable *var : _output_variables) {
    smetOut << " " << mio::IOUtils::strToLower(var->toString());
  }
  smetOut << std::endl << "units        = none m";
  for(const StreamVariable *var : _output_variables) {
    smetOut << " " << var->getUnits();
  }
  smetOut << std::endl << "[DATA]" << std::endl;

  smetOut.close();
}


/**
 * @brief This method writes the selected reach variables in their associated
 * output SMET file
 * @param currTime Current time
 * @param reach Stream reach of interest
 */
void LumpedNetworkSMETWriter::updateFile(const mio::Date& currTime, const StreamReach& reach) const {
  const std::string filename(getFileName(reach));
  std::ofstream smetOut(filename, std::ofstream::app);
  if(smetOut.fail())
    throw mio::AccessException(filename.c_str(), AT);

  //We can safely use static_cast since the StreamNetwork instance has
  //already been checked to contain lumped Subwatersheds
  const LumpedReachInterface& lumpedReach(static_cast<const LumpedReachInterface&>(reach));
  smetOut.fill(' ');
  smetOut << std::right << std::fixed;
  smetOut << currTime.toString(mio::Date::ISO) << " ";
  smetOut << std::setprecision(3);
  smetOut << std::setw(10) << lumpedReach.getWidth();
  for(const StreamVariable *var : _output_variables) {
    smetOut << " " << std::setw(10) << lumpedReach.get(*var);
  }
  smetOut << "\n";
  smetOut.close();
}


/**
 * @brief Returns the name of the output SMET file associated with the given
 * stream reach
 * @param reach Stream reach of interest
 * @return Name of the output file associated with the stream reach of interest
 */
std::string LumpedNetworkSMETWriter::getFileName(const StreamReach& reach) const {
  std::ostringstream os;
  os << _output_path << "/" << _FILE_NAME_PREFIX << reach.getIndex()
     << _FILE_NAME_EXTENSION;
  return os.str();
}


/**
 * @brief Returns the path to the folder in which the output files should be
 * written
 * @param cfg Reference to the Config object holding the simulation parameters
 * @return Folder in which the output files should be written
 */
std::string LumpedNetworkSMETWriter::getOutputPath(const mio::Config& cfg) {
  std::string path;
  cfg.getValue("STREAM::PATH", "OUTPUT", path, mio::IOUtils::nothrow);
  if(path.empty()) {
    cfg.getValue("WATERSHED::PATH", "OUTPUT", path, mio::IOUtils::nothrow);
    if(path.empty())
      throw MissingConfigKeyException("At least one of the two keys STREAM::PATH "
          "or WATERSHED::PATH has to be specified in section [OUTPUT] of the "
          "configuration file", AT);
  }
  return path;
}


//-----------------------------------------------------------------------------
// Implementation of class DiscretizedNetworkSMETWriter
//-----------------------------------------------------------------------------

const std::string DiscretizedNetworkSMETWriter::_FILE_NAME_PREFIX("reach");
const std::string DiscretizedNetworkSMETWriter::_FILE_NAME_EXTENSION(".smet");


/**
 * @brief Constructor of class DiscretizedNetworkSMETWriter
 * @param cfg Reference to the Config object holding the simulation parameters
 * @param network Reference to the StreamNetwork instance whose state
 * should be written using the current object
 */
DiscretizedNetworkSMETWriter::DiscretizedNetworkSMETWriter(const mio::Config& cfg,
    const StreamNetwork& network) : NetworkWriter(cfg, network),
        _output_path(getOutputPath(cfg))
{
  const double timeZone = cfg.get("TIME_ZONE", "OUTPUT");
  for(const size_t& reachId : _output_reach_ids)
    this->initializeFiles(network.getReach(reachId), timeZone);
}


/**
 * @brief Writes the current state (water depth, discharge and temperature) of
 * each stream reach belonging to the network into its corresponding SMET files
 * (one for each hydrological variable)
 * @param currTime Current time
 * @param network Reference to the StreamNetwork object containing the stream
 * reaches whose state is to be written out in the SMET files
 */
void DiscretizedNetworkSMETWriter::writeState(const mio::Date& currTime,
    const StreamNetwork& network) const {
  if(network.getDiscretization() != DiscretizedObject::Discretization::Distributed)
    throw RuntimeException("Class DiscretizedNetworkSMETWriter can only be"
        " used to write the state of discretized StreamNetwork objects", AT);

  for(const size_t& reachId : _output_reach_ids)
    this->updateFiles(currTime, network.getReach(reachId));
}


/**
 * @brief Destructor of class DiscretizedNetworkSMETWriter
 */
DiscretizedNetworkSMETWriter::~DiscretizedNetworkSMETWriter() {}


/**
 * @brief This method writes the headers of the output SMET files associated
 * with each hydrological variable of the stream reach passed as input argument
 * @param reach Stream reach whose associated output files should be initialized
 * @param timeZone Time zone of the time values written in the output files
 */
void DiscretizedNetworkSMETWriter::initializeFiles(const StreamReach& reach,
    const double& timeZone) {
  std::string fileName, varName;
  for(const StreamVariable *var : _output_variables) {
    varName  = mio::IOUtils::strToLower(var->toString());
    fileName = this->getFileName(reach.getIndex(), varName);
    std::ofstream smetOut(fileName);
    if(smetOut.fail())
      throw mio::AccessException(fileName, AT);

    smetOut << "SMET 1.1 ASCII" << std::endl;
    smetOut << "[HEADER]" << std::endl;
    smetOut << "station_id   = "              << reach.getIndex() << std::endl;
    smetOut << "station_name = stream_reach_" << reach.getIndex() << std::endl;
    smetOut << std::left << std::fixed << std::setprecision(6);
    smetOut << "latitude     =";
    for(const StreamCell& cell : reach) smetOut << " " << std::setw(11) << cell.getLat();
    smetOut << std::endl << "longitude    =";
    for(const StreamCell& cell : reach) smetOut << " " << std::setw(11) << cell.getLon();
    smetOut << std::endl << std::fixed << std::setprecision(1);
    const short int epsg(reach.getCell(0).coords.getEPSG());
    if(epsg != -1) {
      smetOut << "epsg         = " << epsg << std::endl;
      smetOut << "easting      =";
      for(const StreamCell& cell : reach) {
        smetOut << " " << std::setw(10) << cell.getEasting();
      }
      smetOut << std::endl << "northing     =";
      for(const StreamCell& cell : reach) {
        smetOut << " " << std::setw(10) << cell.getNorthing();
      }
      smetOut << std::endl;
    }
    smetOut << "altitude     =";
    for(const StreamCell& cell : reach) smetOut << " " << std::setw(7) << cell.getAltitude();
    smetOut << std::endl << "length       = " << reach.getLength()  << std::endl;
    smetOut << "comment      = the above coordinates correspond to those of "
            << "the points defining the stream reach" << std::endl;
    smetOut << "comment      = reach length and reach width are in m, "
            << varName << " is in " << var->getUnits() << std::endl;
    smetOut << "nodata       = " << mio::IOUtils::nodata << std::endl;
    smetOut << "tz           = " << timeZone << std::endl;
    smetOut << "fields       = timestamp width";
    for(size_t iCell = 0; iCell < reach.getNumCells(); ++iCell)
      smetOut << " " << varName << "_" << std::setw(1) << iCell;
    smetOut << std::endl << "[DATA]" << std::endl;

    smetOut.close();
  }
}


/**
 * @brief This method writes the current water depth, stream discharge and
 * stream temperature of the given stream reach in their associated output
 * SMET files
 * @param currTime Current time
 * @param reach Stream reach of interest
 */
void DiscretizedNetworkSMETWriter::updateFiles(const mio::Date& currTime, const StreamReach& reach) const {
  std::string fileName;
  for(const StreamVariable *var : _output_variables) {
    fileName = this->getFileName(reach.getIndex(),
        mio::IOUtils::strToLower(var->toString()));
    std::ofstream smetOut(fileName, std::ofstream::app);
    if(smetOut.fail())
      throw mio::AccessException(fileName, AT);

    //We can safely use static_cast since the StreamNetwork instance has
    //already been checked to contain discretized reaches
    const std::vector<double>& data(static_cast<const DiscretizedReachInterface&>(reach).get(*var));
    smetOut.fill(' ');
    smetOut << std::right << std::fixed << std::setprecision(3);
    smetOut << currTime.toString(mio::Date::ISO);
    smetOut << " " << std::setw(10) << reach.getWidth();
    for(const double& value : data)
      smetOut << " " << std::setw(10) << value;

    smetOut << "\n";
    smetOut.close();
  }
}


/**
 * @brief Returns the name of the output SMET file associated with the given
 * stream reach and hydrological variable
 * @param reachId ID number of the stream reach of interest
 * @param var Name of the hydrological variable of interest
 * @return Name of the output file associated with the stream reach and
 * variable of interest
 */
std::string DiscretizedNetworkSMETWriter::getFileName(const size_t& reachId,
    const std::string& var) const {
  std::ostringstream os;
  os << _output_path << "/" << _FILE_NAME_PREFIX << reachId
     << "_" << var << _FILE_NAME_EXTENSION;
  return os.str();
}


/**
 * @brief Returns the path to the folder in which the output files should be
 * written
 * @param cfg Reference to the Config object holding the simulation parameters
 * @return Folder in which the output files should be written
 */
std::string DiscretizedNetworkSMETWriter::getOutputPath(const mio::Config& cfg) {
  std::string path;
  cfg.getValue("STREAM::PATH", "OUTPUT", path, mio::IOUtils::nothrow);
  if(path.empty()) {
    cfg.getValue("WATERSHED::PATH", "OUTPUT", path, mio::IOUtils::nothrow);
    if(path.empty())
      throw MissingConfigKeyException("At least one of the two keys STREAM::PATH "
          "or WATERSHED::PATH has to be specified in section [OUTPUT] of the "
          "configuration file", AT);
  }
  return path;
}

} /* namespace sflow */
