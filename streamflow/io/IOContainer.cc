/*****************************************************************************/
/* Copyright 2017 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */
#include <streamflow/io/IOContainer.h>
#include <streamflow/Watershed.h>
#include <streamflow/StreamNetwork.h>
#include <streamflow/StreamflowConfig.h>
#include <streamflow/utils/StreamflowExceptions.h>

#include <meteoio/IOExceptions.h>      // for AT

#include <exception>                   // for exception
#include <stdexcept>                   // for runtime_error

namespace sflow {

/************************************************************
 * static section                                           *
 ************************************************************/
//std::vector<std::string> IOContainer::typenames;

IOContainer::IOContainer(const mio::Config& cfg_imp, const mio::Date _startData, const mio::Date _endData, const Watershed& wshed, const StreamNetwork& snetwork) :
      cfg(cfg_imp), startData(_startData.getJulian()), endData(_endData.getJulian()), time_zone(cfg.get("TIME_ZONE", "INPUT")),
      watershed(wshed), streamnetwork(snetwork), available_data(), available_dates(),
      io_grid_manager(std::unique_ptr<mio::IOManager>(new mio::IOManager(cfg_imp))), io_watershed_manager(), io_network_manager(), io_stations_manager(std::unique_ptr<mio::IOManager> (new mio::IOManager(cfg_imp)))


{

  //InitializeTypeNames();
  //for(size_t i=0; i< mio::MeteoGrids::nrOfParameters; i++)
  //{
  //  VariablesNames.push_back(mio::MeteoGrids::getParameterName(i));
  //}
}
/*
void IOContainer::InitializeTypeNames()
{
  typenames.push_back("GRID_POINTS");
  typenames.push_back("NETWORK_POINTS");
  typenames.push_back("NETWORK_AVG");
  typenames.push_back("WATERSHED_AVG");
  typenames.push_back("WATERSHED_SUM");
}

const std::string& IOContainer::getTypesName(const size_t& parindex){
  if (parindex >= DataType::D_SIZE){
    throw InvalidIndexException("Trying to get name for type that does not exist", AT);
  }
  return typenames[parindex];
}
*/
bool IOContainer::isAvailable(Variables var, DataType type)
{
  if(available_data.find(var) != available_data.end()){
    if(available_data[var].find(type) != available_data[var].end()){
        return available_data[var][type];
    }
  }
  if(type == GRID_POINTS){
     available_data[var][type] = findGrids(var);
  }
  else if(type == WATERSHED_AVG || type == WATERSHED_SUM)
  {
    findWatershedData();
  }
  else{
    findStreamReachData();
  }

  return available_data[var][type];
}

void IOContainer::setStreamReachFalse(){
  for(size_t iParam(0); iParam < mio::MeteoGrids::nrOfParameters; ++iParam) {
    std::string name = mio::MeteoGrids::getParameterName (iParam);
    available_data[static_cast<Variables>(iParam)][NETWORK_POINTS]=false;
    available_data[static_cast<Variables>(iParam)][NETWORK_AVG]=false;
  }
}

bool IOContainer::findStreamReachData(){

  std::cout << "[i] Checking available SMET files for streamreaches" << std::endl;

  std::set<std::string> cell_ind;
  for(size_t ind : watershed.getSubwatershedIndices()){
    std::vector<StreamCell> cells(streamnetwork.getReach(ind).getCells());
    for(StreamCell cell : cells){
      mio::Coords coord(cell.getCoords());
      std::stringstream ss;
      ss << coord.getGridI() << "_" << coord.getGridJ();
      cell_ind.insert(ss.str());
    }
  }

  mio::Config tmp_cfg(cfg);

  if(!tmp_cfg.keyExists("METEOPATH", "INPUT")){
    setStreamReachFalse();
    std::cout << "[i] No key METEOPATH found for meteo. Streamreaches meteo won't be loaded from SMET"<< std::endl;
    return false;
  }

  const std::string dir = tmp_cfg.get("METEOPATH", "INPUT"); //< mandatory parameter


  const std::list<std::string> files = mio::FileUtils::readDirectory(dir,"dischma");
  if(files.empty())
  {
    setStreamReachFalse();
    std::cout << "[i] No SMET found for streamreaches in directory: " << dir << ".  Streamreaches meteo won't be loaded from SMET" << std::endl;
    return false;
  }

  std::list<std::string> files_ind;
  for(auto name : files){
    files_ind.push_back(name.substr(0, name.find("_",name.find("_",0)+1)));
  }


  for(auto& name : cell_ind){
    std::list<std::string>::iterator it = std::find(files_ind.begin(),files_ind.end(),name);
    if (it==files_ind.end())
    {
      setStreamReachFalse();
      std::cout << "[i] Reach missing in SMET" << std::endl;
      return false;
    }
  }

  std::cout << "[i] Parsing available SMET files for streamreaches" << std::endl;

  std::set<std::string> available_smet_data;
  std::vector<double> available_smet_dates;
  try
  {
    parseSmetFile(dir, files, available_smet_data, available_smet_dates);
  }
  catch(const std::exception& e) {
    setStreamReachFalse();
    std::cout << "[w] Error parsing SMET files for Streamreaches: "<< e.what() <<"[w] Streamreaches won't be loaded from SMET" << std::endl;
    return false;
  }

  std::sort(available_smet_dates.begin(), available_smet_dates.end());

  if(!validateDates(available_smet_dates))
  {
    setStreamReachFalse();
    std::cout << "[i] Available SMET data dates does not fit for streamreaches. Streamreaches won't be loaded from SMET" << std::endl;
    return false;
  }

  std::shared_ptr<std::vector<double>> dates_ptr = std::make_shared<std::vector<double>>(available_smet_dates);

  for(size_t iParam(0); iParam < mio::MeteoGrids::nrOfParameters; ++iParam) {
    std::string name = mio::MeteoGrids::getParameterName (iParam);
    if(std::find(available_smet_data.begin(),available_smet_data.end(),mio::MeteoGrids::getParameterName (iParam)) != available_smet_data.end())
    {
      available_data[static_cast<Variables>(iParam)][NETWORK_POINTS]=true;
      available_data[static_cast<Variables>(iParam)][NETWORK_AVG]=true;
      available_dates[static_cast<Variables>(iParam)][SMET]=dates_ptr;
    }
    else{
      available_data[static_cast<Variables>(iParam)][NETWORK_POINTS]=false;
      available_data[static_cast<Variables>(iParam)][NETWORK_AVG]=false;
      tmp_cfg.addKey(name + "::RESAMPLE", "INTERPOLATIONS1D", "NONE");
    }
  }

  tmp_cfg.addKey("TOTAL_RO::RESAMPLE", "INTERPOLATIONS1D", "LINEAR");
  tmp_cfg.addKey("TSOIL::RESAMPLE", "INTERPOLATIONS1D", "LINEAR");
  tmp_cfg.addKey("METEO", "INPUT", "SMET");  //< runoff tables are currently hard-written in SMET format by Alpine3D
  tmp_cfg.addKey("METEOPATH", "INPUT", dir); //< directory in which the runoff tables are located
  tmp_cfg.deleteKeys("STATION", "INPUT");

  size_t iFile(1);
  std::ostringstream os;
  for(const std::string& file : files) {
    os << "STATION" << iFile;
    tmp_cfg.addKey(os.str(), "INPUT", file);
    os.str("");
    os.clear();
    ++iFile;
  }
  io_network_manager = std::unique_ptr<mio::IOManager>(new mio::IOManager(tmp_cfg));
  return true;
}

void IOContainer::setWatershedFalse(){
  for(size_t iParam(0); iParam < mio::MeteoGrids::nrOfParameters; ++iParam) {
    std::string name = mio::MeteoGrids::getParameterName (iParam);
    available_data[static_cast<Variables>(iParam)][WATERSHED_SUM]=false;
    available_data[static_cast<Variables>(iParam)][WATERSHED_AVG]=false;
  }
}

bool IOContainer::findWatershedData(){

  std::cout << "[i] Checking available SMET files for watershed" << std::endl;
  mio::Config tmp_cfg(cfg);

  if(!tmp_cfg.keyExists("RUNOFFPATH", "INPUT")){
    setWatershedFalse();
    std::cout << "[i] No key RUNOFFPATH found for watersheds in ini file. Watersheds won't be loaded from SMET"<< std::endl;
    return false;
  }

  const std::string dir = tmp_cfg.get("RUNOFFPATH", "INPUT");

  std::string file_prefix="catch";
  const std::list<std::string> files = mio::FileUtils::readDirectory(dir,file_prefix);
  if(files.empty())
  {
    setWatershedFalse();
    std::cout << "[w] No SMET found for watersheds in directory: " << dir << ".  Watersheds won't be loaded from SMET" << std::endl;
    return false;
  }

  std::list<int> files_ind;
  for(auto name : files){
    files_ind.push_back(std::stoi(name.substr(name.find("catch",0)+5,name.find(".",0)-(name.find("catch",0)+5))));
  }

  const std::vector<size_t>& wshed_ind = watershed.getSubwatershedIndices();
  for(auto& name : wshed_ind){
    std::list<int>::iterator it = std::find(files_ind.begin(),files_ind.end(),name);
    if (it==files_ind.end())
    {
      setWatershedFalse();
      std::cout << "[w] Watershed " << name << " is missing in SMET. Watersheds won't be loaded from SMET" << std::endl;
      return false;
    }
  }

  std::cout << "[i] Parsing available SMET files for watersheds" << std::endl;
  std::set<std::string> available_smet_data;
  std::vector<double> available_smet_dates;
  try
  {
    parseSmetFile(dir, files, available_smet_data, available_smet_dates);
  }
  catch(const std::exception& e) {
    std::cout << "[w] Error parsing SMET files for watersheds: "<< e.what() <<"[w] Watersheds won't be loaded from SMET" << std::endl;
    return false;
  }

  std::sort(available_smet_dates.begin(), available_smet_dates.end());

  if(!validateDates(available_smet_dates))
  {
    setWatershedFalse();
    std::cout << "[w] Watersheds won't be loaded from smet because of date mismatch" << std::endl;
    return false;
  }

  std::shared_ptr<std::vector<double>> dates_ptr = std::make_shared<std::vector<double>>(available_smet_dates);

  for(size_t iParam(0); iParam < mio::MeteoGrids::nrOfParameters; ++iParam) {
    std::string name = mio::MeteoGrids::getParameterName (iParam);
      if(std::find(available_smet_data.begin(),available_smet_data.end(),mio::MeteoGrids::getParameterName (iParam)) != available_smet_data.end())
      {
        DataType t1 = WATERSHED_AVG;
        DataType t2 = WATERSHED_SUM;
        if(name=="ROT" || name == "PSUM" || name == "PSUM_S" || name == "PSUM_L" )
        {
          t1 = WATERSHED_SUM;
          t2 = WATERSHED_AVG;
        }
        available_data[static_cast<Variables>(iParam)][t1]=true;
        available_data[static_cast<Variables>(iParam)][t2]=false;

        available_dates[static_cast<Variables>(iParam)][SMET] = dates_ptr;
      }
      else{
        available_data[static_cast<Variables>(iParam)][WATERSHED_SUM]=false;
        available_data[static_cast<Variables>(iParam)][WATERSHED_AVG]=false;
        tmp_cfg.addKey(name + "::RESAMPLE", "INTERPOLATIONS1D", "NONE");
      }
  }

  tmp_cfg.addKey("TOTAL_RO::RESAMPLE", "INTERPOLATIONS1D", "LINEAR");
  tmp_cfg.addKey("TSOIL::RESAMPLE", "INTERPOLATIONS1D", "LINEAR");
  tmp_cfg.addKey("METEO", "INPUT", "SMET");  //< runoff tables are currently hard-written in SMET format by Alpine3D
  tmp_cfg.addKey("METEOPATH", "INPUT", dir); //< directory in which the runoff tables are located
  tmp_cfg.deleteKeys("STATION", "INPUT");
  size_t iFile(1);
  std::ostringstream os;
  for(const std::string& file : files) {
    os << "STATION" << iFile;
    tmp_cfg.addKey(os.str(), "INPUT", file);
    os.str("");
    os.clear();
    ++iFile;
  }

  io_watershed_manager = std::unique_ptr<mio::IOManager>(new mio::IOManager(tmp_cfg));

  return true;
}

bool IOContainer::parseSmetFile(const std::string& dir, const std::list<std::string>& files, std::set<std::string>& available_smet_data, std::vector<double>& available_smet_dates)
{
  bool first=true;
  for(const auto& name : files){
    std::string filename = dir+"/"+name;
    std::vector<double> av_dates;
    std::set<std::string> av_data;
    std::ifstream f(filename);
    if(!f) throw mio::AccessException("Cannot open file " + filename, AT);
    try {
      std::map<std::string, std::string> header(parseHeader(f));

      std::vector<std::string> fields;
      mio::IOUtils::getValueForKey(header, "fields", fields);


      const size_t iTimeCol(std::find(fields.begin(), fields.end(), "timestamp") -
          fields.begin());
      if(iTimeCol == fields.size())
        throw std::runtime_error("missing field 'timestamp'");
      double tz;
      mio::IOUtils::getValueForKey(header, "tz", tz);
      extractDates(f, tz, iTimeCol, av_dates);

      for(size_t iField(0); iField < fields.size(); ++iField) {
        if(iField != iTimeCol)
        {
          if(mio::IOUtils::strToUpper(fields[iField])=="TOTAL_RO")
          {
            av_data.insert("ROT");
          }
          else
          {
            av_data.insert(mio::IOUtils::strToUpper(fields[iField]));
          }
        }
      }
    } catch(const std::exception& e) {
      throw mio::InvalidFormatException("Error while parsing file " + filename + ": "
          + e.what(),AT);
    }
    f.close();
    if(first){
      available_smet_data = av_data;
      available_smet_dates = av_dates;
      first=false;
    }
    else{
      if(available_smet_data!=av_data){
        throw std::runtime_error("Error, variables in file " + filename + " do not correspond to variables in previous files. The SMET files sould be identical.");
      }
      if(available_smet_dates!=av_dates){
        throw std::runtime_error("Error, dates in file " + filename + " do not correspond to dates in previous files. The SMET files sould be identical.");
      }
    }

  }

  return true;
}

std::map<std::string, std::string> IOContainer::parseHeader(std::ifstream& f) {
  const size_t iDataLine(reachSection(f, "DATA"));
  const size_t iHeaderLine(reachSection(f, "HEADER"));
  std::string line;
  std::map<std::string, std::string> header;
  for (size_t iLine(iHeaderLine + 1); iLine < iDataLine; ++iLine){
    if(std::getline(f, line)) {
      mio::IOUtils::trim(line);
      std::string key, value;
      if(line.empty()) {
        continue;
      } else if(mio::IOUtils::readKeyValuePair(line, "=", key, value)) {
        header[mio::IOUtils::strToLower(key)] = value;
      } else { //  means if ((key == "") || (value==""))
        std::ostringstream out;
        out << "invalid key-value pair at line " << iLine;
        throw std::runtime_error(out.str());
      }
    } else {
      throw std::runtime_error("premature EOF while reading header");
    }
  }
  return header;
}

size_t IOContainer::reachSection(std::ifstream& f, std::string sectionName) {
  //Rewind to the beginning of the file
  f.clear();
  f.seekg(std::ios_base::beg); //< seek beginning of file
  //Reformat the section name
  mio::IOUtils::trim(sectionName);
  mio::IOUtils::toUpper(sectionName);
  sectionName = "[" + sectionName + "]";
  //Look for the desired section in the file contents
  size_t iLine(0);
  std::string line;
  while(std::getline(f, line)) {
    ++iLine;
    mio::IOUtils::trim(line);
    mio::IOUtils::toUpper(line);
    if(line == sectionName) break;
  }
  //Check that no error occurred
  if(f.eof()) {
    throw std::runtime_error("missing " + sectionName + " section");
  } else if(!f.good()) {
    std::ostringstream os;
    os << "error reading line " << iLine;
    throw std::runtime_error(os.str());
  }
  return iLine;
}

void IOContainer::extractDates(std::ifstream& f, const double& tz,
    const size_t& iTimeColumn, std::vector<double>& av_dates) {
  mio::Date output;
  reachSection(f, "DATA");
  for (std::string line; std::getline(f, line); )
    {
    std::istringstream stream(line);
    std::string tmp;
    for(size_t iCol(0); iCol <= iTimeColumn; ++iCol){
      stream >> tmp;
    }
    mio::IOUtils::convertString(output, tmp, tz);
    av_dates.push_back(output.getJulian());
    }
}



bool IOContainer::check_available_grids(Variables var, std::shared_ptr<std::vector<double> > dates)
{

  if(!cfg.keyExists("GRID2D", "INPUT"))
  {
    std::cout << "[W] No format for grids indicated in key INPUT::GRID2D, default value of ARC will be used";
  }

  const std::string grid_type= cfg.get("GRID2D", "INPUT","ARC");

  if(grid_type == "ARC" && mio::MeteoGrids::getParameterName(var)=="TSOIL" &&
     cfg.keyExists("SOIL_GRIDS_NAME", "INPUT"))
  {
    const std::string grid_path = cfg.get("GRID2DPATH", "INPUT");
    const std::string varname =  cfg.get("SOIL_GRIDS_NAME", "INPUT");
    std::list< std::string > grid_names = mio::FileUtils::readDirectory (grid_path,varname+".asc", true);
    for (auto path : grid_names)
    {
      path = path.substr(0,19);
      std::replace(path.begin(), path.end(), '.', ':');
      mio::Date load_date;

      if(!mio::IOUtils::convertString(load_date, path,time_zone,std::dec))
      {
          std::cout << "[W] Cannot read date from file name: " << path << std::endl;
      }
      else if(load_date.getJulian() >= (startData-1) && load_date.getJulian() <= (endData+1))
      {
        dates->push_back(load_date.getJulian());
      }
    }
  }
  else{
    std::map<mio::Date, std::set<size_t> > list;
    io_grid_manager->list2DGrids(mio::Date(startData-1,time_zone),mio::Date(endData+1,time_zone),list);
    for (auto l : list)
    {
      if (l.second.find(var) != l.second.end())
      {
        dates->push_back(l.first.getJulian());
      }
    }
  }
  return true;
}

bool IOContainer::findGrids(Variables var)
{
  std::cout << "[i] Checking available grids for variable: " << mio::MeteoGrids::getParameterName(var) << std::endl;

  available_dates[var][GRID] = std::shared_ptr<std::vector<double> > (new std::vector<double>());

  check_available_grids(var, available_dates[var][GRID]);

  if(available_dates[var][GRID]->size()==0)
  {
    std::cout << "[i] No grid found for variable: " << mio::MeteoGrids::getParameterName(var)  << std::endl;
    return false;
  }

  std::sort(available_dates[var][GRID]->begin(), available_dates[var][GRID]->end());

  if(validateDates(*available_dates[var][GRID]))
  {
    return true;
  }
  else
  {
    available_dates[var].erase(GRID);
    std::cout << "[i] Variable: " << mio::MeteoGrids::getParameterName(var) << " won't be loaded from grid because of date mismatch" << std::endl;
    return false;
  }
}

bool IOContainer::validateDates(const std::vector<double>& dates) const{

  if(dates.size() == 0){
    std::cout << "[W] Variable found but no data found in the date range" << std::endl;
    return false;
  }

  if (std::upper_bound(dates.begin(), dates.end(), startData, [](const double& a, const double& b) {return (a-b)<(-1e-5);})== dates.begin())
  {
    std::cout << "[W] Variable found but first data found (" << mio::Date(*(dates.begin()),time_zone).toString(mio::Date::ISO) <<
                  ") is after the simulation or warmup start date (" << mio::Date(startData,time_zone).toString(mio::Date::ISO) << ")" << std::endl;
    return false;
  }

  if (std::lower_bound(dates.begin(), dates.end(), endData, [](const double& a, const double& b) {return (a-b)<(-1e-5);} ) == dates.end())
  {
    std::cout << "[W] Variable found but last data found (" << mio::Date(*(std::prev(dates.end())),time_zone).toString(mio::Date::ISO) <<
                  ") is before the simulation or warmup end date (" << mio::Date(endData,time_zone).toString(mio::Date::ISO) << ")" << std::endl;
    return false;
  }

  return true;
}

const std::shared_ptr<const std::vector<double>> IOContainer::getAvailableDates(Variables var,DataSource source) const
{
  if(available_dates.find(var) == available_dates.end()){
    throw mio::InvalidFormatException("Requesting dates from a var that is not available. This is due to a source code implementation error.",AT);
  }
  else if (available_dates.at(var).find(source) == available_dates.at(var).end()){
    throw mio::InvalidFormatException("Requesting dates from a source that is not available. This is due to a source code implementation error.",AT);
  }
  else{
    return available_dates.at(var).at(source);
  }
}


} /* namespace sflow */
