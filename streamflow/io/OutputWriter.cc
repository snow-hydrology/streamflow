/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/io/OutputWriter.h>

namespace sflow {

const double OutputWriter::_TIME_PRECISION(0.1/86400.); //< [days]


OutputWriter::OutputWriter(const mio::Config& cfg, Clock& clock) :
    ModelObserver(), TimeStepLimiter(clock), _cfg(cfg), _clock(clock),
    _write_watershed(cfg.get("WATERSHED::WRITE", "OUTPUT")),
    _write_network(cfg.get("STREAM::WRITE", "OUTPUT")),
    _watershed_write_step(), _network_write_step(),
    _watershed_write_start(), _network_write_start(),
    _watershed_writer(nullptr), _network_writer(nullptr),
    _output_format(getOutputFormat())
{
  if(_write_watershed)
    this->setWriteProperties("WATERSHED", _watershed_write_step,
        _watershed_write_start);

  if(_write_network)
    this->setWriteProperties("STREAM", _network_write_step,
        _network_write_start);
}


void OutputWriter::reset() {
  //do nothing
}


void OutputWriter::update(const mio::Date& currTime, const Watershed& watershed,
    const StreamNetwork& network) {
  if(_write_watershed) {
    if(_watershed_write_start.isUndef()) {
      _watershed_write_start.setUndef(false);
      _watershed_write_start += currTime;
      this->constructWatershedWriter(watershed);
    }

        double offset(currTime.getJulian() - _watershed_write_start.getJulian());
        offset = roundToNearestSecond(offset);
    if(offset > -_TIME_PRECISION &&
        fabs(remainder(offset, _watershed_write_step)) < _TIME_PRECISION)
      _watershed_writer->writeState(currTime, watershed);
  }

  if(_write_network) {
    if(_network_write_start.isUndef()) {
      _network_write_start.setUndef(false);
      _network_write_start += currTime;
      this->constructNetworkWriter(network);
    }

    double offset(currTime.getJulian() - _network_write_start.getJulian());
    offset = roundToNearestSecond(offset);
    if(offset > - _TIME_PRECISION &&
        fabs(remainder(offset, _network_write_step)) < _TIME_PRECISION)
      _network_writer->writeState(currTime, network);
  }
}


int OutputWriter::getRequiredMaximumTimeStep() const {
  double maxTimeStep(86400.);

  if(_write_watershed && !_watershed_write_start.isUndef()) {
    const double offset(roundToNearestSecond(_clock.getCurrTime().getJulian() -
        _watershed_write_start.getJulian()));
    double dtMaxWshd(_watershed_write_step - fmod(offset, _watershed_write_step));
    if(dtMaxWshd < _TIME_PRECISION)
      dtMaxWshd = _watershed_write_step;

    maxTimeStep = fmin(maxTimeStep, dtMaxWshd*86400.);
  }

  if(_write_network && !_network_write_start.isUndef()) {
    const double offset(roundToNearestSecond(_clock.getCurrTime().getJulian() -
        _network_write_start.getJulian()));
    double dtMaxNtwk(_network_write_step - fmod(offset, _network_write_step));
    if(dtMaxNtwk < _TIME_PRECISION)
      dtMaxNtwk = _network_write_step;

    maxTimeStep = fmin(maxTimeStep, dtMaxNtwk*86400.);
  }

  if(maxTimeStep < 1.) {
    std::ostringstream os;
    os << "[w] The OutputWriter instance requires the time step to be lower "
       << "than 1 second at time "
       << _clock.getCurrTime().toString(mio::Date::ISO)
       << ". Simulated data might be missing in the output file(s) for this "
       << "date.\n";
  }

  return round(maxTimeStep);
}


OutputWriter::~OutputWriter() {
  delete _watershed_writer;
  delete _network_writer;
}


void OutputWriter::setWriteProperties(const std::string element, double& writeStep,
    mio::Date& startDate) const {
  //Get the time step used to write the element status
  writeStep = 1.0; //< in days
  std::string key(element + "::WRITE_STEP");
  _cfg.getValue(key, "OUTPUT", writeStep, mio::IOUtils::nothrow);  //< in days
  writeStep = roundToNearestSecond(writeStep);

  //Get the time at which the element state should start to be written,
  //expressed as a delay with respect to the simulation start date
  double delay(0.0); //< in days
  key = element + "::WRITE_START";
  _cfg.getValue(key, "OUTPUT", delay, mio::IOUtils::nothrow); //< in days
  delay = roundToNearestSecond(delay); //< round to the nearest second

  //For the moment, store only the delay in the output writing start date,
  //since the simulation start date will only be known when method update()
  //is called for the first time
  startDate.setDate(delay, 0.0);
  startDate.setUndef(true); //< so as to remember that the simulation start date still needs to be added to it
}


void OutputWriter::constructWatershedWriter(const Watershed& watershed) {
  if(_output_format == "SMET") {
    switch(watershed.getDiscretization()) {
    case DiscretizedObject::Discretization::Lumped:
      _watershed_writer = new LumpedWatershedSMETWriter(_cfg, watershed);
      break;
    case DiscretizedObject::Discretization::Distributed:
      throw NotImplementedException("SMET writer for discretized Watershed "
          "objects has not been implemented yet!", AT);
      //break;
    default:
      std::ostringstream os;
      os << "Error with the discretization retuened by watershed.getDiscretization(). Unknown value";
      throw RuntimeException(os.str(), AT);
    }
  }
}


void OutputWriter::constructNetworkWriter(const StreamNetwork& network) {
  if(_output_format == "SMET") {
    switch(network.getDiscretization()) {
    case DiscretizedObject::Discretization::Lumped:
      _network_writer = new LumpedNetworkSMETWriter(_cfg, network);
      break;
    case DiscretizedObject::Discretization::Distributed:
      _network_writer = new DiscretizedNetworkSMETWriter(_cfg, network);
      break;
    default:
      std::ostringstream os;
      os << "Error with the discretization retuened by network.getDiscretization(). Unknown value";
      throw RuntimeException(os.str(), AT);
    }
  }
}


std::string OutputWriter::getOutputFormat() const {
  std::string type("SMET");
  _cfg.getValue("STREAM", "OUTPUT", type, mio::IOUtils::nothrow);
  mio::IOUtils::toUpper(type);
  if(type != "SMET") {
    throw InvalidConfigKeyException("Key STREAM in section [OUTPUT] of the "
        "configuration file can take only the following value: SMET", AT);
  }
  return type;
}


double OutputWriter::roundToNearestSecond(const double& timeInDays) {
    return round(timeInDays*86400.)/86400.;
}

} /* namespace sflow */
