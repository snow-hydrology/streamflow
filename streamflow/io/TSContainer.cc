
/*****************************************************************************/
/* Copyright 2017 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */
#include <streamflow/io/TSContainer.h>


namespace sflow {

TSContainer::~TSContainer() {}


/////// TS CONATAINER IMPLEMANTATION ///////////////////////////////

TSContainer::TSContainer(const mio::Config& _cfg, IOContainer& _io_container, const mio::Date _startData, const mio::Date _endData,
            const std::map<size_t, mio::Grid2DObject>& _masks, const std::map<size_t,size_t>& _wshed_continuous_id, const wshed_stream_cells_map& _wshed_stream_cells ):
            cfg(_cfg), io_container(_io_container), startData(_startData.getJulian()), endData(_endData.getJulian()),
            time_step( static_cast<double>(cfg.get("TIME_STEP", "HydroModel"))/1440.), time_zone(cfg.get("TIME_ZONE", "INPUT")),
            variables(), masks(_masks), wshed_continuous_id(_wshed_continuous_id),wshed_stream_cells(_wshed_stream_cells)
{
}

void TSContainer::error_fct(const std::string& func){
    throw NotImplementedException(func+" is not implementd. This is due to a source code implementation error.",AT);
}

void TSContainer::getPosition(const std::vector<double>& dates, const double date, size_t& pos1, size_t& pos2, double& dist1, double& dist2){
  std::pair<const std::vector<double>::const_iterator,const std::vector<double>::const_iterator > range =
        std::equal_range(dates.begin(), dates.end(), date, [](const double& a, const double& b) {return (a-b)<(-1e-5);});
  if (range.first != range.second){
      pos1 = std::distance(dates.begin(), range.first);
  }
  else
  {
    pos1 = std::distance(dates.begin(), std::prev(range.first));
    dist1 = (*(range.second) - date);
    pos2 = std::distance(dates.begin(), range.second);
    dist2 = (date-*std::prev(range.first));
  }
}
}
