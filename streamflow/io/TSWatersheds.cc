
/*****************************************************************************/
/* Copyright 2017 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */
#include <streamflow/io/TSWatersheds.h>
#include <streamflow/Watershed.h>
#include <streamflow/StreamNetwork.h>
#include <streamflow/io/TSGrids.h>
#include <streamflow/utils/GridOperations.h>

#ifdef _OPENMP
  #include <omp.h>
#endif

namespace sflow {

TSWatersheds::~TSWatersheds() {}

//////////// TSWatersheds

TSWatersheds::TSWatersheds(const mio::Config& _cfg, IOContainer& _io_container,const mio::Date _startData, const mio::Date _endData, const std::map<size_t, mio::Grid2DObject>& _masks,
        const std::map<size_t,size_t>& _wshed_continuous_id, const wshed_stream_cells_map& _wshed_stream_cells, TSGrids& _grids):
  TSContainer(_cfg, _io_container, _startData, _endData, _masks, _wshed_continuous_id, _wshed_stream_cells), grids(_grids),loaded_dates(),loaded_data(), memory_resized(false),smet_loaded(false)
{
}


void TSWatersheds::addData(Variables var, DataType type, DataSource source){
  variables[var]=std::make_pair(type, source);
}

void TSWatersheds::prepareMemory(Variables){
  if(!memory_resized){
    memory_resized=true;
    std::cout << "\t[i] Resizing memory for watersheds data" << std::endl;
    for (auto const& var: variables){
      loaded_dates[var.first].insert(std::make_pair(var.second.first, io_container.getAvailableDates(var.first,var.second.second)));
      const size_t size_of_data = loaded_dates[var.first][var.second.first]->size();
      loaded_data[var.first].insert(std::make_pair(var.second.first, std::unique_ptr<std::vector< std::vector< double > > > (new std::vector<std::vector< double > >(size_of_data))));
      for (size_t i=0; i<size_of_data;++i)
      {
        loaded_data[var.first][var.second.first]->at(i).resize(wshed_continuous_id.size(),mio::IOUtils::nodata);
      }
    }
  }
}

void TSWatersheds::loadData(Variables var_in)
{
  if (variables.find(var_in) == variables.end())
  {
    return;
  }
  std::pair<DataType, DataSource> var = variables[var_in];
  std::map<Variables, DataType> smet_vars;
  if(var.second == DataSource::GRID )
  {
    loadGridData(var_in,var.first);
  }
  else{
    for (auto const& v: variables){
      if(v.second.second == DataSource::SMET)
      {
        smet_vars[v.first] = v.second.first;
      }
    }
    if(!smet_vars.empty() & !smet_loaded)
    {
      smet_loaded=true;
      loadFromSmet(smet_vars);
    }
  }
}

void TSWatersheds::loadGridData(Variables var, DataType type)
{
  std::cout << "\t[i] Reading watersheds data for var : " << mio::MeteoGrids::getParameterName(var) << " from GRIDS" << std::endl;
  #ifdef _OPENMP
  #pragma omp parallel for
  #endif
  for (size_t i=0; i<loaded_dates[var][type]->size();++i){
    const double date(loaded_dates[var][type]->at(i));
    const mio::Grid2DObject grid = grids.getGrid(date, var, DataType::GRID_POINTS);
    for(auto& id : wshed_continuous_id){
      if(type == DataType::WATERSHED_AVG)
      {
        double mean = GridOperations::mean(grid, masks.at(id.first));
        loaded_data[var][type]->at(i).at(id.second) = mean;
      }
      else if(type == DataType::WATERSHED_SUM)
      {
        double sum = GridOperations::sum(grid, masks.at(id.first));
        loaded_data[var][type]->at(i).at(id.second) = sum;
      }
    }
  }

}

void TSWatersheds::loadFromSmet(std::map<Variables, DataType> vars)
{
  std::cout << "\t[i] Reading watersheds data for: ";
  for (const auto& var : vars)
  {
    std::cout << mio::MeteoGrids::getParameterName(var.first) << " ";
  }
  std::cout << "from SMET" << std::endl;

  const std::string file_prefix="catch";
  size_t i=0;
  for (const auto& date : *loaded_dates[vars.begin()->first][vars.begin()->second])
  {
    std::vector<mio::MeteoData> read_data;
    io_container.io_watershed_manager->getMeteoData(mio::Date(date,time_zone), read_data);
    IOContainer::wshed_files_map watershed_files;

    for(auto& id : wshed_continuous_id){
      std::string station_id = read_data[id.second].meta.getStationID();
      station_id.erase(station_id.find(file_prefix), file_prefix.length());
      watershed_files.insert(std::make_pair(std::stoi(station_id),id.second));
    }

    for (const auto& var : vars)
    {
      for(const auto& file : watershed_files)
      {
        std::string varName = mio::MeteoGrids::getParameterName(var.first);
        if (varName == "ROT")
        {
          varName="TOTAL_RO";
          loaded_data[var.first][var.second]->at(i).at(wshed_continuous_id.at(file.first)) = read_data[file.second](varName)/3600.;
        }
        else
        {
          loaded_data[var.first][var.second]->at(i).at(wshed_continuous_id.at(file.first)) = read_data[file.second](varName);
        }
      }
    }
    ++i;
  }

  io_container.io_watershed_manager->clear_cache();
}

double TSWatersheds::getMeanVal(const double date, size_t subw_ind, Variables var)
{
  if(variables.find(var) == variables.end())
  {
    std::cout << "variable not found" << std::endl;
    exit(0);
  }

  DataType type = variables[var].first;
  const size_t ind = wshed_continuous_id.at(subw_ind);
  size_t pos1,pos2=0;
  double dist1,dist2=0;
  TSContainer::getPosition(*(loaded_dates.at(var).at(type)), date, pos1, pos2, dist1, dist2);

  if(pos2==0){
    return loaded_data.at(var).at(type)->at(pos1).at(ind);
  }
  else{
    return (loaded_data.at(var).at(type)->at(pos1).at(ind)*dist1+loaded_data.at(var).at(type)->at(pos2).at(ind)*dist2)/(dist1+dist2);
  }
}


void TSWatersheds::getMeanMeteo(const double date, size_t subw_ind, mio::MeteoData& data)
{
  const size_t ind = wshed_continuous_id.at(subw_ind);
  for(auto const& var : variables) {
    const DataType type = var.second.first;
    size_t pos1,pos2=0;
    double dist1,dist2=0;
    TSContainer::getPosition(*(loaded_dates.at(var.first).at(type)), date, pos1, pos2, dist1, dist2);
    const std::string& varName(mio::MeteoGrids::getParameterName(var.first));
    if(!data.param_exists(varName))
    {
      data.addParameter(varName);
    }
    size_t iParam = data.getParameterIndex(varName);
    if(pos2==0){
      data(iParam) = loaded_data.at(var.first).at(type)->at(pos1).at(ind);
    }
    else{
      data(iParam) = (loaded_data.at(var.first).at(type)->at(pos1).at(ind)*dist1+loaded_data.at(var.first).at(type)->at(pos2).at(ind)*dist2)/(dist1+dist2);
    }
  }
}

double TSWatersheds::getTimeAveragedMeanVal(double startDate, const double endDate, size_t subw_ind, Variables var)
{
  double mean=0;
  double count=0;
  const size_t ind = wshed_continuous_id.at(subw_ind);
  const DataType type = variables[var].first;
  while (startDate <= endDate){
    size_t pos1,pos2=0;
    double dist1,dist2=0;
    TSContainer::getPosition(*(loaded_dates.at(var).at(type)), startDate, pos1, pos2, dist1, dist2);
    if(pos2==0){
      mean +=loaded_data.at(var).at(type)->at(pos1).at(ind);
    }
    else{
      mean += (loaded_data.at(var).at(type)->at(pos1).at(ind)*dist1+loaded_data.at(var).at(type)->at(pos2).at(ind)*dist2)/(dist1+dist2);
    }
    ++count;
    startDate += time_step;
  }
  return mean/count;
}
}
