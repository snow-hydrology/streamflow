
/*****************************************************************************/
/* Copyright 2017 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/io/SimulatedDataContainer.h>
#include <streamflow/Watershed.h>
#include <streamflow/StreamNetwork.h>

#ifdef _OPENMP
  #include <omp.h>
#endif

//#include "TSContainer.h"


namespace sflow {

/**
 * @brief Destructor of class DataContainer
 */

SimulatedDataContainer::~SimulatedDataContainer()
 {}

/**
 * @brief Constructor of class DataContainer
 * @param cfg Reference to the Config instance holding the model parameters
 * @param clk Simulation clock object
 * @param wshed Watershed object (not necessarily initialized yet)
 * @param snetwork StreamNetwork object (not necessarily initialized yet)
 */

SimulatedDataContainer::SimulatedDataContainer(const mio::Config& _cfg, const mio::Date& _start_time, const mio::Date& _end_time, const CalibrationDataContainer& _measures_reader):
                                    start_time(_start_time.getJulian()),end_time(_end_time.getJulian()),
                                    time_step(static_cast<double>(_cfg.get("TIME_STEP", "HydroModel"))/1440.),time_zone(_cfg.get("TIME_ZONE", "INPUT")), run_dates(_measures_reader.getLoadedDates()),
                                    id_to_pos(_measures_reader.getIdToPos()),required_data(_measures_reader.getRequiredVariable()),
                                    points_coords(_measures_reader.getMeasPointCoords()), stations_meta(_measures_reader.getStationsMeta()),
                                    _subwatershed_indices(), _cell_indices(), run_data(), isSetUp(false)
{


}


SimulatedDataContainer::SimulatedDataContainer(const SimulatedDataContainer& _source ) : start_time(_source.start_time),end_time(_source.end_time),
                      time_step(_source.time_step), time_zone(_source.time_zone), run_dates(_source.run_dates),
                      id_to_pos(_source.id_to_pos),required_data(_source.required_data),
                      points_coords(_source.points_coords), stations_meta(_source.stations_meta),
                       _subwatershed_indices(_source._subwatershed_indices),
                       _cell_indices(_source._cell_indices),run_data(), isSetUp(false)
{
  reset();
}



const mio::Coords& SimulatedDataContainer::getMeasPointCoords(const size_t& ptId) const {
  if(ptId > id_to_pos.size() || ptId == 0) {
    std::ostringstream os;
    os << "Unknown measurement point with ID number " << ptId;
    throw mio::IndexOutOfBoundsException(os.str(), AT);
  }
  size_t id = id_to_pos.at(ptId);
  return points_coords.at(id);
}

const std::vector<mio::Coords>& SimulatedDataContainer::getMeasPointCoords() const {
  return points_coords;
}

double SimulatedDataContainer::getData(size_t ptId, const HydroVariable& var, const mio::Date& date) const{
  if(ptId > id_to_pos.size() || ptId == 0) {
    std::ostringstream os;
    os << "Unknown measurement point with ID number " << ptId;
    throw mio::IndexOutOfBoundsException(os.str(), AT);
  }
  size_t id = id_to_pos.at(ptId);
  std::vector<double>::const_iterator it;
  it = std::find(run_dates->begin(), run_dates->end(), date.getJulian());
  if(it != run_dates->end()) {
      size_t pos = std::distance(run_dates->begin(), it);
      return run_data.at(id).at(var.toString()).at(pos);
  }
  else
  {
    throw MissingConfigKeyException("Cannot calibrate the model - missing key "
        "MEASUREMENT_POINT# in section [INPUT] of the configuration file", AT);
  }
}


const std::vector<double>& SimulatedDataContainer::getData(size_t ptId, const HydroVariable& var) const
{
  size_t id = id_to_pos.at(ptId);
  return run_data.at(id).at(var.toString());
}


bool SimulatedDataContainer::getData(const mio::Date& time, const HydroVariable& var, const size_t& ptId, double& value)
{
  if(ptId > id_to_pos.size() || ptId == 0) {
    std::ostringstream os;
    os << "Unknown measurement point with ID number " << ptId;
    throw mio::IndexOutOfBoundsException(os.str(), AT);
  }
  size_t id = id_to_pos.at(ptId);
  std::vector<double>::const_iterator it;
  it = std::find(run_dates->begin(), run_dates->end(), time.getJulian());
  if(it != run_dates->end()) {
      size_t pos = std::distance(run_dates->begin(),it);
      value = run_data.at(id).at(var.toString()).at(pos);

  }
  else
  {
    throw MissingConfigKeyException("Cannot calibrate the model - missing key "
        "MEASUREMENT_POINT# in section [INPUT] of the configuration file", AT);
  }
  return true;
}

bool SimulatedDataContainer::getData(const mio::Date& time, const HydroVariable& var, const std::vector<size_t>& ptIds, std::vector<double>& values)
{
  values.clear();
  values.reserve(ptIds.size());
  for(auto const ptId : ptIds)
  {
    size_t id = id_to_pos.at(ptId);
    std::vector<double>::const_iterator it;
    it = std::find(run_dates->begin(), run_dates->end(), time.getJulian());
    if(it != run_dates->end()) {
        size_t pos = std::distance(run_dates->begin(),it);
        values.push_back(run_data.at(id).at(var.toString()).at(pos));
    }
    else
    {
      throw MissingConfigKeyException("Cannot calibrate the model - missing key "
          "MEASUREMENT_POINT# in section [INPUT] of the configuration file", AT);
    }
  }
  return true;
}

size_t SimulatedDataContainer::getMeasPointId(const mio::Coords& pt) const
{
  std::vector<mio::Coords>::const_iterator it;
  it = std::find(points_coords.begin(), points_coords.end(), pt);
  if(it == points_coords.end()) {
    std::ostringstream os;
    os << "Point with coordinates (" << pt.getEasting() << ","
         << pt.getNorthing() << ") is not a measurement point";
    throw mio::InvalidArgumentException(os.str(), AT);
  }
  return it - points_coords.begin();
}

const std::vector<double>& SimulatedDataContainer::getRunDates() const
{
  return *run_dates;
}
const std::vector< std::map< std::string, std::vector<double> > > & SimulatedDataContainer::getRunData() const
{
  return run_data;
}

bool  SimulatedDataContainer::update(const mio::Date& currTime, const Watershed& watershed, const StreamNetwork& network)

{
  (void)watershed;

  if(!isSetUp){
      size_t _n_points = id_to_pos.size();
    _subwatershed_indices.reserve(_n_points);
    _cell_indices.reserve(_n_points);
    size_t sdIndex, cellIndex;
    for(const mio::Coords& point : points_coords) {
      if(!network.localizePoint(point, sdIndex, cellIndex)) {
        std::ostringstream os;
        os << "Point with coordinates (" << point.getNorthing() << ","
           << point.getEasting() << ") was declared as a point where "
           << "stream measurements (water depth, discharge or temperature)"
           << " are available, but is not located on a stream reach. "
           << "Please correct the point coordinates in the measurement file";
        throw RuntimeException(os.str(), AT);
      }
      _subwatershed_indices.push_back(sdIndex);
      _cell_indices.push_back(cellIndex);
    }
  isSetUp=true;
  }

  std::vector<double>::const_iterator it;
  it = std::find(run_dates->begin(), run_dates->end(), currTime.getJulian());
  if(it == run_dates->end()) {
    throw MissingConfigKeyException("Cannot calibrate the model - missing key "
        "MEASUREMENT_POINT# in section [INPUT] of the configuration file", AT);
  }
  size_t pos = std::distance(run_dates->begin(),it);
  for (auto const& id : id_to_pos)
  {
    for (auto const& v : required_data.at(id.second))
    {
      run_data.at(id.second).at(v.get().toString()).at(pos) =  network.getReach(_subwatershed_indices.at(id.second)).get(v, _cell_indices.at(id.second));
    }
  }
  return true;
}

void SimulatedDataContainer::reset()
{
  run_data.clear();
  run_data.reserve(id_to_pos.size());
  for (auto const& id : id_to_pos)
  {
    run_data.push_back(std::map< std::string, std::vector<double> >() );
    for (auto const& v : required_data.at(id.second))
    {
      run_data.at(id.second).insert(std::make_pair(v.get().toString(), std::vector< double > (run_dates->size(),mio::IOUtils::nodata)));
    }
  }
}

}
