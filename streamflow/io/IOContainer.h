/*****************************************************************************/
/* Copyright 2017 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STREAMFLOW_IOCONTAINER_H_
#define STREAMFLOW_IOCONTAINER_H_

// #include <iostream>
#include <string>
#include <vector>
#include <map>
#include <list>
#include <meteoio/Config.h>                 // for Config
#include <meteoio/dataClasses/MeteoData.h>  // for MeteoGrids, MeteoGrids::P...
#include <meteoio/dataClasses/Date.h>  // for Date
#include <meteoio/IOManager.h>
#include <stddef.h>                         // for size_t
#include <fstream>                          // for ifstream, string

namespace sflow {

class Watershed;
class StreamNetwork;

typedef mio::MeteoGrids::Parameters Variables;

typedef enum DataType
  {
    GRID_POINTS,
    NETWORK_POINTS,
    NETWORK_AVG,
    WATERSHED_AVG,
    WATERSHED_SUM,
    D_SIZE
  } DataType;

//void InitializeTypenames();

//static const std::string& getTypesName(const size_t& parindex);

typedef enum DataSource
  {
    GRID,
    SMET,
    STATION,
    NONE
  } DataSource;

/**
 * @brief This class extends class meteoio::Config for the purpose of StreamFlow.
 * In particular, it copies some keys located in section [Output] and used in the
 * Alpine3D simulation into section [InputFromA3D].
 * @author Aurelien Gallice
 * @date 2016-02-15
 */
class IOContainer {
  public:
    IOContainer(const mio::Config& cfg_imp, const mio::Date startData, const mio::Date endData, const Watershed& watershed, const StreamNetwork& streamnetwork);
    bool isAvailable(Variables var,DataType type);
    const std::shared_ptr<const std::vector<double>>  getAvailableDates(Variables var,DataSource source) const;

    //static const std::string& getTypesName(const size_t& parindex);

    typedef std::map<size_t, size_t > wshed_files_map;
    typedef std::map<std::string, size_t > network_files_map;

    static bool parseSmetFile(const std::string& dir, const std::list<std::string>& files,
              std::set<std::string>& available_smet_data, std::vector<double>& available_smet_dates);
    static size_t reachSection(std::ifstream& f, std::string sectionName);
    static std::map<std::string, std::string> parseHeader(std::ifstream& f);
    static void extractDates(std::ifstream& f, const double& tz, const size_t& iTimeColumn, std::vector<double>& available_smet_dates);

  private:
    const mio::Config& cfg;
    const double startData;
    const double endData;
    const double time_zone;
    const Watershed& watershed;
    const StreamNetwork& streamnetwork;
    typedef std::map<Variables, std::map<DataType, bool> > available_data_map;
    typedef std::map<Variables, std::map<DataSource, std::shared_ptr< std::vector<double> > > > available_dates_map;
    //static std::vector<std::string> typenames;
    //void InitializeTypeNames();

    available_data_map available_data;
    available_dates_map available_dates;
    bool findData(Variables var,DataType type);
    bool check_available_grids(Variables var, std::shared_ptr<std::vector<double> > dates);
    bool findGrids(Variables var);
    bool validateDates(const std::vector<double>& dates) const;
    bool findWatershedData();
    bool findStreamReachData();
    void setWatershedFalse();
    void setStreamReachFalse();
  public:
    std::unique_ptr<mio::IOManager> io_grid_manager;
    std::unique_ptr<mio::IOManager> io_watershed_manager;
    std::unique_ptr<mio::IOManager> io_network_manager;
    std::unique_ptr<mio::IOManager> io_stations_manager;


  //  std::list<std::string> VariablesNames;


};
} /* namespace sflow */

#endif /* STREAMFLOW_IOCONTAINER_H_ */
