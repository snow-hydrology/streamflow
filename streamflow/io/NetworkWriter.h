/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NETWORKWRITER_H_
#define NETWORKWRITER_H_

#include <string>
#include <cctype>
#include <sstream>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/utils/StreamflowExceptions.h>
#include <streamflow/StreamNetwork.h>
#include <streamflow/streamReach/StreamReach.h>
#include <meteoio/MeteoIO.h>

namespace sflow {

/**
 * @class NetworkWriter
 * @brief This class is used to write the current state of class StreamNetwork
 * in output file(s).
 * @ingroup io
 * @author Aurelien Gallice
 * @date 2015-09-10
 */
class NetworkWriter {
  public:
    NetworkWriter(const mio::Config& cfg, const StreamNetwork& network);
    virtual void writeState(const mio::Date& currTime,
        const StreamNetwork& network) const = 0;
    virtual ~NetworkWriter();

  protected:
    const std::vector<size_t> _output_reach_ids;
    const std::vector<const StreamVariable*> _output_variables;

    static std::vector<size_t> getOutputReachIds(const mio::Config& cfg,
        const StreamNetwork& network);
    static std::vector<const StreamVariable*> getOutputVariables(const mio::Config& cfg);
};


/**
 * @class LumpedNetworkSMETWriter
 * @brief This class implements the interface NetworkWriter. It writes the
 * current state of a StreamNetwork object in SMET files, one for each of the
 * stream reaches composing the network. The files are named reach[i].smet,
 * where [i] denotes the ID number of the subwatershed in which the reach is
 * located.
 * @author Aurelien Gallice
 * @date 2015-09-10
 */
class LumpedNetworkSMETWriter : public NetworkWriter {
  public:
    LumpedNetworkSMETWriter(const mio::Config& cfg, const StreamNetwork& network);
    virtual void writeState(const mio::Date& currTime,
        const StreamNetwork& network) const;
    virtual ~LumpedNetworkSMETWriter();

  protected:
    const std::string _output_path;

    static const std::string _FILE_NAME_PREFIX;
    static const std::string _FILE_NAME_EXTENSION;

    virtual void initializeFile(const StreamReach& reach, const double& timeZone);
    virtual void updateFile(const mio::Date& currTime, const StreamReach& reach) const;
    virtual std::string getFileName(const StreamReach& reach) const;

    static std::string getOutputPath(const mio::Config& cfg);
};


/**
 * @class DiscretizedNetworkSMETWriter
 * @brief This class implements the interface NetworkWriter. It writes the
 * current state of a StreamNetwork object in SMET files, one for each
 * hydrological variable (water depth, discharge and temperature) and for each
 * of the stream reaches composing the network. The files are named
 * reach[i]_[var].smet, where [i] denotes the ID number of the subwatershed in
 * which the reach is located, and [var] stands for the hydrological variable
 * stored in the file ([var] can be either 'h' for water depth, 'q' for
 * discharge or 't' for temperature).
 * @author Aurelien Gallice
 * @date 2015-10-14
 */
class DiscretizedNetworkSMETWriter : public NetworkWriter {
  public:
    DiscretizedNetworkSMETWriter(const mio::Config& cfg,
        const StreamNetwork& network);
    virtual void writeState(const mio::Date& currTime,
        const StreamNetwork& network) const;
    virtual ~DiscretizedNetworkSMETWriter();

  protected:
    const std::string _output_path;

    static const std::string _FILE_NAME_PREFIX;
    static const std::string _FILE_NAME_EXTENSION;

    virtual void initializeFiles(const StreamReach& reach, const double& timeZone);
    virtual void updateFiles(const mio::Date& currTime, const StreamReach& reach) const;
    virtual std::string getFileName(const size_t& reachId, const std::string& var) const;

    static std::string getOutputPath(const mio::Config& cfg);
};

} /* namespace sflow */

#endif /* NETWORKWRITER_H_ */
