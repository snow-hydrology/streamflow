/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OUTPUTWRITER_H_
#define OUTPUTWRITER_H_

#include <sstream>
#include <iostream>
#include <streamflow/Watershed.h>
#include <streamflow/StreamNetwork.h>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/utils/StreamflowExceptions.h>
#include <streamflow/io/WatershedWriter.h>
#include <streamflow/io/NetworkWriter.h>
#include <streamflow/subwatershed/Subwatershed.h>
#include <streamflow/streamReach/StreamReach.h>
#include <meteoio/MeteoIO.h>

namespace sflow {

/**
 * @class OutputWriter
 * @brief This class is used to write the state of the HydrologicalModel
 * instance in output files. Each subclass should correspond to a different
 * output format and should implement the protected pure virtual methods.
 *
 * The subwatershed outputs are written independently from the stream reach
 * outputs. The following two boolean keys are mandatory in section [OUTPUT] of
 * the configuration file: WRITE_WATERSHED and WRITE_STREAM; they control
 * whether the subwatershed output and stream reach output should be written
 * out or not.
 *
 * In case either WRITE_WATERSHED = true or WRITE_STREAM = true, key STREAM
 * can optionally be specified in section [OUTPUT] of the configuration file.
 * This key specifies the output file format. For the time being, only the
 * SMET format is implemented, but other formats could follow in the future .
 *
 * In case WRITE_WATERSHED = true, the following keys can optionally be added
 * in section [OUTPUT] of the configuration file:
 *     WATERSHED_WRITE_STEP, corresponding to the period (expressed in days) at
 *         which the watershed output should be written. Defaults to 1.0 (day).
 *     WATERSHED_WRITE_START, corresponding to the date at which the watershed
 *         output should start to be written, expressed as an offset in days
 *         with respect to the simulation start date. Defaults to 0.0 (day),
 *         i.e. the beginning of the simulation.
 *
 * In case WRITE_STREAM = true, the following two keys can optionally be
 * added in section [OUTPUT]: STREAM_WRITE_STEP and STREAM_WRITE_START. They
 * correspond to the counterparts of keys WATERSHED_WRITE_STEP and
 * WATERSHED_WRITE_START described above, and default to the same values if not
 * specified in the configuration file.
 * @ingroup io
 * @author Aurelien Gallice
 * @date 2015-10-13
 */
class OutputWriter: public ModelObserver, public TimeStepLimiter {
  public:
    OutputWriter(const mio::Config& cfg, Clock& clock);
    OutputWriter(const OutputWriter&) = delete;
    OutputWriter& operator=(const OutputWriter&) = delete;
    virtual void reset();
    virtual void update(const mio::Date& currTime, const Watershed&,
        const StreamNetwork&);
    virtual int getRequiredMaximumTimeStep() const;
    virtual ~OutputWriter();

  protected:
    const mio::Config& _cfg;
    const Clock& _clock;
    const bool _write_watershed;
    const bool _write_network;
    double _watershed_write_step;
    double _network_write_step;
    mio::Date _watershed_write_start;
    mio::Date _network_write_start;
    WatershedWriter* _watershed_writer;
    NetworkWriter* _network_writer;
    const std::string _output_format;

    static const double _TIME_PRECISION; //< [days]

    virtual void setWriteProperties(const std::string element, double& step,
        mio::Date& delay) const;
    virtual void constructWatershedWriter(const Watershed& watershed);
    virtual void constructNetworkWriter(const StreamNetwork& network);
    virtual std::string getOutputFormat() const;

    static double roundToNearestSecond(const double& timeInDays);
};

} /* namespace sflow */

#endif /* OUTPUTWRITER_H_ */
