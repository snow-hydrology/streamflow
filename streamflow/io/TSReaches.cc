
/*****************************************************************************/
/* Copyright 2017 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */
#include <streamflow/io/TSReaches.h>
#include <streamflow/io/TSGrids.h>
#include <streamflow/utils/GridOperations.h>

namespace sflow {

TSReaches::~TSReaches() {}

//////////// TSReaches

TSReaches::TSReaches(const mio::Config& _cfg, IOContainer& _io_container,const mio::Date _startData, const mio::Date _endData,  const std::map<size_t, mio::Grid2DObject>& _masks,
        const std::map<size_t,size_t>& _wshed_continuous_id, const wshed_stream_cells_map& _wshed_stream_cells, TSGrids& _grids):
  TSContainer(_cfg, _io_container, _startData, _endData, _masks, _wshed_continuous_id, _wshed_stream_cells), grids(_grids),loaded_dates(),loaded_data(),memory_resized(false),smet_loaded(false)
{
}

void TSReaches::addData(Variables var, DataType type, DataSource source){
  variables[var]=std::make_pair(type, source);
}

void TSReaches::prepareMemory(Variables)
{
  if(!memory_resized){
    memory_resized=true;
    std::cout << "\t[i] Resizing memory for reaches data" << std::endl;
    for (auto const& var: variables){
      loaded_dates[var.first].insert(std::make_pair(var.second.first, io_container.getAvailableDates(var.first,var.second.second)));
      const size_t size_of_data = loaded_dates[var.first][var.second.first]->size();
      loaded_data[var.first].insert(std::make_pair(var.second.first, std::shared_ptr< std::vector< std::vector< std::vector< double > > >> (new std::vector<std::vector< std::vector< double > > >(size_of_data))));
      for (size_t i=0; i<size_of_data;++i)
      {
        loaded_data[var.first][var.second.first]->at(i).resize(wshed_continuous_id.size());
        for (auto const& j : wshed_continuous_id)
        {
          loaded_data[var.first][var.second.first]->at(i).at(j.second).resize(wshed_stream_cells.at(j.first).size(),mio::IOUtils::nodata);
        }
      }
    }
  }
}

void TSReaches::loadData(Variables var_in){
  if (variables.find(var_in) == variables.end())
  {
    return;
  }
  std::pair<DataType, DataSource> var = variables[var_in];
  std::map<Variables, DataType> smet_vars;
  if(var.second == DataSource::GRID )
  {
    loadGridData(var_in,var.first);
  }
  else{
    for (auto const& v: variables){
      if(v.second.second == DataSource::SMET)
      {
        smet_vars[v.first] = v.second.first;
      }
    }
    if(!smet_vars.empty() & !smet_loaded)
    {
      smet_loaded=true;
      loadFromSmet(smet_vars);
    }
  }
}

void TSReaches::loadFromSmet(std::map<Variables, DataType> vars){

  std::cout << "\t[i] Reading streams cells data for: ";
  for (const auto& var : vars)
  {
    std::cout << mio::MeteoGrids::getParameterName(var.first) << " ";
  }
  std::cout << "from SMET" << std::endl;
  std::vector<mio::MeteoData> read_data;
  size_t i=0;
  for (const auto& date : *loaded_dates[vars.begin()->first][vars.begin()->second]){
    io_container.io_network_manager->getMeteoData(mio::Date(date,time_zone), read_data);
    IOContainer::network_files_map network_files;
    size_t file_num=0;
    for(const auto& data : read_data){
        std::string id = data.meta.getStationID();
        network_files.insert(std::make_pair(id,file_num));
        ++file_num;
    }
    for(const auto& wshed : wshed_stream_cells){
      size_t j= 0;
      for(auto const& reach : wshed.second){
        std::stringstream ss;
        ss << reach.first << "_" << reach.second;
        size_t data_file = network_files.at(ss.str());
        for (const auto& var : vars)
        {
          std::string varName = mio::MeteoGrids::getParameterName(var.first);
          if (varName=="ROT")
          {
            varName="TOTAL_RO";
            loaded_data[var.first][var.second]->at(i).at(wshed_continuous_id.at(wshed.first)).at(j)=read_data[data_file](varName)/3600.;
          }
          else
          {
            loaded_data[var.first][var.second]->at(i).at(wshed_continuous_id.at(wshed.first)).at(j)=read_data[data_file](varName);
          }
        }
        ++j;
      }
    }
    ++i;
  }
  io_container.io_network_manager->clear_cache();
}

void TSReaches::loadGridData(Variables var, DataType type)
{
  std::cout << "\t[i] Reading streams cells data for var : " << mio::MeteoGrids::getParameterName(var) << " from GRIDS" << std::endl;
  #ifdef _OPENMP
  #pragma omp parallel for
  #endif
  for (size_t i=0; i<loaded_dates[var][type]->size();++i){
    const double date(loaded_dates[var][type]->at(i));
    const mio::Grid2DObject grid = grids.getGrid(date, var, DataType::GRID_POINTS);
    for(const auto& wshed : wshed_stream_cells){
      size_t j= 0;
      for(auto const& reach : wshed.second){
        size_t nx = reach.first;
        size_t ny = reach.second;
        const double gridPoint = grid(nx,ny);
        loaded_data[var][type]->at(i).at(wshed_continuous_id.at(wshed.first)).at(j)=gridPoint;
        ++j;
      }
    }
  }
}

double TSReaches::getMeanVal(const double date, size_t subw_ind, Variables var)
{
  if(variables.find(var) == variables.end())
  {
    std::cout << "variable not found" << std::endl;
  }

  const DataType type = variables[var].first;
  const size_t ind = wshed_continuous_id.at(subw_ind);
  size_t pos1,pos2=0;
  double dist1,dist2=0;
  TSContainer::getPosition(*(loaded_dates.at(var).at(type)), date, pos1, pos2, dist1, dist2);
  double mean(0);
  size_t nCells(0);
  for (size_t i=0; i <  wshed_stream_cells.at(subw_ind).size(); ++i){
    if(pos2==0){
      mean +=  loaded_data.at(var).at(type)->at(pos1).at(ind).at(i);
    }
    else{
      mean +=  (loaded_data.at(var).at(type)->at(pos1).at(ind).at(i)*dist1+loaded_data.at(var).at(type)->at(pos2).at(ind).at(i)*dist2)/(dist1+dist2);
    }
    ++ nCells;
  }
  return mean = nCells > 0 ? mean/double(nCells) : mio::IOUtils::nodata;
}



std::vector<double> TSReaches::getVectVal(const double date, size_t subw_ind, Variables var)
{
  if(variables.find(var) == variables.end())
  {
    std::cout << "variable not found" << std::endl;
    exit(0);
  }
  const DataType type = variables[var].first;
  const size_t ind = wshed_continuous_id.at(subw_ind);
  size_t pos1,pos2=0;
  double dist1,dist2=0;
  TSContainer::getPosition(*(loaded_dates.at(var).at(type)), date, pos1, pos2, dist1, dist2);
  if(pos2==0){
    return loaded_data.at(var).at(type)->at(pos1).at(ind);
  }
  else{
    std::vector<double> vect;
    vect.reserve(wshed_stream_cells.at(subw_ind).size());
    for (size_t i=0; i <  wshed_stream_cells.at(subw_ind).size(); ++i)
    {
      vect.push_back((loaded_data.at(var).at(type)->at(pos1).at(ind).at(i)*dist1+loaded_data.at(var).at(type)->at(pos2).at(ind).at(i)*dist2)/(dist1+dist2));
    }
    return vect;
  }
}


void TSReaches::initializeMeteo(size_t subw_ind, std::vector<mio::MeteoData>& data)
{
  data.clear();
  data.resize(wshed_stream_cells.at(subw_ind).size());
  for(auto const& var : variables){
    for (size_t i=0; i <  wshed_stream_cells.at(subw_ind).size(); ++i){
      const std::string& varName(mio::MeteoGrids::getParameterName(var.first));
      if(!data[i].param_exists(varName))
      {
        data[i].addParameter(varName);
      }
    }
  }
}


void TSReaches::getMeteo(const double date, size_t subw_ind, std::vector<mio::MeteoData>& data)
{
  for(auto const& var : variables){
    const DataType type = var.second.first;
    const size_t ind = wshed_continuous_id.at(subw_ind);
    size_t pos1,pos2=0;
    double dist1,dist2=0;
    TSContainer::getPosition(*(loaded_dates.at(var.first).at(type)), date, pos1, pos2, dist1, dist2);

    const std::shared_ptr< std::vector< std::vector< std::vector<double> > > > var_data(loaded_data.at(var.first).at(type));

    for (size_t i=0; i <  wshed_stream_cells.at(subw_ind).size(); ++i){
      const std::string& varName(mio::MeteoGrids::getParameterName(var.first));
       size_t iParam = data[i].getParameterIndex(varName);
      if(pos2==0){
        data[i](iParam) =var_data->at(pos1).at(ind).at(i);
      }
      else{
        data[i](iParam)= (var_data->at(pos1).at(ind).at(i)*dist1+var_data->at(pos2).at(ind).at(i)*dist2)/(dist1+dist2);
      }
    }
  }
}



void TSReaches::getMeanMeteo(const double date, size_t subw_ind, mio::MeteoData& data)
{
  data.reset();
  for(auto const& var : variables) {
    const DataType type = var.second.first;
    const size_t ind = wshed_continuous_id.at(subw_ind);
    size_t pos1,pos2=0;
    double dist1,dist2=0;
    TSContainer::getPosition(*(loaded_dates.at(var.first).at(type)), date, pos1, pos2, dist1, dist2);

    const std::string& varName(mio::MeteoGrids::getParameterName(var.first));
    if(!data.param_exists(varName))
    {
      data.addParameter(varName);
    }
    size_t iParam = data.getParameterIndex(varName);

    const std::shared_ptr< std::vector< std::vector< std::vector<double> > > > var_data(loaded_data.at(var.first).at(type));

    double mean(0);
    size_t nCells(0);
    for (size_t i=0; i <  wshed_stream_cells.at(subw_ind).size(); ++i){
      if(pos2==0){
        mean +=  var_data->at(pos1).at(ind).at(i);
      }
      else{
        mean +=  (var_data->at(pos1).at(ind).at(i)*dist1+var_data->at(pos2).at(ind).at(i)*dist2)/(dist1+dist2);
      }
      ++ nCells;
    }
    mean = nCells > 0 ? mean/double(nCells) : mio::IOUtils::nodata;
    data(iParam) = mean;
  }
}
}
