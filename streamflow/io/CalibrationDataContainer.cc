
/*****************************************************************************/
/* Copyright 2017 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/io/CalibrationDataContainer.h>
//#include <streamflow/Watershed.h>
//#include <streamflow/StreamNetwork.h>

#ifdef _OPENMP
	#include <omp.h>
#endif

//#include "TSContainer.h"


namespace sflow {

const std::string CalibrationDataContainer::_FUNCTION_KEY_PATTERN = "OBJECTIVE_FUNCTION";
const std::string CalibrationDataContainer::_MEASURES_KEY_PATTERN = "MEASUREMENT_POINT";

/**
 * @brief Destructor of class DataContainer
 */

CalibrationDataContainer::~CalibrationDataContainer()
 {}

/**
 * @brief Constructor of class DataContainer
 * @param cfg Reference to the Config instance holding the model parameters
 * @param clk Simulation clock object
 * @param wshed Watershed object (not necessarily initialized yet)
 * @param snetwork StreamNetwork object (not necessarily initialized yet)
 */

CalibrationDataContainer::CalibrationDataContainer(const mio::Config& _cfg, const mio::Date& _start_time, const mio::Date& _end_time):
																		start_time(_start_time.getJulian()),end_time(_end_time.getJulian()),
																		time_step(static_cast<double>(_cfg.get("TIME_STEP", "HydroModel"))/1440.), time_zone(_cfg.get("TIME_ZONE", "INPUT")),
																		id_to_pos(getId(_cfg)), files(getFiles(_cfg)), required_data(getRequiredData(_cfg)),available_dates(),available_data(),
																		points_coords(), loaded_dates(std::make_shared<const std::vector<double>>(getDatesToLoad())), io_managers(processCfg(_cfg)),
																		indices_calib_dates(), stations_meta(), loaded_data()

{
	loadData();
}

///////////////////////////////////////////////
////////// EXTERNAL INTERFACE /////////////////
///////////////////////////////////////////////


const mio::Coords& CalibrationDataContainer::getMeasPointCoords(const size_t& ptId) const {
	if(ptId > id_to_pos->size() || ptId == 0) {
		std::ostringstream os;
		os << "Unknown measurement point with ID number " << ptId;
		throw mio::IndexOutOfBoundsException(os.str(), AT);
	}
	size_t id = id_to_pos->at(ptId);
	return points_coords.at(id);
}

const std::vector<mio::Coords>& CalibrationDataContainer::getMeasPointCoords() const {
	return points_coords;
}

double CalibrationDataContainer::getData(size_t ptId, const HydroVariable& var, const mio::Date& date) const{
	if(ptId > id_to_pos->size() || ptId == 0) {
		std::ostringstream os;
		os << "Unknown measurement point with ID number " << ptId;
		throw mio::IndexOutOfBoundsException(os.str(), AT);
	}
	size_t id = id_to_pos->at(ptId);
	std::vector<double>::const_iterator it;
	it = std::find(loaded_dates->begin(), loaded_dates->end(), date.getJulian());
	if(it != loaded_dates->end()) {
			size_t pos = std::distance(loaded_dates->begin(), it);
			return loaded_data.at(id).at(var.toString()).at(pos);
	}
	else
	{
		throw MissingConfigKeyException("Cannot calibrate the model - missing key "
				"MEASUREMENT_POINT# in section [INPUT] of the configuration file", AT);
	}
}


const std::vector<double>& CalibrationDataContainer::getData(size_t ptId, const HydroVariable& var) const
{
 	if(ptId > id_to_pos->size() || ptId == 0) {
	 	std::ostringstream os;
	 	os << "Unknown measurement point with ID number " << ptId;
	 	throw mio::IndexOutOfBoundsException(os.str(), AT);
	}
	size_t id = id_to_pos->at(ptId);
	return loaded_data.at(id).at(var.toString());
}

const std::vector<size_t>& CalibrationDataContainer::getIndicesCalibDates(size_t ptId, const HydroVariable& var) const
{
	if(ptId > id_to_pos->size() || ptId == 0) {
	 	std::ostringstream os;
	 	os << "Unknown measurement point with ID number " << ptId;
	 	throw mio::IndexOutOfBoundsException(os.str(), AT);
	}
	size_t id = id_to_pos->at(ptId);
	return *(indices_calib_dates.at(id).at(var.toString()));
}



bool CalibrationDataContainer::getData(const mio::Date& time, const HydroVariable& var, const size_t& ptId, double& value) const
{
	if(ptId > id_to_pos->size() || ptId == 0) {
		std::ostringstream os;
		os << "Unknown measurement point with ID number " << ptId;
		throw mio::IndexOutOfBoundsException(os.str(), AT);
	}
	size_t id = id_to_pos->at(ptId);
	std::vector<double>::const_iterator it;
	it = std::find(loaded_dates->begin(), loaded_dates->end(), time.getJulian());
	if(it != loaded_dates->end()) {
			size_t pos = std::distance(loaded_dates->begin(),it);
			value = loaded_data.at(id).at(var.toString()).at(pos);

	}
	else
	{
		throw MissingConfigKeyException("Cannot calibrate the model - missing key "
				"MEASUREMENT_POINT# in section [INPUT] of the configuration file", AT);
	}
	return true;

}

const std::vector<std::vector<std::reference_wrapper < const StreamVariable> > >& CalibrationDataContainer::getRequiredVariable() const
{
	return *required_data;
}

const std::map<size_t,size_t>& CalibrationDataContainer::getIdToPos() const
{
	return *id_to_pos;
}


bool CalibrationDataContainer::getData(const mio::Date& time, const HydroVariable& var, const std::vector<size_t>& ptIds, std::vector<double>& values) const
{
	values.clear();
	values.reserve(ptIds.size());
	for(auto const ptId : ptIds)
	{
		size_t id = id_to_pos->at(ptId);
		std::vector<double>::const_iterator it;
		it = std::find(loaded_dates->begin(), loaded_dates->end(), time.getJulian());
		if(it != loaded_dates->end()) {
				size_t pos = std::distance(loaded_dates->begin(),it);
				values.push_back(loaded_data.at(id).at(var.toString()).at(pos));
		}
		else
		{
			throw MissingConfigKeyException("Cannot calibrate the model - missing key "
					"MEASUREMENT_POINT# in section [INPUT] of the configuration file", AT);
		}
	}
	return true;
}


size_t CalibrationDataContainer::getMeasPointId(const mio::Coords& pt) const {
	std::vector<mio::Coords>::const_iterator it;
	it = std::find(points_coords.begin(), points_coords.end(), pt);
	if(it == points_coords.end()) {
		std::ostringstream os;
		os << "Point with coordinates (" << pt.getEasting() << ","
	       << pt.getNorthing() << ") is not a measurement point";
		throw mio::InvalidArgumentException(os.str(), AT);
	}
	return it - points_coords.begin();
}

const std::shared_ptr< const std::vector<double> > CalibrationDataContainer::getLoadedDates() const
{
	return loaded_dates;
}

const mio::StationData& CalibrationDataContainer::getStationMeta(const size_t& ptId) const
{
	size_t id = id_to_pos->at(ptId);
	return stations_meta.at(id);
}

const std::vector<mio::StationData>& CalibrationDataContainer::getStationsMeta() const
{
	return stations_meta;
}

///////////////////////////////////////////////
////////// INTERNAL FUNCTIONS /////////////////
///////////////////////////////////////////////




std::unique_ptr<std::map<size_t,size_t>> CalibrationDataContainer::getId(const mio::Config& cfg)
{
	//CHECK FOR FILES AND POINTS, NOT CHECK FOR VAR AND WEIGHT. CAN BE ADDED HERE
	std::vector<size_t> all_files_points;
	std::vector<size_t> all_used_points;

	std::vector<std::string> keys(cfg.getKeys(_MEASURES_KEY_PATTERN,"INPUT"));
	if(keys.empty()) {
		throw MissingConfigKeyException("Cannot calibrate the model - missing key "
				"MEASUREMENT_POINT# in section [INPUT] of the configuration file", AT);
	}
	for (auto const& k : keys){
		std::stringstream sstream(k.substr(_MEASURES_KEY_PATTERN.size(), k.size()));
		size_t id;
		sstream >> id;
		if( std::find(all_files_points.begin(),all_files_points.end(),id) != all_files_points.end())
		{
			throw MissingConfigKeyException("Duplicated key !", AT);
		}
		all_files_points.push_back(id);
	}
	std::vector<std::string>  allKeys = cfg.getKeys("OBJECTIVE_FUNCTION", "CALIBRATION");
	for (auto const& k : allKeys){
		if(k.find("::") != std::string::npos){continue;}
		std::ostringstream os;
		os << k << "::points";
		std::vector<std::string> points;
		cfg.getValue(os.str(),"CALIBRATION",points);
		for(auto const& p:points)
		{
			if(p.compare("ALL")==0)
			{
				all_used_points=all_files_points;
			}
			else {
				int id_int=stoi(p);
				if (std::find(all_files_points.begin(),all_files_points.end(),id_int)==all_files_points.end()) {
					std::ostringstream err;
					err << "Invalid value " << id_int << " for key " << os.str() << ". This measurments points is not defined in the [INPUT] section.";
					throw InvalidConfigKeyException(err.str(), AT);
				}
				size_t id=static_cast<size_t>(id_int);
				if (std::find(all_used_points.begin(),all_used_points.end(),id)==all_used_points.end())
				{
					all_used_points.push_back(id);
				}
			}
		}
	}
	std::unique_ptr<std::map<size_t,size_t>> ids (new std::map<size_t,size_t>());
	size_t i=0;
	for (auto const& id : all_used_points){
		ids->insert(std::make_pair(id, i));
		++i;
	}
	return ids;
}

std::unique_ptr<std::list<std::string>> CalibrationDataContainer::getFiles(const mio::Config& cfg)
{
	std::unique_ptr<std::list<std::string>> files_ptr(new std::list<std::string>());
	std::vector<std::string> files_vect;
	cfg.getValues("MEASUREMENT_POINT", "INPUT", files_vect);

	for (auto const& id:*id_to_pos)
	{
		std::string file_name;
		std::ostringstream key;
		key << "MEASUREMENT_POINT"<<id.first;
 		cfg.getValue(key.str(), "INPUT", file_name);
		files_ptr->push_back(file_name);
	}
	return files_ptr;
}

std::unique_ptr<std::vector<std::vector<std::reference_wrapper < const StreamVariable> > > > CalibrationDataContainer::getRequiredData(const mio::Config& cfg)
{
	//CHECK FOR VARIABLES
	std::unique_ptr<std::vector<std::vector<std::reference_wrapper < const StreamVariable> > > > required_data_ptr(new std::vector<std::vector<std::reference_wrapper <const StreamVariable> > >(id_to_pos->size()));
	std::vector<std::string> allKeys;
	allKeys = cfg.getKeys("OBJECTIVE_FUNCTION", "CALIBRATION");
	for (auto const& k : allKeys){
		if(k.find("::") != std::string::npos){continue;}
		std::ostringstream os;
		os << k << "::variable";
		std::string var;
		cfg.getValue(os.str(),"CALIBRATION",var);
		try{
			const StreamVariable& v(StreamVariable::getVariable(var));
			std::ostringstream os1;
			os1 << k << "::points";
			std::vector<std::string> points;
			cfg.getValue(os1.str(),"CALIBRATION",points);
			for(auto const& p:points)
			{
				if(p.compare("ALL")==0)
				{
					for(auto& pt:*required_data_ptr)
					{
						pt.push_back(v);
					}
					break;
				}
				int id_int=stoi(p);
				size_t id=static_cast<size_t>(id_int);
				if(find_if(required_data_ptr->at(id_to_pos->at(id)).begin(), required_data_ptr->at(id_to_pos->at(id)).end(), MyClassComp(v))== required_data_ptr->at(id_to_pos->at(id)).end())
				{
					required_data_ptr->at(id_to_pos->at(id)).push_back(v);
				}
			}
		}
		catch(mio::InvalidArgumentException& e)
		{
			std::ostringstream err;
			mio::IOUtils::toUpper(var);
			err << "Invalid calibration variable " << var << " for key " << os.str() <<". " << e.what();
			throw InvalidConfigKeyException(err.str(), AT);// if no conversion could be performed
		}
	}
	return required_data_ptr;
}


bool CalibrationDataContainer::validateDates(const std::vector<double>& dates, const std::string& file) const{

	if(dates.size() == 0){
		std::cout << "[E] Variable foud but no data found in the date range" << std::endl;
		return false;
	}

	if (std::upper_bound(dates.begin(),dates.end(),start_time,[](const double& a, const double& b) {return (a-b)<(-1e-5);})== dates.begin())
	{
		std::cout << "[W] Calibration variable found in file "<< file <<" but first data found (" << mio::Date(*(dates.begin()),time_zone).toString(mio::Date::ISO) <<
									") is after the simulation or warmup start date (" << mio::Date(start_time,time_zone).toString(mio::Date::ISO) << ")" << std::endl;
	}

	if (std::lower_bound(dates.begin(),dates.end(),end_time,[](const double& a, const double& b) {return (a-b)<(-1e-5);}) == dates.end())
	{
		std::cout << "[W] Calibration variable found in file "<< file <<" but last data found (" << mio::Date(*(std::prev(dates.end())),time_zone).toString(mio::Date::ISO) <<
									") is before the simulation or warmup end date (" << mio::Date(end_time,time_zone).toString(mio::Date::ISO) << ")" << std::endl;
	}
	const double diff = (dates.back()-dates.front())/(static_cast<double>(dates.size())-1);
	if(diff > time_step){
		std::cout << "[W] The simulation time step is set to " << time_step*1440. << " minutes and the mean measurments time step for calibration is of "
							<<diff*1440. << " minutes in file " << file << ". Note that the calibration measurments will be extrapolated and the result might be incorrect." << std::endl;
	}
	return true;
}

const std::vector<double> CalibrationDataContainer::getDatesToLoad(){
	size_t tot_steps = static_cast<size_t>(std::ceil((end_time-start_time)/time_step) +1);
	std::vector<double> tmp_loaded_dates;
	tmp_loaded_dates.reserve(tot_steps);
	mio::Date date(start_time,time_zone);
	const mio::Date end(end_time,time_zone);
	while(date <= end){
		tmp_loaded_dates.push_back(date.getJulian());
		date+=time_step;
	}
	tmp_loaded_dates.shrink_to_fit();
	return tmp_loaded_dates;
}


std::unique_ptr<std::vector<mio::IOManager>> CalibrationDataContainer::processCfg(const mio::Config& _cfg)
{
	std::unique_ptr<std::vector<mio::IOManager>>_io_managers(new std::vector<mio::IOManager>());
	_io_managers->reserve(files->size());
	size_t iFile(0);
	for (auto const& f : *files){
		mio::Config cfg(_cfg);
		const std::string dir = _cfg.get("STREAM_MEASUREMENTS_PATH", "INPUT"); //< mandatory parameter
		std::list<std::string> file_tmp{f};
		try
		{
			IOContainer::parseSmetFile(dir, file_tmp, available_data[iFile], available_dates[iFile]);
		}
		catch(const std::exception& e) {
			std::cout << "[w] Error parsing SMET files for : "<< e.what() <<"[E] Calibration not posible" << std::endl;
			throw;
		}
		if(!validateDates(available_dates[iFile],f))
		{
			std::ostringstream os;
			os << "Measurments for Measurment point " << iFile+1 << " in file : " << f ;
			throw std::runtime_error(os.str());
		}
		cfg.addKey("STATION1" , "INPUT", f);
		cfg.deleteKeys("::", "FILTERS", true);
		std::string fileFormat("SMET");
		cfg.getValue("STREAM_MEASUREMENTS", "INPUT", fileFormat, mio::IOUtils::nothrow);
		cfg.addKey("METEO", "INPUT", fileFormat);
		cfg.addKey("METEOPATH", "INPUT", cfg.get("STREAM_MEASUREMENTS_PATH", "INPUT")); //< mandatory key
		_io_managers->emplace_back(cfg);
		++iFile;
	}
	return _io_managers;
}



void CalibrationDataContainer::loadData(){

	//Prepare dates container
	loaded_data.reserve(id_to_pos->size());
	for (auto const& id : *id_to_pos)
	{
		loaded_data.push_back(std::map< std::string, std::vector<double> > () );
		for (auto const& v : required_data->at(id.second))
		{
			loaded_data.at(id.second).insert(std::make_pair(v.get().toString(),std::vector< double >(loaded_dates->size(),mio::IOUtils::nodata)));
			indices_calib_dates[id.second][v.get().toString()] = std::unique_ptr< std::vector< size_t > >  (new std::vector< size_t >());
			indices_calib_dates.at(id.second).at(v.get().toString())->reserve(loaded_dates->size());
		}
	}
	//Fill station meta vector
	for (auto const& id : *id_to_pos)
	{
		std::vector<mio::MeteoData> vecMeteo;
		io_managers->at(id.second).getMeteoData(mio::Date(loaded_dates->front(),time_zone),vecMeteo);
		stations_meta.push_back(vecMeteo.front().meta);
	}

	//Load data
	size_t i=0;
	for (auto const& d : *loaded_dates){
		std::vector<mio::MeteoData> read_data;
		std::map <size_t,size_t> file_map;
		for (auto const& id : *id_to_pos)
		{
			io_managers->at(id.second).getMeteoData(mio::Date(d,time_zone), read_data);
			for (auto const& v : required_data->at(id.second))
			{
				loaded_data.at(id.second).at(v.get().toString()).at(i) = read_data[0](v.get().toString());
				if(loaded_data.at(id.second).at(v.get().toString()).at(i)!=mio::IOUtils::nodata)
				{
					indices_calib_dates.at(id.second).at(v.get().toString())->push_back(i);
				}
			}
		}
		++i;
	}

	//Store indices for which data != noData
	for (auto const& id : *id_to_pos)
	{
		for (auto const& v : required_data->at(id.second))
		{
			indices_calib_dates.at(id.second).at(v.get().toString())->shrink_to_fit();
		}
	}

	// Creat coord vector
	points_coords.resize(id_to_pos->size());
	for (auto const& id : *id_to_pos)
	{
		mio::STATIONS_SET points;
		io_managers->at(id.second).getStationData({}, points);
		points_coords.at(id.second) = points.front().position;
	}
}
}
