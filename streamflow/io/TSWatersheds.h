 /*****************************************************************************/
/* Copyright 2017 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STREAMFLOW_TSWATERSHEDS_H_
#define STREAMFLOW_TSWATERSHEDS_H_

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <typeinfo>
#include <fstream>
#include <iterator>
#include <streamflow/io/IOContainer.h>
#include <streamflow/io/TSContainer.h>





namespace sflow {
class TSGrids;
/**
 * @brief This class loads the data required from the diverse module.
 * It deremines the most suited input and load only required data (if needed
 * temp data can be loaded and thus deleted).
 * It relies on the IOContainer class to know which input data are available.
 * The data are actually stored in a TS container class.
 * This class provides the interface to streamflow to access all the required data.
 * @author Adrien Michel
 * @date 2017-08-31
 */
 class DataContainer;


class TSWatersheds : public TSContainer {
  public:
    TSWatersheds(const mio::Config& _cfg, IOContainer& _io_container,  const mio::Date _startData, const mio::Date _endData,  const std::map<size_t, mio::Grid2DObject>& _masks,
        const std::map<size_t,size_t>& _wshed_continuous_id, const wshed_stream_cells_map& _wshed_stream_cells,
        TSGrids& grids);

    double getMeanVal(const double date, size_t subw_ind, Variables var);
    void getMeanMeteo(const double date, size_t subw_ind, mio::MeteoData& data);
    double getTimeAveragedMeanVal(double startDate, const double endDate, size_t subw_ind, Variables var);

    void addData(Variables, DataType, DataSource) override;
    void prepareMemory(Variables) override;
    void loadData(Variables) override;

    ~TSWatersheds() override;

  private:
    TSGrids& grids;

    void loadGridData(Variables var, DataType type);
    void loadFromSmet(std::map<Variables, DataType> vars);

    std::map<Variables,std::map<DataType, const std::shared_ptr<const std::vector<double> > > >  loaded_dates;
    std::map<Variables,std::map<DataType, const std::unique_ptr< std::vector< std::vector<double> > > > > loaded_data;
    bool memory_resized;
    bool smet_loaded;

};


} /* namespace sflow */

#endif /*STREAMFLOW_TSWATERSHEDS_H_ */
