/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REACHCELLSREADER_H_
#define REACHCELLSREADER_H_

#include <map>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <streamflow/streamReach/StreamCell.h>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/utils/StreamflowExceptions.h>
#include <streamflow/io/DataContainer.h>

#include <meteoio/MeteoIO.h>


namespace sflow {

/**
 * @class ReachCellsReader
 * @brief This class is used to read the coordinates and physical properties of
 * the cells belonging to a given stream reach.
 * @ingroup io
 * @author Aurelien Gallice
 * @date 2015-08-05
 */
class ReachCellsReader : public SharedObject {
  public:
    ReachCellsReader() : SharedObject() {}
    virtual const std::vector<StreamCell> getReachCells(const size_t& index) const = 0;
    virtual const std::vector<size_t> getUpstreamReaches(const size_t& index) const = 0;
    virtual const std::vector<size_t> getSortedReachIndices() const = 0;
    virtual ~ReachCellsReader() {}
};


/**
 * @class TaudemReachFilesReader
 * @brief Implementation of class ReachCellsReader reading the coordinates and
 * physical properties of the cells belonging to a given stream reach by
 * parsing the standard output text files of TauDEM. Note that the TauDEM
 * documentation specifies that the ID numbers attributed to the stream reaches
 * do not necessarily correspond to the ID numbers of their associated
 * watersheds. The indices expected by the methods of the present class
 * correspond to the reach indices, as returned by the method
 * "getSortedReachIndices".
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-08-05
 */
class TaudemReachFilesReader : public ReachCellsReader {
  public:
    TaudemReachFilesReader(const mio::Config& cfg, std::shared_ptr<DataContainer> dataContainer);
    virtual const std::vector<StreamCell> getReachCells(const size_t& index) const;
    virtual const std::vector<size_t> getUpstreamReaches(const size_t& index) const;
    virtual const std::vector<size_t> getSortedReachIndices() const;
    virtual ~TaudemReachFilesReader();

  protected:
    struct ReachInfo {
      size_t i_start;
      size_t i_end;
      long int upstream_reach_1;
      long int upstream_reach_2;
      bool is_outlet_reach;
      int strahler_order;
    };
    struct CellInfo {
      CellInfo() : coords(), distance_to_outlet(), tot_drainage_area() {}
      mio::Coords coords;
      double distance_to_outlet; //< [m] distance to the catchment outlet
      double tot_drainage_area;  //< [m^2] total area drained by the point
    };

    std::shared_ptr<DataContainer> dataContainer;
    std::map<size_t, ReachInfo> _reach_infos; //< we use a map since TauDEM does not guarantee reach indices to be continuous
    std::vector<CellInfo> _cell_infos;
    std::string _coordinate_system;
    std::string _coordinate_params;

    virtual void parseTreeFile(const std::string& fileName);
    virtual void parseCoordsFile(const std::string& fileName);
    virtual size_t getOutletReachIndex() const;
    virtual void pushBackReachIndex(const size_t& index,
        std::vector<size_t>& indices) const;

    static void parseTreeFileLine(const std::string& line, size_t& reachIndex,
        ReachInfo& reachInfo);
    static void parseCoordsFileLine(const std::string& line, CellInfo& cellInfo);
    static size_t getNumLines(std::ifstream& f);
};

} /* namespace sflow */

#endif /* REACHCELLSSREADER_H_ */
