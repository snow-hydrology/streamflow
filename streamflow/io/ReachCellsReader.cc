/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/io/ReachCellsReader.h>

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class TaudemReachFilesReader
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class TaudemReachFilesReader
 * @param cfg Reference to the Config object which holds the model parameters
 */
TaudemReachFilesReader::TaudemReachFilesReader(const mio::Config& cfg, std::shared_ptr<DataContainer> dataCtr) :
    ReachCellsReader(), dataContainer(dataCtr), _reach_infos(), _cell_infos(),
    _coordinate_system(), _coordinate_params() {
  cfg.getValue("COORDSYS", "INPUT", _coordinate_system);
  cfg.getValue("COORDPARAM", "INPUT", _coordinate_params, mio::IOUtils::nothrow);
  std::string fileName;
  cfg.getValue("TAUDEM_TREE_FILE", "INPUT", fileName);
  this->parseTreeFile(fileName);
  cfg.getValue("TAUDEM_COORDS_FILE", "INPUT", fileName);
  this->parseCoordsFile(fileName);
  cfg.getValue("CATCHMENT", "INPUT", fileName);
}

/**
 * @brief Returns the cells belonging to the specified stream reach. The
 * returned cells are sorted in the streamwise direction (first element of the
 * output vector corresponds to the cell located at the upstream end of the
 * reach, and last element to the cell located at the downstream end of the
 * reach)
 * @param index ID number of the stream reach whose cells must be returned
 * @return Cells belonging to the stream reach of interest
 */
const std::vector<StreamCell> TaudemReachFilesReader::getReachCells(const size_t& index) const {
  const ReachInfo& currReach(_reach_infos.at(index));
  const size_t nCells(currReach.i_end - currReach.i_start + 1);
  std::vector<StreamCell> cells(nCells);


  //Create first cell (at the upstream end of the reach)
  const CellInfo& firstCell(_cell_infos.at(currReach.i_start));
  const CellInfo& secondCell(_cell_infos.at(currReach.i_start+1));
  double specDrainageArea(firstCell.tot_drainage_area);
  const long int usReachIds[2] = {currReach.upstream_reach_1,
      currReach.upstream_reach_2};
  for(int iReach = 0; iReach < 2; ++iReach) {
    if(usReachIds[iReach] >= 0) {
      const size_t usReachIEnd = _reach_infos.at(usReachIds[iReach]).i_end;
      specDrainageArea -= _cell_infos.at(usReachIEnd-1).tot_drainage_area;
    }
  }
  double length(0.5*(firstCell.distance_to_outlet - secondCell.distance_to_outlet));
  double slope((firstCell.coords.getAltitude() - secondCell.coords.getAltitude())/
      (2.0*length));
  StreamCell cell(firstCell.coords, dataContainer->getDem(), specDrainageArea,
      firstCell.tot_drainage_area, length, slope);
  cells[0] = cell;

  //Create the middle cells
  for(size_t iCell = currReach.i_start + 1; iCell < currReach.i_end; ++iCell) {
    const CellInfo& prevCell(_cell_infos.at(iCell-1));
    const CellInfo& currCell(_cell_infos.at(iCell));
    const CellInfo& nextCell(_cell_infos.at(iCell+1));
    specDrainageArea = currCell.tot_drainage_area - prevCell.tot_drainage_area;
    length = 0.5*(prevCell.distance_to_outlet - nextCell.distance_to_outlet);
    slope = (currCell.coords.getAltitude() - nextCell.coords.getAltitude())/
        (currCell.distance_to_outlet - nextCell.distance_to_outlet);
    cell.set(currCell.coords,dataContainer->getDem(), specDrainageArea, currCell.tot_drainage_area,
        length, slope);
    cells[iCell-currReach.i_start] = cell;
  }

  //Create last cell (at the downstream end of the reach)
  const CellInfo& penultCell(_cell_infos.at(currReach.i_end - 1));
  const CellInfo& lastCell(_cell_infos.at(currReach.i_end));
  const double totDrainageArea(currReach.is_outlet_reach ?
      lastCell.tot_drainage_area : penultCell.tot_drainage_area);
  specDrainageArea = totDrainageArea - penultCell.tot_drainage_area; //< zero except for the last cell of the outlet reach
  length = 0.5*(penultCell.distance_to_outlet - lastCell.distance_to_outlet);
  slope  = ((penultCell.coords.getAltitude() - lastCell.coords.getAltitude())/
      (2.0*length));
  cell.set(lastCell.coords, dataContainer->getDem(), specDrainageArea, totDrainageArea, length, slope);
  cells[nCells-1] = cell;

  return cells;
}

/**
 * @brief Returns the indices of the stream reaches which discharge into the
 * reach of interest (empty vector if the reach of interest is located at the
 * upstream end of a branch of the network).
 * @param index ID number of the stream reach of interest
 * @return ID numbers of the stream reaches which are located directly upstream
 * of the stream reach of interest
 */
const std::vector<size_t> TaudemReachFilesReader::getUpstreamReaches(const size_t& index) const {
  std::vector<size_t> upstreamReaches;
  const ReachInfo& info(_reach_infos.at(index));
  if(info.upstream_reach_1 != -1)
    upstreamReaches.push_back(info.upstream_reach_1);
  if(info.upstream_reach_2 != -1)
    upstreamReaches.push_back(info.upstream_reach_2);
  return upstreamReaches;
}

/**
 * @brief Returns the indices of the stream reaches, sorted according to their
 * respective positions within the stream network. The beginning of the
 * returned vector corresponds to the headwater reaches, and the last element
 * of the vector corresponds to the stream reach located at the watershed
 * outlet. This sorting is necessary for the class StreamNetwork to know which
 * stream reaches should be updated in time before which other reaches.
 * @return Sorted reach indices
 */
const std::vector<size_t> TaudemReachFilesReader::getSortedReachIndices() const {
  std::vector<size_t> indices;
  indices.reserve(_reach_infos.size());
  this->pushBackReachIndex(this->getOutletReachIndex(), indices);
  std::reverse(indices.begin(), indices.end());
  return indices;
}

/**
 * @brief Destructor of class TaudemReachFilesReader
 */
TaudemReachFilesReader::~TaudemReachFilesReader() {}

/**
 * @brief Method parsing the TauDEM file describing the stream network
 * connectivity (see parameter TAUDEM_TREE_FILE in the configuration file)
 * @param fileName Path (can be relative) to the TauDEM file
 */
void TaudemReachFilesReader::parseTreeFile(const std::string& fileName) {
  std::ifstream f(fileName.c_str());
  if(f) {
    std::string line;
    size_t lineNumber(1);
    size_t reachIndex;
    ReachInfo info;
    std::ostringstream os;
    while(std::getline(f, line)) {
      try {
        parseTreeFileLine(line, reachIndex, info);
      } catch (const std::exception& e) {
        os << "Error parsing file \"" << fileName << "\" at line " <<
            lineNumber << ": " << e.what();
        throw mio::InvalidFormatException(os.str(), AT);
      }
      _reach_infos[reachIndex] = info;
      ++lineNumber;
    }
  } else {
    throw mio::AccessException("Cannot open file \"" + fileName + "\"", AT);
  }
}

/**
 * @brief Method parsing the TauDEM file containing the coordinates of the
 * stream cells (see parameter TAUDEM_COORDS_FILE in the configuration file)
 * @param fileName Path (can be relative) to the TauDEM file
 */
void TaudemReachFilesReader::parseCoordsFile(const std::string& fileName) {
  std::ifstream f(fileName.c_str());
  if(f) {
    _cell_infos.reserve(getNumLines(f));
    std::string line;
    size_t lineNumber(1);
    CellInfo info;
    info.coords.setProj(_coordinate_system, _coordinate_params);
    std::ostringstream os;
    while(std::getline(f, line)) {
      try {
        parseCoordsFileLine(line, info);
      } catch (const std::exception& e) {
        os << "Error parsing file \"" << fileName << "\" at line " <<
            lineNumber << ": " << e.what();
        throw mio::InvalidFormatException(os.str(), AT);
      }
      _cell_infos.push_back(info);
      ++lineNumber;
    }
  } else {
    throw mio::AccessException("Cannot open file \""+ fileName +"\"", AT);
  }
}

/**
 * @brief Returns the ID number of the stream reach located at the watershed
 * outlet
 * @return ID number of the outlet reach
 */
size_t TaudemReachFilesReader::getOutletReachIndex() const {
  size_t index(0);
  for(std::map<size_t,ReachInfo>::const_iterator it = _reach_infos.begin();
      it != _reach_infos.end(); ++it) {
    if(it->second.is_outlet_reach) {
      index = it->first;
    }
  }
  return index;
}

/**
 * @brief Inserts the index of the specified stream reach at the end of the
 * provided vector. The indices of the stream reaches located upstream of the
 * specified reach are also added iteratively in the order they appear when
 * going up the stream network counter-streamwise.
 * @param index Index of the stream network to be inserted in the vector
 * @param indices Vector in which the stream network index (and the indices of
 * the upstream reaches) should be inserted
 */
void TaudemReachFilesReader::pushBackReachIndex(const size_t& index,
    std::vector<size_t>& indices) const {
  indices.push_back(index);
  const ReachInfo& info(_reach_infos.at(index));
  if(info.upstream_reach_1 != -1)
    this->pushBackReachIndex(info.upstream_reach_1, indices);
  if(info.upstream_reach_2 != -1)
    this->pushBackReachIndex(info.upstream_reach_2, indices);
}

/**
 * @brief Parses one line of the file holding information about the stream
 * network connectivity
 * @param[in] line Line contents
 * @param[out] reachIndex ID number of the stream reach whose connectivity is
 * described by the current line
 * @param[out] reachInfo Information regarding the reach connectivity
 */
void TaudemReachFilesReader::parseTreeFileLine(const std::string& line,
    size_t& reachIndex, ReachInfo& reachInfo) {
  if(line.empty())
    throw std::invalid_argument("empty line");
  int downstreamReach;
  std::istringstream is(line);
  is >> reachIndex >> reachInfo.i_start >> reachInfo.i_end >> downstreamReach;
  is >> reachInfo.upstream_reach_1 >> reachInfo.upstream_reach_2;
  is >> reachInfo.strahler_order;
  reachInfo.is_outlet_reach = downstreamReach < 0;
}

/**
 * @brief Parses one line of the file holding information about the stream cells
 * @param[in] line Line contents
 * @param[out] info Information regarding the stream cell described in the
 * current line
 */
void TaudemReachFilesReader::parseCoordsFileLine(const std::string& line, CellInfo& info) {
  if(line.empty())
    throw std::invalid_argument("empty line");
  double easting, northing, altitude;
  std::istringstream is(line);
  is >> easting >> northing >> info.distance_to_outlet >> altitude;
  is >> info.tot_drainage_area;
  info.coords.setXY(easting, northing, altitude);
}

/**
 * @brief Returns the total number of lines in the given file
 * @param f Input stream to the file of interest
 * @return Number of lines in the file
 */
size_t TaudemReachFilesReader::getNumLines(std::ifstream& f) {
  const std::streamoff currPos = f.tellg();
  f.seekg(0, f.beg);
  size_t numLines(0);
  std::string line;
  while(getline(f, line))
    ++numLines;
  f.clear();
  f.seekg(currPos);
  return numLines;
}

} /* namespace sflow */
