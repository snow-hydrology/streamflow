
/*****************************************************************************/
/* Copyright 2017 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */
#include <streamflow/io/TSGrids.h>

namespace sflow {

TSGrids::~TSGrids() {}

TSGrids::TSGrids(const mio::Config& _cfg, IOContainer& _io_container, const mio::Date _startData, const mio::Date _endData,
         const std::map<size_t, mio::Grid2DObject>& _masks, const std::map<size_t,size_t>& _wshed_continuous_id, const wshed_stream_cells_map& _wshed_stream_cells):
  TSContainer(_cfg, _io_container, _startData, _endData, _masks, _wshed_continuous_id, _wshed_stream_cells),tmp_variables(),loaded_dates(),loaded_data()
{
}

////INTERFACE////

void TSGrids::getVal(const double date, size_t nx, size_t ny, Variables var){
  std::cout << "Looking for: " << mio::MeteoGrids::getParameterName(var) << " at: " << date << std::endl;
  std::vector<double>::const_iterator pos = std::find(loaded_dates.at(var).at(GRID_POINTS)->begin(),
                                                loaded_dates.at(var).at(GRID_POINTS)->end(), date);
  if(pos == loaded_dates[var][GRID_POINTS]->end()){
    std::cout << "[E] Asking for non existing grid" << std::endl;
  }
  size_t position = std::distance(loaded_dates.at(var).at(GRID_POINTS)->begin(), pos);
  std::cout << std::setprecision(3) << loaded_data.at(var).at(GRID_POINTS)->at(position)(nx,ny) << std::endl;
}




const mio::Grid2DObject TSGrids::getGrid(const double date, Variables var, DataType type)
{
  size_t pos1,pos2=0;
  double dist1,dist2=0;
  TSContainer::getPosition(*(loaded_dates.at(var).at(type)), date, pos1, pos2, dist1, dist2);
  if(pos2==0){
    return loaded_data.at(var).at(GRID_POINTS)->at(pos1);
  }
   else{
    return (loaded_data.at(var).at(GRID_POINTS)->at(pos1)*dist1+loaded_data.at(var).at(type)->at(pos2)*dist2)/(dist1+dist2);
  }
}


const std::shared_ptr<const std::vector<double>> TSGrids::getLoadedDates(Variables var, DataType type) const
{
  return loaded_dates.at(var).at(type);
}

///INTERNAL LOADING////////

void TSGrids::addTmpData(Variables var, DataType type, DataSource source)
{
  tmp_variables[var]=std::make_pair(type, source);
  addData(var, type, source);
}

void TSGrids::clearTmpData(Variables var_in)
{
  if (variables.find(var_in) == variables.end())
  {
    return;
  }
  std::pair<DataType, DataSource> var = variables[var_in];
  std::cout << "\t[i] Clearing memory of tmp GRIDS for var: " << mio::MeteoGrids::getParameterName(var_in) << std::endl;
  loaded_dates[var_in].erase(var.first);
  loaded_data[var_in].erase(var.first);
}

void TSGrids::prepareMemory(Variables var_in)
{
  if (variables.find(var_in) == variables.end())
  {
    return;
  }
  std::pair<DataType, DataSource> var = variables[var_in];
  if(var.second==GRID){
    std::cout << "\t[i] Reserving memory for GRIDS for var: " << mio::MeteoGrids::getParameterName(var_in) << std::endl;
    loaded_dates[var_in].insert(std::make_pair(var.first, io_container.getAvailableDates(var_in,var.second)));
    size_t size_of_data = loaded_dates[var_in][var.first]->size();
    loaded_data[var_in].insert(std::make_pair(var.first, std::unique_ptr<std::vector< mio::Grid2DObject > > (new std::vector<mio::Grid2DObject>(size_of_data))));
  }
}

void TSGrids::addData(Variables var, DataType type, DataSource source){
  variables[var]=std::make_pair(type, source);
}

void TSGrids::loadData(Variables var_in){
  if (variables.find(var_in) == variables.end())
  {
    return;
  }
  std::pair<DataType, DataSource> var = variables[var_in];
  loadData(var_in, var.first);
}

void TSGrids::loadData(Variables var, DataType type)
{
  std::cout << "\t[i] Loading GRIDS for var: " << mio::MeteoGrids::getParameterName(var) << std::endl;
  bool error=false;
  std::string err_date;
  const std::string varname = (mio::MeteoGrids::getParameterName(var)=="TSOIL"
                                && cfg.keyExists("SOIL_GRIDS_NAME", "INPUT") ?
                                cfg.get("SOIL_GRIDS_NAME", "INPUT") :
                                mio::MeteoGrids::getParameterName(var));

  const std::string grid_type= cfg.get("GRID2D", "INPUT","ARC");

  if(grid_type == "ARC"){
    #ifdef _OPENMP
    #pragma omp parallel shared(error,err_date,var)
    #endif
    {
      mio::IOManager tmp_manager(io_container.io_grid_manager->getConfig());
      mio::DEMObject dem_grid;
      tmp_manager.readDEM(dem_grid);

      #ifdef _OPENMP
      #pragma omp for
      #endif
      for (size_t i=0; i<loaded_dates[var][type]->size();++i){
        if(error) continue;
        const mio::Date date(loaded_dates[var][type]->at(i),time_zone);
        if(grid_type == "ARC"){
          const std::string grid_option = varname + "@" + date.toString(mio::Date::ISO);
          tmp_manager.read2DGrid(loaded_data[var][type]->at(i), grid_option);
        }
        if(!dem_grid.isSameGeolocalization(loaded_data[var][type]->at(i)))
        {
          #ifdef _OPENMP
          #pragma omp critical
          #endif
          {
            error=true;
            err_date=date.toString(mio::Date::ISO);
          }
        }
        if(var == Variables::ROT)
        {
          loaded_data[var][type]->at(i) *= (loaded_data[var][type]->at(i).cellsize)*(loaded_data[var][type]->at(i).cellsize)/3600./1000.;
        }
      }
    }
    if(error)
    {
      std::ostringstream msg;
      msg << "Grid for var " << mio::MeteoGrids::getParameterName(var) << " at date " << err_date << " is different from DEM. Quitting now.";
      throw RuntimeException(msg.str(),AT);
    }
  }
  else{
    mio::IOManager tmp_manager(io_container.io_grid_manager->getConfig());
    mio::DEMObject dem_grid;
    tmp_manager.readDEM(dem_grid);
    for (size_t i=0; i<loaded_dates[var][type]->size();++i){
      const mio::Date date(loaded_dates[var][type]->at(i),time_zone);
      tmp_manager.read2DGrid(loaded_data[var][type]->at(i), var, date);
      if(!dem_grid.isSameGeolocalization(loaded_data[var][type]->at(i)))
      {
        std::ostringstream msg;
        msg << "Grid for var " << mio::MeteoGrids::getParameterName(var) << " at date " << date.toString(mio::Date::ISO) << " is different from DEM. Quitting now.";
        throw RuntimeException(msg.str(),AT);
      }
      if(var == Variables::ROT)
      {
        loaded_data[var][type]->at(i) *= (loaded_data[var][type]->at(i).cellsize)*(loaded_data[var][type]->at(i).cellsize)/3600./1000.;
      }
    }
  }
}
}
