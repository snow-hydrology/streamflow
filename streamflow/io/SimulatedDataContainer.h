/*****************************************************************************/
/* Copyright 2017 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STREAMFLOW_SIMULATEDDATACONTAINER_H_
#define STREAMFLOW_SIMULATEDDATACONTAINER_H_

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <typeinfo>
#include <fstream>

#include <streamflow/utils/HydroVariable.h>
#include <streamflow/io/CalibrationDataContainer.h>


namespace sflow {
class Watershed;
class StreamNetwork;

class SimulatedDataContainer {
  public:
    SimulatedDataContainer(const mio::Config& _cfg, const mio::Date& _start_time, const mio::Date& _end_time, const CalibrationDataContainer& _measures_reader);
    SimulatedDataContainer(const SimulatedDataContainer& _source );
    SimulatedDataContainer& operator=(const SimulatedDataContainer&) = delete;

    double getData(size_t ptId, const HydroVariable& var, const mio::Date& date) const;
    const std::vector<double>& getData(size_t ptId, const HydroVariable& var) const;

    const mio::Coords& getMeasPointCoords(const size_t& ptId) const;
    const std::vector<mio::Coords>& getMeasPointCoords() const;
    size_t getMeasPointId(const mio::Coords& pt) const;
    bool getData(const mio::Date& time, const HydroVariable& var,
        const size_t& ptId, double& value);
    bool getData(const mio::Date& time, const HydroVariable& var,
        const std::vector<size_t>& ptIds, std::vector<double>& values);
    bool update(const mio::Date& currTime, const Watershed& watershed, const StreamNetwork& network);

    const std::vector<double>& getRunDates() const;
    const std::vector< std::map< std::string, std::vector<double> > > & getRunData() const;

    void reset();

    ~SimulatedDataContainer();

  private:
    const double start_time;
    const double end_time;
    const double time_step;
    const double time_zone;
    const std::shared_ptr< const std::vector<double> > run_dates;
    const std::map<size_t,size_t> id_to_pos;
    const std::vector<std::vector<std::reference_wrapper < const StreamVariable> > > required_data;
    const std::vector<mio::Coords> points_coords;
    const std::vector<mio::StationData> stations_meta;

    std::vector<size_t> _subwatershed_indices;
    std::vector<size_t> _cell_indices;
    std::vector< std::map< std::string, std::vector<double> > > run_data;

    bool isSetUp;

};

} /* namespace sflow */

#endif /* STREAMFLOW_DATACONTAINER_H_ */
