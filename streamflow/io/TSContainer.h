 /*****************************************************************************/
/* Copyright 2017 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STREAMFLOW_TSCONTAINER_H_
#define STREAMFLOW_TSCONTAINER_H_

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <typeinfo>
#include <fstream>
#include <iterator>
#include <streamflow/io/IOContainer.h>
#include <streamflow/utils/GridOperations.h>




namespace sflow {

/**
 * @brief This class loads the data required from the diverse module.
 * It deremines the most suited input and load only required data (if needed
 * temp data can be loaded and thus deleted).
 * It relies on the IOContainer class to know which input data are available.
 * The data are actually stored in a TS container class.
 * This class provides the interface to streamflow to access all the required data.
 * @author Adrien Michel
 * @date 2017-08-31
 */
 class DataContainer;


class TSContainer {
  public:
    typedef std::map<size_t, std::vector< std::pair<size_t,size_t> > > wshed_stream_cells_map;

    TSContainer(const mio::Config& _cfg, IOContainer& _io_container, const mio::Date startData, const mio::Date endData,
          const std::map<size_t, mio::Grid2DObject>& _masks, const std::map<size_t,size_t>& _wshed_continuous_id , const wshed_stream_cells_map& _wshed_stream_cells);
    virtual ~TSContainer();

    virtual void loadData(Variables) = 0;
    virtual void addData(Variables, DataType, DataSource) = 0;
    virtual void prepareMemory(Variables) = 0;

  protected:

    const mio::Config& cfg;
    IOContainer& io_container;

    const double startData;
    const double endData;
    const double time_step;
    const double time_zone;
    void error_fct(const std::string& func);
    std::map<Variables,std::pair<DataType, DataSource> > variables;

    const std::map<size_t, mio::Grid2DObject>& masks;
    const std::map<size_t,size_t>& wshed_continuous_id;
    const wshed_stream_cells_map& wshed_stream_cells;

    void getPosition(const std::vector<double>& dates, const double date, size_t& pos1, size_t& pos2, double& dist1, double& dist2);


};



} /* namespace sflow */

#endif /*STREAMFLOW_TSCONTAINER_H_ */
