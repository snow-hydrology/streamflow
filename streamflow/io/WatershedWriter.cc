/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/io/WatershedWriter.h>

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class WatershedWriter
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class WatershedWriter
 * @param cfg Reference to the Config object holding the simulation parameters
 * @param watershed Reference to the Watershed instance whose state should be
 * written using the current object
 */
WatershedWriter::WatershedWriter(const mio::Config& cfg, const Watershed& watershed) :
    _output_subwatershed_ids(getOutputSubwatershedIds(cfg, watershed)),
    _output_variables(getOutputVariables(cfg)) {}


/**
 * @brief Destructor of class WatershedWriter
 */
WatershedWriter::~WatershedWriter() {}


/**
 * @brief Returns the ID numbers of the subwatersheds whose associated state
 * variables should be written in the output file(s)
 * @param cfg Reference to the Config object holding the simulation parameters
 * @param watershed Reference to the Watershed instance whose state should be
 * written using the current object
 * @return ID numbers of the subwatersheds of interest
 */
std::vector<size_t> WatershedWriter::getOutputSubwatershedIds(const mio::Config& cfg,
    const Watershed& watershed) {
  std::string indices;
  cfg.getValue("WATERSHED::INDICES", "OUTPUT", indices, mio::IOUtils::nothrow);

  std::vector<size_t> output;
  if(indices.empty() || mio::IOUtils::strToLower(indices) == "all") {
    output = watershed.getSubwatershedIndices();
  } else {
    std::vector<double> tmp;
    mio::IOUtils::readLineToVec(indices, tmp);
    output.reserve(tmp.size());
    for(const double& index : tmp) {
      if(!watershed.containsSubwatershed(size_t(index))) {
        std::ostringstream os;
        os << "Unknown subwatershed index " << size_t(index)
           << ". Please modify the value of key WATERSHED::INDICES in "
           << "section [OUTPUT] of the config file";
        throw InvalidConfigKeyException(os.str(), AT);
      }
      output.push_back(size_t(index));
    }
  }

  return output;
}


/**
 * @brief Returns the variables which should be written in the output file(s)
 * @param cfg Reference to the Config object holding the simulation parameters
 * @return Variables to write in the output file(s)
 */
std::vector<const SubwatershedVariable*> WatershedWriter::getOutputVariables(const mio::Config& cfg) {
  std::vector<std::string> variables;
  cfg.getValue("WATERSHED::VARIABLES", "OUTPUT", variables, mio::IOUtils::nothrow);

  std::vector<const SubwatershedVariable*> output;
  if(variables.empty()) {
    output = {&SubwatershedVariable::Q_outlet, &SubwatershedVariable::T_outlet};
  } else {
    output.reserve(variables.size());
    for(const std::string& name : variables)
      output.push_back(&SubwatershedVariable::getVariable(name));
  }

  return output;
}


//-----------------------------------------------------------------------------
// Implementation of class LumpedWatershedSMETWriter
//-----------------------------------------------------------------------------

const std::string LumpedWatershedSMETWriter::_FILE_NAME_PREFIX("subwatershed");
const std::string LumpedWatershedSMETWriter::_FILE_NAME_EXTENSION(".smet");

/**
 * @brief Constructor of class LumpedWatershedSMETWriter
 * @param cfg Reference to the Config object holding the simulation parameters
 * @param watershed Reference to the Watershed instance whose state should be
 * written using the current object
 */
LumpedWatershedSMETWriter::LumpedWatershedSMETWriter(const mio::Config& cfg,
    const Watershed& watershed) :
        WatershedWriter(cfg, watershed), _output_path(getOutputPath(cfg))
{
  const double timeZone = cfg.get("TIME_ZONE", "OUTPUT");
  for(const size_t& index : _output_subwatershed_ids)
    this->initializeFile(watershed.getSubwatershed(index), timeZone);
}


/**
 * @brief Writes the current state (outlet discharge and optionally outlet
 * temperature) of each subwatershed into its corresponding SMET file
 * @param currTime Current time
 * @param watershed Watershed containing the subwatersheds whose state is to
 * be written in the output files
 */
void LumpedWatershedSMETWriter::writeState(const mio::Date& currTime,
    const Watershed& watershed) const {
  for(const size_t& index : _output_subwatershed_ids)
    this->updateFile(currTime, watershed.getSubwatershed(index));
}


/**
 * @brief Destructor of class LumpedWatershedSMETWriter
 */
LumpedWatershedSMETWriter::~LumpedWatershedSMETWriter() {}


/**
 * @brief This method writes the header of the output SMET file associated with
 * the subwatershed passed as input argument
 * @param sd Subwatershed whose associated output file should be initialized
 * @param timeZone Time zone of the time values written in the output file
 */
void LumpedWatershedSMETWriter::initializeFile(const Subwatershed& sd,
    const double& timeZone) const {
  const std::string filename(getFileName(sd));
  std::ofstream smetOut(filename);
  if(smetOut.fail())
    throw mio::AccessException(filename, AT);

  smetOut << "SMET 1.1 ASCII\n";
  smetOut << "[HEADER]\n";
  smetOut << "station_id   = "              << sd.getIndex() << "\n";
  smetOut << "station_name = subwatershed_" << sd.getIndex() << "\n";
  smetOut << std::left << std::fixed << std::setprecision(8);
  const mio::Coords& llCorner(sd.getMask().llcorner);
  smetOut << "latitude     = " << llCorner.getLat() << "\n";
  smetOut << "longitude    = " << llCorner.getLon() << "\n";
  smetOut << std::setprecision(2);
  smetOut << "altitude     = " << llCorner.getAltitude() << "\n";
  smetOut << "area         = " << sd.getArea()*1e-6 << "\n";
  smetOut << "comment      = area is in km^2\n";
  smetOut << std::right << std::fixed << std::setprecision(1);
  smetOut << "nodata       = " << mio::IOUtils::nodata << "\n";
  smetOut << "tz           = " << timeZone << "\n";
  smetOut << "fields       = timestamp";
  for(const SubwatershedVariable *var : _output_variables) {
    smetOut << " " << mio::IOUtils::strToLower(var->toString());
  }
  smetOut << "\nunits        = none";
  for(const SubwatershedVariable *var : _output_variables) {
    smetOut << " " << var->getUnits();
  }
  smetOut << "\n[DATA]\n";

  smetOut.close();
}


/**
 * @brief This method writes the outlet discharge and outlet temperature of the
 * given subwatershed in its associated output SMET file
 * @param currTime Current time
 * @param sd Subwatershed of interest
 */
void LumpedWatershedSMETWriter::updateFile(const mio::Date& currTime, const Subwatershed& sd) const {
  const std::string filename(getFileName(sd));
  std::ofstream smetOut(filename, std::ofstream::app);
  if(smetOut.fail())
    throw mio::AccessException(filename, AT);

  smetOut.fill(' ');
  smetOut << std::right << std::fixed;
  smetOut << currTime.toString(mio::Date::ISO) << " ";
  smetOut << std::setprecision(3);
  for(const SubwatershedVariable *var : _output_variables) {
    smetOut << " " << std::setw(10) << sd.get(*var);
  }
  smetOut << "\n";
  smetOut.close();
}


/**
 * @brief Returns the name of the output SMET file associated with the given
 * subwatershed
 * @param sd Subwatershed of interest
 * @return Name of the output file associated with the subwatershed of interest
 */
std::string LumpedWatershedSMETWriter::getFileName(const Subwatershed& sd) const {
  std::ostringstream os;
  os << _output_path << "/" << _FILE_NAME_PREFIX << sd.getIndex()
     << _FILE_NAME_EXTENSION;
  return os.str();
}


/**
 * @brief Returns the path to the folder in which the output files should be
 * written
 * @param cfg Reference to the Config object holding the simulation parameters
 * @return Folder in which the output files should be written
 */
std::string LumpedWatershedSMETWriter::getOutputPath(const mio::Config& cfg) {
  std::string path;
  cfg.getValue("WATERSHED::PATH", "OUTPUT", path, mio::IOUtils::nothrow);
  if(path.empty()) {
    cfg.getValue("STREAM::PATH", "OUTPUT", path, mio::IOUtils::nothrow);
    if(path.empty())
      throw MissingConfigKeyException("At least one of the two keys WATERSHED::PATH "
          "or STREAM::PATH has to be specified in section [OUTPUT] of the "
          "configuration file", AT);
  }
  return path;
}

} /* namespace sflow */
