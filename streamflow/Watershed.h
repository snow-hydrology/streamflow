/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WATERSHED_H_
#define WATERSHED_H_

#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include <meteoio/MeteoIO.h>
#include <streamflow/StreamNetwork.h>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/utils/StreamflowExceptions.h>
#include <streamflow/io/DataContainer.h>
#include <streamflow/calibration/ParameterSet.h>
#include <streamflow/subwatershed/Subwatershed.h>
#include <streamflow/subwatershed/LumpedSubwatershed.h>
#include <streamflow/subwatershed/LumpedSubwatershedDecorator.h>

namespace sflow {

class StreamNetwork; //< forward declaration to avoid cyclic includes

/**
 * @class Watershed
 * @brief This class represents a watershed, composed of multiple subwatersheds.
 * It provides facilities to handle all subwatersheds at the same time, such as
 * evolving them in time.
 * The watershed parameters correspond to the collection of all the parameters
 * of every subwatershed (plus potentially additional parameters). In order to
 * distinguish between the parameters of the different subwatersheds, each
 * parameter name is prepended with the prefix "SUBWATERSHED[i]_", where [i]
 * denotes the index of the subwatershed it is associated with. Additional
 * parameters which are not specific to any subwatershed are not renamed.
 * @author Aurelien Gallice
 * @date 2015-07-14
 */
class Watershed: public ParametrizableObject, public TimeEvolvingObject,
    public DiscretizedObject {
  public:
    class const_iterator;

    Watershed(const mio::Config& _cfg, const Clock& _clk,
        std::shared_ptr<DataContainer> dataContainer);
    Watershed(const Watershed& _watershed, const mio::Config& _cfg, const Clock& _clk,
        std::shared_ptr<DataContainer> dataContainer);

    //Watershed(const Watershed&) = delete;
    Watershed& operator=(const Watershed&) = delete;
    void initialize();

    virtual bool containsSubwatershed(const size_t& index) const;
    virtual const Subwatershed& getSubwatershed(const size_t& index) const;
    virtual const std::vector<size_t>& getSubwatershedIndices() const;
    virtual const size_t& getNumSubwatersheds() const;
    virtual const_iterator begin() const;
    virtual const_iterator end() const;
    virtual const bool& computesTemperature() const;
    virtual const double& getArea() const;
    virtual Discretization getDiscretization() const;
    virtual ParameterSet getParams() const;
    virtual void setParams(const ParameterSet&);
    virtual void setStreamNetwork(const StreamNetwork& network);
    virtual void advanceInTime();
    virtual void reset();
    virtual void setDisplayInfo(const bool& display);
    virtual bool localizePoint(mio::Coords point, size_t& subwatershedId) const;
    virtual ~Watershed();

  protected:
    std::vector<SharedObject*> _shared_objects;        //< contains the objects which are referenced to by all the subwatersheds
    std::vector<ParametrizableObject*> _shared_params; //< points to the objects in _shared_objects which are ParametrizableObject objects
    std::vector<Subwatershed*> _subwatersheds;
    std::vector<size_t> _subwatershed_indices;
    size_t _n_subwatersheds;                           //< number of subwatersheds, to avoid repeating calls to _subwatersheds.size()
    const bool _computes_temp;                         //< true if the temperature of the subwatershed's outflow discharge is computed
    double _area;                                      //< basin area in [km^2]
    bool _is_set;                                      //< becomes true after call to setStreamNetwork

    static const std::string _PARAMETER_PREFIX; //< prefix to prepend to the names of the subwatershed parameters

    virtual void computeSubwatershedIndices(const mio::Grid2DObject& grid);

    virtual void computeWatershedArea(const mio::Grid2DObject& grid);

    virtual void createSubwatersheds(const mio::Config& cfg,
        const mio::Grid2DObject& grid, std::shared_ptr<DataContainer> dataContainer);

    void copySubwatersheds(const Watershed& watershed, const mio::Config& cfg,
        const Clock& clk, std::shared_ptr<DataContainer> dataContainer);

    virtual void createTwoVerticalLinearReservoirs(const mio::Config& cfg,
        const mio::Grid2DObject& grid, std::shared_ptr<DataContainer> dataContainer);

    void copyTwoVerticalLinearReservoirs(const mio::Config& cfg,
        const Clock& clk, const Watershed& _watershed, std::shared_ptr<DataContainer> dataContainer);

    virtual void applyEnergyBalanceDecorator(const mio::Config& cfg,
        std::shared_ptr<DataContainer> dataContainer);

    virtual void applySoilTemperatureDecorator(const mio::Config& cfg,
        std::shared_ptr<DataContainer> dataContainer);

    virtual void applyHSPFTemperatureDecorator(const mio::Config& cfg,
        std::shared_ptr<DataContainer> dataContainer);

    static std::string getTemperatureModelName(const mio::Config& cfg);

    static const StreamReach& identifyStreamReach(const Subwatershed& sub,
        const StreamNetwork& network);
};


/**
 * @class Watershed::const_iterator
 * @brief This class behaves as an iterator operating on Watershed objects.
 * It can be used to access the Subwatersheds contained in the Watershed
 * in an iterative manner, and allows the use of the range-based for-loop
 * introduced in c++11.
 * @author Aurelien Gallice
 * @date 2015-09-10
 */
class Watershed::const_iterator {
  public:
    const_iterator(const Watershed& watershed, const size_t& pos);
    const_iterator(const const_iterator& copy);
    virtual const_iterator& operator=(const const_iterator& copy);
    virtual const Subwatershed& operator*() const;
    virtual const Subwatershed* operator->() const;
    virtual const_iterator& operator++();
    virtual const_iterator operator+(const size_t& offset) const;
    virtual const_iterator operator-(const size_t& offset) const;
    virtual bool operator!=(const const_iterator& other) const;
    virtual ~const_iterator();

  protected:
    const Watershed *_watershed;
    size_t _curr_pos;
};

} /* namespace sflow */

#endif /* WATERSHED_H_ */
