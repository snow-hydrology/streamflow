/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STREAMFLOW_STREAMFLOWCONFIG_H_
#define STREAMFLOW_STREAMFLOWCONFIG_H_

#include <iostream>
#include <string>
#include <meteoio/MeteoIO.h>

namespace sflow {

/**
 * @brief This class extends class meteoio::Config for the purpose of StreamFlow.
 * In particular, it copies some keys located in section [Output] and used in the
 * Alpine3D simulation into section [InputFromA3D].
 * @author Aurelien Gallice
 * @date 2016-02-15
 */
class StreamflowConfig : public mio::Config {
	public:
		StreamflowConfig();
		StreamflowConfig(const std::string& filename);
		virtual void addFile(const std::string& filename);
		virtual ~StreamflowConfig();

	protected:
		virtual void copyUsefulKeysInA3DInputSection();
};

} /* namespace sflow */

#endif /* STREAMFLOW_STREAMFLOWCONFIG_H_ */
