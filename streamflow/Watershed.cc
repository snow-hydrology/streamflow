/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/Watershed.h>

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class Watershed
//-----------------------------------------------------------------------------

const std::string Watershed::_PARAMETER_PREFIX = "SUBWATERSHED";

/**
 * @brief Constructor of class Watershed, initializing all attributes except
 * _subwatersheds (to be initialized by the subclasses)
 * @param cfg Reference to the Config object holding the model parameters
 * @param clk Reference to the Clock instance holding the current simulation
 * time
 * @param inputReader Reference to the InputReader object used to read the input
 * values of the subwatersheds (inflow rate, meteo variables etc.)
 */
Watershed::Watershed(const mio::Config& _cfg, const Clock& _clk, std::shared_ptr<DataContainer> _dataContainer) :
    ParametrizableObject(), TimeEvolvingObject(_clk),
    DiscretizedObject(), _shared_objects(), _shared_params(), _subwatersheds(),
    _subwatershed_indices(), _n_subwatersheds(),
    _computes_temp(getTemperatureModelName(_cfg) != "NONE"), _area(),
    _is_set(false)
{
  mio::Config cfg_tmp(_cfg);
  // Tweak config to read catchment grid
  if(cfg_tmp.keyExists("GRID2DPATH", "INPUT")){
    cfg_tmp.deleteKey("GRID2DPATH","INPUT");
  }
  cfg_tmp.addKey("GRID2DPATH","INPUT",_cfg.get("CATCHMENTPATH", "INPUT"));//< mandatory key

  if(cfg_tmp.keyExists("GRID2D", "INPUT")){
    cfg_tmp.deleteKey("GRID2D","INPUT");
  }
  cfg_tmp.addKey("GRID2D","INPUT","ARC");


  mio::IOManager io(cfg_tmp);
  mio::Grid2DObject grid;
  io.read2DGrid(grid, _cfg.get("CATCHMENT", "INPUT")); //< mandatory key
  this->computeSubwatershedIndices(grid);
  this->computeWatershedArea(grid);
  this->createSubwatersheds(_cfg, grid, _dataContainer);
}


Watershed::Watershed(const Watershed& _watershed, const mio::Config& _cfg,
    const Clock& _clk, std::shared_ptr<DataContainer> dataContainer) :
    ParametrizableObject(), TimeEvolvingObject(_clk), DiscretizedObject(),
    _shared_objects(), _shared_params(), _subwatersheds(),
    _subwatershed_indices(_watershed._subwatershed_indices), _n_subwatersheds(_watershed._n_subwatersheds),
    _computes_temp(getTemperatureModelName(_cfg) != "NONE"), _area(_watershed._area),
    _is_set(false)
{
  this->copySubwatersheds(_watershed, _cfg, _clk, dataContainer);
}


void Watershed::initialize()
{
  for (Subwatershed* subwatershed : _subwatersheds)
  {
    subwatershed->initialize();
  }
}



/**
 * @brief Checks whether the subwatershed with the given index is contained in
 * the watershed
 * @param index Index of the subwatershed to look for
 * @return Is the requested subwatershed contained in the watershed?
 */
bool Watershed::containsSubwatershed(const size_t& index) const {
  return std::find(_subwatershed_indices.begin(), _subwatershed_indices.end(),
      index) != _subwatershed_indices.end();
}


/**
 * @brief Returns a reference to the specified subwatershed
 * @param index Index of the subwatershed to be returned
 */
const Subwatershed& Watershed::getSubwatershed(const size_t& index) const {
  std::vector<size_t>::const_iterator it;
  it = std::find(_subwatershed_indices.begin(), _subwatershed_indices.end(), index);
  if(it == _subwatershed_indices.end()) {
    std::ostringstream os;
    os << "Watershed with index " << index << " does not exist in the watershed";
    throw InvalidIndexException(os.str(), AT);
  }
  return *(_subwatersheds.at(std::distance(_subwatershed_indices.begin(), it)));
}


/**
 * @brief Returns the indices of the subwatersheds contained in the watershed
 * @return Subwatershed indices
 */
const std::vector<size_t>& Watershed::getSubwatershedIndices() const {
  return _subwatershed_indices;
}


/**
 * @brief Returns the number of subwatersheds in the watershed
 * @return Number of subwatersheds
 */
const size_t& Watershed::getNumSubwatersheds() const {
  return _n_subwatersheds;
}


/**
 * @brief Returns a begin iterator on the subwatersheds contained in the
 * watershed.
 * @return Begin iterator
 */
Watershed::const_iterator Watershed::begin() const {
  const_iterator it(*this, 0);
  return it;
}


/**
 * @brief Returns an end iterator on the subwatersheds contained in the
 * watershed.
 * @return End iterator
 */
Watershed::const_iterator Watershed::end() const {
  const_iterator it(*this, _n_subwatersheds);
  return it;
}


/**
 * @brief Returns whether the temperature of the subwatershed's outflow
 * discharge is computed
 * @return Is outlet flow temperature computed?
 */
const bool& Watershed::computesTemperature() const {
  return _computes_temp;
}


/**
 * @brief Returns the watershed area in [km^2]
 * @return Watershed area in [km^2]
 */
const double& Watershed::getArea() const {
  return _area;
}


/**
 * @brief Returns the discretization level of the subwatersheds contained in
 * the watershed
 * @return Discretization level of the subwatersheds
 */
DiscretizedObject::Discretization Watershed::getDiscretization() const {
  return _subwatersheds.front()->getDiscretization();
}


/**
 * @brief Returns the watershed parameters. As explained in the class description,
 * the parameters which are specific to a particular subwatershed are prepended
 * with the prefix "SUBWATERSHED[i]_", where [i] denotes the index of the
 * subwatershed. In this way, parameters with identical names in different
 * subwatersheds can still be distinguished.
 * @return Set of watershed parameters
 */
ParameterSet Watershed::getParams() const {
  ParameterSet params, tmp;
  std::ostringstream os;

  //Get the subwatershed parameters
  for(size_t ix = 0; ix < _n_subwatersheds; ++ix, os.str(""), os.clear()) {
    os << _PARAMETER_PREFIX << _subwatershed_indices[ix] << "_";
    tmp = _subwatersheds[ix]->getParams();
    tmp.prependNames(os.str());
    params.add(tmp);
  }

  //Get the parameters of the objects referenced to by all the subwatersheds
  for(ParametrizableObject *sharedObject : _shared_params)
    params.add(sharedObject->getParams());

  return params;
}


/**
 * @brief Sets the watershed parameters. The parameters which are specific to
 * subwatershed [i] should be prepended with the prefix "SUBWATERSHED[i]_". Not
 * all parameters need be specified, only those contained in the input argument
 * will be modified in the Watershed instance. Unknown parameters will be ignored.
 * @param params New values of the watershed parameters.
 */
void Watershed::setParams(const ParameterSet& params) {
  ParameterSet tmp;
  std::string prefix;
  std::ostringstream os;

  //Set the subwatershed parameters
  for(size_t ix = 0; ix < _n_subwatersheds; ++ix, os.str(""), os.clear()) {
    os << _PARAMETER_PREFIX << _subwatershed_indices[ix] << "_";
    tmp = params.getSubset(os.str());
    tmp.ltrimNames(os.str());
    _subwatersheds[ix]->setParams(tmp);
  }

  //Set the parameters of the objects referenced to by all the subwatersheds
  for(ParametrizableObject *sharedObject : _shared_params)
    sharedObject->setParams(params);
}


/**
 * @brief Links each subwatershed contained in the watershed to its associated
 * stream reach.
 * @param network Stream network containing all the stream reaches
 */
void Watershed::setStreamNetwork(const StreamNetwork& network) {
  for(Subwatershed *sub : _subwatersheds)
  {
    sub->setStreamReach(identifyStreamReach(*sub, network));
  }
  _is_set = true;
}


void Watershed::advanceInTime() {
  if(!_is_set)
    throw RuntimeException("Method setStreamNetwork must be called before "
        "advancing the watershed in time", AT);

  for(Subwatershed* subwatershed : _subwatersheds) {
    subwatershed->updateBoundaryConditions();
    subwatershed->advanceInTime();
  }
}


void Watershed::reset() {
  for(Subwatershed *subwatershed : _subwatersheds)
    subwatershed->reset();
}


void Watershed::setDisplayInfo(const bool& display) {
  for(Subwatershed* subwatershed : _subwatersheds)
    subwatershed->setDisplayInfo(display);
}


/**
 * @brief Returns the ID number of the subwatershed in which the given point
 * is located
 * @param[in] point Point to localize
 * @param[out] subwatershedId ID number of the subwatershed in which the point
 * is located. In case the point is not located in any subwatershed, the value
 * of this argument is undefined.
 * @return True if point could be localized successfully, false otherwise
 */
bool Watershed::localizePoint(mio::Coords point, size_t& subwatershedId) const {
  mio::Grid2DObject mask;
  for(Subwatershed *subwatershed : _subwatersheds) {
    mask = subwatershed->getMask();
    if(mask.gridify(point) &&
        mask(point.getGridI(), point.getGridJ()) != mio::IOUtils::nodata) {
      subwatershedId = subwatershed->getIndex();
      return true;
    }
  }
  return false;
}


/**
 * @brief Destructor of class Watershed
 */
Watershed::~Watershed() {
  for(Subwatershed *subwatershed : _subwatersheds)
    delete subwatershed;

  for(SharedObject *object : _shared_objects)
    delete object;
}


/**
 * @brief Creates and initializes the subwatersheds according to the
 * specifications contained in the Config file
 * @param cfg Reference to the Config object containing the simulation
 * parameters
 * @param grid Grid defining the subwatersheds
 * @param inputReader Reference to the InputReader object used to read the input
 * values of the subwatersheds (inflow rate, meteo variables etc.)
 */
void Watershed::createSubwatersheds(const mio::Config& cfg, const mio::Grid2DObject& grid, std::shared_ptr<DataContainer> dataContainer) {
  _subwatersheds.reserve(_n_subwatersheds);

  //Instantiate the proper class of subwatersheds
  std::string name("TWO_VERTICAL_LINEAR_RESERVOIRS");
  cfg.getValue("SUBWATERSHED_CLASS", SFConstants::CONFIG_SECTION_NAME, name,
      mio::IOUtils::nothrow);
  mio::IOUtils::toUpper(name);
  std::cout << "[i] Selected subcatchment type: " << name << std::endl;
  if(name == "TWO_VERTICAL_LINEAR_RESERVOIRS")
    this->createTwoVerticalLinearReservoirs(cfg, grid, dataContainer);
  else
    throw InvalidConfigKeyException("Key SUBWATERSHED_CLASS in section [" +
        SFConstants::CONFIG_SECTION_NAME + "] of the configuration "
        "file can only take the following value: "
        "TWO_VERTICAL_LINEAR_RESERVOIRS", AT);

  //If the temperature of the subwatershed outflow discharge should be
  //computed, apply the appropriate decorator to the subwatershed objects

  if(_computes_temp) {
    const std::string modelType(getTemperatureModelName(cfg));
    std::cout << "[i] Selected subcatchment temperature model: "
              << modelType << std::endl;
    if(modelType == "ENERGY_BALANCE")
      this->applyEnergyBalanceDecorator(cfg, dataContainer);
    else if(modelType == "SOIL_TEMPERATURE")
      this->applySoilTemperatureDecorator(cfg, dataContainer);
    else if(modelType == "HSPF")
      this->applyHSPFTemperatureDecorator(cfg, dataContainer);
    else
      throw InvalidConfigKeyException("Key SUBWATERSHED_TEMPERATURE_MODEL in "
          "section [" + SFConstants::CONFIG_SECTION_NAME + "] of the "
          "configuration file can only take the following values: "
          "NONE, ENERGY_BALANCE, SOIL_TEMPERATURE or HSPF", AT);
  }

}


/**
 * @brief TO DO
 */
void Watershed::copySubwatersheds(const Watershed& _watershed, const mio::Config& cfg,
    const Clock& clk, std::shared_ptr<DataContainer> dataContainer) {
  _subwatersheds.reserve(_n_subwatersheds);

  std::string name("TWO_VERTICAL_LINEAR_RESERVOIRS");
  cfg.getValue("SUBWATERSHED_CLASS", SFConstants::CONFIG_SECTION_NAME, name,
      mio::IOUtils::nothrow);
  mio::IOUtils::toUpper(name);
  if(name == "TWO_VERTICAL_LINEAR_RESERVOIRS"){
    this->copyTwoVerticalLinearReservoirs(cfg, clk, _watershed, dataContainer);
   }
}


/**
 * @brief This method fills attribute _subwatersheds with instances of class
 * SimpleSubwatershed
 * @param cfg Reference to the Config object containing the simulation
 * parameters
 * @param grid Grid defining the subwatersheds
 * @param inputReader Reference to the InputReader object used to read the input
 * values of the subwatersheds (inflow rate, meteo variables etc.)
 */
void Watershed::createTwoVerticalLinearReservoirs(const mio::Config& cfg,
    const mio::Grid2DObject& grid, std::shared_ptr<DataContainer> dataContainer) {
  //Instantiate the object holding the parameters which are common to all the
  //subwatersheds
  dataContainer->addLoadInstruction(Variables::ROT,DataType::WATERSHED_SUM);


  TwoVerticalLinearReservoirs::Parameters *params;
  params = new TwoVerticalLinearReservoirs::Parameters(cfg, _area);
  _shared_objects.push_back(params);
  _shared_params.push_back(params);

  Subwatershed *tmp(nullptr);
  for(const size_t& subwatershedId : _subwatershed_indices) {
    tmp = new TwoVerticalLinearReservoirs(grid, subwatershedId, _clock, *params, dataContainer);
    _subwatersheds.push_back(tmp);
  }
}


/**
* @brief TO DO
*/
void Watershed::copyTwoVerticalLinearReservoirs(const mio::Config& cfg, const Clock& clk, const Watershed& _watershed, std::shared_ptr<DataContainer> dataContainer) {

  TwoVerticalLinearReservoirs::Parameters *params;
  params = new TwoVerticalLinearReservoirs::Parameters(cfg, _area);
  _shared_objects.push_back(params);
  _shared_params.push_back(params);
  Subwatershed *tmp(nullptr);
  //Copy subwatershed, except the clock that must be from the new hydrological model
  //Copy is preferable to new creation because of the grid construction, othwewise
  //the threads try to read simultaneoulsy the watershed file
  for(const Subwatershed& subwatershed : _watershed) { //< we copy the pointers by reference!
    if(!_computes_temp) {
      tmp = new TwoVerticalLinearReservoirs(dynamic_cast<const TwoVerticalLinearReservoirs&>(subwatershed), clk, *params);
     }
    else {
      tmp = new TwoVerticalLinearReservoirs(dynamic_cast<const TwoVerticalLinearReservoirs&>(
          *dynamic_cast<const LumpedSubwatershedDecorator&>(subwatershed)._wrapped_object), clk, *params);
     }
    _subwatersheds.push_back(tmp);
  }

  //Apply the correct decorator (easier to re-create than to copy)
  if(_computes_temp) {
    const std::string modelType(getTemperatureModelName(cfg));
    if(modelType == "ENERGY_BALANCE")
      this->applyEnergyBalanceDecorator(cfg, dataContainer);
    else if(modelType == "SOIL_TEMPERATURE")
      this->applySoilTemperatureDecorator(cfg, dataContainer);
    else if(modelType == "HSPF")
      this->applyHSPFTemperatureDecorator(cfg, dataContainer);
  }
}


/**
 * @brief This method applies decorator SimpleSubwatershedWithEB to the
 * Subwatershed objects so that they can compute the temperature of the outflow
 * discharge using the model described in Comola et al. (2015)
 *
 * Reference:
 * Comola, F., Schaefli, B., Rinaldo, A., and Lehning, M., Thermodynamics in the
 * hydrologic response: Travel time formulation and application to Alpine
 * catchments, WRR, 51, 2015.
 * @param cfg Reference to the Config object holding the simulation parameters
 */
void Watershed::applyEnergyBalanceDecorator(const mio::Config& cfg, std::shared_ptr<DataContainer> dataContainer) {
  //Create the object holding the parameter which is common to all
  //subwatersheds

  SimpleSubwatershedWithEB::Parameters *diffTime(nullptr);

  diffTime = new SimpleSubwatershedWithEB::Parameters(cfg);

  _shared_objects.push_back(diffTime);
  _shared_params.push_back(diffTime);

  dataContainer->addLoadInstruction(Variables::TSOIL,DataType::WATERSHED_AVG);
  //Decorate the subwatershed objects
  LumpedSubwatershedInterface *tmp(nullptr);
  for(Subwatershed*& subwatershed : _subwatersheds) { //< we copy the pointers by reference!
    tmp = dynamic_cast<LumpedSubwatershedInterface*>(subwatershed);
    subwatershed = new SimpleSubwatershedWithEB(tmp, *diffTime, dataContainer);
  }

}


/**
 * @brief This method applies decorator SubwatershedWithSoilTempOutflow to the
 * Subwatershed objects so that they can set the temperature of the outflow
 * discharge equal to the soil temperature.
 * @param cfg Reference to the Config object holding the simulation parameters
 * @param reader Reference to the object used to read the input of the
 * subwatersheds and stream reaches.
 */
void Watershed::applySoilTemperatureDecorator(const mio::Config&, std::shared_ptr<DataContainer> dataContainer) {

  dataContainer->addLoadInstruction(Variables::TSOIL,DataType::WATERSHED_AVG);

  LumpedSubwatershedInterface *tmp(nullptr);
  for(Subwatershed*& subwatershed : _subwatersheds) { //< we copy the pointers by reference!
    tmp = dynamic_cast<LumpedSubwatershedInterface*>(subwatershed);
    subwatershed = new SubwatershedWithSoilTempOutflow(tmp, dataContainer);
  }
}


/**
 * @brief This method applies decorator SubwatershedWithHSPFOutflowTemp to the
 * Subwatershed objects so that they can compute the temperature of the outflow
 * discharge according to the method implemented in the HSPF model.
 * @param cfg Reference to the Config object holding the simulation parameters
 * @param inputReader Reference to the object used to read the input data of the
 * subwatersheds and stream reaches.
 */
void Watershed::applyHSPFTemperatureDecorator(const mio::Config& cfg, std::shared_ptr<DataContainer> dataContainer) {
  //Create the object holding the parameters which are common to all
  //subwatersheds

  dataContainer->addLoadInstruction(Variables::TA,DataType::WATERSHED_AVG);

  SubwatershedWithHSPFOutflowTemp::Parameters *params(nullptr);
  params = new SubwatershedWithHSPFOutflowTemp::Parameters(cfg);
  _shared_objects.push_back(params);
  _shared_params.push_back(params);

  LumpedSubwatershedInterface *tmp(nullptr);
  for(Subwatershed*& subwatershed : _subwatersheds) { //< we copy the pointers by reference!
    tmp = dynamic_cast<LumpedSubwatershedInterface*>(subwatershed);
    subwatershed = new SubwatershedWithHSPFOutflowTemp(tmp, *params, dataContainer);
  }
}


/**
 * @brief Computes the ID numbers of the subwatershed contained in the
 * watershed
 * @param grid Grid defining the subwatersheds
 */
void Watershed::computeSubwatershedIndices(const mio::Grid2DObject& grid) {
  std::vector<double> values;
  values.reserve(grid.grid2D.getCount());
  for(size_t idx = 0; idx < grid.getNx()*grid.getNy(); ++idx) {
    const double& val = grid(idx);
    if(val != mio::IOUtils::nodata) values.push_back(val);
  }
  std::sort(values.begin(), values.end());
  std::vector<double>::iterator it = std::unique(values.begin(), values.end());
  _subwatershed_indices.resize(std::distance(values.begin(), it));
  std::copy(values.begin(), it, _subwatershed_indices.begin());
  _n_subwatersheds = _subwatershed_indices.size();
  std::cout << "[i] " << _n_subwatersheds << " subcatchments identified in "
      "the watershed" << std::endl;
}


/**
 * @brief Returns the name of the model to be used in order to compute the
 * temperature of the watersheds' outflow discharge
 * @param cfg Reference to the Config instance holding the model parameters
 * @return Name of the temperature model to be used by the watershed
 */
std::string Watershed::getTemperatureModelName(const mio::Config& cfg) {
  std::string name("None");
  cfg.getValue("SUBWATERSHED_TEMPERATURE_MODEL", SFConstants::CONFIG_SECTION_NAME,
      name, mio::IOUtils::nothrow);
  mio::IOUtils::toUpper(name);
  return name;
}


/**
 * @brief Computes the area of the watershed. The watershed area needs to be
 * computed before the creation of the subwatersheds, since some subwatershed
 * classes require this piece of information at construction time.
 * @param grid Grid defining the subwatersheds
 */
void Watershed::computeWatershedArea(const mio::Grid2DObject& grid) {
  _area = double(grid.grid2D.getCount())*grid.cellsize*grid.cellsize*1e-6; //< convert from m^2 to km^2
  std::cout << "[i] Watershed area: " << std::fixed << std::setprecision(2)
            << _area << " km^2" << std::endl;
}


/**
 * @brief Returns a reference to the stream reach contained in the subwatershed
 * passed as input argument
 * @param sub Subwatershed whose associated stream reach should be returned
 * @param network Stream network containing all the reaches
 * @return Reference to the stream reach contained in the input subwatershed
 */
const StreamReach& Watershed::identifyStreamReach(const Subwatershed& sub,
    const StreamNetwork& network) {
  const StreamReach *streamReach(nullptr);
  for(const StreamReach& reach : network) {
    if(sub.contains(reach, true)) {
      streamReach = &reach;
      break;
    }
  }

  if(!streamReach) {
        std::ostringstream os;
        os << "Could not find the stream reach associated with subwatershed "
           << sub.getIndex() << ". Please check that the file(s) containing "
           << "the stream cell coordinates coincide(s) with the grid defining "
           << "the subwatersheds.";
        throw RuntimeException(os.str(), AT);
  }

  return *streamReach;
}


//-----------------------------------------------------------------------------
// Implementation of class const_iterator
//-----------------------------------------------------------------------------

Watershed::const_iterator::const_iterator(const Watershed& watershed, const size_t& pos) :
    _watershed(&watershed), _curr_pos(std::min(pos, watershed.getNumSubwatersheds())) {}

Watershed::const_iterator::const_iterator(const const_iterator& copy) :
    _watershed(copy._watershed), _curr_pos(copy._curr_pos) {}

Watershed::const_iterator& Watershed::const_iterator::operator=(const const_iterator& copy) {
  _watershed = copy._watershed;
  _curr_pos = copy._curr_pos;
  return *this;
}

const Subwatershed& Watershed::const_iterator::operator*() const {
  return *(this->operator->());
}

const Subwatershed* Watershed::const_iterator::operator->() const {
  if(_curr_pos == _watershed->_subwatersheds.size())
    throw InvalidIteratorException("Watershed iterator points on "
        "last-plus-one element", AT);

  return _watershed->_subwatersheds.at(_curr_pos);
}

Watershed::const_iterator& Watershed::const_iterator::operator++() {
  if(_curr_pos != _watershed->_subwatersheds.size())
    ++_curr_pos;

  return *this;
}

Watershed::const_iterator Watershed::const_iterator::operator+(const size_t& offset) const {
  size_t newPos = std::min(_curr_pos + offset, _watershed->_subwatersheds.size());
  const_iterator it(*_watershed, newPos);
  return it;
}

Watershed::const_iterator Watershed::const_iterator::operator-(const size_t& offset) const {
  size_t newPos = std::max(size_t(0), _curr_pos - offset);
  const_iterator it(*_watershed, newPos);
  return it;
}

bool Watershed::const_iterator::operator!=(const const_iterator& other) const {
  return (_watershed != other._watershed) || (_curr_pos != other._curr_pos);
}

Watershed::const_iterator::~const_iterator() {}

} /* namespace sflow */
