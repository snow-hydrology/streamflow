/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PARAMETER_H_
#define PARAMETER_H_

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include <limits.h>
#include <streamflow/utils/Units.h>
#include <streamflow/utils/StreamflowExceptions.h>
#include <meteoio/MeteoIO.h>

namespace sflow {

/**
 * @class Parameter
 * @brief Class representing a module parameter. This parameter is assumed to
 * be real-valued and is associated with a name, units, and a description.
 * @ingroup calibration
 * @author A. Gallice
 * @date 2015-03-03
 */
class Parameter {
	public:
		Parameter();
		Parameter(const std::string& name, const double& value, const Units& units,
				const std::vector<double>& limits = _INFINITY_RANGE,
				const bool& calibrate = true, const std::string& desc = "");
		Parameter(const mio::Config& cfg, const std::string& name,
				const std::string& section, const Units& units,
				const std::vector<double>& defaultRange = _INFINITY_RANGE,
				const std::string& desc = "");

		//Getters
		const double& getValue() const;
		double getValue(const Units& units) const;
 		const Units& getUnits() const;
		const std::string& getName() const;
		const std::string& getDescription() const;
		const std::vector<double>& getRange() const;
		std::vector<double> getRange(const Units& units) const;
		const bool& toBeCalibrated() const;

		//Setters
		void set(const std::string& name, const double& value,
				const Units& units,	const std::vector<double>& limits =	_INFINITY_RANGE,
				const bool& calibrate = true, const std::string& desc = "");
		void set(const mio::Config& cfg, const std::string& name,
				const std::string& section, const Units& units,
				const std::vector<double>& defaultRange = _INFINITY_RANGE,
				const std::string& desc = "");
		void setName(const std::string& name);
		void setValue(const double & val);
		void setValue(const double & val, const Units& units);
		void setRangeLowerLimit(const double& val);
		void setRangeLowerLimit(const double& val, const Units& units);
		void setRangeUpperLimit(const double& val);
		void setRangeUpperLimit(const double& val, const Units& units);
		void setRange(const std::vector<double>& range);
		void setRange(std::vector<double> range, const Units& units);
		void setCalibrate(const bool& val);

		bool isEqual(const Parameter& param, const bool& compareNames = true) const;
		const std::string toString() const;
		friend std::ostream& operator<<(std::ostream& os, const Parameter& p);

	protected:
		std::string _name;
		double _value;
		Units _units;
		std::vector<double> _range;
		std::string _description;
		bool _calibrate; //< should the parameter be calibrated?

		static const std::vector<double> _INFINITY_RANGE; //< to be used as default argument

		void checkParamValue() const;
};

} /* namespace sflow */

#endif /* PARAMETER_H_ */
