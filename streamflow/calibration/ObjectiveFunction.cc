/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/calibration/ObjectiveFunction.h>
#include <streamflow/io/CalibrationDataContainer.h>

namespace sflow {

const std::string ObjectiveFunction::_FUNCTION_KEY_PATTERN = "OBJECTIVE_FUNCTION";

ObjectiveFunction::ObjectiveFunction(const mio::Config& cfg,CalibrationDataContainer& _measures_reader, SimulatedDataContainer _simulation_reader) : ModelObserver(),
		 measures_reader(_measures_reader), simulation_reader(_simulation_reader) ,perf_measures(), _perf_measure_names(getPerfMeasureNames(cfg)),
		 perf_measure_points(getPerfMeasuresPoints(cfg)),weights(getWeights(cfg)),daily(getDaily(cfg)),variables(getVariables(_perf_measure_names,cfg)), perf_measures_score()
{
	const size_t nFuncs = _perf_measure_names.size();
	perf_measures.reserve(nFuncs);

	size_t iMeas(0);
	for(const std::string& name : _perf_measure_names) {
		perf_measures_score[name]=mio::IOUtils::nodata;
		perf_measures.push_back(this->getPerfMeasure(name, cfg,perf_measure_points.at(iMeas)));
		++ iMeas;
	}
}

void ObjectiveFunction::reset() {
	simulation_reader.reset();
}

void ObjectiveFunction::update(const mio::Date& currTime, const Watershed& watershed,
		const StreamNetwork& network) {
	simulation_reader.update(currTime, watershed, network);
}

double ObjectiveFunction::getValue(){
	double result = 0.0;

	for(size_t iMeas = 0; iMeas < perf_measures.size(); ++iMeas) {
		perf_measures_score[_perf_measure_names.at(iMeas)] = perf_measures.at(iMeas)(simulation_reader,measures_reader,perf_measure_points.at(iMeas),variables.at(iMeas),daily.at(iMeas));
		result += perf_measures_score.at(_perf_measure_names.at(iMeas))*weights.at(iMeas);
	}
	return result;
}

std::map<std::string, double> ObjectiveFunction::getPerfMeasures() const {
	return perf_measures_score;
}


const std::vector<mio::StationData>& ObjectiveFunction::getStationsMeta() const
{
	return measures_reader.getStationsMeta();
}
const std::vector<std::vector<std::reference_wrapper < const StreamVariable> > >& ObjectiveFunction::getRequiredVariable() const
{
	return measures_reader.getRequiredVariable();
}
const std::map<size_t,size_t>& ObjectiveFunction::getIdToPos() const
{
	return measures_reader.getIdToPos();
}
const std::vector<double>& ObjectiveFunction::getRunDates() const
{
	return simulation_reader.getRunDates();
}
const std::vector< std::map< std::string, std::vector<double> > >& ObjectiveFunction::getRunData() const
{
	return simulation_reader.getRunData();
}


const std::vector<std::string>& ObjectiveFunction::getPerfMeasureNames() const {
	return _perf_measure_names;
}

std::vector<std::string> ObjectiveFunction::getPerfMeasureNames(const mio::Config& cfg) {
	std::vector<std::string> allKeys;
	allKeys = cfg.getKeys(_FUNCTION_KEY_PATTERN, "CALIBRATION");

	std::string name;
	std::vector<std::string> funcNames;
	funcNames.reserve(allKeys.size());
	for(const std::string& key : allKeys) {
		name = key.substr(0, key.find("::"));
		if(std::find(funcNames.begin(), funcNames.end(), name) == funcNames.end())
			funcNames.push_back(name);
	}
	funcNames.shrink_to_fit();
	return funcNames;
}

std::vector<std::vector<size_t>> ObjectiveFunction::getPerfMeasuresPoints(const mio::Config& cfg) const
{
	std::vector<std::vector<size_t>> _perf_measure_points(_perf_measure_names.size());
	std::vector<size_t> all_points;
	for (auto const & pt:measures_reader.getIdToPos())
	{
		all_points.push_back(pt.first);
	}
	size_t i = 0;
	for(auto const& name: _perf_measure_names)
	{
		std::vector<std::string> points;
		std::ostringstream os1;
		os1 << name << "::points";
		cfg.getValue(os1.str(),"CALIBRATION",points);
		for(auto const& p:points)
		{
			if(p.compare("ALL")==0)
			{
				_perf_measure_points.at(i)=all_points;
				break;
			}
			else{
				int id_int=stoi(p);
				_perf_measure_points.at(i).push_back(static_cast<size_t>(id_int));
			}
		}
		++i;
	}
	return _perf_measure_points;
}

std::vector<double> ObjectiveFunction::getWeights(const mio::Config& cfg) const
{
	std::vector<double> _weights_;
	for(const std::string& name : _perf_measure_names) {
		double weight = 1.0;
		cfg.getValue(name + "::weight", "CALIBRATION", weight);
		_weights_.push_back(weight);
	}
	return _weights_;
}

std::vector<bool> ObjectiveFunction::getDaily(const mio::Config& cfg) const
{
	std::vector<bool> _daily_;
	for(const std::string& name : _perf_measure_names) {
		bool daily_loc = false;
		cfg.getValue(name + "::daily", "CALIBRATION", daily_loc, mio::IOUtils::nothrow);
		_daily_.push_back(daily_loc);
	}
	return _daily_;
}

ObjectiveFunction::PerfMeasFct ObjectiveFunction::getPerfMeasure(const std::string& name, const mio::Config& cfg, const std::vector<size_t>& points)
{
	std::string type = cfg.get(name, "CALIBRATION"); //< mandatory key
	mio::IOUtils::toUpper(type);
	std::string variable("DISCHARGE");
	cfg.getValue(name + "::variable", "CALIBRATION", variable, mio::IOUtils::nothrow);
	mio::IOUtils::toUpper(variable);

	PerfMeasFct measure;
	if(type == "RMSE") {
		measure = PerformanceMeasure::RMSE;
	} else if(type == "MAE") {
		measure = PerformanceMeasure::MAE;
	} else if(type == "BIAS") {
		measure = PerformanceMeasure::BIAS;
	} else if(type == "NSE") {
		if(points.size() > 1)
			throw InvalidConfigKeyException("The Nash-Sutcliffe efficiency can only "
					"be computed in a single point. Please specify only one "
					"point in the configuration file at key " + name + "::points", AT);
		measure = PerformanceMeasure::NSE;
	}
	else if(type == "KGE" || type =="KGE2012" || type =="KGE12") {
		if(points.size() > 1)
			throw InvalidConfigKeyException("The Kling-Gupta efficiency can only "
					"be computed in a single point. Please specify only one "
					"point in the configuration file at key " + name + "::points", AT);
		measure = PerformanceMeasure::KGE12;
	}
	else if(type == "KGE2009" || type =="KGE09") {
		if(points.size() > 1)
			throw InvalidConfigKeyException("The Kling-Gupta efficiency can only "
					"be computed in a single point. Please specify only one "
					"point in the configuration file at key " + name + "::points", AT);
		measure = PerformanceMeasure::KGE09;
	}
	else {
		throw InvalidConfigKeyException("Invalid value " + type + " for key " +
				name + "::type in section [CALIBRATION] of the configuration "
				"file (the allowed values are: RMSE, MAE, BIAS and NSE)", AT);
	}

	return measure;
}

const std::vector<std::reference_wrapper < const HydroVariable> >  ObjectiveFunction::getVariables(const std::vector<std::string>& names,
		const mio::Config& cfg){

	std::vector<std::reference_wrapper < const HydroVariable> > _variables;
	for(const std::string& name : names) {
		std::string variable("DISCHARGE");
		cfg.getValue(name + "::variable", "CALIBRATION", variable, mio::IOUtils::nothrow);
		mio::IOUtils::toUpper(variable);
		_variables.push_back(getVariable(variable));
	}
	return _variables;

}


const HydroVariable& ObjectiveFunction::getVariable(const std::string& name) {
	try {
		return StreamVariable::getVariable(name);
	} catch (const std::invalid_argument&) {
		return SubwatershedVariable::getVariable(name);
	}
}

} /* namespace sflow */
