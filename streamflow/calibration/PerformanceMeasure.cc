/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/calibration/PerformanceMeasure.h>
#include <streamflow/io/CalibrationDataContainer.h>
#include <streamflow/io/SimulatedDataContainer.h>

namespace sflow {

double PerformanceMeasure::RMSE(const SimulatedDataContainer& _simulation_reader, const CalibrationDataContainer& _measures_reader,const std::vector<size_t>& points, const HydroVariable& var, const bool daily)
{

	double sse=0;
	double n_values=0;
	for(auto const & pt: points) {
		const std::vector<double>& measures(_measures_reader.getData(pt,var));
		const std::vector<double>& simulation(_simulation_reader.getData(pt,var));
		std::vector<size_t> indices_calib_dates(_measures_reader.getIndicesCalibDates(pt,var));
		reduceIndices(simulation, indices_calib_dates);

		std::vector<double> daily_measures;
		std::vector<double> daily_simulation;
		if(daily)
		{
			get_daily_means(measures,simulation, *_measures_reader.getLoadedDates(),
										_simulation_reader.getRunDates(),indices_calib_dates,daily_measures,daily_simulation);
		}
		const std::vector<double>& measures_final=daily?daily_measures:measures;
		const std::vector<double>& simulation_final=daily?daily_simulation:simulation;

		std::vector<double> diff=getDifference(measures_final,simulation_final,indices_calib_dates);
		sse += std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
		n_values += static_cast<double>(measures_final.size());
	}
	return n_values > 0 ? sqrt(sse/n_values) : mio::IOUtils::nodata;
}

double PerformanceMeasure::MAE(const SimulatedDataContainer& _simulation_reader, const CalibrationDataContainer& _measures_reader,const std::vector<size_t>& points, const HydroVariable& var, const bool daily)
{

	double sae=0;
	double n_values=0;

	for(auto const & pt: points) {
		const std::vector<double>& measures(_measures_reader.getData(pt,var));
		const std::vector<double>& simulation(_simulation_reader.getData(pt,var));
		std::vector<size_t> indices_calib_dates(_measures_reader.getIndicesCalibDates(pt,var));
		reduceIndices(simulation, indices_calib_dates);

		std::vector<double> daily_measures;
		std::vector<double> daily_simulation;
		if(daily)
		{
			get_daily_means(measures,simulation, *_measures_reader.getLoadedDates(),
										_simulation_reader.getRunDates(),indices_calib_dates,daily_measures,daily_simulation);
		}
		const std::vector<double>& measures_final=daily?daily_measures:measures;
		const std::vector<double>& simulation_final=daily?daily_simulation:simulation;

		std::vector<double> diff=getDifference(measures_final,simulation_final,indices_calib_dates);
		sae+=std::accumulate(diff.begin(),diff.end(),0.0,[](double sum, double inc){return sum+fabs(inc);});
		n_values += static_cast<double>(indices_calib_dates.size());

	}
	return n_values > 0 ? sae/n_values : mio::IOUtils::nodata;
}


double PerformanceMeasure::BIAS(const SimulatedDataContainer& _simulation_reader, const CalibrationDataContainer& _measures_reader,const std::vector<size_t>& points, const HydroVariable& var, const bool daily)
{

	double ae=0;
	double n_values=0;
	for(auto const & pt: points) {
		const std::vector<double>& measures(_measures_reader.getData(pt,var));
		const std::vector<double>& simulation(_simulation_reader.getData(pt,var));
		std::vector<size_t> indices_calib_dates(_measures_reader.getIndicesCalibDates(pt,var));
		reduceIndices(simulation, indices_calib_dates);

		std::vector<double> daily_measures;
		std::vector<double> daily_simulation;
		if(daily)
		{
			get_daily_means(measures,simulation, *_measures_reader.getLoadedDates(),
										_simulation_reader.getRunDates(),indices_calib_dates,daily_measures,daily_simulation);
		}
		const std::vector<double>& measures_final=daily?daily_measures:measures;
		const std::vector<double>& simulation_final=daily?daily_simulation:simulation;

		std::vector<double> diff=getDifference(measures_final,simulation_final,indices_calib_dates);
		ae+=std::accumulate(diff.begin(),diff.end(),0.0);
		n_values += static_cast<double>(indices_calib_dates.size());
	}
	return n_values > 0 ? ae/n_values : mio::IOUtils::nodata;

}


double PerformanceMeasure::NSE(const SimulatedDataContainer& _simulation_reader, const CalibrationDataContainer& _measures_reader,const std::vector<size_t>& points, const HydroVariable& var, const bool daily)
{

	size_t pt = points.front();

	const std::vector<double>& measures(_measures_reader.getData(pt,var));
	const std::vector<double>& simulation(_simulation_reader.getData(pt,var));
	std::vector<size_t> indices_calib_dates(_measures_reader.getIndicesCalibDates(pt,var));
	reduceIndices(simulation, indices_calib_dates);
	if(indices_calib_dates.size()==0){
		return mio::IOUtils::nodata;
	}

	std::vector<double> daily_measures;
	std::vector<double> daily_simulation;
	if(daily)
	{
			get_daily_means(measures,simulation, *_measures_reader.getLoadedDates(),
											_simulation_reader.getRunDates(),indices_calib_dates,daily_measures,daily_simulation);
	}
	const std::vector<double>& measures_final=daily?daily_measures:measures;
	const std::vector<double>& simulation_final=daily?daily_simulation:simulation;

	double _mean=getMean(measures_final,indices_calib_dates);
	std::vector<double> diff=getDifference(measures_final,_mean,indices_calib_dates);
	double tss = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
	std::vector<double> diff2=getDifference(measures_final,simulation_final,indices_calib_dates);
	double sse = std::inner_product(diff2.begin(), diff2.end(), diff2.begin(), 0.0);
	return 1.0 - sse/tss;
}


double PerformanceMeasure::KGE12(const SimulatedDataContainer& _simulation_reader, const CalibrationDataContainer& _measures_reader,const std::vector<size_t>& points, const HydroVariable& var, const bool daily)
{
// Implementation from (Kling et al. 2012)
	size_t pt = points.front();
	const std::vector<double>& measures(_measures_reader.getData(pt,var));
	const std::vector<double>& simulation(_simulation_reader.getData(pt,var));
	std::vector<size_t> indices_calib_dates(_measures_reader.getIndicesCalibDates(pt,var));
	reduceIndices(simulation, indices_calib_dates);
	if(indices_calib_dates.size()==0){
		return mio::IOUtils::nodata;
	}

	std::vector<double> daily_measures;
	std::vector<double> daily_simulation;
	if(daily)
	{
		get_daily_means(measures,simulation, *_measures_reader.getLoadedDates(),
										_simulation_reader.getRunDates(),indices_calib_dates,daily_measures,daily_simulation);
	}
	const std::vector<double>& measures_final=daily?daily_measures:measures;
	const std::vector<double>& simulation_final=daily?daily_simulation:simulation;

	double mean_meas=getMean(measures_final,indices_calib_dates);
	double mean_sim=getMean(simulation_final,indices_calib_dates);
	std::vector<double> diff_meas = getDifference(measures_final, mean_meas, indices_calib_dates);
	double diff_squared_meas = std::inner_product(diff_meas.begin(), diff_meas.end(), diff_meas.begin(), 0.0);
	std::vector<double> diff_sim = getDifference(simulation_final, mean_sim, indices_calib_dates);
	double diff_squared_sim = std::inner_product(diff_sim.begin(), diff_sim.end(), diff_sim.begin(), 0.0);
	double sigma_meas = sqrt(diff_squared_meas/static_cast<double>(indices_calib_dates.size()));
	double sigma_sim = sqrt(diff_squared_sim/static_cast<double>(indices_calib_dates.size()));
	double CV_sim = sigma_sim / mean_sim;
	double CV_meas = sigma_meas / mean_meas;
	double VR = CV_sim / CV_meas;
	double Beta = mean_sim / mean_meas;
	double r = std::inner_product(diff_sim.begin(), diff_sim.end(), diff_meas.begin(), 0.0)/sqrt(diff_squared_sim*diff_squared_meas);
	return 1 - sqrt( (r-1.)*(r-1.) + (VR-1.)*(VR-1.) + (Beta-1.)*(Beta-1.) );
}


double PerformanceMeasure::KGE09(const SimulatedDataContainer& _simulation_reader, const CalibrationDataContainer& _measures_reader,const std::vector<size_t>& points, const HydroVariable& var, const bool daily)
{
 // Implementation from (Gupta et al. 2009)
	size_t pt = points.front();
	const std::vector<double>& measures(_measures_reader.getData(pt,var));
	const std::vector<double>& simulation(_simulation_reader.getData(pt,var));
	std::vector<size_t> indices_calib_dates(_measures_reader.getIndicesCalibDates(pt,var));
	reduceIndices(simulation, indices_calib_dates);
	if(indices_calib_dates.size()==0){
		return mio::IOUtils::nodata;
	}

	std::vector<double> daily_measures;
	std::vector<double> daily_simulation;
	if(daily)
	{
		get_daily_means(measures,simulation, *_measures_reader.getLoadedDates(),
										_simulation_reader.getRunDates(),indices_calib_dates,daily_measures,daily_simulation);
	}
	const std::vector<double>& measures_final=daily?daily_measures:measures;
	const std::vector<double>& simulation_final=daily?daily_simulation:simulation;

	double mean_meas=getMean(measures_final,indices_calib_dates);
	double mean_sim=getMean(simulation_final,indices_calib_dates);
	std::vector<double> diff_meas = getDifference(measures_final, mean_meas, indices_calib_dates);
	double diff_squared_meas = std::inner_product(diff_meas.begin(), diff_meas.end(), diff_meas.begin(), 0.0);
	std::vector<double> diff_sim = getDifference(simulation_final, mean_sim, indices_calib_dates);
	double diff_squared_sim = std::inner_product(diff_sim.begin(), diff_sim.end(), diff_sim.begin(), 0.0);
	double sigma_meas = sqrt(diff_squared_meas/static_cast<double>(indices_calib_dates.size()));
	double sigma_sim = sqrt(diff_squared_sim/static_cast<double>(indices_calib_dates.size()));
	double VR = sigma_sim / sigma_meas;
	double Beta = mean_sim / mean_meas;
	double r = std::inner_product(diff_sim.begin(), diff_sim.end(), diff_meas.begin(), 0.0)/sqrt(diff_squared_sim*diff_squared_meas);
	return 1 - sqrt( (r-1.)*(r-1.) + (VR-1.)*(VR-1.) + (Beta-1.)*(Beta-1.) );
}


double PerformanceMeasure::getMean(const std::vector<double>& to_mean, const std::vector<size_t>& indices_calib_dates)
{
	double sum=0;
	for(auto const i:indices_calib_dates)
	{
		sum+=to_mean.at(i);
	}
	return sum/static_cast<double>(indices_calib_dates.size());
}


std::vector<double> PerformanceMeasure::getDifference(const std::vector<double>& vec_1,const double scal, const std::vector<size_t>& indices_calib_dates)
{
	std::vector<double> diff;
	diff.reserve(indices_calib_dates.size());
	for(auto const i:indices_calib_dates)
	{
		diff.push_back(vec_1.at(i)-scal);
	}
	return diff;
}


std::vector<double> PerformanceMeasure::getDifference(const std::vector<double>& vec_1,const std::vector<double>& vec_2, const std::vector<size_t>& indices_calib_dates)
{
	std::vector<double> diff;
	diff.reserve(indices_calib_dates.size());
	for(auto const i:indices_calib_dates)
	{
		diff.push_back(vec_1.at(i)-vec_2.at(i));
	}
	return diff;
}


void PerformanceMeasure::reduceIndices(const std::vector<double>& simulation, std::vector<size_t>& indices_calib_dates)
{
	std::vector<size_t>::iterator  iter = indices_calib_dates.begin();
	while (iter != indices_calib_dates.end())
	{
		if(simulation.at(*iter)==mio::IOUtils::nodata)
		{
				iter = indices_calib_dates.erase(iter);
		}
		else
		{
				++iter;
		}
	}
}


void PerformanceMeasure::getDailyDates(std::vector<int>& daily_dates,
  std::set<int>& daily_dates_unique, const std::vector<double>& dates)
{
	for(auto && d:dates)
	{
		const mio::Date dd(d,1);
		int date = mio::Date(d,1).getYear()*1000+mio::Date(d,1).getJulianDayNumber();
		daily_dates.push_back(date);
		daily_dates_unique.insert(date);
	}
}


void PerformanceMeasure::get_daily_means(const std::vector<double>& measures,
  const std::vector<double>& simulation, const std::vector<double>& measures_dates,
  const std::vector<double>& simulations_date, std::vector<size_t>& indices_calib_dates,
  std::vector<double>& daily_measures, std::vector<double>& daily_simulation)
{
	// Get dates and unique dates data, format YYYYDDD
	std::vector<int> sim_daily_dates;
	std::set<int> sim_daily_dates_unique;
	sim_daily_dates.reserve(simulations_date.size());
	getDailyDates(sim_daily_dates,sim_daily_dates_unique,simulations_date);

	std::vector<int> meas_daily_dates;
	std::set<int> meas_daily_dates_unique;
	meas_daily_dates.reserve(measures_dates.size());
	getDailyDates(meas_daily_dates,meas_daily_dates_unique,measures_dates);

	// Compute and store daily means
	daily_measures.reserve(sim_daily_dates_unique.size());
	daily_simulation.reserve(sim_daily_dates_unique.size());
	for(auto && d:sim_daily_dates_unique)
	{
		auto p_sim = std::equal_range(sim_daily_dates.begin(), sim_daily_dates.end(), d);
		auto p_meas = std::equal_range(meas_daily_dates.begin(), meas_daily_dates.end(), d);
		if(std::distance(p_sim.first,p_sim.second)==24 && std::distance(p_meas.first,p_meas.second)==24)
		{
			if(std::find(p_sim.first,p_sim.second,mio::IOUtils::nodata)==p_sim.second &&
				 std::find(p_meas.first,p_meas.second,mio::IOUtils::nodata)==p_meas.second)
			{
				size_t index = p_sim.first - sim_daily_dates.begin();
				daily_simulation.push_back(std::accumulate(simulation.begin() + index, simulation.begin() + index + 24, 0.)/24.);
				index = p_meas.first - meas_daily_dates.begin();
				daily_measures.push_back(std::accumulate(measures.begin() + index,  measures.begin() + index + 24, 0.)/24.);
			}
		}
	}
	// Set indices_calib_dates
	indices_calib_dates.resize(daily_simulation.size());
	std::iota(indices_calib_dates.begin(),indices_calib_dates.end(),0);
}

}
