/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CALIBRATIONALGORITHM_H_
#define CALIBRATIONALGORITHM_H_

#include <fstream>
#include <sstream>
#include <meteoio/MeteoIO.h>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/utils/StreamflowExceptions.h>
#include <streamflow/calibration/ParameterSet.h>
#include <streamflow/calibration/ObjectiveFunction.h>


namespace sflow {

class HydrologicalModel;
class CalibrationDataContainer;
class SimulatedDataContainer;
/**
 * @class CalibrationAlgorithm
 * @brief This class defines the interface which must be implemented by each
 * model calibration algorithm.
 * @ingroup calibration
 * @author Aurelien Gallice
 * @date 2015-09-25
 */
class CalibrationAlgorithm {
	public:
		CalibrationAlgorithm() {}
		virtual unsigned int calibrate(ObservableModel& model, ObjectiveFunction& fun) = 0;
		virtual std::string getExitMessage() const = 0;
		virtual const std::vector<ScoredParameterSet>& getBestParams() = 0;
		virtual ~CalibrationAlgorithm() {}
};



/**
 * @class MonteCarlo
 * @brief This class implements a Monte-Carlo calibration algorithm.
 * @ingroup calibration
 * @author Aurelien Gallice
 * @date 2015-09-25
 */
class MonteCarlo : public CalibrationAlgorithm {
	public:
		MonteCarlo( const size_t num_iter, const size_t n_sets_to_keep, bool write_time_series);
	 	unsigned int calibrate(ObservableModel& model, ObjectiveFunction& fun) override;
		std::string getExitMessage() const override;
		const std::vector<ScoredParameterSet>& getBestParams() override;
		~MonteCarlo() override;

	protected:
		const size_t _num_iter;
		const size_t _n_sets_to_keep;
		const size_t _display_info_step;
		ParameterSet _curr_params;
		std::vector<ScoredParameterSet> _best_params;
		bool _write_time_series;

		void updateBestScores(ObjectiveFunction& fun);
		void computeNewParams();
};


/**
 * @class ModelCalibration
 * @brief This class creates the correct instance of class CalibrationAlgorithm
 * based on the parameters specified in the configuration file, calibrates the
 * model, and destructs the instance of CalibrationAlgorithm
 * @ingroup calibration
 * @author Aurelien Gallice
 * @date 2015-09-25
 */
class ModelCalibration {
	public:
		ModelCalibration() = delete;
		static void calibrate(HydrologicalModel& model, const mio::Config& cfg, const mio::Date& start_time,
				const mio::Date& end_time);

	protected:
		static CalibrationAlgorithm& getCalibrationAlgorithm(const mio::Config& cfg, const size_t num_iter, const size_t n_sets_to_keep, bool write_time_series);
		static void writeBestParams(const std::string outputFile,
				const std::vector<ScoredParameterSet>& sets);
		static void printOutputFileHeader(std::ostream& of,
				const std::vector<ScoredParameterSet>& sets);
		static void printOutputFileData(std::ostream& of,
				const std::vector<ScoredParameterSet>& sets);
		static void writeTimeSeries(const std::string filePath, const std::string filePrefix,
				const std::vector<ScoredParameterSet>& sets, const double time_zone);
		static void printTSFileHeader(std::ostream& of, const double time_zone,
				const ScoredParameterSet set,size_t position,const mio::StationData& station_meta,
		  	const std::vector<std::reference_wrapper < const StreamVariable> >& variables);
		static void printTSFileData(std::ostream& of, const double time_zone,
				const std::vector<std::reference_wrapper < const StreamVariable> >& variables,
				const std::vector<double>& run_dates,
				const std::map< std::string, std::vector<double> >& loaded_data);
		static void writeConfig(const mio::Config& cfg, std::string path);

		static size_t getNumIterations(const mio::Config& cfg);
		static size_t getNumSetsToKeep(const mio::Config& cfg);
		static size_t getNumTimeSeries(const mio::Config& cfg);


};

} /* namespace sflow */

#endif /* CALIBRATIONALGORITHM_H_ */
