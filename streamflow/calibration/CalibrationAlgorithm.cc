/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/calibration/CalibrationAlgorithm.h>
#include <streamflow/HydrologicalModel.h>
#include <streamflow/io/CalibrationDataContainer.h>
#include <streamflow/io/SimulatedDataContainer.h>

#ifdef _OPENMP
	#include <omp.h>
#endif

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class MonteCarlo
//-----------------------------------------------------------------------------

MonteCarlo::MonteCarlo(const size_t num_iter, const size_t n_sets_to_keep, bool write_time_series) :
		CalibrationAlgorithm(), _num_iter(num_iter),
		_n_sets_to_keep(n_sets_to_keep),
		_display_info_step(std::max(size_t(1), _num_iter/100)), _curr_params(),
		_best_params(_n_sets_to_keep),
		_write_time_series(write_time_series)
{
	std::srand((unsigned int)std::time(nullptr));
}

unsigned int MonteCarlo::calibrate(ObservableModel& model, ObjectiveFunction& fun) {
	_curr_params = model.getParams();
	_curr_params.removeFixedParameters();

	#ifdef _OPENMP
		const double tstart(omp_get_wtime());
	#else
		const clock_t tStart(clock());
	#endif

	#ifdef _OPENMP
	//Run with default params only in the first master loop,
	//for other trhreads already take random params
	if(omp_get_thread_num()>0)
		{
		this->computeNewParams();
		model.setParams(_curr_params);
	}
	#endif

	for(size_t iter(0); iter < _num_iter; ++iter) {
		if(iter % _display_info_step == 0) {
			#ifdef _OPENMP
			#pragma omp critical(print_status)
			#endif
			{
			const double currCompletion(round(double(iter)/double(_num_iter)*100.));
			std::cout << "[i] " << std::setw(3) << std::setprecision(0)
			          << currCompletion << "% of the calibration completed";
			#ifdef _OPENMP
				std::cout << " from thread " << omp_get_thread_num();
				const double elapsedTime(omp_get_wtime()-tstart);
			#else
				const double elapsedTime(double(clock() - tStart)/CLOCKS_PER_SEC);
			#endif

			if(currCompletion > 0. && elapsedTime > 60.) {
				const double remainingTime((100./currCompletion - 1.)*elapsedTime);
				std:: cout << " (estimated completion in "
				           << SFUtils::timeToString(remainingTime) << ")";
			}
			std::cout << std::endl;
			}
		}
		model.run();
		this->updateBestScores(fun);
		this->computeNewParams();
		model.setParams(_curr_params);
		model.reset();
	}

	return 0;
}


std::string MonteCarlo::getExitMessage() const {
	return "";
}

const std::vector<ScoredParameterSet>& MonteCarlo::getBestParams() {
	return _best_params;
}

MonteCarlo::~MonteCarlo() {}


void MonteCarlo::updateBestScores(ObjectiveFunction& fun) {
	const double score = fun.getValue();
	std::vector<ScoredParameterSet>::iterator it;
	it = std::max_element(_best_params.begin(), _best_params.end());

	if(score < it->getScore()){
		if(_write_time_series)
		{
			it->set(_curr_params, score, fun.getPerfMeasures(), fun.getStationsMeta(), fun.getRequiredVariable(), fun.getIdToPos(), fun.getRunDates(),fun.getRunData());
		}
		else{
			it->set(_curr_params, score, fun.getPerfMeasures());
		}
	}
}

void MonteCarlo::computeNewParams() {
	std::vector<double> range;
	double randNum;
	for(Parameter& param : _curr_params) {
		range = param.getRange();
		randNum = double(std::rand())/double(RAND_MAX);
		param.setValue(randNum*(range[1] - range[0]) + range[0]);
	}
}



//-----------------------------------------------------------------------------
// Implementation of class ModelCalibration
//-----------------------------------------------------------------------------

void ModelCalibration::calibrate(HydrologicalModel& _model, const mio::Config& cfg, const mio::Date& start_time, const mio::Date& end_time) {

	std::vector<ScoredParameterSet> bestParams;

	#ifdef _OPENMP
		const double tStart(omp_get_wtime());
	#else
		const clock_t tStart(clock());
	#endif

	const std::string fileName = cfg.get("CALIBRATION", "OUTPUT");
	if(!mio::FileUtils::validFileAndPath(fileName))
		throw InvalidConfigKeyException("Key CALIBRATION in section [OUTPUT] of the "
				"configuration file does not contain a valid file name", AT);

	std::string write_time_series_str("FALSE");
	bool write_time_series=false;
	cfg.getValue("WRITE_TIME_SERIES", "CALIBRATION", write_time_series_str,mio::IOUtils::nothrow);
	mio::IOUtils::toUpper(write_time_series_str);

	if(write_time_series_str.compare("TRUE")==0){
		write_time_series=true;
		std::string path = cfg.get("CALIBRATION_TIME_SERIES", "OUTPUT");
		if(!mio::FileUtils::validFileAndPath(path))
			throw InvalidConfigKeyException("Key CALIBRATION_TIME_SERIES in section [OUTPUT] of the "
					"configuration file does not contain a valid path", AT);
	}


	const size_t tot_iter = getNumIterations(cfg);
	size_t num_iter = tot_iter;

	const size_t n_sets_to_keep = std::min(num_iter, getNumSetsToKeep(cfg));

	std::cout << "[i] Reading calibration measurments"<< std::endl;

	CalibrationDataContainer measures(cfg, start_time, end_time);
	SimulatedDataContainer simulated(cfg, start_time, end_time, measures);

	#ifdef _OPENMP
		#pragma omp parallel firstprivate(num_iter)
	#endif
	{
	#ifdef _OPENMP
		#pragma omp master
	#endif
	{std::cout.setstate(std::ios_base::failbit);}
	#ifdef _OPENMP
		#pragma omp barrier
	#endif

	HydrologicalModel model(_model);
	#ifdef _OPENMP
		#pragma omp barrier
	#endif

	#ifdef _OPENMP
		#pragma omp master
	#endif
	{std::cout.clear();}
	#ifdef _OPENMP
		#pragma omp barrier
	#endif

	ObjectiveFunction fun(cfg, measures, simulated);

	#ifdef _OPENMP
		num_iter = tot_iter/omp_get_num_threads();
		if(static_cast<size_t>(omp_get_thread_num())< tot_iter%static_cast<size_t>(omp_get_num_threads()))
		{++num_iter;}
	#endif

	CalibrationAlgorithm& algo(getCalibrationAlgorithm(cfg, num_iter,n_sets_to_keep,write_time_series));
	model.addObserver(fun);
	model.setDisplayInfo(false);

	#ifdef _OPENMP
		#pragma omp critical(print_start)
		std::cout << "[i] Starting model calibration from thread: " << omp_get_thread_num()<< " - " << num_iter << " calirations will be executed by this thread" << std::endl;
	#else
		std::cout << "[i] Starting model calibration \n";
	#endif

	const size_t exitFlag = algo.calibrate(model, fun);
	if(exitFlag != 0) {
		std::ostringstream os;
		os << "Model calibration algorithm exited with error code "
		   << exitFlag << ":" << algo.getExitMessage();
		throw RuntimeException(os.str(), AT);
	}

	#ifdef _OPENMP
		#pragma omp critical(print_end)
		{
			bestParams.insert(bestParams.end(), algo.getBestParams().begin(), algo.getBestParams().end());
			std::cout << "[i] Model calibration from thread " << omp_get_thread_num() << " completed\n";
		}
	#else
		bestParams.insert(bestParams.end(), algo.getBestParams().begin(), algo.getBestParams().end());
		std::cout << "[i] Model calibration completed\n";
	#endif
	delete &algo;
	}
	///END OF THE PARALLEL SECTION ///

	std::sort (bestParams.begin(), bestParams.end());
	bestParams.resize(n_sets_to_keep);
	writeBestParams(fileName, bestParams);
	if(write_time_series)
	{
		size_t num_time_series = getNumTimeSeries(cfg);
		num_time_series = std::min(n_sets_to_keep, num_time_series);
		bestParams.resize(num_time_series);
		std::string filePath = cfg.get("CALIBRATION_TIME_SERIES", "OUTPUT");
		std::string filePrefix = "";
		cfg.getValue("CALIBRATION_TIME_SERIES_PREFIX", "OUTPUT",filePrefix,mio::IOUtils::nothrow);
		writeTimeSeries(filePath, filePrefix, bestParams,cfg.get("TIME_ZONE", "INPUT"));
	}
	writeConfig(cfg, mio::FileUtils::getPath(fileName));


	#ifdef _OPENMP
		std::cout << "[i] Execution time of the model calibration: "
	          << SFUtils::timeToString(omp_get_wtime() - tStart)
	           << std::endl;
	#else
		std::cout << "[i] Execution time of the model calibration: "
	          << SFUtils::timeToString(double(clock() - tStart)/CLOCKS_PER_SEC)
	          << std::endl;
	#endif

}

CalibrationAlgorithm& ModelCalibration::getCalibrationAlgorithm(const mio::Config& cfg, const size_t num_iter, const size_t n_sets_to_keep,bool write_time_series) {
	CalibrationAlgorithm *algo;
	std::string name("MONTE_CARLO");
	cfg.getValue("ALGORITHM", "CALIBRATION", name, mio::IOUtils::nothrow);
	mio::IOUtils::toUpper(name);

	if(name == "MONTE_CARLO"){
		algo = new MonteCarlo(num_iter, n_sets_to_keep, write_time_series);
	}
	else{
		throw InvalidConfigKeyException("Key ALGORITHM in section [CALIBRATION] "
				"can only take the value MONTE_CARLO", AT);
	}
	return *algo;
}

void ModelCalibration::writeBestParams(const std::string outputFile,
		const std::vector<ScoredParameterSet>& sets) {
	//Write parameter sets in a SMET file
	std::ofstream smetOut(outputFile);
	if(smetOut.fail())
		throw mio::AccessException(outputFile, AT);

	printOutputFileHeader(smetOut, sets);
	printOutputFileData(smetOut, sets);

	smetOut.close();

	std::cout << "[i] Set of best model parameters written out in file "
	          << outputFile << std::endl;
}


void ModelCalibration::printOutputFileHeader(std::ostream& of,
		const std::vector<ScoredParameterSet>& sets) {
	of << std::right;
	of << std::fixed;
	of << "SMET 1.1 ASCII\n";
	of << "# Produced with Libstreamflow " << sflow::getLibVersion()  << "\n";
	of << "# and MeteoIO " << mio::getLibVersion() << "\n";
	of << "[HEADER]\n";
	of << "station_id   = model_calibration\n";
	of << "station_name = model_calibration\n";
	of << "latitude     = " << std::setw(7) << std::setprecision(0) << mio::IOUtils::nodata << "\n";
	of << "longitude    = " << std::setw(7) << std::setprecision(0) << mio::IOUtils::nodata << "\n";
	of << "altitude     = " << std::setw(7) << std::setprecision(0) << mio::IOUtils::nodata << "\n";
	of << "nodata       = " << std::setw(7) << std::setprecision(0) << mio::IOUtils::nodata << "\n";
	of << "tz           = " << std::setw(7) << std::setprecision(0) << mio::IOUtils::nodata << "\n";
	of << "comments     = total_score is the value of the different objective functions defined in the ini file, ";
	of << "multiplied by their respective weights. This is the value minimized by the calibration algorithm. The ini file has been copied in the ";
	of << "smae directory than this file\n ";
	of << "fields       = total_score";
	for(const std::pair<const std::string, double>& measure : sets.front().getPerfMeasures())
		of << " " << measure.first;
	for(const Parameter& param : sets.front())
		of << " " << param.getName().substr(param.getName().find("::") + 2);

	of << "\nunits        = -";
	for(size_t iMeas = 0; iMeas < sets.front().getNumPerfMeasures(); ++iMeas)
		of << " -";
	for(const Parameter& param : sets.front())
		of << " " << param.getUnits();

	of << "\n";
}

void ModelCalibration::printOutputFileData(std::ostream& of,
		const std::vector<ScoredParameterSet>& sets) {
	of << "\n[DATA]\n";
	of.fill(' ');
	of << std::left;

	for(const ScoredParameterSet& params : sets) {
		of << std::fixed << std::setw(8) << std::setprecision(3) << params.getScore();
		for(const std::pair<const std::string, double>& measure : params.getPerfMeasures())
			of << " " << std::fixed << std::setw(8) << std::setprecision(3) << measure.second;
		for(const Parameter& param : params)
			of << " " << std::scientific << std::setw(11) << std::setprecision(3) << param.getValue();
		of << "\n";
	}
}



void ModelCalibration::writeTimeSeries(const std::string filePath, const std::string filePrefix,
		const std::vector<ScoredParameterSet>& sets, const double time_zone) {
	size_t position=1;
	for(auto const& set:sets){
		const std::shared_ptr<const BestRun> best_run(set.getBestRun());
		const std::vector<std::vector<std::reference_wrapper < const StreamVariable> > >&  variables(best_run->getRequiredVariable());
		const std::vector<mio::StationData>& stations_meta(best_run->getStationsMeta());
	 	const std::vector<double>& run_dates(best_run->getRunDates());
		const std::vector< std::map< std::string, std::vector<double> > > & run_data(best_run->getRunData());
		for (auto const& id:best_run->getIdToPos())
		{
			std::string fileName = filePath+"/"+filePrefix+"best_calib_run_"+std::to_string(position)+"_at_meas_point"+std::to_string(id.first)+".smet";
			std::ofstream smetOut(fileName);
			if(smetOut.fail())
			{
				throw mio::AccessException(fileName, AT);
			}
			printTSFileHeader(smetOut,time_zone, set, position, stations_meta.at(id.second), variables.at(id.second) );
 			printTSFileData(smetOut, time_zone, variables.at(id.second), run_dates, run_data.at(id.second));
			smetOut.close();
		}
		++position;
	}
	std::cout << "[i] Set of best model time series written out in directory "
	          << filePath << std::endl;
}

void ModelCalibration::printTSFileHeader(std::ostream& of, const double time_zone,
		const ScoredParameterSet set,size_t position,const mio::StationData& station_meta,
  	const std::vector<std::reference_wrapper < const StreamVariable> >& variables) {
	of << std::right;
	of << std::fixed;
	of << "SMET 1.1 ASCII\n";
	of << "# Produced with Libstreamflow " << sflow::getLibVersion()  << "\n";
	of << "# and MeteoIO " << mio::getLibVersion() << "\n";
	of << "[HEADER]\n";
	of << "station_id   = " << station_meta.getStationID() <<"\n";
	of << "station_name = " << station_meta.getStationName() <<"\n";
	of << "latitude     = " << station_meta.getPosition().getLat() <<"\n";
	of << "longitude    = " << station_meta.getPosition().getLon() <<"\n";
	of << "northing     = " << station_meta.getPosition().getNorthing() <<"\n";
	of << "easting      = " << station_meta.getPosition().getEasting() <<"\n";
	of << "altitude     = " << station_meta.getAltitude() <<"\n";
	of << "nodata       = " << mio::IOUtils::nodata <<"\n";
	of << "tz           = " << static_cast<int>(time_zone) <<"\n";
	of << "comments     = Best calibration run number "<< position<<" obtained with:";
	for(const Parameter& param : set)
		of << " " << param.getName() << "="<< std::scientific << std::setprecision(3) << param.getValue();
	of << "\n";
	of << "fields       = timestamp";
	for(auto const& var : variables)
	{
		of << " " << var.get().toString();
	}
	of << "\nunits        = - ";
	for(auto const& var : variables)
	{
		of << " " << var.get().getUnits();
	}
	of << "\n";
}

void ModelCalibration::printTSFileData(std::ostream& of, const double time_zone,
		const std::vector<std::reference_wrapper < const StreamVariable> >& variables,
		const std::vector<double>& run_dates,
		const std::map< std::string, std::vector<double> >& run_data){
	of << "[DATA]\n";
	of.fill(' ');
	of << std::left;
	for(size_t i=0; i<run_dates.size(); ++i ) {
			of << mio::Date(run_dates.at(i),time_zone).toString(mio::Date::ISO);
			for(auto const& var : variables)
			{
				of << "\t" << std::setprecision(3) << run_data.at(var.get().toString()).at(i);
			}
			of << "\n";
	}
}

void ModelCalibration::writeConfig(const mio::Config& cfg, std::string path)
{
	const std::string source_path=cfg.getConfigRootDir()+"/"+mio::FileUtils::getFilename(cfg.getSourceName());
	const std::string dest_path=path+"/"+mio::FileUtils::getFilename(cfg.getSourceName());
	mio::FileUtils::copy_file(source_path,dest_path);
}


size_t ModelCalibration::getNumIterations(const mio::Config& cfg) {
	double nIter(cfg.get("NUM_ITERATIONS", "CALIBRATION"));
	if(nIter < 1.)
		throw InvalidConfigKeyException("The number of Monte-Carlo simulations must "
				"be at least 1. Please change the value of key NUM_ITERATIONS in "
				"section [CALIBRATION] of the configuration file.", AT);

	return size_t(nIter);
}

size_t ModelCalibration::getNumSetsToKeep(const mio::Config& cfg) {
	size_t nSets(15);
	cfg.getValue("NUM_PARAM_SETS_TO_KEEP", "CALIBRATION", nSets, mio::IOUtils::nothrow);
	return std::max(nSets, size_t(1));
}

size_t ModelCalibration::getNumTimeSeries(const mio::Config& cfg) {
	size_t nTS(1);
	cfg.getValue("NUM_TIME_SERIES_TO_KEEP", "CALIBRATION", nTS, mio::IOUtils::nothrow);
	return std::max(nTS, size_t(1));
}


} /* namespace sflow */
