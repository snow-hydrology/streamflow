/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PERFORMANCEMEASURE_H_
#define PERFORMANCEMEASURE_H_

#include <numeric>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/utils/HydroVariable.h>


namespace sflow {
class CalibrationDataContainer;
class SimulatedDataContainer;
/**
 * @class PerformanceMeasure
 * @brief This class defines the interface to be implemented by the performance
 * measures of a hydrological model (e.g. RMSE, Nash-Sutcliffe efficiency, bias,
 * etc.).
 * @ingroup calibration
 */
class PerformanceMeasure : public ModelObserver {
	public:

		PerformanceMeasure() = delete;
		PerformanceMeasure(const PerformanceMeasure&) = delete;
		PerformanceMeasure& operator=(const PerformanceMeasure&) = delete;

		static double RMSE(const SimulatedDataContainer& _simulation_reader,
                       const CalibrationDataContainer& _measures_reader,
                       const std::vector<size_t>& points,
                       const HydroVariable& var,
                       const bool daily);
		static double NSE(const SimulatedDataContainer& _simulation_reader,
                      const CalibrationDataContainer& _measures_reader,
                      const std::vector<size_t>& points,
                      const HydroVariable& var,
                      const bool daily);
		static double MAE(const SimulatedDataContainer& _simulation_reader,
                      const CalibrationDataContainer& _measures_reader,
                      const std::vector<size_t>& points,
                      const HydroVariable& var,
                      const bool daily);
		static double BIAS(const SimulatedDataContainer& _simulation_reader,
                       const CalibrationDataContainer& _measures_reader,
                       const std::vector<size_t>& points,
                       const HydroVariable& var,
                       const bool daily);
		static double KGE12(const SimulatedDataContainer& _simulation_reader,
                        const CalibrationDataContainer& _measures_reader,
                        const std::vector<size_t>& points,
                        const HydroVariable& var,
                        const bool daily);
		static double KGE09(const SimulatedDataContainer& _simulation_reader,
                        const CalibrationDataContainer& _measures_reader,
                        const std::vector<size_t>& points,
                        const HydroVariable& var,
                        const bool daily);

	private:
		static void reduceIndices(const std::vector<double>& simulation,
                              std::vector<size_t>& indices_calib_dates);
		static double getMean(const std::vector<double>& to_mean,
                          const std::vector<size_t>& indices_calib_dates);
		static std::vector<double> getDifference(const std::vector<double>& vec_1,
           const double scal, const std::vector<size_t>& indices_calib_dates);
		static std::vector<double> getDifference(const std::vector<double>& vec_1,
           const std::vector<double>& vec_2, const std::vector<size_t>& indices_calib_dates);
		static void getDailyDates(std::vector<int>& daily_dates,
                std::set<int>& daily_dates_unique, const std::vector<double>& dates);
		static void get_daily_means(const std::vector<double>& measures,
      const std::vector<double>& simulation, const std::vector<double>& measures_dates,
      const std::vector<double>& simulations_date, std::vector<size_t>& indices_calib_dates,
      std::vector<double>& daily_measures, std::vector<double>& daily_simulation);
};
}


#endif /* PERFORMANCEMEASURE_H_ */
