/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/calibration/Parameter.h>

namespace sflow {

const std::vector<double> Parameter::_INFINITY_RANGE = {
		std::numeric_limits<double>::min(),
		std::numeric_limits<double>::max()};


/**
 * @brief Default constructor of class Parameter
 */
Parameter::Parameter() :
		_name(), _value(mio::IOUtils::nodata), _units(), _range(_INFINITY_RANGE),
		_description(), _calibrate(true) {}


/**
 * @brief Constructor of class Parameter taking the values of all the class
 * attributes as input
 * @param name  Parameter name
 * @param value Parameter value
 * @param units Parameter units
 * @param limits Minimum and maximum limits of the range of allowed values for
 * the parameter. Should be specified as a 2-element vector.
 * @param calibrate Should the parameter be calibrated?
 * @param desc Description of the parameter (intended to be more exhaustive than
 * the parameter name)
 */
Parameter::Parameter(const std::string& name, const double& value,
		const Units& units, const std::vector<double>& limits,
		const bool& calibrate, const std::string& desc) :
				_name(name), _value(value), _units(units), _range(),
				_description(desc), _calibrate(calibrate)
{
	this->setRange(limits);
	this->checkParamValue();
}


/**
 * @brief Constructor of class Parameter, instantiating the parameter based on
 * the information contained in the configuration file. The default parameter
 * properties passed as input arguments to the constructor can be overriden by
 * specifying the following keys in the configuration file:
 *     - [parameter name]\::LIMITS to override the default range of valid values.
 *       This range is used only for calibration purposes and must be expressed
 *       in the same units as the parameter value.
 *     - [parameter name]\::UNITS to specify the parameter value in other units
 *       than the default ones. This key also applies to the limits of the valid
 *       range if specified (see key [parameter name]\::LIMITS above). This key
 *       does not change the behavior of methods getValue() and getRange(),
 *       which both return values expressed in the default units regardless of
 *       the units specified in the configuration file.
 *     - [parameter name]\::CALIBRATE to specify whether the parameter should be
 *       calibrated or not (defaults to true).
 *
 * For example:
 * @code
 * WIND_FUNCTION_FACTOR = 1
 * WIND_FUNCTION_FACTOR::LIMITS = 0 2
 * WIND_FUNCTION_FACTOR::UNITS  = s/m
 * WIND_FUNCTION_FACTOR::CALIBRATE = false
 * @endcode
 * @param cfg Reference to the Config object holding the simulation parameters
 * @param name Key of the configuration file which defines the parameter value
 * @param section Section of the configuration file in which key is located
 * @param defaultUnits Default parameter units. These units are also those in
 * which the parameter value is returned by methods getValue() and getRange().
 * @param defaultRange Default value range (to be used if parameter range is not
 * specified in the configuration file). This range is assumed to be expressed
 * in the default units.
 * @param desc Parameter description (intended to be more exhaustive than the
 * parameter name)
 */
Parameter::Parameter(const mio::Config& cfg, const std::string& name,
		const std::string& section, const Units& defaultUnits,
		const std::vector<double>& defaultRange, const std::string& desc) :
				_name(), _value(), _units(), _range(), _description(),
				_calibrate()
{
	this->set(cfg, name, section, defaultUnits, defaultRange, desc);
}


/**
 * @brief Returns the parameter value, expressed in the units returned by method
 * getUnits()
 * @return Parameter value
 */
const double& Parameter::getValue() const {
	return _value;
}


/**
 * @brief Returns the parameter value, expressed in the units passed as input
 * argument
 * @param units Units in which the parameter value should be returned
 * @return Parameter value
 */
double Parameter::getValue(const Units& units) const {
	if(units.getQuantity() != _units.getQuantity()) {
		std::ostringstream os;
		os << "Cannot return value of parameter " << _name << " in [" << units
	       << "]: units are associated with a different physical quantity than "
		   << "the parameter";
		throw InvalidUnitsException(os.str(), AT);
	} else if(_units != units) {
		return _units.getConversionFunctionTo(units)(_value);
	} else {
		return _value;
	}
}


/**
 * @brief Returns the units in which the values returned by methods getValue()
 * and getRange() are expressed
 * @return Parameter units
 */
const Units& Parameter::getUnits() const {
	return _units;
}


/**
 * @brief Returns the parameter name
 * @return Parameter name
 */
const std::string& Parameter::getName() const {
	return _name;
}


/**
 * @brief Returns the parameter description (intended to be more exhaustive than
 * the parameter name)
 * @return Parameter description
 */
const std::string& Parameter::getDescription() const {
	return _description;
}


/**
 * @brief Returns the minimum and maximum values that the parameter can adopt,
 * expressed in the units returned by method getUnits().
 * @return Valid range of parameter values
 */
const std::vector<double>& Parameter::getRange() const {
	return _range;
}


/**
 * @brief Returns the minimum and maximum values that the parameter can adopt,
 * expressed in the units passed as input argument.
 * @param units Units in which the returned values should be expressed
 * @return Valid range of parameter values
 */
std::vector<double> Parameter::getRange(const Units& units) const {
	if(units.getQuantity() != _units.getQuantity()) {
		std::ostringstream os;
		os << "Cannot return range of parameter " << _name << " in [" << units
	       << "]: units are associated with a different physical quantity than "
		   << "the parameter";
		throw InvalidUnitsException(os.str(), AT);
	} else if(units != _units) {
		std::vector<double> output(_range);
		const LinearFunction1D convert(_units.getConversionFunctionTo(units));
		for(size_t ie(0); ie < 2; ++ie) {
			if(output[ie] != _INFINITY_RANGE[ie])
				output[ie] = convert(output[ie]);
		}
		return output;
	} else {
		return _range;
	}
}


/**
 * @brief Returns whether the parameter should be calibrated.
 * @return Should the parameter be calibrated?
 */
const bool& Parameter::toBeCalibrated() const {
	return _calibrate;
}


/**
 * @brief Sets all the class attributes in one go.
 * @param name  Parameter name
 * @param value Parameter value
 * @param units Parameter units
 * @param limits Minimum and maximum limits of the range of allowed values for
 * the parameter. Should be specified as a 2-element vector.
 * @param calibrate Should the parameter be calibrated?
 * @param desc  Description of the parameter (intended to be more
 * exhaustive than the parameter name)
 */
void Parameter::set(const std::string& name, const double& value, const Units& units,
		const std::vector<double>& limits, const bool& calibrate, const std::string& desc) {
	_name  = name;
	_value = value;
	_units = units;
	this->setRange(limits);
	_calibrate = calibrate;
	if(!desc.empty()) _description = desc;
	this->checkParamValue();
}


/**
 * @brief Sets all the class attributes in one go, based on the information
 * contained in the Config object passed as input argument. This method is
 * equivalent to the constructor taking the same input arguments, except that it
 * acts on already existing objects.
 * @param cfg Reference to the Config object holding the simulation parameters
 * @param name Key of the configuration file which defines the parameter value
 * @param section Section of the configuration file in which key is located
 * @param units Default parameter units. These units are also those in
 * which the parameter value is returned by methods getValue() and getRange().
 * @param defaultRange Default value range (to be used if parameter range is not
 * specified in the configuration file). This range is assumed to be expressed
 * in the default units.
 * @param desc Parameter description (intended to be more exhaustive than the
 * parameter name)
 */
void Parameter::set(const mio::Config& cfg, const std::string& name,
		const std::string& section, const Units& units,
		const std::vector<double>& defaultRange, const std::string& desc) {
	_name  = section + "::" + name;
	_value = cfg.get(name, section);
	_units = units;
	if(!desc.empty()) _description = desc;

	bool isDefaultRange(false);
	_range.clear();
	cfg.getValue(name + "::LIMITS", section, _range, mio::IOUtils::nothrow);
	if(_range.empty()) {
		_range = defaultRange;
		isDefaultRange = true;
	} else if(_range.size() != 2) {
		throw InvalidConfigKeyException("Range of parameter " + _name + " must be "
				"specified as a 2-element vector (cf. key " + _name + "::LIMITS)", AT);
	}
	std::sort(_range.begin(), _range.end());

	std::string strUnits;
	cfg.getValue(name + "::UNITS", section, strUnits, mio::IOUtils::nothrow);
	if(!strUnits.empty()) {
		Units cfgUnits(Units::parseString(strUnits));
		if(units.getQuantity() != cfgUnits.getQuantity())
			throw InvalidConfigKeyException("Cannot create parameter " + _name + ": "
					"default parameter units are not associated with the same "
					"physical quantity as the units specified in the configuration "
					"file (cf. key " + _name + "::UNITS)", AT);

		const LinearFunction1D convert(cfgUnits.getConversionFunctionTo(units));
		_value = convert(_value);
		if(!isDefaultRange)
			std::transform(_range.begin(), _range.end(), _range.begin(), convert);
	}

	_calibrate = true;
	cfg.getValue(name + "::CALIBRATE", section, _calibrate, mio::IOUtils::nothrow);

	this->checkParamValue();
}


/**
 * @brief Sets the parameter name
 * @param name New parameter name
 */
void Parameter::setName(const std::string& name) {
	this->_name = name;
}


/**
 * @brief Sets the parameter value
 * @param value New parameter value, expressed in the units returned by method
 * getUnits()
 */
void Parameter::setValue(const double& value) {
	if(value < _range[0] || value > _range[1]) {
		std::ostringstream os;
		os << "Cannot change value of parameter " << _name << " from " << _value
		   << " to " << value << ": new value is out of allowed range ["
		   << _range[0] << "," << _range[1] << "]";
		throw mio::InvalidArgumentException(os.str(), AT);
	}
	this->_value = value;
}


/**
 * @brief Sets the parameter value
 * @param value New parameter value, expressed in the units specified as second
 * input argument
 * @param units Units in which the new parameter value is expressed
 */
void Parameter::setValue(const double& value, const Units& units) {
	if(units.getQuantity() != _units.getQuantity()) {
		std::ostringstream os;
		os << "Cannot change value of parameter " << _name << " from " << _value
		   << " " << _units << " to " << value << " " << units << ":  units are"
		   << " associated with different physical quantities";
		throw InvalidUnitsException(os.str(), AT);
	}
	this->setValue(units.getConversionFunctionTo(_units)(value));
}


/**
 * @brief Sets the minimum acceptable value for the parameter, expressed in the
 * units returned by method getUnits()
 * @param val Minimum acceptable parameter value
 */
void Parameter::setRangeLowerLimit(const double& val) {
	if(val >= _range[1])
		throw mio::InvalidArgumentException("New value for the range's lower limit of "
				"parameter " + _name + " must be lower than the actual upper limit", AT);

	_range[0] = val;
	_value = fmax(val, _value);
}


/**
 * @brief Sets the minimum acceptable value for the parameter, expressed in the
 * units passed as second input argument
 * @param val Minimum acceptable parameter value
 * @param units Units in which val is expressed
 */
void Parameter::setRangeLowerLimit(const double& val, const Units& units) {
	if(units.getQuantity() != _units.getQuantity()) {
		std::ostringstream os;
		os << "Cannot change minimum range limit of parameter " << _name << ": "
		   << "specified units [" << units << "] are associated with a different"
		   << " physical quantity than the parameter (which is expressed in ["
		   << _units <<  "])";
		throw InvalidUnitsException(os.str(), AT);
	}
	this->setRangeLowerLimit(units.getConversionFunctionTo(_units)(val));
}


/**
 * @brief Sets the maximum acceptable value for the parameter, expressed in the
 * units returned by method getUnits()
 * @param val Maximum acceptable parameter value
 */
void Parameter::setRangeUpperLimit(const double& val) {
	if(val <= _range[0])
		throw mio::InvalidArgumentException("New value for the range's upper limit of "
				"parameter " + _name + " must be greater than the actual lower limit", AT);

	_range[1] = val;
	_value = fmin(val, _value);
}


/**
 * @brief Sets the maximum acceptable value for the parameter, expressed in the
 * units passed as second input argument
 * @param val Maximum acceptable parameter value
 * @param units Units in which val is expressed
 */
void Parameter::setRangeUpperLimit(const double& val, const Units& units) {
	if(units.getQuantity() != _units.getQuantity()) {
		std::ostringstream os;
		os << "Cannot change maximum range limit of parameter " << _name << ": "
		   << "specified units [" << units << "] are associated with a different"
		   << " physical quantity than the parameter (which is expressed in ["
		   << _units <<  "])";
		throw InvalidUnitsException(os.str(), AT);
	}
	this->setRangeUpperLimit(units.getConversionFunctionTo(_units)(val));
}


/**
 * @brief Sets the range of acceptable parameter values, expressed in the
 * units returned by method getUnits().
 * @param limits Minimum and maximum acceptable values of the parameter,
 * specified as a two-element vector
 */
void Parameter::setRange(const std::vector<double>& limits) {
	if(limits.size() != 2)
		throw mio::InvalidArgumentException("Valid range of parameter " + _name +
				" must be specified as a 2-element vector", AT);

	_range = limits;
	std::sort(_range.begin(), _range.end());
	_value = fmax(_range[0], fmin(_range[1], _value));
}


/**
 * @brief Sets the range of acceptable parameter values, expressed in the
 * units passed as second input argument
 * @param limits Minimum and maximum acceptable values of the parameter,
 * specified as a two-element vector
 * @param units Units in which the minimum and maximum parameter values are
 * expressed
 */
void Parameter::setRange(std::vector<double> limits, const Units& units) {
	if(units.getQuantity() != _units.getQuantity()) {
		std::ostringstream os;
		os << "Cannot change range of parameter " << _name << ": specified units"
		   << " [" << units << "] are associated with a different physical quantity"
		   << " than the parameter (which is expressed in [" << _units <<  "])";
		throw InvalidUnitsException(os.str(), AT);
	}
	const LinearFunction1D convert(units.getConversionFunctionTo(_units));
	std::transform(limits.begin(), limits.end(), limits.begin(), convert);
	this->setRange(limits);
}


/**
 * @brief Sets whether the parameter should be calibrated
 * @param calibrate Calibrate the parameter?
 */
void Parameter::setCalibrate(const bool& calibrate) {
	_calibrate = calibrate;
}


/**
 * @brief Method returning true if the two parameters are equal
 * (conversion is performed if units are not the same). The optional
 * argument specifies if names must be compared on top of the parameter
 * values (true by default). Descriptions are ignored.
 * @param param Parameter to compare with the current one.
 * @param compareNames Should names be compared?
 */
bool Parameter::isEqual(const Parameter& param, const bool& compareNames) const {
	if(_units.getQuantity() != param._units.getQuantity())
		return false;
	bool isEq = param._value == _units.getConversionFunctionTo(param._units)(_value);
	if (compareNames)
		isEq &= param._name == _name;
	return isEq;
}


/**
 * @brief Method used to convert the object to a string.
 * @return String
 */
const std::string Parameter::toString() const {
	std::ostringstream os;
	os << "<parameter>\n";
	os << "name:\t\t"  << _name << "\n";
	os << "value:\t\t" << _value << " [" << _units << "]\n";
	os << "description:\t" << _description << "\n";
	os << "range:\t\t[" << _range.at(0) << ", " << _range.at(1) << "]\n";
	os << "calibrate:\t" << _calibrate << "\n";
	os << "</parameter>\n";
	return os.str();
}


std::ostream& operator<<(std::ostream& os, const Parameter& p) {
	os << p.toString();
	return os;
}

/**
 * @brief Checks whether the parameter value is contained within its allowed
 * range (throws an error if not)
 */
void Parameter::checkParamValue() const {
	if(_value < _range[0] || _value > _range[1]) {
		std::ostringstream os;
		os << "Cannot set value of parameter " << _name << " to " << _value
		   << ", value is out of allowed range [" << _range[0] << ","
		   << _range[1] << "]";
		throw mio::InvalidArgumentException(os.str(), AT);
	}
}

} /* namespace sflow */
