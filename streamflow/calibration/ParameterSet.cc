/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/calibration/ParameterSet.h>


namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class ParameterSet
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class ParameterSet initializing the parameter
 * set to an empty set.
 */
ParameterSet::ParameterSet() : _params() {}


/**
 * @brief Returns a begin iterator on the Parameter objects contained in the
 * parameter set.
 * @return Begin iterator
 */
std::vector<Parameter>::iterator ParameterSet::begin() {
	return _params.begin();
}


/**
 * @brief Returns a begin const_iterator on the Parameter objects contained in
 * the parameter set.
 * @return Begin const_iterator
 */
std::vector<Parameter>::const_iterator ParameterSet::begin() const {
	return _params.begin();
}


/**
 * @brief Returns an end iterator on the Parameter objects contained in
 * the parameter set.
 * @return End iterator
 */
std::vector<Parameter>::iterator ParameterSet::end() {
	return _params.end();
}


/**
 * @brief Returns an end const_iterator on the Parameter objects contained in
 * the parameter set.
 * @return End const_iterator
 */
std::vector<Parameter>::const_iterator ParameterSet::end() const {
	return _params.end();
}


/**
 * @brief Method to add a parameter to the set. An error is thrown if a
 * parameter with same name is already present in the set.
 * @param param Parameter to add to the set.
 */
void ParameterSet::add(const Parameter& param) {
	const std::string& name(param.getName());
	if(this->contains(name))
		throw RuntimeException("Parameter \"" + name + "\" already in the set.", AT);
	_params.push_back(param);
}


/**
 * @brief Method to add a list of parameters to the set. An error is thrown if
 * any parameter in the input list has the same name as one which is already
 * present in the set.
 * @param set Parameters to add to the set.
 */
void ParameterSet::add(const ParameterSet& set) {
	for(const Parameter& param : set)
		this->add(param);
}


/**
 * @brief Method to remove a parameter from the set. Nothing happens if no
 * parameter with the given name could be found.
 * @param name Name of the parameter to delete
 */
void ParameterSet::remove(const std::string& name) {
	for(std::vector<Parameter>::iterator it = _params.begin(); it != _params.end(); ++it) {
		if(it->getName() == name) {
			_params.erase(it);
			return;
		}
	}
}


/**
 * @brief Removes the parameters which are fixed (i.e. which should not be
 * calibrated) from the parameter set.
 */
void ParameterSet::removeFixedParameters() {
	std::vector<Parameter> newSet;
	newSet.reserve(_params.size());
	for(const Parameter& param : _params) {
		if(param.toBeCalibrated())
			newSet.push_back(param);
	}
	_params = newSet;
}


/**
 * @brief Overload of the operator [], not allowing any modification of
 * the returned element. An error is thrown if the specified parameter
 * is not present in the list.
 * @param name Name of the parameter to access.
 */
const Parameter& ParameterSet::operator[](const std::string& name) const {
	for(const Parameter& param : _params) {
		if(param.getName() == name) return param;
	}
	throw mio::InvalidArgumentException("Unknown parameter \"" + name + "\".", AT);
}


/**
 * @brief Overload of the operator [] which allows modification of
 * the returned element. An error is thrown if the specified parameter
 * is not present in the list.
 * @param name Name of the parameter to access.
 */
Parameter& ParameterSet::operator[](const std::string& name) {
	for(Parameter& param : _params) {
		if(param.getName() == name) return param;
	}
	throw mio::InvalidArgumentException("Unknown parameter \"" + name + "\".", AT);
}


/**
 * @brief Overload of the operator ==, returning true if the two
 * parameter sets contain the same parameters with identical respective
 * values (conversion is performed if the parameter units are not the
 * same).
 * @param a First object
 * @param b Second object
 * @return Are the two parameter sets identical?
 */
bool operator==(const ParameterSet& a, const ParameterSet& b) {
	if(a._params.size() != b._params.size()) return false;
	for(const Parameter& param : a._params) {
		if(!b.contains(param.getName()) || !(param.isEqual(b[param.getName()])))
			return false;
	}
	return true;
}


/**
 * @brief Overload of the operator !=, returning true if the two
 * parameter sets are different.
 * @param a First object
 * @param b Second object
 * @return Are the two parameter sets different?
 */
bool operator!=(const ParameterSet& a, const ParameterSet& b) {
	return !(a == b);
}


/**
 * @brief Method to check if the given parameter is already in the set.
 * @param name Name of the parameter to look for.
 */
bool ParameterSet::contains(const std::string& name) const {
	for(const Parameter& param : _params) {
		if(param.getName() == name) return true;
	}
	return false;
}


/**
 * @brief Method to get the names of the parameters currently contained in the
 * set.
 * @return Parameter names.
 */
const std::set<std::string> ParameterSet::getParameterNames() const {
	std::set<std::string> names;
	for(const Parameter& param : _params) {
		names.insert(param.getName());
	}
	return names;
}


/**
 * @brief Prepends the names of all the parameters in the set with the specified
 * prefix. For the parameters whose name is composed of several parts separated
 * by the characters '::', the prefix will be prepended to the last part (e.g.
 * HYDROMODEL::MUSKINGUM_K will become HYDROMODEL::[prefix]MUSKINGUM_K). This
 * method is the opposite of method ltrimNames.
 * @param prefix Prefix to prepend to all the parameter names
 */
void ParameterSet::prependNames(const std::string& prefix) {
	if(!prefix.empty()) {
		std::string name;
		for(Parameter& param : _params) {
			name = param.getName();
			const size_t sep(name.rfind("::"));
			if(sep == std::string::npos)
				param.setName(prefix + name);
			else
				param.setName(name.insert(sep + 2, prefix));
		}
	}
}


/**
 * @brief Removes the specified prefix from all the parameter names which start
 * with it. For the parameters whose name is composed of several parts separated
 * by the characters '::', the prefix search and removal (if applicable) will be
 * performed on the last part. This method is the opposite of method
 * prependNames.
 * @param prefix Prefix to be removed from the parameter names
 */
void ParameterSet::ltrimNames(const std::string& prefix) {
	if(!prefix.empty()) {
		const size_t lenPrefix = prefix.size();
		std::string name;
		for(Parameter& param : _params) {
			name = param.getName();
			const size_t sep(name.rfind("::"));
			if(sep == std::string::npos && name.find(prefix) == 0)
				param.setName(name.substr(lenPrefix));
			else if(sep != std::string::npos && name.rfind(prefix) == sep + 2)
				param.setName(name.erase(sep + 2, lenPrefix));
		}
	}
}


/**
 * @brief Method used to extract the subset of parameters whose name starts with
 * the given prefix.
 * @param prefix Prefix to be searched for
 */
ParameterSet ParameterSet::getSubset(const std::string& prefix) const {
	ParameterSet out;
	for(const Parameter& param : _params) {
		if(param.getName().compare(0, prefix.size(), prefix) == 0)
			out.add(param);
	}
	return out;
}


/**
 * @brief Returns the number of parameters contained in the set
 * @return Number of parameters currently contained in the set
 */
size_t ParameterSet::getNumParameters() const {
	return _params.size();
}


/**
 * @brief Method to convert the parameter set to a string
 */
const std::string ParameterSet::toString() const {
	std::ostringstream os;
	os << "<set>\n";
	for (const Parameter& param : _params) {
		os << param;
	}
	os << "</set>";
	return os.str();
}


std::ostream& operator<<(std::ostream& os, const ParameterSet& s) {
	os << s.toString();
	return os;
}



BestRun::BestRun(const std::map<size_t,size_t>& _id_to_pos, const std::vector<std::vector<std::reference_wrapper < const StreamVariable>>>& _variables,
	const std::vector<mio::StationData>& _stations_meta,  const std::vector<double>& _run_dates): id_to_pos(_id_to_pos), variables(_variables),
	stations_meta(_stations_meta), run_dates(_run_dates),run_data()
{

}

void BestRun::set(const std::vector< std::map< std::string, std::vector<double> > >& _run_data)
{
  run_data=_run_data;
}


const std::vector<mio::StationData>& BestRun::getStationsMeta() const
{
	return stations_meta;
}
const std::vector<std::vector<std::reference_wrapper < const StreamVariable> > >& BestRun::getRequiredVariable() const
{
	return variables;
}
const std::map<size_t,size_t>& BestRun::getIdToPos() const
{
	return id_to_pos;
}
const std::vector<double>& BestRun::getRunDates() const
{
	return run_dates;
}
const std::vector< std::map< std::string, std::vector<double> > >& BestRun::getRunData() const
{
	return run_data;
}


//-----------------------------------------------------------------------------
// Implementation of class ScoredParameterSet
//-----------------------------------------------------------------------------

/**
 * @brief Default constructor of class ScoredParameterSet
 */
ScoredParameterSet::ScoredParameterSet() : ParameterSet(),
		_score(std::numeric_limits<double>::max()), _perf_measures(),best_run(nullptr) {} //


/**
 * @brief Sets all the class attributes in one go
 * @param params Parameters to add to the set
 * @param score Evaluation score of the parameter set (the lower the score the
 * better)
 * @param perfMeasures Values of the model performance measures associated with
 * the current parameter set
 */
void ScoredParameterSet::set(const ParameterSet& params, const double& score,
		const std::map<std::string, double>& perfMeasures,const std::vector<mio::StationData>& station_meta,
		const std::vector<std::vector<std::reference_wrapper < const StreamVariable> > >& variables,
		const std::map<size_t,size_t>& id_to_pos,
		const std::vector<double>& run_dates,
		const std::vector< std::map< std::string, std::vector<double> > > & run_data) {

	_params.clear();
	_params.reserve(params.getNumParameters());
	for(const Parameter& param : params)
		_params.push_back(param);
	_score = score;
	_perf_measures = perfMeasures;
	if(!best_run){
		best_run = std::make_shared<BestRun>(id_to_pos,variables,station_meta, run_dates);
	}

	best_run->set(run_data);

}

void ScoredParameterSet::set(const ParameterSet& params, const double& score,
		const std::map<std::string, double>& perfMeasures) {

	_params.clear();
	_params.reserve(params.getNumParameters());
	for(const Parameter& param : params)
		_params.push_back(param);
	_score = score;
	_perf_measures = perfMeasures;

}

const std::shared_ptr<BestRun> ScoredParameterSet::getBestRun() const
{
	return best_run;
}


/**
 * @brief Sets the evaluation score of the parameter set
 * @param score Evaluation score
 */
void ScoredParameterSet::setScore(const double& score) {
	_score = score;
}


/**
 * @brief Sets the performance measure values associated with the parameter set
 * @param perfMeasures Names and values of the model performance measures
 * associated with the current parameter set
 */
void ScoredParameterSet::setPerfMeasures(const std::map<std::string, double>& perfMeasures) {
	_perf_measures = perfMeasures;
}


/**
 * @brief Returns the score of the parameter set
 * @return Parameter set score
 */
const double& ScoredParameterSet::getScore() const {
	return _score;
}


/**
 * @brief Returns the names and values of the model performance values
 * associated with the parameter set
 * @return Values of the model performance measures
 */
const std::map<std::string, double>& ScoredParameterSet::getPerfMeasures() const {
	return _perf_measures;
}


/**
 * @brief Returns the number of model performance measures associated with
 * the current parameter set
 * @return Number of performance measures
 */
size_t ScoredParameterSet::getNumPerfMeasures() const {
	return _perf_measures.size();
}


/**
 * @brief Returns whether two ScoredParameterSet objects are the same
 * @param a First object
 * @param b Second object
 * @return Is a equal to b?
 */
bool operator==(const ScoredParameterSet& a, const ScoredParameterSet& b) {
	if(a._score != b._score || a._perf_measures.size() != b._perf_measures.size() ||
			dynamic_cast<const ParameterSet&>(a) != dynamic_cast<const ParameterSet&>(b)) {
		return false;
	} else {
		for(const std::pair<const std::string, double>& meas : a._perf_measures) {
			if(b._perf_measures.count(meas.first) == 0 ||
					b._perf_measures.at(meas.first) != meas.second)
				return false;
		}
		return true;
	}
}

bool operator==(const ParameterSet& a, const ScoredParameterSet& b) {
	return a == dynamic_cast<const ParameterSet&>(b);
}

bool operator==(const ScoredParameterSet& a, const ParameterSet& b) {
	return dynamic_cast<const ParameterSet&>(a) == b;
}


/**
 * @brief Returns whether two ScoredParameterSet objects are different
 * @param a First object
 * @param b Second object
 * @return Is a different from b?
 */
bool operator!=(const ScoredParameterSet& a, const ScoredParameterSet& b) {
	return !(a == b);
}

bool operator!=(const ParameterSet& a, const ScoredParameterSet& b) {
	return a != dynamic_cast<const ParameterSet&>(b);
}

bool operator!=(const ScoredParameterSet& a, const ParameterSet& b) {
	return dynamic_cast<const ParameterSet&>(a) != b;
}


/**
 * @brief Compares two ScoredParameterSet objects. The lhs is considered to be
 * smaller than the rhs if its score is lower (i.e. better)
 * @param a First object
 * @param b Second object
 * @return Is a better than b?
 */
bool operator<(const ScoredParameterSet& a, const ScoredParameterSet& b) {
	return a._score < b._score;
}


/**
 * @brief Compares two ScoredParameterSet objects. The lhs is considered to be
 * smaller or equal to the rhs if its score is lower (i.e. better) or equal
 * to the one of the rhs
 * @param a First object
 * @param b Second object
 * @return Is a better than b?
 */
bool operator<=(const ScoredParameterSet& a, const ScoredParameterSet& b) {
	return a._score <= b._score;
}


/**
 * @brief Compares two ScoredParameterSet objects. The lhs is considered to be
 * greater than the rhs if its score is greater (i.e. worse)
 * @param a First object
 * @param b Second object
 * @return Is a worse than b?
 */
bool operator>(const ScoredParameterSet& a, const ScoredParameterSet& b) {
	return a._score > b._score;
}


/**
 * @brief Compares two ScoredParameterSet objects. The lhs is considered to be
 * greater or equal to the rhs if its score is greater (i.e. worse) or equal
 * to the one of the rhs
 * @param a First object
 * @param b Second object
 * @return Is a worse than b?
 */
bool operator>=(const ScoredParameterSet& a, const ScoredParameterSet& b) {
	return a._score >= b._score;
}


} /* namespace sflow */
