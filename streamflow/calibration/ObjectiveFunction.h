/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OBJECTIVEFUNCTION_H_
#define OBJECTIVEFUNCTION_H_

#include <set>
#include <map>
#include <vector>
#include <string>
#include <sstream>
#include <numeric>
#include <functional>
#include <meteoio/MeteoIO.h>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/utils/StreamflowExceptions.h>
#include <streamflow/calibration/PerformanceMeasure.h>
#include <streamflow/io/SimulatedDataContainer.h>


namespace sflow {

class Watershed;     //< to avoid unnecessary includes
class StreamNetwork; //< to avoid unnecessary includes
class CalibrationDataContainer;
/**
 * @class ObjectiveFunction
 * @ingroup calibration
 */
class ObjectiveFunction : public ModelObserver {
	public:
		ObjectiveFunction(const mio::Config& cfg, CalibrationDataContainer& _measures_reader, SimulatedDataContainer _simulation_reader);
		void reset() override;
		void update(const mio::Date& currTime, const Watershed& watershed,
				const StreamNetwork& network) override;
		double getValue();
		std::map<std::string, double> getPerfMeasures() const;
		const std::vector<std::string>& getPerfMeasureNames() const;
		//std::vector<double> getPerfMeasureValues() const;
		std::vector<std::vector<size_t>> getPerfMeasuresPoints(const mio::Config& cfg) const;
		std::vector<double> getWeights(const mio::Config& cfg) const;
		std::vector<bool> getDaily(const mio::Config& cfg) const;

		const std::vector<mio::StationData>& getStationsMeta() const;
		const std::vector<std::vector<std::reference_wrapper < const StreamVariable> > >& getRequiredVariable() const;
		const std::map<size_t,size_t>& getIdToPos() const;
		const std::vector<double>& getRunDates() const;
		const std::vector< std::map< std::string, std::vector<double> > >& getRunData() const;


	protected:
		typedef std::function<double(const SimulatedDataContainer&, const CalibrationDataContainer&, const std::vector<size_t>&, const HydroVariable&, const bool)> PerfMeasFct;

		CalibrationDataContainer& measures_reader;
		SimulatedDataContainer simulation_reader;

		std::vector<PerfMeasFct> perf_measures;
		std::vector<std::string> _perf_measure_names;
		std::vector<std::vector<size_t>> perf_measure_points;
		std::vector<double> weights;
		std::vector<bool> daily;

		const std::vector<std::reference_wrapper < const HydroVariable> >  variables;
		std::map<std::string, double> perf_measures_score;


		static const std::string _FUNCTION_KEY_PATTERN;

		PerfMeasFct getPerfMeasure(const std::string& name,
				const mio::Config& cfg, const std::vector<size_t>& points);
		static std::vector<std::string> getPerfMeasureNames(const mio::Config& cfg);
		const std::vector<std::reference_wrapper < const HydroVariable> > getVariables(const std::vector<std::string>& names,
				const mio::Config& cfg);
		static const HydroVariable& getVariable(const std::string& name);
};

} /* namespace sflow */

#endif /* OBJECTIVEFUNCTION_H_ */
