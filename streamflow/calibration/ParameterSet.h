/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PARAMETERSET_H_
#define PARAMETERSET_H_

#include <string>
#include <vector>
#include <limits>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include <streamflow/calibration/Parameter.h>
#include <streamflow/utils/StreamflowExceptions.h>
#include <streamflow/utils/HydroVariable.h>

namespace sflow {

/**
 * @class ParameterSet
 * @brief A class to represent a set of parameters, intended to be
 * used in association with the Module class. Each parameter is uniquely
 * identified by its name within the set.
 * @ingroup calibration
 * @author A. Gallice
 * @date 2015-03-04
 */
class ParameterSet {
	public:
		ParameterSet();
		ParameterSet(const ParameterSet&) = default;
		ParameterSet& operator=(const ParameterSet&) = default;
		virtual ~ParameterSet() = default;

		std::vector<Parameter>::iterator begin();
		std::vector<Parameter>::const_iterator begin() const;
		std::vector<Parameter>::iterator end();
		std::vector<Parameter>::const_iterator end() const;
		void add(const Parameter& param);
		void add(const ParameterSet& set);
		void remove(const std::string& name);
		void removeFixedParameters();
		const Parameter& operator[](const std::string& name) const;
		Parameter& operator[](const std::string& name);
		bool contains(const std::string& name) const;
		const std::set<std::string> getParameterNames() const;
		void prependNames(const std::string& prefix);
		void ltrimNames(const std::string& prefix);
		ParameterSet getSubset(const std::string& pattern) const;
		size_t getNumParameters() const;
		const std::string toString() const;

		friend bool operator==(const ParameterSet& a, const ParameterSet& b);
		friend bool operator!=(const ParameterSet& a, const ParameterSet& b);
		friend std::ostream& operator<<(std::ostream& os, const ParameterSet& s);

	protected:
		std::vector<Parameter> _params;

};



class BestRun {

	public:
		BestRun(const std::map<size_t,size_t>& _id_to_pos, const std::vector<std::vector<std::reference_wrapper < const StreamVariable>>>& _variables,
			const std::vector<mio::StationData>& _stations_meta, const std::vector<double>& _dates);
		BestRun(const BestRun&) = delete;
		BestRun& operator=(const BestRun&) = delete;
		void set(const std::vector< std::map< std::string, std::vector<double> > >& _run_data);
		const std::vector<mio::StationData>& getStationsMeta() const;
		const std::vector<std::vector<std::reference_wrapper < const StreamVariable> > >& getRequiredVariable() const;
		const std::map<size_t,size_t>& getIdToPos() const;
		const std::vector<double>& getRunDates() const;
		const std::vector< std::map< std::string, std::vector<double> > >& getRunData() const;


	private:
		const std::map<size_t,size_t> id_to_pos;
		const std::vector<std::vector<std::reference_wrapper < const StreamVariable> > >  variables;
		const std::vector<mio::StationData> stations_meta;
		const std::vector<double> run_dates;
		std::vector< std::map< std::string, std::vector<double> > > run_data;
};

/**
 * @class ScoredParameterSet
 * @brief This class implements a scored parameter set, i.e. a parameter set
 * which has been evaluated with respect to a given objective function and can
 * therefore be ranked with respect to other parameter sets.
 * @ingroup calibration
 * @author Aurelien Gallice
 * @date 2015-09-30
 */
class ScoredParameterSet : public ParameterSet {
	public:
		ScoredParameterSet();

		void set(const ParameterSet& params, const double& score,
				const std::map<std::string, double>& perfMeasures);

		void set(const ParameterSet& params, const double& score,
				const std::map<std::string, double>& perfMeasures, const std::vector<mio::StationData>& station_meta,
				const std::vector<std::vector<std::reference_wrapper < const StreamVariable> > >& variables,
				const std::map<size_t,size_t>& id_to_pos,
				const std::vector<double>& run_dates,
				const std::vector< std::map< std::string, std::vector<double> > > & run_data);

		const std::shared_ptr<BestRun> getBestRun() const;

		void setScore(const double& score);
		void setPerfMeasures(const std::map<std::string, double>& perfMeasures);
		const double& getScore() const;
		const std::map<std::string, double>& getPerfMeasures() const;
		size_t getNumPerfMeasures() const;

		friend bool operator==(const ScoredParameterSet& a, const ScoredParameterSet& b);
		friend bool operator==(const ParameterSet& a, const ScoredParameterSet& b);
		friend bool operator==(const ScoredParameterSet& a, const ParameterSet& b);
		friend bool operator!=(const ScoredParameterSet& a, const ScoredParameterSet& b);
		friend bool operator!=(const ParameterSet& a, const ScoredParameterSet& b);
		friend bool operator!=(const ScoredParameterSet& a, const ParameterSet& b);
		friend bool operator<(const ScoredParameterSet& a, const ScoredParameterSet& b);
		friend bool operator<=(const ScoredParameterSet& a, const ScoredParameterSet& b);
		friend bool operator>(const ScoredParameterSet& a, const ScoredParameterSet& b);
		friend bool operator>=(const ScoredParameterSet& a, const ScoredParameterSet& b);

	protected:
		double _score;
		std::map<std::string, double> _perf_measures;
		std::shared_ptr<BestRun> best_run;

};

} /* namespace sflow */

#endif /* PARAMETERSET_H_ */
