/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LUMPEDSUBWATERSHEDDECORATOR_H_
#define LUMPEDSUBWATERSHEDDECORATOR_H_

#include <sstream>
#include <iostream>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/utils/StreamflowExceptions.h>
#include <streamflow/calibration/Parameter.h>
#include <streamflow/calibration/ParameterSet.h>
#include <streamflow/subwatershed/Subwatershed.h>
#include <streamflow/subwatershed/LumpedSubwatershed.h>
#include <streamflow/streamReach/StreamCell.h>

namespace sflow {

/**
 * @class LumpedSubwatershedDecorator
 * @brief This class implements a decorator to class LumpedSubwatershedInterface.
 * For a description of the decorator pattern,
 * see <a>https://en.wikipedia.org/wiki/Decorator_pattern</a>. Only a decorator
 * enabling the watershed to compute water temperature has been implemented so
 * far (see class LumpedSubwatershedTempInterface), but other decorators (e.g. to
 * compute solute transport) could be added...
 * @ingroup subwatershed
 * @author Aurelien Gallice
 * @date 2015-07-02
 */
class LumpedSubwatershedDecorator : public LumpedSubwatershedInterface {
	using LumpedSubwatershedInterface::getStreamLateralInflow; //< import all overloaded methods getStreamLateralInflow() of base class

	public:
		LumpedSubwatershedDecorator(LumpedSubwatershedInterface *wrappedObject);
		LumpedSubwatershedDecorator(const LumpedSubwatershedDecorator&) = delete;
		LumpedSubwatershedDecorator& operator=(const LumpedSubwatershedDecorator&) = delete;
		virtual ParameterSet getParams() const override;
		virtual void setParams(const ParameterSet&) override;
		virtual void advanceInTime() override;
		virtual void reset() override;
		virtual void setDisplayInfo(const bool& display) override;
		virtual bool isHeadSubwatershed() const override;
		virtual bool isSet() const override;
		virtual bool contains(const StreamReach& reach,
				const bool& discardLastCell) const override;
		virtual void setStreamReach(const StreamReach& reach) override;
		virtual const StreamReach& getStreamReach() const override;
		virtual const mio::Grid2DObject& getMask() const override;
		virtual size_t getIndex() const override;
		virtual double getArea() const override;
		virtual double get(const SubwatershedVariable& var) const override;
		virtual void getStreamLateralInflow(CellInflow& inflow) const override;
		virtual void getStreamLateralInflow(std::vector<CellInflow>& inflow) const override;
		virtual void updateBoundaryConditions() override;
		virtual void initialize() override;
		virtual ~LumpedSubwatershedDecorator() override;

	protected:
		LumpedSubwatershedInterface *_wrapped_object;

	friend class Watershed;
};

/**
 * @class SimpleSubwatershedWithEB
 * @brief This class implements the machinery for computing the temperature of
 * the subsurface runoff flowing out of a subwatershed. It is based on the
 * energy-balance of the subwatershed, treated as a single point (see Comola
 * et al, 2015).
 *
 * Note: this decorator can only be applied to TwoVerticalLinearReservoirs objects.
 *
 * Reference:
 * Comola, F., Schaefli, B., Rinaldo, A., and Lehning, M., Thermodynamics in the
 * hydrologic response: Travel time formulation and application to Alpine
 * catchments, WRR, 51, 2015.
 * @ingroup subwatershed
 * @author Aurelien Gallice
 * @date 2015-10-05
 */
class SimpleSubwatershedWithEB : public LumpedSubwatershedDecorator {
	using LumpedSubwatershedDecorator::getStreamLateralInflow; //< import all overloaded methods getStreamLateralInflow() of base class

	public:
		class Parameters;

		SimpleSubwatershedWithEB(LumpedSubwatershedInterface *wrappedObject,
				Parameters& diffTime, std::shared_ptr<DataContainer> dataContainer);
		virtual void updateBoundaryConditions() override;
		virtual void advanceInTime() override;
		virtual void reset() override;
		virtual double get(const SubwatershedVariable& var) const override;
		virtual void getStreamLateralInflow(CellInflow& inflow) const override;
		virtual void getStreamLateralInflow(std::vector<CellInflow>& inflow) const override;
		virtual void initialize() override;

		virtual ~SimpleSubwatershedWithEB() override;

	protected:
		Parameters& _diff_time;
		std::shared_ptr<DataContainer> dataContainer;
		double _upper_res_temp;   //< [K] temperature of water stored in the upper compartment
		double _lower_res_temp;   //< [K] temperature of water stored in the lower compartment
		double _outlet_temp;      //< [K] temperature of water flowing out of the subwatershed
		double _old_inflow_temp;  //< [K] temperature of the infiltration water at the previous time step
		double _curr_inflow_temp; //< [K] temperature of water flowing from the bottom of the Alpine3D surface layer into the compartments
		double _soil_temp;  //< [K] soil temperature (assumed constant)
		static const double _MIN_RESERVOIR_LEVEL; //< [m] level below which the reservoir is assumed to be empty

		virtual double getSoilTemperature() const;
};


/**
 * class SimpleSubwatershedWithEB::Parameters
 * @brief This class holds the value of the characteristic time of energy
 * exchange between each reservoir of the subwatershed model and the
 * surrounding soil.
 * @ingroup subwatershed
 * @author Aurelien Gallice
 * @date 2015-10-09
 */
class SimpleSubwatershedWithEB::Parameters : public SharedObject,
		public ParametrizableObject {
	public:
		Parameters(const mio::Config& cfg);
		virtual const double& getTempDiffusionTime() const;
		virtual ParameterSet getParams() const override;
		virtual void setParams(const ParameterSet& params) override;
		virtual ~Parameters() override;

	protected:
		Parameter _diff_time;
		double _diff_time_SI; //< [s] value of _diff_time in SI units (to avoid conversion at each time step)
};


/**
 * @class SubwatershedWithSoilTempOutflow
 * @brief This class decorates LumpedSubwatershed elements by giving them the
 * ability to compute the temperature of water infiltrating into the stream.
 * This temperature is assumed to be the same as the soil temperature simulated
 * by Alpine3D over the reach cells at a given depth.
 * @ingroup subwatershed
 * @author Aurelien Gallice
 * @date 2015-11-25
 */
class SubwatershedWithSoilTempOutflow : public LumpedSubwatershedDecorator {
	using LumpedSubwatershedDecorator::getStreamLateralInflow; //< import all overloaded methods getStreamLateralInflow() of base class

	public:
		SubwatershedWithSoilTempOutflow(LumpedSubwatershedInterface *wrappedObject,
				 std::shared_ptr<DataContainer> dataContainer);
		virtual void advanceInTime() override;
		virtual double get(const SubwatershedVariable& var) const override;
		virtual void getStreamLateralInflow(CellInflow& inflow) const override;
		virtual void getStreamLateralInflow(std::vector<CellInflow>& inflow) const override;
		virtual ~SubwatershedWithSoilTempOutflow() override;

	protected:
		mio::Date _curr_time;     //< time corresponding to the current subwatershed state (might be current simulation time + dt)
		std::shared_ptr<DataContainer> dataContainer;
};


/**
 * @class SubwatershedWithHSPFOutflowTemp
 * @brief This class decorates LumpedSubwatershed elements by giving them the
 * ability to compute the temperature of water infiltrating into the stream
 * according to the procedure implemented in the HSPF model (see Leach and
 * Moore, 2015).
 *
 * Reference:
 * Leach and Moore, Observations and modeling of hillslope throughflow
 * temperatures in a coastal forested catchment, WRR, 51, 2015
 * @ingroup subwatershed
 * @author Aurelien Gallice
 * @date 2015-11-25
 */
class SubwatershedWithHSPFOutflowTemp : public LumpedSubwatershedDecorator {
	using LumpedSubwatershedDecorator::getStreamLateralInflow; //< import all overloaded methods getStreamLateralInflow() of base class

	public:
		class Parameters;

		SubwatershedWithHSPFOutflowTemp(LumpedSubwatershedInterface *wrappedObject,
				const Parameters& params, std::shared_ptr<DataContainer> dataContainer);
		virtual void updateBoundaryConditions() override;
		virtual void advanceInTime() override;
		virtual void reset() override;
		virtual double get(const SubwatershedVariable& var) const override;
		virtual void getStreamLateralInflow(CellInflow& inflow) const override;
		virtual void getStreamLateralInflow(std::vector<CellInflow>& inflow) const override;
		virtual ~SubwatershedWithHSPFOutflowTemp() override;
		virtual void initialize() override;

	protected:
		const Parameters& _params;
		double _inflow_temperature;   //< [K]
		double _air_temperature_old;  //< [K]
		double _air_temperature_curr; //< [K]
		std::shared_ptr<DataContainer> dataContainer;
		virtual void updateAirTemperature();
};


/**
 * @class SubwatershedWithHSPFOutflowTemp::Parameters
 * @brief This class contains the parameters of the HSPF model used to compute
 * the temperature of water infiltrating from the hillslopes into the stream.
 * @ingroup subwatershed
 * @author Aurelien Gallice
 * @date 2015-11-27
 */
class SubwatershedWithHSPFOutflowTemp::Parameters : public SharedObject,
			public ParametrizableObject {
	public:
		Parameters(const mio::Config& cfg);
		virtual ParameterSet getParams() const override;
		virtual void setParams(const ParameterSet& params) override;
		virtual const double& getTemperatureDifference() const;
		virtual const double& getSmoothingFactor() const;
		virtual ~Parameters() override;

	protected:
		Parameter _temperature_diff; //< [K] difference between mean air temperature and mean temperature of infiltrating water
		Parameter _smoothing_factor; //< [-] the closer this factor to zero, the smoother the curve of infiltrating water temperature
};

} /* namespace sflow */

#endif /* LUMPEDSUBWATERSHEDDECORATOR_H_ */
