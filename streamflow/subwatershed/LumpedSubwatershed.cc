/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/subwatershed/LumpedSubwatershed.h>

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class LumpedSubwatershed
//-----------------------------------------------------------------------------

LumpedSubwatershed::LumpedSubwatershed(const mio::Grid2DObject& grid,
		const size_t& index, const Clock& clk) :
		LumpedSubwatershedInterface(clk), _mask(constructMask(grid, index)),
		_index(index), _area(computeArea(_mask)), _reach(nullptr),
		_source_drainage_fraction(0.0), _cell_drainage_fractions() {}


LumpedSubwatershed::LumpedSubwatershed(const mio::Coords& llcorner, const double& cellSize,
		const std::vector<size_t>& ix, const std::vector<size_t>& iy, const size_t& index,
		const Clock& clk) :
		LumpedSubwatershedInterface(clk),
		_mask(constructMask(llcorner, cellSize, ix, iy)), _index(index),
		_area(computeArea(_mask)), _reach(nullptr),
		_source_drainage_fraction(0.0), _cell_drainage_fractions() {}


LumpedSubwatershed::LumpedSubwatershed(const LumpedSubwatershed& copy, const Clock& _clk) :
		LumpedSubwatershedInterface(_clk), _mask(copy._mask),
		_index(copy._index), _area(copy._area), _reach(copy._reach),
		_source_drainage_fraction(copy._source_drainage_fraction),
		_cell_drainage_fractions(copy._cell_drainage_fractions) {}


bool LumpedSubwatershed::isHeadSubwatershed() const {
	return _reach && _reach->isHeadReach();
}


bool LumpedSubwatershed::isSet() const {
	return _reach;
}


bool LumpedSubwatershed::contains(const StreamReach& reach,
		const bool& discardLastCell) const {
	bool areAllCellsInMask(true);
	const size_t iLastCell(reach.getNumCells() - 1 - discardLastCell);
	for(size_t iCell(0); iCell <= iLastCell; ++iCell) {
		mio::Coords pt(reach.getCell(iCell).getCoords());
		pt.setXY(reach.getCell(iCell).getCoords().getEasting(),reach.getCell(iCell).getCoords().getNorthing(),reach.getCell(iCell).getCoords().getAltitude());
		if(!_mask.gridify(pt) || _mask(pt.getGridI(),pt.getGridJ()) == mio::IOUtils::nodata) {
			areAllCellsInMask = false;
			break;
		}
	}
	return areAllCellsInMask;
}


void LumpedSubwatershed::setStreamReach(const StreamReach& reach) {
	_reach = &reach;
	if(reach.isHeadReach()) {
		_source_drainage_fraction = reach.getCell(0).tot_drainage_area/_area;
		_cell_drainage_fractions  = this->computeCellDrainageFractions(true);
	} else {
		_source_drainage_fraction = 0.0;
		_cell_drainage_fractions  = this->computeCellDrainageFractions(false);
	}
}


const StreamReach& LumpedSubwatershed::getStreamReach() const {
	if(!_reach) {
		std::ostringstream os;
		os << "The stream reach associated with subwatershed #" << _index
		   << " has not been defined";
		throw RuntimeException(os.str(), AT);
	}
	return *_reach;
}


size_t LumpedSubwatershed::getIndex() const {
	return _index;
}


double LumpedSubwatershed::getArea() const {
	return _area;
}


const mio::Grid2DObject& LumpedSubwatershed::getMask() const {
	return _mask;
}


double LumpedSubwatershed::get(const SubwatershedVariable& var) const {
	if(var == SubwatershedVariable::Q_source)
		return this->get(SubwatershedVariable::Q_outlet)*_source_drainage_fraction;
	else
		return mio::IOUtils::nodata;
}


void LumpedSubwatershed::getStreamLateralInflow(CellInflow& inflow) const {
	inflow.discharge = this->get(SubwatershedVariable::Q_outlet)*
			(1.0 - _source_drainage_fraction);
}


void LumpedSubwatershed::getStreamLateralInflow(std::vector<CellInflow>& inflow) const {
	if(!_reach) {
		std::ostringstream os;
		os << "The stream reach associated with subwatershed #" << _index
		   << " has not been defined";
		throw RuntimeException(os.str(), AT);
	}

	const double outletDischarge = this->get(SubwatershedVariable::Q_outlet);
	inflow.resize(_cell_drainage_fractions.size());
	for(size_t iCell = 0; iCell < _cell_drainage_fractions.size(); ++iCell) {
		inflow[iCell].discharge = outletDischarge*_cell_drainage_fractions[iCell];
	}
}


LumpedSubwatershed::~LumpedSubwatershed() {}


std::vector<double> LumpedSubwatershed::computeCellDrainageFractions(const bool& skipFirstCell) const {
	const size_t nCells(_reach->getNumCells());

	std::vector<double> fractions(nCells, 0.);
	double total(0.0), fraction;
	for(size_t iCell = size_t(skipFirstCell); iCell < nCells; ++iCell) {
		fraction = _reach->getCell(iCell).spec_drainage_area/_area;
		fractions[iCell] = fraction;
		total += fraction;
	}
	if(skipFirstCell) total += _source_drainage_fraction;

	if(fabs(total - 1.0) > 1e-5) {
		std::ostringstream os;
		os << "Error when constructing subwatershed " << _index
		   << ": the respective fractions of the subwatershed runoff which flow "
		   << "into the reach source point and the reach cells sum up to "
		   << total << " (1.0 expected)";
		//throw RuntimeException(os.str(), AT);
		std::cout << os.str() << std::endl;
	}
	return fractions;
}


mio::Grid2DObject LumpedSubwatershed::constructMask(const mio::Grid2DObject& grid,
		const size_t& index, const double& eps) {
	//Get the indices of the cells belonging to the mask
	const size_t nx(grid.getNx());
	const size_t ny(grid.getNy());
	const size_t ntot(nx*ny);
	std::vector<size_t> ixMask, iyMask;
	ixMask.reserve(ntot);
	iyMask.reserve(ntot);
	for(size_t iy = 0; iy < ny; ++iy) {
		for(size_t ix = 0; ix < nx; ++ix) {
			if(fabs(grid(ix,iy) - double(index)) < eps) {
				ixMask.push_back(ix);
				iyMask.push_back(iy);
			}
		}
	}
	ixMask.shrink_to_fit();
	iyMask.shrink_to_fit();
	if(ixMask.empty() || iyMask.empty()) {
		std::ostringstream os;
		os << "Could not find any cell belonging to subwatershed " << index
		   << " in the grid defining the subwatersheds";
		throw mio::InvalidArgumentException(os.str(), AT);
	}

	return constructMask(grid.llcorner, grid.cellsize, ixMask, iyMask);
}


mio::Grid2DObject LumpedSubwatershed::constructMask(const mio::Coords& llcorner,
		const double& cellSize, const std::vector<size_t>& ixMask,
		const std::vector<size_t>& iyMask) {
	if(ixMask.size() != iyMask.size())
		throw mio::InvalidArgumentException("The vectors containing the row and "
				"column indices of the cells belonging to the mask must have the "
				"same number of elements", AT);

	//Get min and max indices in the x and y directions
	const size_t ixMin(*std::min_element(ixMask.begin(), ixMask.end()));
	const size_t ixMax(*std::max_element(ixMask.begin(), ixMask.end()));
	const size_t iyMin(*std::min_element(iyMask.begin(), iyMask.end()));
	const size_t iyMax(*std::max_element(iyMask.begin(), iyMask.end()));

	//Create mask, i.e. a grid containing 1.0 in the cells belonging to the
	//mask and nodata elsewhere
	mio::Grid2DObject mask(ixMax+1, iyMax+1, cellSize, llcorner,
			mio::IOUtils::nodata);
	for(size_t iCell = 0; iCell < ixMask.size(); ++iCell) {
			mask(ixMask.at(iCell),iyMask.at(iCell)) = 1.0;
	}

	//Crop mask so as to remove as many nodata cells as possible
	const size_t nx(ixMax - ixMin + 1);
	const size_t ny(iyMax - iyMin + 1);
	mio::Grid2DObject croppedMask(mask, ixMin, iyMin, nx, ny);
	return croppedMask;
}


double LumpedSubwatershed::computeArea(const mio::Grid2DObject& mask) {
	return double(mask.grid2D.getCount())*mask.cellsize*mask.cellsize;
}


//-----------------------------------------------------------------------------
// Implementation of class TwoVerticalLinearReservoirs
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class TwoVerticalLinearReservoirs
 * @param grid Grid from which the subwatershed should be extracted. In this grid,
 * all the cells belonging to the subwatershed should be identified with a unique
 * number corresponding to the subwatershed index
 * @param index Subwatershed index
 * @param clk Reference to an external Clock object holding the current
 * simulation time
 * @param reader Reference to an external SubwatershedInflowReader object used
 * to get the boundary conditions of the subwatershed
 * @param params Reference to the instance of class Parameters which contains
 * the subwatershed parameters
 */
TwoVerticalLinearReservoirs::TwoVerticalLinearReservoirs(const mio::Grid2DObject& grid,
		const size_t& index, const Clock& clk, Parameters& params, std::shared_ptr<DataContainer> dataCont) :
				LumpedSubwatershed(grid, index, clk), _params(params), _upper_res(0., 0., 0.),
				_lower_res(0., 0.3, 0.), dataContainer(dataCont)
{

}

TwoVerticalLinearReservoirs::TwoVerticalLinearReservoirs(const TwoVerticalLinearReservoirs& _TwoVerticalLinearReservoirs,
		const Clock& _clk, Parameters& params) :
				LumpedSubwatershed(_TwoVerticalLinearReservoirs, _clk), _params(params), _upper_res(0., 0., 0.),
				_lower_res(0., 0.3, 0.), dataContainer(_TwoVerticalLinearReservoirs.dataContainer)
{

}

void TwoVerticalLinearReservoirs::initialize()
{
	this->updateReservoirOutflowDischarges();
	this->updateBoundaryConditions();
	_upper_res.old_inflow = _upper_res.curr_inflow;
	_lower_res.old_inflow = _lower_res.curr_inflow;
}

/**
 * @brief Constructor of class SimpleSubwatershed, expecting all the required class
 * attributes as input parameters
 * @param llcorner Coordinates of lower-left corner of the watershed's mask
 * @param cellSize Length of the edge of a cell of the watershed's mask
 * @param ix Column indices of the mask cells which belong to the watershed
 * (easting indices)
 * @param iy Row indices of the mask cells which belong to the watershed
 * (northing indices)
 * @param index Watershed index
 * @param clk Reference to the Clock instance holding the current simulation
 * time
 * @param reader Reference to an external SubwatershedInflowReader object used
 * to get the boundary conditions of the subwatershed
 * @param params Reference to the instance of class Parameters which contains
 * the subwatershed parameters
 */
TwoVerticalLinearReservoirs::TwoVerticalLinearReservoirs(const mio::Coords& llcorner,
		const double& cellSize, const std::vector<size_t>& ix,
		const std::vector<size_t>& iy, const size_t& index, const Clock& clk,
		Parameters& params, std::shared_ptr<DataContainer> dataCont) :
				LumpedSubwatershed(llcorner, cellSize, ix, iy, index, clk),
				_params(params),
				_upper_res(0., 0., 0.), _lower_res(0., 0.3, 0.), dataContainer(dataCont)
{
	this->updateReservoirOutflowDischarges();
	this->updateBoundaryConditions();
	_upper_res.old_inflow = _upper_res.curr_inflow;
	_lower_res.old_inflow = _lower_res.curr_inflow;
}


void TwoVerticalLinearReservoirs::advanceInTime() {
	const double dt(_clock.getTimeStep());
	const double lowerResInflow((_lower_res.old_inflow + _lower_res.curr_inflow)/2.0);
	const double upperResInflow((_upper_res.old_inflow + _upper_res.curr_inflow)/2.0);
	const double upperResTau(_params.getUpperReservoirTau(*this));
	const double lowerResTau(_params.getLowerReservoirTau(*this));
	const double upperFactor(dt/(2.0*upperResTau));
	const double lowerFactor(dt/(2.0*lowerResTau));
	_upper_res.level = (dt*upperResInflow + (1.0 - upperFactor)*
			_upper_res.level)/(1.0 + upperFactor);
	_upper_res.level = fmax(0.0, _upper_res.level);
	_lower_res.level = (dt*lowerResInflow + (1.0 - lowerFactor)*
			_lower_res.level)/(1.0 + lowerFactor);
	_lower_res.level = fmax(0.0, _lower_res.level);
	_upper_res.outflow = _upper_res.level/upperResTau;
	_lower_res.outflow = _lower_res.level/lowerResTau;
}


void TwoVerticalLinearReservoirs::setDisplayInfo(const bool&) {
	//do nothing
}


/**
 * @brief Method used to update the boundary conditions of the subwatershed.
 */
void TwoVerticalLinearReservoirs::updateBoundaryConditions() {
	_upper_res.old_inflow = _upper_res.curr_inflow;
	_lower_res.old_inflow = _lower_res.curr_inflow;
	const double currInflow = dataContainer->getLumpedMeteoData(_clock.getNextTime(),
			"ROT", *this)*(1.-_params.getFracLostWater());
	//const double currInflow = _input_reader.getLumpedMeteoData(_clock.getNextTime(),
	//		"ROT", *this);
	const double infiltration = fmax(0.0, currInflow)/_area; //< [m/s]
	_lower_res.curr_inflow = fmin(_params.getMaxInfiltrationRate(), infiltration);
	_upper_res.curr_inflow = infiltration - _lower_res.curr_inflow;
}


ParameterSet TwoVerticalLinearReservoirs::getParams() const {
	ParameterSet params; //< empty set
	return params;
}


void TwoVerticalLinearReservoirs::setParams(const ParameterSet&) {
	//do nothing
}


void TwoVerticalLinearReservoirs::reset() {
	_upper_res.level = 0.0;
	_lower_res.level = 0.3;
	this->updateReservoirOutflowDischarges();
	this->updateBoundaryConditions();
	_upper_res.old_inflow = _upper_res.curr_inflow;
	_lower_res.old_inflow = _lower_res.curr_inflow;
}


double TwoVerticalLinearReservoirs::get(const SubwatershedVariable& var) const {
	if(var == SubwatershedVariable::Q_outlet)
		return (_lower_res.outflow + _upper_res.outflow)*_area; //< [m^3/s]
	else
		return LumpedSubwatershed::get(var);
}


/**
 * @brief Destructor of class TwoVerticalLinearReservoirs
 */
TwoVerticalLinearReservoirs::~TwoVerticalLinearReservoirs() {}


/**
 * @brief Method updating the values of the ouflow discharges of the upper and
 * lower reservoirs
 */
void TwoVerticalLinearReservoirs::updateReservoirOutflowDischarges() {
	_upper_res.outflow = _upper_res.level/_params.getUpperReservoirTau(*this);
	_lower_res.outflow = _lower_res.level/_params.getLowerReservoirTau(*this);
}


//-----------------------------------------------------------------------------
// Implementation of class TwoVerticalLinearReservoirs::Parameters
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class TwoVerticalLinearReservoirs\::Parameters
 * @param cfg Config object holding the model parameters
 * @param watershedArea Area of the watershed defined by all the considered
 * subwatersheds, in [km^2]
 */
TwoVerticalLinearReservoirs::Parameters::Parameters(const mio::Config& cfg,
		const double& watershedArea) :
		ParametrizableObject(), _watershed_area(watershedArea),
		_max_infiltration_rate(), _upper_reservoir_tau(), _lower_reservoir_tau(),
		_frac_lost_water(), _max_infiltration_rate_SI(), _upper_reservoir_tau_SI(),
		_lower_reservoir_tau_SI(), _cache_upper_res_taus(), _cache_lower_res_taus()
{
	const std::string& section = SFConstants::CONFIG_SECTION_NAME;
	_max_infiltration_rate.set(cfg, "MAX_INFILTRATION_RATE", section,
			Units::mm/Units::day, {0., 100.}, "Maximum infiltration rate from "
			"the surface soil layer into the lower groundwater compartment");
	_upper_reservoir_tau.set(cfg, "UPPER_RESERVOIR_TAU", section, Units::day,
			{0., 7.}, "Mean residence time in the upper groundwater compartment");
	_lower_reservoir_tau.set(cfg, "LOWER_RESERVOIR_TAU", section, Units::day,
			{0., 50.}, "Mean residence time in the lower groundwater compartment");
  _frac_lost_water.set(cfg, "FRAC_LOST_WATER", section, Units::NONE,
			{0., 0.5}, "Fraction of the runoff lost in soil");

	_max_infiltration_rate_SI = _max_infiltration_rate.getValue(Units::m/Units::s);
	_upper_reservoir_tau_SI   = _upper_reservoir_tau.getValue(Units::s);
	_lower_reservoir_tau_SI   = _lower_reservoir_tau.getValue(Units::s);
}


ParameterSet TwoVerticalLinearReservoirs::Parameters::getParams() const {
	ParameterSet params;
	params.add(_max_infiltration_rate);
	params.add(_upper_reservoir_tau);
	params.add(_lower_reservoir_tau);
	params.add(_frac_lost_water);
	return params;
}


void TwoVerticalLinearReservoirs::Parameters::setParams(const ParameterSet& params) {
	std::string paramName(_max_infiltration_rate.getName());
	if(_max_infiltration_rate.toBeCalibrated() && params.contains(paramName)) {
		_max_infiltration_rate.setValue(params[paramName].getValue(_max_infiltration_rate.getUnits()));
		_max_infiltration_rate_SI = _max_infiltration_rate.getValue(Units::m/Units::s);
	}

	paramName = _upper_reservoir_tau.getName();
	if(_upper_reservoir_tau.toBeCalibrated() && params.contains(paramName)) {
		_upper_reservoir_tau.setValue(params[paramName].getValue(_upper_reservoir_tau.getUnits()));
		_upper_reservoir_tau_SI = _upper_reservoir_tau.getValue(Units::s);
	}

	paramName = _lower_reservoir_tau.getName();
	if(_lower_reservoir_tau.toBeCalibrated() && params.contains(paramName)) {
		_lower_reservoir_tau.setValue(params[paramName].getValue(_lower_reservoir_tau.getUnits()));
		_lower_reservoir_tau_SI = _lower_reservoir_tau.getValue(Units::s);
	}

	paramName = _frac_lost_water.getName();
	if(_frac_lost_water.toBeCalibrated() && params.contains(paramName)) {
		_frac_lost_water.setValue(params[paramName].getValue(_frac_lost_water.getUnits()));
	}

	_cache_upper_res_taus.clear();
	_cache_lower_res_taus.clear();
}


double TwoVerticalLinearReservoirs::Parameters::getUpperReservoirTau(const TwoVerticalLinearReservoirs& subw) {
	const size_t subwatershedId(subw.getIndex());
	if(_cache_upper_res_taus.count(subwatershedId) == 0) {
		const double factor = pow(subw._area*1e-6, 1.0/3.0);
		_cache_upper_res_taus[subwatershedId] = _upper_reservoir_tau_SI*factor;
	}
	return _cache_upper_res_taus[subwatershedId];
}


double TwoVerticalLinearReservoirs::Parameters::getLowerReservoirTau(const TwoVerticalLinearReservoirs& subw) {
	const size_t subwatershedId(subw.getIndex());
	if(_cache_lower_res_taus.count(subwatershedId) == 0) {
		const double factor = pow(subw._area*1e-6, 1.0/3.0);
		_cache_lower_res_taus[subwatershedId] = _lower_reservoir_tau_SI*factor;
	}
	return _cache_lower_res_taus[subwatershedId];
}


double TwoVerticalLinearReservoirs::Parameters::getMaxInfiltrationRate() const {
	return _max_infiltration_rate_SI;
}

double TwoVerticalLinearReservoirs::Parameters::getFracLostWater() const {
	return _frac_lost_water.getValue();
}


TwoVerticalLinearReservoirs::Parameters::~Parameters() {}

} /* namespace sflow */
