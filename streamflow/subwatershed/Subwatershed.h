/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SUBWATERSHED_H_
#define SUBWATERSHED_H_

#include <vector>
#include <meteoio/MeteoIO.h>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/utils/HydroVariable.h>

namespace sflow {

//Forward declarations to avoid unnecessary includes
class StreamCell;
class CellInflow;
class StreamReach;


/**
 * @class Subwatershed
 * @brief This class represents an abstract subwatershed used by the
 * hydrological model to propagate water from the hillslopes into the stream
 * network. Each subwatershed is associated with a given stream reach, as
 * returned by the TauDEM tools.
 * @ingroup subwatershed
 * @author Aurelien Gallice
 * @date 2015-07-02
 */
class Subwatershed: public ParametrizableObject, public TimeEvolvingObject,
		public DiscretizedObject {
	public:
		Subwatershed(const Clock& clk) : ParametrizableObject(),
				TimeEvolvingObject(clk), DiscretizedObject() {}
		virtual bool isHeadSubwatershed() const = 0;
		virtual bool isSet() const = 0;
		virtual bool contains(const StreamReach& reach,
				const bool& discardLastCell) const = 0;
		virtual void setStreamReach(const StreamReach& reach) = 0;
		virtual const StreamReach& getStreamReach() const = 0;
		virtual const mio::Grid2DObject& getMask() const = 0;
		virtual size_t getIndex() const = 0;
		virtual double getArea() const = 0;
		virtual double get(const SubwatershedVariable& var) const = 0;
		virtual void getStreamLateralInflow(CellInflow& inflow) const = 0;
		virtual void getStreamLateralInflow(std::vector<CellInflow>& inflow) const = 0;
		virtual void updateBoundaryConditions() = 0;
		virtual void initialize() = 0;
		virtual ~Subwatershed() {}
};


/**
 * @class LumpedSubwatershedInterface
 * @brief This class represents an abstract lumped subwatershed, i.e. a
 * subwatershed whose outflow discharge is computed using a bucket model (or
 * several bucket models).
 * @ingroup subwatershed
 * @author Aurelien Gallice
 * @date 2015-07-02
 */
class LumpedSubwatershedInterface : public Subwatershed {
	public:
		LumpedSubwatershedInterface(const Clock& clk) : Subwatershed(clk) {}
		virtual Discretization getDiscretization() const;
		virtual ~LumpedSubwatershedInterface() {}
};


/**
 * @class DiscretizedSubwatershedInterface
 * @brief This class represents an abstract discretized subwatershed, i.e. a
 * subwatershed whose outflow discharge is computed using a cell-to-cell flow
 * advection scheme.
 * @ingroup subwatershed
 * @author Aurelien Gallice
 * @date 2015-07-02
 */
class DiscretizedSubwatershedInterface : public Subwatershed {
	public:
		DiscretizedSubwatershedInterface(const Clock& clk) : Subwatershed(clk) {}
		virtual Discretization getDiscretization() const;
		virtual ~DiscretizedSubwatershedInterface() {}
};

} /* namespace sflow */

#endif /* SUBWATERSHED_H_ */
