/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/subwatershed/LumpedSubwatershedDecorator.h>

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class LumpedSubwatershedDecorator
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class LumpedSubwatershedDecorator
 * @param wrappedObject Pointer to the object to be decorated
 */
LumpedSubwatershedDecorator::LumpedSubwatershedDecorator(LumpedSubwatershedInterface *wrappedObject) :
		LumpedSubwatershedInterface(wrappedObject->getClock()), _wrapped_object(wrappedObject) {}

ParameterSet LumpedSubwatershedDecorator::getParams() const {
	return _wrapped_object->getParams();
}

void LumpedSubwatershedDecorator::setParams(const ParameterSet& set) {
	_wrapped_object->setParams(set);
}

void LumpedSubwatershedDecorator::reset() {
	_wrapped_object->reset();
}

void LumpedSubwatershedDecorator::advanceInTime() {
	_wrapped_object->advanceInTime();
}

void LumpedSubwatershedDecorator::setDisplayInfo(const bool& display) {
	_wrapped_object->setDisplayInfo(display);
}

bool LumpedSubwatershedDecorator::isHeadSubwatershed() const {
	return _wrapped_object->isHeadSubwatershed();
}

bool LumpedSubwatershedDecorator::isSet() const {
	return _wrapped_object->isSet();
}

bool LumpedSubwatershedDecorator::contains(const StreamReach& reach,
		const bool& discardLastCell) const {
	return _wrapped_object->contains(reach, discardLastCell);
}

void LumpedSubwatershedDecorator::setStreamReach(const StreamReach& reach) {
	_wrapped_object->setStreamReach(reach);
}

void LumpedSubwatershedDecorator::initialize() {
	_wrapped_object->initialize();
}


const StreamReach& LumpedSubwatershedDecorator::getStreamReach() const {
	return _wrapped_object->getStreamReach();
}

const mio::Grid2DObject& LumpedSubwatershedDecorator::getMask() const {
	return _wrapped_object->getMask();
}

size_t LumpedSubwatershedDecorator::getIndex() const {
	return _wrapped_object->getIndex();
}

double LumpedSubwatershedDecorator::getArea() const {
	return _wrapped_object->getArea();
}

double LumpedSubwatershedDecorator::get(const SubwatershedVariable& var) const {
	return _wrapped_object->get(var);
}

void LumpedSubwatershedDecorator::getStreamLateralInflow(CellInflow& inflow) const {
	_wrapped_object->getStreamLateralInflow(inflow);
}

void LumpedSubwatershedDecorator::getStreamLateralInflow(std::vector<CellInflow>& inflow) const {
	_wrapped_object->getStreamLateralInflow(inflow);
}

void LumpedSubwatershedDecorator::updateBoundaryConditions() {
	_wrapped_object->updateBoundaryConditions();
}

LumpedSubwatershedDecorator::~LumpedSubwatershedDecorator() {
	delete _wrapped_object;
}


//-----------------------------------------------------------------------------
// Implementation of class SimpleSubwatershedWithEB
//-----------------------------------------------------------------------------

const double SimpleSubwatershedWithEB::_MIN_RESERVOIR_LEVEL = 1e-10;


/**
 * @brief Constructor of class SimpleSubwatershedWithEB
 * @param wrappedObject Pointer to the SimpleSubwatershed object to be
 * decorated (warning: this object will be destroyed during the decorator
 * destruction)
 * @param diffTime Reference to the TemperatureDiffusionTime object which holds
 * the characteristic time of temperature diffusion between the subwatershed
 * reservoirs and the surrounding soil
 */
SimpleSubwatershedWithEB::SimpleSubwatershedWithEB(LumpedSubwatershedInterface *wrappedObject,
		Parameters& diffTime, std::shared_ptr<DataContainer> dataCont) :
		LumpedSubwatershedDecorator(wrappedObject), _diff_time(diffTime), dataContainer(dataCont),
		_upper_res_temp(278.15), _lower_res_temp(278.15), _outlet_temp(278.15),
		_old_inflow_temp(278.15), _curr_inflow_temp(278.15), _soil_temp()
{
}

void SimpleSubwatershedWithEB::initialize(){
	LumpedSubwatershedDecorator::initialize();
	_soil_temp = getSoilTemperature();
}

void SimpleSubwatershedWithEB::updateBoundaryConditions() {
	LumpedSubwatershedDecorator::updateBoundaryConditions();

	//We can safely use static_cast since type has already been checked in the constructor
	TwoVerticalLinearReservoirs* subwatershed = static_cast<TwoVerticalLinearReservoirs*>(_wrapped_object);
	_old_inflow_temp  = _curr_inflow_temp;

	_curr_inflow_temp = subwatershed->dataContainer->getLumpedMeteoData(_clock.getNextTime(),
		"TSOIL", *this);

}


void SimpleSubwatershedWithEB::advanceInTime() {
	//We can safely use static_cast since type has already been checked in the constructor
	TwoVerticalLinearReservoirs* subwatershed = static_cast<TwoVerticalLinearReservoirs*>(_wrapped_object);
	const double upperResOldLevel(subwatershed->_upper_res.level);
	const double lowerResOldLevel(subwatershed->_lower_res.level);
	const double kinv(1.0/_diff_time.getTempDiffusionTime());
	const double dtHalf(double(_clock.getTimeStep())/2.0);

	LumpedSubwatershedDecorator::advanceInTime();

	//Compute temperature of the water stored in the upper compartment
	const double& upperResCurrLevel(subwatershed->_upper_res.level);
	if(upperResCurrLevel > _MIN_RESERVOIR_LEVEL &&
			upperResOldLevel > _MIN_RESERVOIR_LEVEL) {
		const double oldFactor(dtHalf*(kinv +
				subwatershed->_upper_res.old_inflow/upperResOldLevel));
		const double currFactor(dtHalf*(kinv +
				subwatershed->_upper_res.curr_inflow/upperResCurrLevel));
		_upper_res_temp = (1.0 - oldFactor)*_upper_res_temp +
				oldFactor*_old_inflow_temp + currFactor*_curr_inflow_temp;
		_upper_res_temp = fmax(_upper_res_temp/(1.0 + currFactor), 273.15);
	}

	//Compute temperature of the water stored in the lower compartment
	const double& lowerResCurrLevel(subwatershed->_lower_res.level);
	if(lowerResCurrLevel > _MIN_RESERVOIR_LEVEL &&
			lowerResOldLevel > _MIN_RESERVOIR_LEVEL) {
		const double oldAlpha(dtHalf*subwatershed->_lower_res.old_inflow/lowerResOldLevel);
		const double currAlpha(dtHalf*subwatershed->_lower_res.curr_inflow/lowerResCurrLevel);
		const double beta(dtHalf*kinv);
		_lower_res_temp = (1.0 - oldAlpha - beta)*_lower_res_temp
				+ oldAlpha*_old_inflow_temp + currAlpha*_curr_inflow_temp
				+ 2*beta*_soil_temp;
		_lower_res_temp = fmax(_lower_res_temp/(1.0 + currAlpha + beta), 273.15);
	}

	//Compute outlet temperature
	const double upperResH(subwatershed->_upper_res.outflow*_upper_res_temp);
	const double lowerResH(subwatershed->_lower_res.outflow*_lower_res_temp);
	const double totOutflow(subwatershed->_upper_res.outflow +
			subwatershed->_lower_res.outflow);
	_outlet_temp = totOutflow > 0.0 ? (upperResH + lowerResH)/totOutflow : mio::IOUtils::nodata;
}


void SimpleSubwatershedWithEB::reset() {
	LumpedSubwatershedDecorator::reset();

	_upper_res_temp  = _lower_res_temp = _outlet_temp = 278.15;
	_old_inflow_temp = _curr_inflow_temp = 278.15;
}


double SimpleSubwatershedWithEB::get(const SubwatershedVariable& var) const {
	if(var == SubwatershedVariable::T_outlet || var == SubwatershedVariable::T_source)
		return _outlet_temp;
	else
		return LumpedSubwatershedDecorator::get(var);
}


void SimpleSubwatershedWithEB::getStreamLateralInflow(CellInflow& inflow) const {

	LumpedSubwatershedDecorator::getStreamLateralInflow(inflow);

	inflow.temperature = this->get(SubwatershedVariable::T_outlet);
}


void SimpleSubwatershedWithEB::getStreamLateralInflow(std::vector<CellInflow>& inflow) const {
	LumpedSubwatershedDecorator::getStreamLateralInflow(inflow);

	const double outletTemp(this->get(SubwatershedVariable::T_outlet));
	for(CellInflow& cell : inflow)
		cell.temperature = outletTemp;
}


/**
 * @brief Destructor of class SimpleSubwatershedWithEB
 */
SimpleSubwatershedWithEB::~SimpleSubwatershedWithEB() {}


/**
 * @brief Returns the mean soil temperature value over the simulation time
 * period
 * @return Mean soil temperature over the simulation time period
 */
double SimpleSubwatershedWithEB::getSoilTemperature() const {
	TwoVerticalLinearReservoirs *subwatershed = dynamic_cast<TwoVerticalLinearReservoirs*>(_wrapped_object);
	if(!subwatershed)
		throw RuntimeException("Decorator SimpleSubwatershedWithEBalance can "
				"only be applied to objects of type SimpleSubwatershed", AT);

	return subwatershed->dataContainer->getLumpedTimeAveragedMeteoData(_clock.getStartTime(),
			_clock.getEndTime(), "TSOIL", *this);

}


//-----------------------------------------------------------------------------
// Implementation of class SimpleSubwatershedWithEB::Parameters
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class SimpleSubwatershedWithEB::TemperatureDiffusionTime
 * @param cfg Reference to the Config object holding the simulation parameters
 */
SimpleSubwatershedWithEB::Parameters::Parameters(const mio::Config& cfg) :
	ParametrizableObject(), _diff_time(cfg, "TEMP_DIFFUSION_TIME",
			SFConstants::CONFIG_SECTION_NAME, Units::day, {1./86400., 1.},
			"Characteristic time of temperature diffusion between the reservoirs "
			"of the subwatershed model and the surrounding soil"),
			_diff_time_SI(_diff_time.getValue(Units::s)) {}


/**
 * @brief Returns the value of the characteristic time of temperature diffusion
 * between the reservoirs of the subwatershed and the surrounding soil
 * @return Characteristic time of temperature diffusion in seconds
 */
const double& SimpleSubwatershedWithEB::Parameters::getTempDiffusionTime() const {
	return _diff_time_SI;
}


/**
 * @brief Method returning the characteristic time of temperature diffusion as
 * a ParameterSet object
 * @return Model parameters
 */
ParameterSet SimpleSubwatershedWithEB::Parameters::getParams() const {
	ParameterSet params;
	params.add(_diff_time);
	return params;
}


/**
 * @brief Method setting the characteristic time of temperature diffusion from
 * a ParameterSet object. Unknown parameters will be ignored.
 * @param params Set containing the new value of the temperature diffusion time
 */
void SimpleSubwatershedWithEB::Parameters::setParams(const ParameterSet& params) {
	const std::string& paramName(_diff_time.getName());
	if(_diff_time.toBeCalibrated() && params.contains(paramName)) {
		_diff_time.setValue(params[paramName].getValue(_diff_time.getUnits()));
		_diff_time_SI = _diff_time.getValue(Units::s);
	}
}


/**
 * @brief Destructor of class SimpleSubwatershedWithEB::TemperatureDiffusionTime
 */
SimpleSubwatershedWithEB::Parameters::~Parameters() {}


//-----------------------------------------------------------------------------
// Implementation of class SubwatershedWithSoilTempOutflow
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class SubwatershedWithSoilTempOutflow
 * @param wrappedObject Pointer to the SimpleSubwatershed object to be
 * decorated (warning: this object will be destroyed during the decorator
 * destruction)
 * @param reader Reference to the object used to read the values of the meteo
 * and soil temperature data over the stream reach cells
 */
SubwatershedWithSoilTempOutflow::SubwatershedWithSoilTempOutflow(LumpedSubwatershedInterface *wrappedObject,
	std::shared_ptr<DataContainer> dataCont) :
				LumpedSubwatershedDecorator(wrappedObject),
				_curr_time(_clock.getCurrTime()), dataContainer(dataCont) {}


void SubwatershedWithSoilTempOutflow::advanceInTime() {
	LumpedSubwatershedDecorator::advanceInTime();
	_curr_time = _clock.getNextTime();
}

/**
 * @brief Returns the value of the subwatershed variable of interest
 * @param var Subwatershed variable of interest
 * @return Value of the subwatershed variable of interest
 */
double SubwatershedWithSoilTempOutflow::get(const SubwatershedVariable& var) const {
	if(var != SubwatershedVariable::T_outlet && var != SubwatershedVariable::T_source)
		return LumpedSubwatershedDecorator::get(var);

	return fmax(273.15, dataContainer->getLumpedMeteoData(_curr_time,
			"TSOIL", *this));
}

/**
 * @brief Returns the values of the variables associated with the lateral
 * infiltration of water in the stream reach located in the subwatershed. This
 * method is intended to be used with lumped stream reaches (since only one
 * value per variable is returned).
 * @param[out] inflow Variables associated with the infiltration of water in the
 * stream reach
 */
void SubwatershedWithSoilTempOutflow::getStreamLateralInflow(CellInflow& inflow) const {
	LumpedSubwatershedDecorator::getStreamLateralInflow(inflow);
	inflow.temperature = fmax(273.15, dataContainer->getLumpedMeteoData(_curr_time,
			"TSOIL", *this));
}


/**
 * @brief Returns the values of the variables associated with the lateral
 * infiltration of water in the stream reach located in the subwatershed. This
 * method is intended to be used with discretized stream reaches (since one
 * value per stream cell belonging to the reach is returned for each variable).
 * @param[out] inflow Variables associated with the infiltration of water in the
 * stream reach
 */
void SubwatershedWithSoilTempOutflow::getStreamLateralInflow(std::vector<CellInflow>& inflow) const {
	LumpedSubwatershedDecorator::getStreamLateralInflow(inflow);
	//const std::vector<double> soilTemp(_input_reader.getMeteoData(_curr_time,
	//		"TSOIL", this->getStreamReach()));
	const std::vector<double> soilTemp(dataContainer->getMeteoData(_curr_time,
			"TSOIL", this->getStreamReach()));

	if(inflow.size() != soilTemp.size()) {
		std::ostringstream os;
		os << "The numer of elements in the vector used to collect the values of "
		   << "the variables associated with the lateral infiltration of water "
		   << "in stream reach " << this->getIndex() << " at date "
		   << _clock.getCurrTime().toString(mio::Date::ISO) << " is not "
		   << "the same as the number of cells belonging to the stream reach"
		   << std::endl;
		throw mio::InvalidArgumentException(os.str(), AT);
	}

	for(size_t iCell(0); iCell < inflow.size(); ++iCell)
		inflow[iCell].temperature = fmax(soilTemp[iCell], 273.15);
}


/**
 * @brief Destructor of class SubwatershedWithSoilTempOutflow
 */
SubwatershedWithSoilTempOutflow::~SubwatershedWithSoilTempOutflow() {}



//-----------------------------------------------------------------------------
// Implementation of class SubwatershedWithHSPFOutflowTemp
//-----------------------------------------------------------------------------

SubwatershedWithHSPFOutflowTemp::SubwatershedWithHSPFOutflowTemp(LumpedSubwatershedInterface *wrappedObject,
		const Parameters& params, std::shared_ptr<DataContainer> dataCont) :
			LumpedSubwatershedDecorator(wrappedObject), _params(params),
			 _inflow_temperature(278.15),
			_air_temperature_old(), _air_temperature_curr(), dataContainer(dataCont)
{


}

void SubwatershedWithHSPFOutflowTemp::initialize()
{
	LumpedSubwatershedDecorator::initialize();
	//_air_temperature_curr = _input_reader.getLumpedMeteoData(_clock.getCurrTime(),
	//		"TA", *this);
	_air_temperature_curr = dataContainer->getLumpedMeteoData(_clock.getCurrTime(),
			"TA", *this);
	this->updateAirTemperature();
}


void SubwatershedWithHSPFOutflowTemp::updateBoundaryConditions() {
	LumpedSubwatershedDecorator::updateBoundaryConditions();

	this->updateAirTemperature();
}


void SubwatershedWithHSPFOutflowTemp::advanceInTime() {
	LumpedSubwatershedDecorator::advanceInTime();

	const double factor(_params.getSmoothingFactor()*_clock.getTimeStep());
	const double meanTAir(0.5*(_air_temperature_old + _air_temperature_curr));
	_inflow_temperature = (1. - 0.5*factor)*_inflow_temperature + factor*meanTAir +
			factor*_params.getTemperatureDifference();
    _inflow_temperature = fmax(_inflow_temperature/(1. + 0.5*factor), 273.15);
}


void SubwatershedWithHSPFOutflowTemp::reset() {
	LumpedSubwatershedDecorator::reset();

	_inflow_temperature = 278.15;
	//_air_temperature_curr = _input_reader.getLumpedMeteoData(_clock.getCurrTime(),
	//		"TA", *this);
	_air_temperature_curr = dataContainer->getLumpedMeteoData(_clock.getCurrTime(),
			"TA", *this);
	this->updateAirTemperature(); //< HACK: requires clock to have been reset!
}


double SubwatershedWithHSPFOutflowTemp::get(const SubwatershedVariable& var) const {
	if(var == SubwatershedVariable::T_outlet || var == SubwatershedVariable::T_source)
		return _inflow_temperature;
	else
		return LumpedSubwatershedDecorator::get(var);
}


void SubwatershedWithHSPFOutflowTemp::getStreamLateralInflow(CellInflow& inflow) const {
	LumpedSubwatershedDecorator::getStreamLateralInflow(inflow);

	inflow.temperature = _inflow_temperature;
}


void SubwatershedWithHSPFOutflowTemp::getStreamLateralInflow(std::vector<CellInflow>& inflow) const {
	LumpedSubwatershedDecorator::getStreamLateralInflow(inflow);

	for(CellInflow& cell : inflow)
		cell.temperature = _inflow_temperature;
}


SubwatershedWithHSPFOutflowTemp::~SubwatershedWithHSPFOutflowTemp() {}


void SubwatershedWithHSPFOutflowTemp::updateAirTemperature() {
	_air_temperature_old  = _air_temperature_curr;
	//_air_temperature_curr = _input_reader.getLumpedMeteoData(_clock.getNextTime(),
	//		"TA", *this);
	_air_temperature_curr = dataContainer->getLumpedMeteoData(_clock.getNextTime(),
			"TA", *this);
}


//-----------------------------------------------------------------------------
// Implementation of class SubwatershedWithHSPFOutflowTemp::Parameters
//-----------------------------------------------------------------------------

SubwatershedWithHSPFOutflowTemp::Parameters::Parameters(const mio::Config& cfg) :
		SharedObject(), ParametrizableObject(),
		_temperature_diff(cfg, "HSPF_TEMPERATURE_DIFF", SFConstants::CONFIG_SECTION_NAME,
				Units::degK, {-5., 5.}, "Difference between mean air temperature "
				"and mean temperature of water infiltrating into the stream"),
		_smoothing_factor(cfg, "HSPF_SMOOTHING_FACTOR", SFConstants::CONFIG_SECTION_NAME,
				Units::s.pow(-1), {0., 1.}, "Smoothing factor used to reduce the temporal "
				"variability of the temperature of water infiltrating into the stream "
				"(the closer the factor to 0, the more pronounced the smoothing)") {}


ParameterSet SubwatershedWithHSPFOutflowTemp::Parameters::getParams() const {
	ParameterSet output;
	output.add(_temperature_diff);
	output.add(_smoothing_factor);
	return output;
}


void SubwatershedWithHSPFOutflowTemp::Parameters::setParams(const ParameterSet& params) {
	if(_temperature_diff.toBeCalibrated() && params.contains(_temperature_diff.getName())) {
		_temperature_diff.setValue(params[_temperature_diff.getName()].getValue(_temperature_diff.getUnits()));
	}

	if(_smoothing_factor.toBeCalibrated() && params.contains(_smoothing_factor.getName())) {
		_smoothing_factor.setValue(params[_smoothing_factor.getName()].getValue(_smoothing_factor.getUnits()));
	}
}


const double& SubwatershedWithHSPFOutflowTemp::Parameters::getTemperatureDifference() const {
	return _temperature_diff.getValue();
}


const double& SubwatershedWithHSPFOutflowTemp::Parameters::getSmoothingFactor() const {
	return _smoothing_factor.getValue();
}


SubwatershedWithHSPFOutflowTemp::Parameters::~Parameters() {}

} /* namespace sflow */
