/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LUMPEDSUBWATERSHED_H_
#define LUMPEDSUBWATERSHED_H_

#include <cmath>
#include <vector>
#include <sstream>
#include <numeric>
#include <algorithm>
#include <unordered_map>
#include <meteoio/MeteoIO.h>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/utils/StreamflowExceptions.h>
#include <streamflow/io/DataContainer.h>

#include <streamflow/subwatershed/Subwatershed.h>
#include <streamflow/streamReach/StreamReach.h>
#include <streamflow/streamReach/StreamCell.h>
#include <streamflow/calibration/Parameter.h>
#include <streamflow/calibration/ParameterSet.h>

namespace sflow {

/**
 * @class LumpedSubwatershed
 * @brief This class implements part of the virtual class LumpedSubwatershedInterface.
 * The implementation of the time stepping scheme (i.e. of the way water flow
 * is transfered from the hillslopes to the stream network) is delegated to the
 * subclasses.
 * @ingroup subwatershed
 * @author Aurelien Gallice
 * @date 2015-07-02
 */
class LumpedSubwatershed : public LumpedSubwatershedInterface {
	public:
		LumpedSubwatershed(const mio::Grid2DObject& grid, const size_t& index,
				const Clock& clk);
		LumpedSubwatershed(const mio::Coords& llcorner, const double& cellSize,
				const std::vector<size_t>& ix, const std::vector<size_t>& iy,
				const size_t& index, const Clock& clk);
		LumpedSubwatershed(const LumpedSubwatershed& copy, const Clock& _clk);
		LumpedSubwatershed(const LumpedSubwatershed&) = delete;
		LumpedSubwatershed& operator=(const LumpedSubwatershed&) = delete;
		virtual bool isHeadSubwatershed() const;
		virtual bool isSet() const;
		virtual bool contains(const StreamReach& reach,
				const bool& discardLastCell) const;
		virtual void setStreamReach(const StreamReach& reach);
		virtual const StreamReach& getStreamReach() const;
		virtual const mio::Grid2DObject& getMask() const;
		virtual size_t getIndex() const;
		virtual double getArea() const;
		virtual double get(const SubwatershedVariable& var) const;
		virtual void getStreamLateralInflow(CellInflow& inflow) const;
		virtual void getStreamLateralInflow(std::vector<CellInflow>& inflow) const;
		virtual void initialize()=0;
		virtual ~LumpedSubwatershed();

	protected:
		const mio::Grid2DObject _mask;
		const size_t _index;
		const double _area;  //< [m^2] to avoid computing the watershed area at each time step
		const StreamReach *_reach;
		double _source_drainage_fraction; //< fraction of the subwatershed area which is drained by the reach source
		std::vector<double> _cell_drainage_fractions; //< fraction of the subwatershed area which is drained by each reach cell

		virtual std::vector<double> computeCellDrainageFractions(const bool& skipFirstCell = false) const;

		static mio::Grid2DObject constructMask(const mio::Grid2DObject& grid,
				const size_t& index, const double& eps = 1e-10);
		static mio::Grid2DObject constructMask(const mio::Coords& llcorner,
				const double& cellSize, const std::vector<size_t>& ix,
				const std::vector<size_t>& iy);
		static double computeArea(const mio::Grid2DObject& mask);
};


/**
 * @class TwoVerticalLinearReservoirs
 * @brief This class implements a simple subwatershed with two linear reservoirs
 * stacked on toip of each other, as described in Comola et al. (2015). The
 * parameters used to compute the maximum infiltration rate in the lower
 * reservoir and the reservoir residence times are stored in the internal class
 * Parameters. This approach was chosen here so that all model parameters are
 * grouped in a single instance (i.e. the object of class Parameters).
 *
 * References:
 * Comola, F., Schaefli, B., Rinaldo, A., and Lehning, M., Thermodynamics in the
 * hydrologic response: Travel time formulation and application to Alpine
 * catchments, WRR, 51, 2015.
 * @ingroup subwatershed
 * @author Aurelien Gallice
 * @date 2015-07-02
 */
class TwoVerticalLinearReservoirs : public LumpedSubwatershed {
	public:
		class Parameters;
		friend class SimpleSubwatershedWithEB;

		TwoVerticalLinearReservoirs(const mio::Grid2DObject& grid,
				const size_t& index, const Clock& clk,
				Parameters& params, std::shared_ptr<DataContainer> dataContainer);
		TwoVerticalLinearReservoirs(const mio::Coords& llcorner, const double& cellSize,
				const std::vector<size_t>& ix, const std::vector<size_t>& iy,
				const size_t& index, const Clock& clk,
				Parameters& params, std::shared_ptr<DataContainer> dataCont);
		TwoVerticalLinearReservoirs(const TwoVerticalLinearReservoirs& _TwoVerticalLinearReservoirs,
				const Clock& _clk, Parameters& params);
		virtual void updateBoundaryConditions() override;
		virtual void advanceInTime() override;
		virtual void setDisplayInfo(const bool&) override;
		virtual ParameterSet getParams() const override;
		virtual void setParams(const ParameterSet&) override;
		virtual void reset() override;
		virtual void initialize() override;
		virtual double get(const SubwatershedVariable& var) const override;
		virtual ~TwoVerticalLinearReservoirs() override;

	protected:
		struct Reservoir {
			double old_inflow;  //< [m/s] reservoir inflow at the previous time step
			double curr_inflow; //< [m/s] water flowing from the bottom of the Alpine3D upper soil layer into the reservoir
			double level;       //< [m]   water storage in the reservoir
			double outflow;     //< [m/s] reservoir discharge
			Reservoir(const double& Qin, const double& h, const double& Qout) :
				old_inflow(Qin), curr_inflow(Qin), level(h), outflow(Qout) {}
		};

		Parameters& _params;
		Reservoir _upper_res;
		Reservoir _lower_res;
		std::shared_ptr<DataContainer> dataContainer;
		virtual void updateReservoirOutflowDischarges();
};


/**
 * @class TwoVerticalLinearReservoirs::Parameters
 * @brief An instance of this class is intended to be passed as argument to the
 * constructor of class TwoVerticalLinearReservoirs. It contains the parameters
 * used to compute the residence times in the two reservoirs, as well as the
 * maximum infiltration rate from the upper soil layer into the lower reservoir.
 * @ingroup subwatershed
 * @author Aurelien Gallice
 * @date 2015-07-13
 */
class TwoVerticalLinearReservoirs::Parameters : public SharedObject,
		public ParametrizableObject {
	public:
		Parameters(const mio::Config& cfg, const double& watershedArea);
		virtual ParameterSet getParams() const;
		virtual void setParams(const ParameterSet&);
		virtual double getUpperReservoirTau(const TwoVerticalLinearReservoirs& subw);
		virtual double getLowerReservoirTau(const TwoVerticalLinearReservoirs& subw);
		virtual double getMaxInfiltrationRate() const;
		virtual double getFracLostWater() const;
		virtual ~Parameters();

	protected:
		const double _watershed_area;     //< [km^2] area of the watershed containing the subwatershed
		Parameter _max_infiltration_rate; //< maximum infiltration rate from the surface layer into the lower compartment
		Parameter _upper_reservoir_tau;   //< Mean residence time in the upper reservoir
		Parameter _lower_reservoir_tau;   //< Mean residence time in the lower reservoir
    Parameter _frac_lost_water;   //< Mean residence time in the lower reservoir
		double _max_infiltration_rate_SI; //< [m/s] value of _max_infiltration_rate in SI units (avoids conversion at each time step)
		double _upper_reservoir_tau_SI;   //< [s] value of _upper_reservoir_tau in SI units (avoids conversion at each time step)
		double _lower_reservoir_tau_SI;   //< [s] value of _lower_reservoir_tau in SI units (avoids conversion at each time step)
		std::unordered_map<size_t, double> _cache_upper_res_taus; //< stores the residence time in the upper reservoir of each subwatershed
		std::unordered_map<size_t, double> _cache_lower_res_taus; //< stores the residence time in the lower reservoir of each subwatershed
};

} /* namespace sflow */

#endif /* LUMPEDSUBWATERSHED_H_ */
