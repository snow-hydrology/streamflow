/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as 
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public 
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/subwatershed/Subwatershed.h>

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class LumpedSubwatershedInterface
//-----------------------------------------------------------------------------

DiscretizedObject::Discretization LumpedSubwatershedInterface::getDiscretization() const {
	return Discretization::Lumped;
}


//-----------------------------------------------------------------------------
// Implementation of class DiscretizedSubwatershedInterface
//-----------------------------------------------------------------------------

DiscretizedObject::Discretization DiscretizedSubwatershedInterface::getDiscretization() const {
	return Discretization::Distributed;
}

} /* namespace sflow */
