/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FUNCTION1D_H_
#define FUNCTION1D_H_

#include <cmath>
#include <vector>
#include <stdexcept>
#include <algorithm>
#include <streamflow/utils/StreamflowExceptions.h>

namespace sflow {

/**
 * @class Function1D
 * @brief An abstract class to represent a real function \f$f: R \rightarrow R\f$.
 * @author Aurelien Gallice
 * @date 2015-04-02
 */
class Function1D {
	public:
		Function1D();
		virtual double operator()(const double & x) const = 0;
    Function1D(const Function1D&) = default;
		Function1D& operator=(const Function1D&) = default;
		virtual ~Function1D() = default;
};


/**
 * @class LinearFunction1D
 * @brief Class representing a linear function of type \f$y = ax + b\f$.
 * @author Aurelien Gallice
 * @date 2015-03-27
 */
class LinearFunction1D : public Function1D {
	public:
		LinearFunction1D(const double& factor, const double& offset);
		double getFactor() const;
		double getOffset() const;
		void setFactor(const double& factor);
		void setOffset(const double& offset);
		double operator()(const double& x) const override;
		LinearFunction1D operator()(const LinearFunction1D& fun) const;
		LinearFunction1D inv() const;
		bool operator==(const LinearFunction1D& fun) const;
		//virtual ~LinearFunction1D();

	protected:
		double _factor;
		double _offset;
};


/**
 * @class PiecewiseFunction1D
 * @brief Class representing a piecewise-defined function. Outside of its
 * definition range, the function evaluates to its closest definition point.
 * @author Aurelien Gallice
 * @date 2015-05-21
 */
class PiecewiseFunction1D : public Function1D {
	public:
		PiecewiseFunction1D(const std::vector<double>& x, const std::vector<double>& y);
		virtual double operator()(const double& x) const override;
		double getIntegral(const double& x1, const double& x2) const;
		double getMeanVal(const double& x1, const double& x2) const;
		PiecewiseFunction1D(const PiecewiseFunction1D&) = default;
		//PiecewiseFunction1D& operator=(const PiecewiseFunction1D&) = default;
		virtual ~PiecewiseFunction1D() override = default;

	protected:
		const std::vector<double> _x;
		const std::vector<double> _y;
		const size_t _n_points;

		size_t findNextPosIndex(const double& xev) const;
		/* xev corresponds to the evaluation position, and ix to the index of
		 * the largest element in the x vector satisfying x[ix] < xev. */
		virtual double interpolate(const double& xev, const size_t& ix) const = 0;
		double integratePiece(const size_t& ix) const;
		virtual double integratePiece(const double& x1, const double& x2) const = 0;
};


/**
 * @class PiecewiseLinear1D
 * @brief Class representing a piecewise linear function. Outside of its
 * definition range, the function evaluates to the closest definition point.
 * @author Aurelien Gallice
 * @date 2015-05-21
 */
class PiecewiseLinear1D : public PiecewiseFunction1D {
	public:
		PiecewiseLinear1D(const std::vector<double>& x, const std::vector<double>& y);
	protected:
		virtual double interpolate(const double& xev, const size_t& ix) const override;
		virtual double integratePiece(const double& x1, const double& x2) const override;
};


/**
 * @class PiecewiseConstant1D
 * @brief Class representing a piecewise constant function. Outside of its
 * definition range, the function evaluates to the closest definition point.
 * @author Aurelien Gallice
 * @date 2015-05-21
 */
class PiecewiseConstant1D : public PiecewiseFunction1D {
	public:
		PiecewiseConstant1D(const std::vector<double>& x, const std::vector<double>& y);
	protected:
		virtual double interpolate(const double& xev, const size_t& ix) const override;
		virtual double integratePiece(const double& x1, const double& x2) const override;
};


/**
 * @class PowerFunction1D
 * @brief Class representing a power function of the type \f$y = ax^b\f$, with
 * \f$a\f$ and \f$b\f$ constants.
 * @author Aurelien Gallice
 * @date 2015-06-08
 */
class PowerFunction1D : public Function1D {
	public:
		PowerFunction1D(const double& a, const double& b);
		virtual double operator()(const double& x) const override;
		const double& getFactor() const;
		const double& getPower() const;
		void setFactor(const double&);
		void setPower(const double&);

	protected:
		double _factor;
		double _power;
};

} /* namespace sflow */

#endif /* FUNCTION1D_H_ */
