/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STREAMFLOW_UTILS_GRIDOPERATIONS_H_
#define STREAMFLOW_UTILS_GRIDOPERATIONS_H_

#include <streamflow/utils/SFUtils.h>
#include <meteoio/MeteoIO.h>

namespace sflow {

/**
 * @class GridOperations
 * @brief This class defines several operations which can be performed on
 * mio::Grid2DObjects
 * @author Aurelien Gallice
 * @date 2016-06-14
 */
class GridOperations {
	public:
		static mio::Grid2DObject crop2(const mio::Grid2DObject& grid, const mio::Grid2DObject& mask);
		static bool gridify(mio::Coords& point,  const mio::Grid2DObject& grid);
		static double isCellInMask(mio::Coords coords, const mio::Grid2DObject& mask, double ratio, double cellsize);
		static double mean(const mio::Grid2DObject& grid,const mio::Grid2DObject& mask);
		static double sum(const mio::Grid2DObject& grid,const mio::Grid2DObject& mask);

		static double average(const mio::Grid2DObject& grid,
				const size_t& ixMin = 0, size_t ixMax = mio::IOUtils::npos,
				const size_t& iyMin = 0, size_t iyMax = mio::IOUtils::npos);

	protected:
		static const double _MIN_CELL_SIZE;               //< [m] minimum size of a grid cell
		static const double _DISTANCE_ABSOLUTE_PRECISION; //< [m] two points closer to each other than this value will be assumed to overlap

		static mio::Grid2DObject shrinkToData(const mio::Grid2DObject& grid);
};

} /* namespace sflow */

#endif /* STREAMFLOW_UTILS_GRIDOPERATIONS_H_ */
