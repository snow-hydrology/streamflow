/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SFUTILS_H_
#define SFUTILS_H_

#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <unordered_set>
#include <meteoio/MeteoIO.h>
#include <streamflow/utils/StreamflowExceptions.h>


// This file defines some useful small classes used in StreamFlow.


namespace sflow {

//Forward declarations to avoid unnecessary includes
class ParameterSet;
class StreamNetwork;
class Watershed;
class TimeStepLimiter;


std::string getLibVersion();


/**
 * @class ParametrizableObject
 * @brief An abstract class to be used as a (java-like) interface. The subclasses
 * implementing it should correspond to objects whose behavior depends on
 * parameters. The latter are intended to be optimized using some algorithm or
 * Monte-Carlo simulation.
 */
class ParametrizableObject {
	public:
		ParametrizableObject() {}
		virtual ParameterSet getParams() const = 0;
		virtual void setParams(const ParameterSet&) = 0;
		virtual ~ParametrizableObject() {}
};


/**
 * @class Clock
 * @brief A class used to hold the current simulation time. An instance of this
 * class is intended to be shared by all high-level objects which require to
 * know the current simulation time.
 */
class Clock {
	public:
		Clock(const mio::Config& cfg, const mio::Date& startTime, const mio::Date& endTime);
		const mio::Date& getStartTime() const;
		const mio::Date& getEndTime() const;
		const mio::Date& getCurrTime() const;
		const mio::Date getNextTime() const;
		const unsigned int& getTimeStep() const;
		void updateTimeStep();
		void advanceInTime();
		void reset();
		void reset(const mio::Date& newStartTime, const mio::Date& newEndTime);
		void addTimeStepLimiter(const TimeStepLimiter& limiter);
		void removeTimeStepLimiter(const TimeStepLimiter& limiter);
	protected:
		mio::Date _start_time;
		mio::Date _end_time;
		mio::Date _curr_time;
		unsigned int _time_step;         //< [s]
		unsigned int _initial_time_step; //< [s] used by method reset()
		unsigned int _min_time_step;
		unsigned int _max_time_step;
		std::unordered_set<const TimeStepLimiter*> _time_step_limiters;
};


/**
 * @class TimeStepLimiter
 * @brief This class defines an object which requires the simulation time step
 * to remain below a certain limit.
 */
class TimeStepLimiter {
	public:
		TimeStepLimiter(Clock& clock) {clock.addTimeStepLimiter(*this);}
		virtual int getRequiredMaximumTimeStep() const = 0;
		virtual ~TimeStepLimiter() {}
};


/**
 * @class TimeEvolvingObject
 * @brief An abstract class to be used as a (java-like) interface. The subclasses
 * implementing it should correspond to objects whose state can evolve in time.
 * It contains a method advanceInTime(), which needs to be implemented by the
 * subclasses and causes the object state to be incremented in time. The time
 * increment can be obtained from a reference to an external Clock object,
 * which can also be used to get the current time (t) and the next time (t+dt).
 */
class TimeEvolvingObject {
	public:
		explicit TimeEvolvingObject(const Clock& clk) : _clock(clk) {}
		virtual void advanceInTime() = 0;
		virtual void reset() = 0;
		virtual void setDisplayInfo(const bool&) = 0;
		virtual const Clock& getClock() const {return _clock;}
		virtual ~TimeEvolvingObject() {}

	protected:
		const Clock& _clock;
};


/**
 * @class ModelObserver
 * @brief This class implements an observer for ObservableModel objects (see
 * <a href="https://en.wikipedia.org/wiki/Observer_pattern">observer pattern</a>)
 */
class ModelObserver {
	public:
		ModelObserver() {}
		virtual void reset() = 0;
		virtual void update(const mio::Date& currTime, const Watershed&,
				const StreamNetwork&) = 0;
		virtual ~ModelObserver() {}
};


/**
 * @class ObservableModel
 * @brief This class implements an observable model (see
 * <a href="https://en.wikipedia.org/wiki/Observer_pattern">observer pattern</a>)
 */
class ObservableModel : public ParametrizableObject {
	public:
		ObservableModel();
		virtual void run() = 0;
		virtual void reset();
		virtual void setDisplayInfo(const bool& value) = 0;
		virtual void addObserver(ModelObserver& obs);
		virtual void removeObserver(ModelObserver& obs);
		virtual void updateObservers(const mio::Date& currTime,
				const Watershed& watershed, const StreamNetwork& network);
		virtual ~ObservableModel();

	private:
		std::unordered_set<ModelObserver*> _observers;
};


/**
 * @class DiscretizedObject
 * @brief An abstract class to be used as a (java-like) interface. The subclasses
 * implementing it should correspond to objects which can be assigned a
 * discretization level (either lumped or discretized).
 */
class DiscretizedObject {
	public:
		enum class Discretization {Distributed, Lumped};

		DiscretizedObject() {}
		virtual Discretization getDiscretization() const = 0;
		virtual ~DiscretizedObject() {}
};


/**
 * @class SharedObject
 * @brief The sole purpose of this (java-like) interface is to be inherited
 * by the classes which are intended to represent shared objects. A shared
 * object is an object which is pointed to (using a pointer or a reference) by
 * a set of external objects.
 */
class SharedObject {
	public:
		SharedObject() {}
		virtual ~SharedObject() {}
};


/**
 * @class SFConstants
 * @brief A class used to encapsulate all constants which are useful to the
 * StreamFlow program.
 */
struct SFConstants {
	static const std::string CONFIG_SECTION_NAME; //< name of the section holding the parameters of the hydrological model in the config file
	static const double PI;
	static const double PI_HALF;
	static const double PI_TWICE;
	static const double GRAVIT_ACCELERATION;   //< gravitational acceleration in [m/s^2]
	static const double WATER_MASS_DENSITY;    //< water mass density in [kg/m^3]
	static const double WATER_HEAT_CAPACITY;   //< specific heat capacity of water in [J/(kg*K)]
	static const double WATER_MOLAR_MASS;      //< molar mass of water in [kg/mol]
	static const double WATER_EMISSIVITY;      //< water emissivity [dimensionless]
	static const double WATER_VAP_LATENT_HEAT; //< latent heat of water vaporisation in [J/kg]
	static const double AIR_HEAT_CAPACITY;     //< specific heat capacity of air in [J/(kg*K)]
	static const double AIR_MOLAR_MASS;        //< dry air molar mass in [kg/mol]
	static const double STEFAN_BOLTZMANN;      //< Stefan Boltzmann constant in [W/(m^2*K^4)]
	static const double VON_KARMAN;            //< Von Karman constant [dimensionless]
	static const double PERFECT_GAS_CONSTANT;  //< perfect gas constant in [J/(mol*K)]
};


/**
 * @class SFLaws
 * @brief This class contains various methods to compute useful variables.
 */
class SFLaws {
	public:
		static double standardAirPressure(const double& altitude, const bool& isothermal = true);
		static double airMassDensity(const double& temperature,
				const double& pressure = 1.015e5);
		static double waterVaporPressure(const double& temperature);
		static double waterVaporPressureSlope(const double& temperature);
		static double psychrometricConstant(const double& pressure = 1.015e5);
		static double cosSolarZenithAngle(const mio::Coords& location, const mio::Date& date);
		static double sinSolarDeclination(const mio::Date& date);
		static double waterAlbedo(double incidentAngle);
};


/**
 * @brief The namespace SFUtils is a subspace of sflow and contains various
 * utility functions.
 */
namespace SFUtils {

double radToDeg(const double& angle);
double degToRad(const double& angle);
std::string trim(std::string str);
std::string timeToString(const double time);
bool isSpace(const char& letter);
bool isMultiple(const double& a, const double& b, const double& precision = 1e-10);
size_t readLineToVec(const std::string& line, std::vector<double>& data,
		const char& delim = ' ');
//Taken from http://stackoverflow.com/questions/1903954/is-there-a-standard-sign-function-signum-sgn-in-c-c
template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}
template <typename T> T fastPow(const T& num, const int& pow) {
	if(pow < 0)
		return T(1)/fastPow(num, -pow);
	else if(pow == 0)
		return T(1);
	else //pow > 0
		return num*fastPow(num, pow - 1);
}

} /* namespace SFUtils */

} /* namespace sflow */

#endif /* SFUTILS_H_ */
