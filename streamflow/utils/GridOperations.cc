/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/utils/GridOperations.h>

namespace sflow {

const double GridOperations::_MIN_CELL_SIZE(5.);                 //< [m]
const double GridOperations::_DISTANCE_ABSOLUTE_PRECISION(1e-3); //< [m]


mio::Grid2DObject GridOperations::crop2(const mio::Grid2DObject& dem, const mio::Grid2DObject& mask) {

	mio::Grid2DObject tmp(dem, mio::IOUtils::nodata);

	const double mask_cellsize = mask.cellsize;
	const mio::Coords mask_llcorner = mask.llcorner;

	const mio::Coords llcorner = dem.llcorner;
	const double cellsize = dem.cellsize;
	mio::Coords mving_coords;

	double ratio = cellsize/mask_cellsize;

	int mask_sum=0;
	int nx_start = static_cast <int> (floor( (mask_llcorner.getEasting() - llcorner.getEasting())/cellsize));
	int ny_start = static_cast <int> (floor( (mask_llcorner.getNorthing()- llcorner.getNorthing())/cellsize));
	int nx_end = static_cast <int> (ceil( ( mask_llcorner.getEasting()+static_cast <double>(mask.getNx())*mask_cellsize - llcorner.getEasting()) /cellsize));
	int ny_end = static_cast <int> (ceil( ( mask_llcorner.getNorthing()+static_cast <double>(mask.getNy())*mask_cellsize - llcorner.getNorthing())/cellsize));

	if (nx_start < 0 || ny_start < 0 || nx_end > static_cast <long> (dem.getNx()) || ny_end > static_cast <long> (dem.getNy()) ){
		throw mio::InvalidFormatException("Cannot crop grid along the boundaries of the given watershed: watershed extends beyond the DEM boundaries", AT);
	}

	for(size_t nx = 0; nx < mask.getNx(); nx++ )
	{
		for(size_t ny = 0; ny < mask.getNy(); ny++ )
		{
			double val = mask(nx,ny);
			if (val == 1){
				++mask_sum;
			}
		}
	}

	double sum=0;
	for(size_t nx = static_cast <size_t> (nx_start); nx < static_cast <size_t> (nx_end); nx++ )
	{
		for(size_t ny = static_cast <size_t> (ny_start); ny < static_cast <size_t> (ny_end); ny++ )
		{
			mving_coords = llcorner;
			mving_coords.moveByXY(static_cast <double>(nx)*cellsize,static_cast <double>(ny)*cellsize);
			tmp(nx,ny)=isCellInMask(mving_coords,mask,ratio,cellsize);
			sum += tmp(nx,ny);
			if(tmp(nx,ny)==0)
			{
				tmp(nx,ny)=mio::IOUtils::nodata;
			}


		}
	}
	sum *= ratio*ratio;

	if(fabs(mask_sum-sum) > 1e-2 )
	{
		std::cout << "[W] Problem with mask resizing sum: " << std::setprecision(8) << sum << " mask_sum:" << std::setprecision(8) << mask_sum << std::endl;
	}

	return shrinkToData(tmp);

}

mio::Grid2DObject GridOperations::shrinkToData(const mio::Grid2DObject& grid){
	size_t nx_start = 0;
	size_t ny_start =0;
	size_t nx_end = grid.getNx()-1;
	size_t ny_end = grid.getNy()-1;
	bool found=false;

	for(size_t nx = 0; nx < grid.getNx(); ++nx )
	{
		for(size_t ny = 0; ny < grid.getNy(); ++ny )
		{
		 if(grid(nx,ny)!=mio::IOUtils::nodata)
			{
				found=true;
				break;
			}
		}
		if(found){
			break;
		}
		nx_start++;
	}
	found=false;
	for(size_t ny = 0; ny < grid.getNy(); ++ny )
	{
		for(size_t nx = 0; nx < grid.getNx(); ++nx )
		{
		 if(grid(nx,ny)!=mio::IOUtils::nodata)
			{
				found=true;
				break;
			}
		}
		if(found){
			break;
		}
		ny_start++;
	}
	found=false;
	for(size_t nx = grid.getNx(); nx > 0; --nx )
	{
		for(size_t ny = 0; ny < grid.getNy(); ++ny )
		{
		 if(grid(nx-1,ny)!=mio::IOUtils::nodata)
			{
				found=true;
				break;
			}
		}
		if(found){
			break;
		}
		nx_end--;
	}
	found=false;
	for(size_t ny = grid.getNy(); ny > 0; --ny )
	{
		for(size_t nx = 0; nx < grid.getNx(); ++nx )
		{
		 if(grid(nx,ny-1)!=mio::IOUtils::nodata)
			{
				found=true;
				break;
			}
		}
		if(found){
			break;
		}
		ny_end--;
	}

	return mio::Grid2DObject(grid,nx_start,ny_start,nx_end-nx_start+1,ny_end-ny_start+1);
}



double GridOperations::isCellInMask(mio::Coords coords, const mio::Grid2DObject& mask,double ratio, double cellsize){

	double div = ceil(ratio);
	double cellpart= cellsize/div;
	mio::Coords mving_coords;
	double sum=0;

	for(double nx = 0; nx < div; nx++ )
	{
		for(double ny = 0; ny < div; ny++ )
		{
			mving_coords = coords;
			mving_coords.moveByXY(nx*cellpart,ny*cellpart);
			if(	mask.gridify(mving_coords) ){
				double val = mask(mving_coords.getGridI(),mving_coords.getGridJ());
				if (val== 1){
					sum += 1./(div*div);
				}

			}

		}
	}
	return sum;
}


double GridOperations::mean(const mio::Grid2DObject& grid,const mio::Grid2DObject& mask){

	const double mask_cellsize = mask.cellsize;
	const mio::Coords mask_llcorner = mask.llcorner;
	const mio::Coords llcorner = grid.llcorner;

	size_t nx_start = static_cast <int> (floor( (mask_llcorner.getEasting()-llcorner.getEasting())/mask_cellsize));
	size_t ny_start = static_cast <int> (floor( ( mask_llcorner.getNorthing()- llcorner.getNorthing())/mask_cellsize));

	double mean=0;
	double tot=0;
	for(size_t nx = 0; nx < mask.getNx(); nx++ )
	{
		for(size_t ny = 0; ny < mask.getNy(); ny++ )
		{
			double val = mask(nx,ny);
			if (val != mio::IOUtils::nodata && grid(nx_start+nx,ny_start+ny) != mio::IOUtils::nodata){
				tot+=val;
				mean+= grid(nx_start+nx,ny_start+ny)*val;
			}
		}
	}
	return mean/tot;
}

double GridOperations::sum(const mio::Grid2DObject& grid,const mio::Grid2DObject& mask){

	const double mask_cellsize = mask.cellsize;
	const mio::Coords mask_llcorner = mask.llcorner;
	const mio::Coords llcorner = grid.llcorner;
	size_t nx_start = static_cast <int> (floor( (mask_llcorner.getEasting()-llcorner.getEasting())/mask_cellsize));
	size_t ny_start = static_cast <int> (floor( ( mask_llcorner.getNorthing()- llcorner.getNorthing())/mask_cellsize));

	double sum=0;
	for(size_t nx = 0; nx < mask.getNx(); nx++ )
	{
		for(size_t ny = 0; ny < mask.getNy(); ny++ )
		{
			double val = mask(nx,ny);
			if (val != mio::IOUtils::nodata && grid(nx_start+nx,ny_start+ny) != mio::IOUtils::nodata){
				sum+= grid(nx_start+nx,ny_start+ny)*val;
			}
		}
	}
	return sum;
}


bool GridOperations::gridify(mio::Coords& point,  const mio::Grid2DObject& grid)
{
	mio::Coords llcorner = grid.llcorner;
	double cellsize = grid.cellsize;

	if (point.getLat()==mio::IOUtils::nodata || point.getLon()==mio::IOUtils::nodata) {
			//if the point is invalid, there is nothing we can do
			point.setGridIndex(mio::IOUtils::inodata, mio::IOUtils::inodata, mio::IOUtils::inodata, false); //mark the point as invalid
			return false;
	}

	bool status=true;
	int i,j;

	if (point.isSameProj(llcorner)==true) {
		//same projection between the grid and the point -> precise, simple and efficient arithmetics
		i = static_cast<int>(floor( (point.getEasting()-llcorner.getEasting()) / cellsize ));
		j = static_cast<int>(floor( (point.getNorthing()-llcorner.getNorthing()) / cellsize ));

	} else {
		//projections are different, so we have to do an intermediate step...
		mio::Coords tmp_point(point);
		tmp_point.copyProj(llcorner); //getting the east/north coordinates in the grid's projection
		i = static_cast<int>(floor( (tmp_point.getEasting()-llcorner.getEasting()) / cellsize ));
		j = static_cast<int>(floor( (tmp_point.getNorthing()-llcorner.getNorthing()) / cellsize ));

	}

	//checking that the calculated indices fit in the grid2D
	//and giving them the closest value within the grid if not.
	if (i<0) {
		i=0;
		status=false;
	}
	if (static_cast<size_t>(i)>=grid.getNx()) {
		i=static_cast<unsigned int>(grid.getNx()); //#Hack to cast size_t to int witout compilation warning
		status=false;
	}
	if (j<0) {
		j=0;
		status=false;
	}
	if (static_cast<size_t>(j)>=grid.getNy()) {
		j=static_cast<unsigned int>(grid.getNy()); //#Hack to cast size_t to int witout compilation warning
		status=false;
		status=false;
	}

	point.setGridIndex(i, j, mio::IOUtils::inodata, status); //mark as valid or invalid according to status
	return status;
}

} /* namespace sflow */
