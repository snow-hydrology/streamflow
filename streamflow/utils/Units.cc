/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/utils/Units.h>

namespace sflow {

const Units Units::nm   = Units::createBasicUnits("nm",   1, 0, 0, 0, 0, 1e-9, 0);
const Units Units::um   = Units::createBasicUnits("um",   1, 0, 0, 0, 0, 1e-6, 0);
const Units Units::mm   = Units::createBasicUnits("mm",   1, 0, 0, 0, 0, 1e-3, 0);
const Units Units::cm   = Units::createBasicUnits("cm",   1, 0, 0, 0, 0, 1e-2, 0);
const Units Units::dm   = Units::createBasicUnits("dm",   1, 0, 0, 0, 0, 1e-1, 0);
const Units Units::m    = Units::createBasicUnits("m",    1, 0, 0, 0, 0, 1, 0);
const Units Units::km   = Units::createBasicUnits("km",   1, 0, 0, 0, 0, 1e3, 0);
const Units Units::ha   = Units::createBasicUnits("ha",   2, 0, 0, 0, 0, 1e4, 0);
const Units Units::uL   = Units::createBasicUnits("uL",   3, 0, 0, 0, 0, 1e-9, 0);
const Units Units::mL   = Units::createBasicUnits("mL",   3, 0, 0, 0, 0, 1e-6, 0);
const Units Units::L    = Units::createBasicUnits("L",    3, 0, 0, 0, 0, 1e-3, 0);
const Units Units::hL   = Units::createBasicUnits("hL",   3, 0, 0, 0, 0, 1e-1, 0);
const Units Units::ms   = Units::createBasicUnits("ms",   3, 0, 0, 0, 0, 1e-3, 0);
const Units Units::s    = Units::createBasicUnits("s",    0, 0, 0, 1, 0, 1, 0);
const Units Units::min  = Units::createBasicUnits("min",  0, 0, 0, 1, 0, 60, 0);
const Units Units::h    = Units::createBasicUnits("h",    0, 0, 0, 1, 0, 3600, 0);
const Units Units::day  = Units::createBasicUnits("day",  0, 0, 0, 1, 0, 86400, 0);
const Units Units::mg   = Units::createBasicUnits("mg",   0, 1, 0, 0, 0, 1e-6, 0);
const Units Units::g    = Units::createBasicUnits("g",    0, 1, 0, 0, 0, 1e-3, 0);
const Units Units::kg   = Units::createBasicUnits("kg",   0, 1, 0, 0, 0, 1, 0);
const Units Units::mbar = Units::createBasicUnits("mbar", -1, 1, 0, -2, 0, 1e2, 0);
const Units Units::bar  = Units::createBasicUnits("bar",  -1, 1, 0, -2, 0, 1e5, 0);
const Units Units::Pa   = Units::createBasicUnits("Pa",   -1, 1, 0, -2, 0, 1, 0);
const Units Units::hPa  = Units::createBasicUnits("hPa",  -1, 1, 0, -2, 0, 1e2, 0);
const Units Units::atm  = Units::createBasicUnits("atm",  -1, 1, 0, -2, 0, 101325, 0);
const Units Units::mmHg = Units::createBasicUnits("mmHg", -1, 1, 0, -2, 0, 133.322368, 0);
const Units Units::degC = Units::createBasicUnits("degC", 0, 0, 1, 0, 0, 1, 273.15);
const Units Units::degK = Units::createBasicUnits("degK", 0, 0, 1, 0, 0, 1, 0);
const Units Units::cal  = Units::createBasicUnits("cal",  2, 1, 0, -2, 0, 4.184, 0);
const Units Units::kcal = Units::createBasicUnits("kcal", 2, 1, 0, -2, 0, 4.184e3, 0);
const Units Units::J    = Units::createBasicUnits("J",    2, 1, 0, -2, 0, 1, 0);
const Units Units::kJ   = Units::createBasicUnits("kJ",   2, 1, 0, -2, 0, 1e3, 0);
const Units Units::W    = Units::createBasicUnits("W",    2, 1, 0, -3, 0, 1, 0);
const Units Units::deg  = Units::createBasicUnits("deg",  0, 0, 0, 0, 0, SFConstants::PI/180, 0);
const Units Units::rad  = Units::createBasicUnits("rad",  0, 0, 0, 0, 0, 1, 0);
const Units Units::mol  = Units::createBasicUnits("mol",  0, 0, 0, 0, 1, 1, 0);
const Units Units::NONE = Units::createBasicUnits("-",    0, 0, 0, 0, 0, 1, 0);


/**
 * @brief Default constructor of class Units, initializing the units to NONE
 */
Units::Units() : _dims(), _convert_to_SI(1., 0.), _composition()
{
	*this = NONE;
}


/**
 * @brief Constructor of class Units used to create the static instances of the
 * class
 * @param dims Dimensions of the units
 * @param factor Multiplicative factor of the linear function used to
 * convert the units into the SI system
 * @param offset Intercept of the linear function used to convert the
 * units into the SI system
 * @param comp Elementary units composing the units
 */
Units::Units(const Dimensions& dims, const double& factor, const double& offset,
		const std::map<std::string, int>& comp) :
				_dims(dims), _convert_to_SI(factor, offset), _composition(comp) {}


/**
 * @brief Parses the input string and returns the corresponding Units instance.
 * @param strUnits String defining a units. Two notations can be used to define
 * units:
 *     - the scientific notation, which expects the basic units to be separated
 *       with spaces and directly followed with their associated power.
 *       Examples: 'kg m2 s-2', 'm3 s-1'
 *     - the standard notation, which supports the use of the following signs to
 *       combine the basic units: '(', ')', '*' and '/'. The power associated
 *       with each basic unit should be specified using symbol '^'.
 *       Examples: '(kg*m^2)/s^2', 'm^3/s'
 * The function automatically determines which type of notation is used.
 * @return Units instance corresponding to the input string
 */
Units Units::parseString(const std::string& strUnits) {
	try {
		if(strUnits.find_first_of("()*/^") != std::string::npos)
			return parseStandardString(strUnits);
		else
			return parseScientificString(strUnits);
	} catch (const std::exception& e) {
		throw mio::InvalidFormatException("Cannot convert string \"" + strUnits +
				"\" into a Units object: " + e.what(), AT);
	}
}


/**
 * @brief Method returning the linear function to convert the current
 * units into the specified ones.
 * @param units Units to convert the current ones into.
 * @return Conversion function
 */
LinearFunction1D Units::getConversionFunctionTo(const Units& units) const {
	if(_dims != units._dims)
		throw mio::InvalidArgumentException("Cannot convert " + this->toString() +
				" to " + units.toString() + ": units do not refer to the same "
				"physical quantity.", AT);

	return units._convert_to_SI.inv()(_convert_to_SI);
}


/**
 * @brief Method to get the quantity described by the current units
 * (e.g. mass, time, etc.).
 * @return Physical quantity described by the current units.
 */
Units::Quantity Units::getQuantity() const {
	const int dimL(_dims.length);
	const int dimT(_dims.time);
	const int dimM(_dims.mass);
	const int dimC(_dims.temperature);
	if(dimL == 0 && dimT == 0 && dimM == 0 && dimC == 0)
		return Units::Quantity::Number;
	else if(dimL == 1 && dimT == 0 && dimM == 0 && dimC == 0)
		return Units::Quantity::Length;
	else if(dimL == 0 && dimT == 1 && dimM == 0 && dimC == 0)
		return Units::Quantity::Time;
	else if(dimL == 0 && dimT == 0 && dimM == 1 && dimC == 0)
		return Units::Quantity::Mass;
	else if(dimL == 0 && dimT == 0 && dimM == 0 && dimC == 1)
		return Units::Quantity::Temperature;
	else if(dimL == 1 && dimT == -1 && dimM == 0 && dimC == 0)
		return Units::Quantity::Speed;
	else if(dimL == 1 && dimT == -2 && dimM == 0 && dimC == 0)
		return Units::Quantity::Acceleration;
	else if(dimL == 1 && dimT == -2 && dimM == 1 && dimC == 0)
		return Units::Quantity::Force;
	else if(dimL == 2 && dimT == -2 && dimM == 1 && dimC == 0)
		return Units::Quantity::Energy;
	else if(dimL == 2 && dimT == -3 && dimM == 1 && dimC == 0)
		return Units::Quantity::Power;
	else if(dimL == 2 && dimT == 0 && dimM == 0 && dimC == 0)
		return Units::Quantity::Surface;
	else if(dimL == 3 && dimT == 0 && dimM == 0 && dimC == 0)
		return Units::Quantity::Volume;
	else if(dimL == -1 && dimT == -2 && dimM == 1 && dimC == 0)
		return Units::Quantity::Pressure;
	else
		return Units::Quantity::Unknown;
}


/**
 * @brief Method to get the dimensions of the current units, expressed
 * as powers of the basic physical quantities that are: mass, time,
 * length and temperature.
 * @return Dimensions of the current units. The attributes of the structure
 * correspond to the basic physical quantities (mass, time, length and
 * temperature), and the values to the associated powers.
 */
const Units::Dimensions& Units::getDimensions() const {
	return _dims;
}


/**
 * @brief Method returning the current units as a string.
 * @return Units.
 */
std::string Units::toString(const Format& fmt) const {
	std::map<std::string, int> numerator, denominator;
	for(const std::pair<const std::string, int>& element : _composition) {
		if(element.second > 0)
			numerator[element.first] = element.second;
		else
			denominator[element.first] = element.second;
	}

	std::ostringstream os;
	if(fmt == Format::SCI) {
		os << mapToScientificString(numerator);
		if(!numerator.empty())
			os << " ";

		os << mapToScientificString(denominator);
	} else { //< fmt == Format::STD
		os << mapToStandardString(numerator, !denominator.empty());

		if(!denominator.empty()) {
			for(std::pair<const std::string, int>& element : denominator)
				element.second = -element.second;
			os << "/" << mapToStandardString(denominator, true);
		}
	}

	return os.str();
}


std::ostream& operator<<(std::ostream& os, const Units& units) {
	os << units.toString();
	return os;
}


std::istream& operator>>(std::istream& is, Units& units) {
	std::string strUnits;
	is >> strUnits;
	units = Units::parseString(strUnits);
	return is;
}


/**
 * @brief Overload of the operator *=
 * @param units Units to multiply to the current ones
 */
Units& Units::operator*=(const Units& units) {
	_convert_to_SI.setFactor(_convert_to_SI.getFactor()*units._convert_to_SI.getFactor());
	_convert_to_SI.setOffset(0.0);

	_dims += units._dims;

	const std::string noneSymbol(Units::NONE._composition.begin()->first);
	for(const std::pair<const std::string, int>& element : units._composition) {
		if(_composition.count(element.first) > 0) {
			if(_composition[element.first] == -element.second ||
					element.first == noneSymbol)
				_composition.erase(element.first);
			else
				_composition[element.first] += element.second;
		} else if(element.first != noneSymbol) {
			_composition[element.first] = element.second;
		}
	}
	if(_composition.empty())
		_composition[noneSymbol] = 1;

	return *this;
}


/**
 * @brief Overload of the operator /=
 * @param units Units to divide the current ones with
 */
Units& Units::operator/=(const Units& units) {
	*this *= units.pow(-1);
	return *this;
}


/**
 * @brief Overload of the operator *
 * @param a Left-hand side units
 * @param b Units to multiply the left-hand side with
 * @return Result of the units multiplication
 */
Units operator*(Units a, const Units& b) {
	a *= b;
	return a;
}


/**
 * @brief Overload of the operator /
 * @param a Left-hand side units
 * @param b Units to divide the left-hand side with
 * @return Result of the units division
 */
Units operator/(Units a, const Units& b) {
	a /= b;
	return a;
}


/**
 * @brief Method returning the current units, raised to the specified
 * power.
 * @param p Power the current units should be raised to.
 * @return Current units raised to the power pow.
 */
Units Units::pow(const int& p) const {
	if(*this == NONE) {
		return *this;
	} else {
		double newFactor(SFUtils::fastPow(_convert_to_SI.getFactor(), p));
		std::map<std::string, int> newComp(_composition);
		for(std::pair<const std::string, int>& element : newComp)
			element.second *= p;

		return Units(_dims*p, newFactor, 0, newComp);
	}
}


/**
 * @brief Overload of the operator ==, stating whether two units are
 * identical (they do not have to be expressed using the same basic
 * units, e.g. [J] and [kg*m^2/s^2] are considered to be identical).
 * @param units Units to compare with the current ones.
 * @return Are the two units identical?
 */
bool Units::operator==(const Units& units) const {
	return (_dims == units._dims) && (_convert_to_SI == units._convert_to_SI);
}


/**
 * @brief Overload of the operator !=, stating whether two units are different
 * @param units Units to compare with the current ones.
 * @return Are the two units different?
 */
bool Units::operator!=(const Units& units) const {
	return !(*this == units);
}


/**
 * @brief Method used to convert the input string into a Units instance
 * @param strUnits String defining a units using standard notation. Symbols
 * '*', '/', '+', '-',  * '(', ')' and '^' can be used in the string to define
 * composite units, e.g. '(m*kg)/s^2'
 * @return Units corresponding to the string passed as input argument
 */
Units Units::parseStandardString(std::string strUnits) {
	//Delete empty spaces
	strUnits.erase(std::remove_if(strUnits.begin(), strUnits.end(), isspace),
			strUnits.end());

	//Parse string
	if(strUnits.empty() || strUnits == "-" ||
			mio::IOUtils::strToLower(strUnits) == "none") {
		return Units::NONE;

	} else if(strUnits[0] == '(') {
		size_t strLen(strUnits.size()), nBrackets(1), pos(1);
		while(nBrackets > 0 && pos < strLen) {
			if(strUnits[pos] == ')')      --nBrackets;
			else if(strUnits[pos] == '(') ++nBrackets;
			++pos;
		}
		if(nBrackets > 0)
			throw std::invalid_argument("at least one character ')' missing");
		std::string lhs(strUnits.substr(1, pos-2));
		std::string rhs(strUnits.substr(pos));
		std::string exp;
		if(pos < strLen && strUnits[pos] == '^') {
			size_t endPos(std::min(strUnits.find_first_of("*/", pos), strUnits.size()));
			exp = strUnits.substr(pos+1, endPos-pos-1);
			rhs = strUnits.substr(endPos);
		}
		return parseStandardStringParts(lhs, exp, rhs);

	} else if(isalpha(strUnits[0])) {
		size_t endPos(std::min(strUnits.find_first_of("*/"), strUnits.size()));
		size_t pos(strUnits.rfind("^", endPos));
		std::string lhs(strUnits.substr(0, std::min(pos, endPos)));
		std::string rhs(strUnits.substr(endPos));
		std::string exp(pos == std::string::npos ?
				"" : strUnits.substr(pos+1, endPos-pos-1));
		return parseStandardStringParts(lhs, exp, rhs);

	} else {
		throw std::invalid_argument("string does not define a valid units");
	}
}


/**
 * @brief Method used to convert the input string into a Units instance
 * @param strUnits String defining a units using scientific notation. Basic
 * units should be separated with white spaces and followed by their associated
 * power, e.g. 'm kg s-2'
 * @return Units corresponding to the string passed as input argument
 */
Units Units::parseScientificString(const std::string& strUnits) {
	std::vector<std::string> basicUnits;
	mio::IOUtils::readLineToVec(strUnits, basicUnits);

	if(basicUnits.empty() || (basicUnits.size() == 1 &&
			(basicUnits[0] == "-" || basicUnits[0] == "none")))
		return Units::NONE;

	Units output(parseScientificStringPart(basicUnits[0]));
	for(size_t iPart(1); iPart < basicUnits.size(); ++iPart)
		output *= parseScientificStringPart(basicUnits[iPart]);

	return output;
}


/**
 * @brief Parses the two string parts and returns the corresponding Units
 * instance. The second part of the string can either be an empty string or
 * should start with one of the following symbols: '*', '/', '+', '-' or '^'.
 * @param lhs First part of the string defining the units
 * @param exp Exponent of the first part (empty string if equal to one)
 * @param rhs Second part of the string defining the units
 * @return Units instance
 */
Units Units::parseStandardStringParts(const std::string& lhs, std::string exp,
		std::string rhs) {
	Units units(Units::NONE);
	if(lhs.find_first_of("*/()^") == std::string::npos)
		units = toBasicUnits(lhs);
	else
		units = parseStandardString(lhs);

	if(!exp.empty()) {
		int e;
		if(exp.front() == '(' && exp.back() == ')')
			exp = exp.substr(1, exp.size()-2);
		if(!mio::IOUtils::convertString(e, exp))
			throw std::invalid_argument("symbol '^' is not followed by a number");
		units = units.pow(e);
	}

	if(!rhs.empty()) {
		if((rhs[0] != '*' && rhs[0] != '/') || rhs.size() == 1)
			throw std::invalid_argument("string does not define a valid units");

		const char op(rhs[0]);
		rhs.erase(0, 1); //< delete first character
		if(op == '*')         units = units*parseStandardString(rhs);
		else /* op == '/' */  units = units/parseStandardString(rhs);
	}

	return units;
}


/**
 * @brief Parses the input string and returns the corresponding units instance.
 * @param part String to be parsed, which should be formatted as [units][power],
 * such as for instance 'degK', 'm3', or 's-1'
 * @return Units instance
 */
Units Units::parseScientificStringPart(const std::string& part) {
	const size_t sep(part.find_first_of("+-0123456789"));
	Units output(toBasicUnits(part.substr(0, sep)));
	if(sep != std::string::npos) {
		int exp;
		if(!mio::IOUtils::convertString(exp, part.substr(sep)))
			throw std::invalid_argument("invalid units exponent");
		output = output.pow(exp);
	}
	return output;
}


/**
 * @brief This function converts the string passed as input argument into one
 * of the basic units defined as static members of class Units
 * @param strUnits String representing a basic units
 * @return Units instance corresponding to the string passed as input argument
 */
Units Units::toBasicUnits(const std::string& strUnits) {
	if(strUnits == "nm")                           return Units::nm;
	else if(strUnits == "um")                      return Units::um;
	else if(strUnits == "mm")                      return Units::mm;
	else if(strUnits == "cm")                      return Units::cm;
	else if(strUnits == "dm")                      return Units::dm;
	else if(strUnits == "m")                       return Units::m;
	else if(strUnits == "km")                      return Units::km;
	else if(strUnits == "ha")                      return Units::ha;
	else if(strUnits == "uL")                      return Units::uL;
	else if(strUnits == "mL")                      return Units::mL;
	else if(strUnits == "L")                       return Units::L;
	else if(strUnits == "hL")                      return Units::hL;
	else if(strUnits == "ms")                      return Units::ms;
	else if(strUnits == "s" || strUnits == "sec")  return Units::s;
	else if(strUnits == "min")                     return Units::min;
	else if(strUnits == "h" || strUnits == "hour") return Units::h;
	else if(strUnits == "d" || strUnits == "day")  return Units::day;
	else if(strUnits == "mg")                      return Units::mg;
	else if(strUnits == "g")                       return Units::g;
	else if(strUnits == "kg")                      return Units::kg;
	else if(strUnits == "mbar")                    return Units::mbar;
	else if(strUnits == "bar")                     return Units::bar;
	else if(strUnits == "Pa")                      return Units::Pa;
	else if(strUnits == "hPa")                     return Units::hPa;
	else if(strUnits == "atm")                     return Units::atm;
	else if(strUnits == "mmHg")                    return Units::mmHg;
	else if(strUnits == "C" || strUnits == "degC") return Units::degC;
	else if(strUnits == "K" || strUnits == "degK") return Units::degK;
	else if(strUnits == "cal")                     return Units::cal;
	else if(strUnits == "kcal")                    return Units::kcal;
	else if(strUnits == "J")                       return Units::J;
	else if(strUnits == "kJ")                      return Units::kJ;
	else if(strUnits == "W")                       return Units::W;
	else if(strUnits == "deg")                     return Units::deg;
	else if(strUnits == "rad")                     return Units::rad;
	else throw std::invalid_argument("unknown basic units");
}


std::string Units::mapToScientificString(const std::map<std::string, int>& map) {
	std::ostringstream os;
	if(!map.empty()) {
		std::map<std::string, int>::const_iterator it(map.begin());
		std::map<std::string, int>::const_iterator end(map.end());
		--end;
		for(; it != end; ++it) {
			os << it->first;
			if(it->second != 1)
				os << it->second;
			os << " ";
		}
		os << it->first;
		if(it->second != 1)
			os << it->second;
	}
	return os.str();
}


std::string Units::mapToStandardString(const std::map<std::string, int>& map,
		const bool& bracketString) {
	std::ostringstream os;
	if(map.empty()) {
		os << "1";
	} else {
		if(bracketString && map.size() > 1)	os << "(";

		std::map<std::string, int>::const_iterator it(map.begin());
		std::map<std::string, int>::const_iterator end(map.end());
		--end;
		for(; it != end; ++it) {
			os << it->first;
			if(it->second > 1)
				os << "^" << it->second;
			else if(it->second < 0)
				os << "^(" << it->second << ")";
			os << "*";
		}
		os << it->first;
		if(it->second > 1)
			os << "^" << it->second;
		else if(it->second < 0)
			os << "^(" << it->second << ")";

		if(bracketString && map.size() > 1)	os << ")";
	}
	return os.str();
}


/**
 * @brief Method converting a map to a string
 * @param map Map
 * @return String
 */
std::string Units::mapToString(const std::map<std::string, int>& map) {
	std::ostringstream os;
	std::map<std::string, int>::const_iterator lastPos = map.end();
	--lastPos;
	for(std::map<std::string, int>::const_iterator it = map.begin();
			it != map.end(); ++it) {
		os << it->first;
		if(it->second > 1)
			os << "^" << it->second;
		if(it != lastPos)
			os << "*";
	}
	return os.str();
}


/**
 * @brief Method used to instantiate the static public attributes.
 * @param name Units name
 * @param dimL Length dimensions associated with the units
 * @param dimM Mass dimensions associated with the units
 * @param dimC Temperature dimensions associated with the units
 * @param dimT Time dimensions associated with the units
 * @param dimN Mole dimensions associated with the units
 * @param factor Multiplicative factor of the linear function used to
 * convert the units to SI
 * @param offset Intercept of the linear function used to convert the
 * units to SI
 * @return Basic Units
 */
Units Units::createBasicUnits(const std::string& name, const int& dimL,
		const int& dimM, const int& dimC, const int& dimT, const int& dimN,
		const double& factor, const double& offset) {
	Dimensions dims(dimL, dimT, dimM, dimC, dimN);
	std::map<std::string, int> comp = {{name, 1}};
	return Units(dims, factor, offset, comp);
}



//-----------------------------------------------------------------------------
// Implementation of class Units::Dimensions
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class Units::Dimensions
 * @param L length dimensions
 * @param T time dimensions
 * @param M mass dimensions
 * @param C temperature dimensions
 * @param n mole dimensions
 */
Units::Dimensions::Dimensions(const int& _L, const int& _T, const int& _M,
		const int& _C, const int& _n):
				length(_L), time(_T), mass(_M), temperature(_C), mole(_n) {}


Units::Dimensions& Units::Dimensions::operator+=(const Dimensions& other) {
	length      += other.length;
	time        += other.time;
	mass        += other.mass;
	temperature += other.temperature;
	mole        += other.mole;
	return *this;
}


Units::Dimensions& Units::Dimensions::operator-=(const Dimensions& other) {
	length      -= other.length;
	time        -= other.time;
	mass        -= other.mass;
	temperature -= other.temperature;
	mole        -= other.mole;
	return *this;
}


Units::Dimensions& Units::Dimensions::operator*=(const int& exp) {
	length      *= exp;
	time        *= exp;
	mass        *= exp;
	temperature *= exp;
	mole        *= exp;
	return *this;
}


Units::Dimensions operator+(Units::Dimensions a, const Units::Dimensions& b) {
	a += b;
	return a;
}


Units::Dimensions operator-(Units::Dimensions a) {
	a.length      = -a.length;
	a.time        = -a.time;
	a.mass        = -a.mass;
	a.temperature = -a.temperature;
	a.mole        = -a.mole;
	return a;
}


Units::Dimensions operator-(Units::Dimensions a, const Units::Dimensions& b) {
	a -= b;
	return a;
}


Units::Dimensions operator*(Units::Dimensions a, const int& exp) {
	a *= exp;
	return a;
}


Units::Dimensions operator*(const int& exp, const Units::Dimensions& a) {
	return a*exp;
}


bool operator==(const Units::Dimensions& a, const Units::Dimensions& b) {
	return (a.length == b.length) && (a.time == b.time) && (a.mass == b.mass)
			&& (a.temperature == b.temperature) && (a.mole == b.mole);
}


bool operator!=(const Units::Dimensions& a, const Units::Dimensions& b) {
	return !(a == b);
}


} /* namespace sflow */
