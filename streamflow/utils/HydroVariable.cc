/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/utils/HydroVariable.h>

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class HydroVariable
//-----------------------------------------------------------------------------

HydroVariable::HydroVariable(const std::string& name, const Units& units) :
		_name(name), _units(units) {}

const std::string& HydroVariable::toString() const {
	return _name;
}

const Units& HydroVariable::getUnits() const {
	return _units;
}

bool HydroVariable::operator==(const HydroVariable& var) const {
	return (_name == var._name);
}

bool HydroVariable::operator!=(const HydroVariable& var) const {
	return !(*this == var);
}

HydroVariable::~HydroVariable() {}

Units HydroVariable::getLengthUnits() {
	Units::Dimensions dims(1, 0, 0, 0, 0);
	std::map<std::string, int> comp = {{"m", 1}};
	return Units(dims, 1, 0, comp);
}

Units HydroVariable::getPowerUnits() {
	Units::Dimensions dims(2, -3, 1, 0, 0);
	std::map<std::string, int> comp = {{"W", 1}};
	return Units(dims, 1, 0, comp);
}

Units HydroVariable::getTemperatureUnits() {
	Units::Dimensions dims(0, 0, 0, 1, 0);
	std::map<std::string, int> comp = {{"degK", 1}};
	return Units(dims, 1, 0, comp);
}

Units HydroVariable::getEnergyFluxUnits() {
	Units::Dimensions dims(0, -3, 1, 0, 0);
	std::map<std::string, int> comp = {{"W", 1}, {"m", -2}};
	return Units(dims, 1, 0, comp);
}

Units HydroVariable::getDischargeUnits() {
	Units::Dimensions dims(3, -1, 0, 0, 0);
	std::map<std::string, int> comp = {{"m", 3}, {"s", -1}};
	return Units(dims, 1, 0, comp);
}


//-----------------------------------------------------------------------------
// Implementation of class StreamVariable
//-----------------------------------------------------------------------------

const StreamVariable StreamVariable::H(               "WATER_DEPTH", getLengthUnits());
const StreamVariable StreamVariable::Q(               "DISCHARGE",   getDischargeUnits());
const StreamVariable StreamVariable::T(               "TEMPERATURE", getTemperatureUnits());
const StreamVariable StreamVariable::nISWR(           "NISWR",       getEnergyFluxUnits());
const StreamVariable StreamVariable::nILWR(           "NILWR",       getEnergyFluxUnits());
const StreamVariable StreamVariable::ELWR(            "ELWR",        getEnergyFluxUnits());
const StreamVariable StreamVariable::LatentHeatFlux(  "LATENT_HF",   getEnergyFluxUnits());
const StreamVariable StreamVariable::SensibleHeatFlux("SENSIBLE_HF", getEnergyFluxUnits());
const StreamVariable StreamVariable::BedCondHeatFlux( "BED_COND_HF", getEnergyFluxUnits());
const StreamVariable StreamVariable::BedFricHeatFlux( "BED_FRIC_HF", getEnergyFluxUnits());
const StreamVariable StreamVariable::InletHeatFlux(   "INLET_HF",    getPowerUnits());
const StreamVariable StreamVariable::LateralHeatFlux( "LATERAL_HF",  getPowerUnits());

const std::vector<const StreamVariable*> StreamVariable::_ALL_VARIABLES = {
		&H, &Q, &T, &nISWR, &nILWR, &ELWR, &LatentHeatFlux, &SensibleHeatFlux,
		&BedCondHeatFlux, &BedFricHeatFlux, &InletHeatFlux, &LateralHeatFlux
};


const StreamVariable& StreamVariable::getVariable(std::string name) {
	mio::IOUtils::toUpper(name);
	for(const StreamVariable* var : _ALL_VARIABLES) {
		if(var->toString() == name)
			return *var;
	}
	throw mio::InvalidArgumentException("Unknown stream variable with name \"" +
			name + "\"", AT);
}


StreamVariable::StreamVariable(const std::string& name, const Units& units) :
		HydroVariable(name, units) {}


//-----------------------------------------------------------------------------
// Implementation of class SubwatershedVariable
//-----------------------------------------------------------------------------

const SubwatershedVariable SubwatershedVariable::Q_source("SOURCE_DISCHARGE",   getDischargeUnits());
const SubwatershedVariable SubwatershedVariable::Q_outlet("OUTLET_DISCHARGE",   getDischargeUnits());
const SubwatershedVariable SubwatershedVariable::T_source("SOURCE_TEMPERATURE", getTemperatureUnits());
const SubwatershedVariable SubwatershedVariable::T_outlet("OUTLET_TEMPERATURE", getTemperatureUnits());

const std::vector<const SubwatershedVariable*> SubwatershedVariable::_ALL_VARIABLES = {
		&Q_source, &Q_outlet, &T_source, &T_outlet
};


const SubwatershedVariable& SubwatershedVariable::getVariable(std::string name) {
	mio::IOUtils::toUpper(name);
	for(const SubwatershedVariable* var : _ALL_VARIABLES) {
		if(var->toString() == name)
			return *var;
	}
	throw mio::InvalidArgumentException("Unknown subwatershed variable with name \""
			+ name + "\"", AT);
}


SubwatershedVariable::SubwatershedVariable(const std::string& name, const Units& units) :
		HydroVariable(name, units) {}

} /* namespace sflow */
