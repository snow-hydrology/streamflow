/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STREAMFLOW_STREAMFLOWEXCEPTIONS_H_
#define STREAMFLOW_STREAMFLOWEXCEPTIONS_H_

#include <meteoio/IOExceptions.h>

namespace sflow {

/**
 * @class RuntimeException
 * @brief thrown when a runtime error occurs
 */
class RuntimeException : public mio::IOException {
	public:
		RuntimeException(const std::string& message, const std::string& position) :
			IOException("RuntimeException: " + message, position) {}
};


/**
 * @class InvalidConfigKeyException
 * @brief thrown when a key in the configuration file is invalid
 */
class InvalidConfigKeyException : public mio::IOException {
	public:
		InvalidConfigKeyException(const std::string& message, const std::string& position) :
			IOException("InvalidConfigKeyException: " + message, position) {}
};


/**
 * @class MissingConfigKeyException
 * @brief thrown when a key is missing in the configuration file
 */
class MissingConfigKeyException : public mio::IOException {
	public:
		MissingConfigKeyException(const std::string& message, const std::string& position) :
			IOException("MissingConfigKeyException: " + message, position) {}
};


/**
 * @class InvalidIteratorException
 * @brief thrown when attempting at working with an invalid iterator
 */
class InvalidIteratorException : public mio::IOException {
	public:
		InvalidIteratorException(const std::string& message, const std::string& position) :
			IOException("InvalidIteratorException: " + message, position) {}
};


/**
 * @class InvalidIndexException
 * @brief thrown when requesting an element at an non-existing index. This is
 * different from the IndexOutOfBoundsException, which should be used only when
 * the index is not within valid bounds.
 */
class InvalidIndexException : public mio::IOException {
	public:
		InvalidIndexException(const std::string& message, const std::string& position) :
			IOException("InvalidIndexException: " + message, position) {}
};

/**
 * @class InvalidDateException
 * @brief thrown when requesting a meteo value that has not beeen loaded. This
 * should never be thrown at runtime (i.e this is not input dependant). If this
 * exeption is thrown, this indicates in implementation error.
 */
class InvalidDateException : public mio::IOException {
	public:
		InvalidDateException(const std::string& message, const std::string& position) :
			IOException("InvalidDateException: " + message, position) {}
};

/**
 * @class InvalidUnitsException
 * @brief thrown when working with wrong units or attempting at converting a
 * quantity in units which are not valid.
 */
class InvalidUnitsException : public mio::IOException {
	public:
		InvalidUnitsException(const std::string& message, const std::string& position) :
			IOException("InvalidUnitsException: " + message, position) {}
};


/**
 * @class NotImplementedException
 * @brief thrown when attempting at calling a function/method which is not
 * implemented yet
 */
class NotImplementedException : public mio::IOException {
	public:
		NotImplementedException(const std::string& message, const std::string& position) :
			IOException("NotImplementedException: " + message, position) {}
};


/**
 * @class DivisionByZeroException
 * @brief thrown when attempting at dividing a number by zero
 */
class DivisionByZeroException : public mio::IOException {
	public:
		DivisionByZeroException(const std::string& message, const std::string& position) :
			IOException("DivisionByZeroException: " + message, position) {}
};

} /* namespace sflow */

#endif /* STREAMFLOW_STREAMFLOWEXCEPTIONS_H_ */
