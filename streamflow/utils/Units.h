/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UNITS_H_
#define UNITS_H_


#include <map>
#include <cmath>
#include <string>
#include <istream>
#include <ostream>
#include <iterator>
#include <algorithm>
#include <stdexcept>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/utils/Function1D.h>
#include <streamflow/utils/StreamflowExceptions.h>


namespace sflow {

/**
 * @class Units
 * @brief Class defining physical units. Instances of this class can be
 * multiplied or divided between themselves, or even raised to some power...
 * exactly like real-life units!
 * The class provides access to basic standard units as static public members.
 * These basic units can be used to create composite units using the copy
 * constructor or the assignment operator, e.g.:
 * @code
 * Units speed(Units::m/Units::s);
 * speed = Units::km/Units::h;
 * @endcode
 * @author Aurelien Gallice
 * @date 2015-10-26
 */
class Units {
	public:
		/**
		 * @brief Format to be used when printing the units as a string
		 */
		enum class Format : char {
			SCI, /**< scientific format (e.g. "m s-1") */
			STD  /**< standard format (e.g. "m/s") */
		};


		/**
		 * @brief Enum regrouping the physical quantities that can be described
		 * by any units
		 */
		enum class Quantity : char {
			Length,
			Surface,
			Volume,
			Time,
			Mass,
			Pressure,
			Temperature,
			Energy,
			Power,
			Number,
			Angle,
			Speed,
			Acceleration,
			Force,
			Unknown
		};


		/**
		 * @brief Structure defining the 5 basic dimensions (length, time, mass,
		 * temperature and mole) which can be combined together to form any
		 * particular units. Each field of the structure represents a basic
		 * dimension and its value corresponds to the power it is associated
		 * with.
		 */
		struct Dimensions {
			int length;
			int time;
			int mass;
			int temperature;
			int mole;

			Dimensions(const int& L = 0, const int& T = 0, const int& M = 0,
					const int& C = 0, const int& n = 0);
			Dimensions& operator+=(const Dimensions& other);
			Dimensions& operator-=(const Dimensions& other);
			Dimensions& operator*=(const int& exp);
		};


		//Basic standard units intended to be used as building blocks to
		//create more complex composite units
		static const Units nm, um, mm, cm, dm, m, km; //< length units
		static const Units ha; //< surface units
		static const Units uL, mL, L, hL; //< volume units
		static const Units ms, s, min, h, day; //< time units
		static const Units mg, g, kg; //< mass units
		static const Units mbar, bar, Pa, hPa, atm, mmHg; //< pressure units
		static const Units degC, degK; //< temperature units
		static const Units cal, kcal, J, kJ; //< energy units
		static const Units W; //< power units
		static const Units deg, rad; //< angle units
		static const Units mol; //< quantity units
		static const Units NONE; //< No units

		Units();

		static Units parseString(const std::string& strUnits);

		LinearFunction1D getConversionFunctionTo(const Units& units) const;
	 	Quantity getQuantity() const;
		const Dimensions& getDimensions() const;

		Units& operator*=(const Units& units);
		Units& operator/=(const Units& units);
		friend Units operator*(Units a, const Units& b);
		friend Units operator/(Units a, const Units& b);
		Units pow(const int& pow) const;
		bool operator==(const Units& units) const;
		bool operator!=(const Units& units) const;

		std::string toString(const Format& fmt = Format::STD) const;
		friend std::ostream& operator<<(std::ostream& os, const Units& units);
		friend std::istream& operator>>(std::istream& is, Units& units);

		//Class HydroVariable is declared a friend of Units in order to access
		//its private constructor. The idea is to avoid using the static Units
		//instances in order to create composite ones. This avoids the "static
		//initialization order fiasco", since all static instances of class
		//HydroVariable contain units (see https://isocpp.org/wiki/faq/ctors#static-init-order).
		friend class HydroVariable;

	private:
		Dimensions _dims;
		LinearFunction1D _convert_to_SI;         //< linear function used to convert the current units into the SI system
		std::map<std::string, int> _composition; //< attribute to store the names of the units

		Units(const Dimensions& dims, const double& factor,	const double& offset,
				const std::map<std::string, int>& comp);

		static Units parseStandardString(std::string strUnits);
		static Units parseScientificString(const std::string& strUnits);
		static Units parseStandardStringParts(const std::string& lhs,
				std::string exp, std::string rhs);
		static Units parseScientificStringPart(const std::string& part);
		static Units toBasicUnits(const std::string& strUnits);
		static std::string mapToScientificString(const std::map<std::string, int>& map);
		static std::string mapToStandardString(const std::map<std::string, int>& map,
				const bool& bracketString);
		static std::string mapToString(const std::map<std::string, int>& map);
		static Units createBasicUnits(const std::string& name, const int& dimL,
				const int& dimM, const int& dimC, const int& dimT,
				const int& dimN, const double& factor, const double& offset);
};


//Utility functions related to class Units::Dimensions
Units::Dimensions operator+(Units::Dimensions a, const Units::Dimensions& b);
Units::Dimensions operator-(Units::Dimensions a);
Units::Dimensions operator-(Units::Dimensions a, const Units::Dimensions& b);
Units::Dimensions operator*(Units::Dimensions a, const int& exp);
Units::Dimensions operator*(const int& exp, const Units::Dimensions& a);
bool operator==(const Units::Dimensions& a, const Units::Dimensions& b);
bool operator!=(const Units::Dimensions& a, const Units::Dimensions& b);


} /* namespace sflow */

#endif /* UNITS_H_ */
