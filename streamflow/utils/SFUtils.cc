/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/


/**
 * @brief _VERSION is given as a compilation flag to tell us what is the version number \n
 * Please only use SF_VERSION in the code
 */
#ifndef SF_VERSION
	//here below, the double-expansion stringification macro trick...
	#define STR1(x) #x
	#define STR2(x) STR1(x)
	#define SF_VERSION STR2( _VERSION )
#endif

#include <streamflow/utils/SFUtils.h>

namespace sflow {

/**
* @brief Returns the library version
* @return Library version as a string
*/
std::string getLibVersion() {
	std::ostringstream ss;
	ss << SF_VERSION << " compiled on " << __DATE__ << " " << __TIME__;
	return ss.str();
}


//-----------------------------------------------------------------------------
// Implementation of class SFConstants
//-----------------------------------------------------------------------------

const std::string SFConstants::CONFIG_SECTION_NAME = "HYDROMODEL";
const double SFConstants::PI = 3.14159265358979323846;
const double SFConstants::PI_HALF = 1.57079632679489661923;
const double SFConstants::PI_TWICE = 2.*SFConstants::PI;
const double SFConstants::GRAVIT_ACCELERATION = 9.81;        //< gravitational acceleration in [m/s^2]
const double SFConstants::WATER_MASS_DENSITY = 1000.0;       //< water mass density in [kg/m^3]
const double SFConstants::WATER_HEAT_CAPACITY = 4181.3;      //< specific heat capacity of water in [J/(kg*K)]
const double SFConstants::WATER_MOLAR_MASS = 18.0e-3;        //< molar mass of water in [kg/mol]
const double SFConstants::WATER_EMISSIVITY = 0.96;           //< water emissivity [dimensionless]
const double SFConstants::WATER_VAP_LATENT_HEAT = 2264.76e3; //< latent heat of water vaporisation in [J/kg]
const double SFConstants::AIR_HEAT_CAPACITY = 1005.0;        //< specific heat capacity of air in [J/(kg*K)]
const double SFConstants::AIR_MOLAR_MASS = 28.97e-3;         //< dry air molar mass in [kg/mol]
const double SFConstants::STEFAN_BOLTZMANN = 5.670373e-8;    //< Stefan Boltzmann constant in [W/(m^2*K^4)]
const double SFConstants::VON_KARMAN = 0.4;                  //< Von Karman constant [dimensionless]
const double SFConstants::PERFECT_GAS_CONSTANT = 8.314;      //< perfect gas constant in [J/(mol*K)]


//-----------------------------------------------------------------------------
// Implementation of class SFLaws
//-----------------------------------------------------------------------------

/**
 * @brief Returns the atmospheric pressure at the given altitude above sea level
 * according to the standard model. The optional argument specifies whether the
 * atmosphere should be considered as isothermal or not for the pressure
 * computation (defaults to true since computation is less intensive).
 * @param altitude Altitude (in meters) at which the standard atmospheric
 * pressure should be returned
 * @param isothermal Should the atmosphere be considered as isothermal when
 * computing the atmospheric pressure?
 * @return Standard atmospheric pressure in Pa
 */
double SFLaws::standardAirPressure(const double& altitude, const bool& isothermal) {
	const double T0(288.15); //< [K]
	const double P0(101325); //< [Pa]
	const double Rair(SFConstants::PERFECT_GAS_CONSTANT/SFConstants::AIR_MOLAR_MASS);
	if(isothermal) {
		return P0*exp(-SFConstants::GRAVIT_ACCELERATION*altitude/(Rair*T0));
	} else {
		const double lapseRate(-6.5e-3); //< [K/m]
		const double exp(-SFConstants::GRAVIT_ACCELERATION/(Rair*lapseRate));
		return P0*pow(altitude*lapseRate/T0 + 1., exp);
	}
}


/**
 * @brief Returns the mass density of dry air
 * @param temperature Air temperature in [K]
 * @param pressure Air pressure in [Pa]
 * @return Air mass density in [kg/m^3]
 */
double SFLaws::airMassDensity(const double& temperature, const double& pressure) {
	const double density(pressure/(SFConstants::PERFECT_GAS_CONSTANT*temperature));
	return density*SFConstants::AIR_MOLAR_MASS;
}


/**
 * @brief Returns the vapor pressure of water
 * @param temperature Air temperature in [K]
 * @return Water vapor pressure in [Pa]
 */
double SFLaws::waterVaporPressure(const double& temperature) {
	return 612.75*exp(17.27*(temperature - 273.15)/(temperature - 35.85));
}


/**
 * @brief Returns the slope of the curve of water vapor pressure as a function
 * of air temperature
 * @param temperature Air temperature in [K]
 * @return Slope of the water vapor pressure curve in [Pa/K]
 */
double SFLaws::waterVaporPressureSlope(const double& temperature) {
	return 4098.17/mio::Optim::pow2(temperature - 35.85)*waterVaporPressure(temperature);
}


/**
 * @brief Returns the psychrometric constant
 * @param pressure Air pressure in [Pa]
 * @return Psychrometric constant in [Pa/K]
 */
double SFLaws::psychrometricConstant(const double& pressure) {
	return SFConstants::AIR_HEAT_CAPACITY*pressure/(SFConstants::WATER_VAP_LATENT_HEAT*0.622);
}


/**
 * @brief Returns the cosine of the solar zenith angle (angle between the local
 * vertical direction and the sun position) at the given location and at the
 * given date.
 * @param location Location where the zenith angle should be computed
 * @param date Date at which the zenith angle should be computed
 * @return Cosine of the zenith angle
 */
double SFLaws::cosSolarZenithAngle(const mio::Coords& location, const mio::Date& date) {
	const double sinDelta(sinSolarDeclination(date));
	const double cosDelta(sqrt(1. - mio::Optim::pow2(sinDelta)));
	const double latitude(SFUtils::degToRad(location.getLat()));
	const double longitude(SFUtils::degToRad(location.getLon()));
	const double& radPerDay(SFConstants::PI_TWICE);
	const double hourAngle(remainder(date.getJulian(true), 1.)*radPerDay + longitude);
	return sinDelta*sin(latitude) + cosDelta*cos(latitude)*cos(hourAngle);
}


/**
 * @brief Returns the sine of the solar declination (angle between the
 * celestial equator and the sun) at the given date.
 * @param date Date at which the solar declination should be computed
 * @return Sine of the solar declination
 */
double SFLaws::sinSolarDeclination(const mio::Date& date) {
    const double alpha(SFUtils::degToRad(23.44)); //< Earth's axial tilt (in radians)
    const double day(double(date.getJulianDayNumber()));
    return -sin(alpha)*cos(SFConstants::PI_TWICE/365.24*(day + 10.) +
    		0.0334*sin(SFConstants::PI_TWICE/365.24*(day - 2.)));
}


/**
 * @brief Returns the water albedo for the given incident angle
 * @param cosIncidentAngle Cosine of the incident angle for which the water
 * albedo should be computed
 * @return Water albedo
 */
double SFLaws::waterAlbedo(double cosIncidentAngle) {
	cosIncidentAngle = fmax(0., cosIncidentAngle);
	if(cosIncidentAngle < 0.1736) //< 0.1736 = cos(80 deg)
		return 2.9507*(acos(cosIncidentAngle) - SFConstants::PI_HALF) + 1.0;
	else
		return 0.091/cosIncidentAngle - 0.0386;
}


//-----------------------------------------------------------------------------
// Implementation of class Clock
//-----------------------------------------------------------------------------

Clock::Clock(const mio::Config& cfg, const mio::Date& startTime, const mio::Date& endTime) :
		_start_time(startTime), _end_time(endTime), _curr_time(startTime),
		_time_step(), _initial_time_step(), _min_time_step(), _max_time_step(),
		_time_step_limiters()
{
	//Get time step value
	double dtInMinutes(60.); //< 1 hour
	cfg.getValue("TIME_STEP", SFConstants::CONFIG_SECTION_NAME, dtInMinutes,
			mio::IOUtils::nothrow);
	_time_step = (unsigned int)(round(fmax(1., 60.*dtInMinutes)));
	_initial_time_step = _time_step;

	//Get time step limits
	std::vector<double> dtLimits;
	cfg.getValue("TIME_STEP::LIMITS", SFConstants::CONFIG_SECTION_NAME, dtLimits,
			mio::IOUtils::nothrow);
	if(!dtLimits.empty()) {
		if(dtLimits.size() > 2) {
			std::ostringstream os;
			os << "Expecting two numbers for key TIME_STEP::LIMITS in section ["
			   << SFConstants::CONFIG_SECTION_NAME << "], " << std::fixed
			   << std::setprecision(0) << dtLimits.size() << " found";
			throw InvalidConfigKeyException(os.str(), AT);
		}
		_min_time_step = (unsigned int)(round(fmax(1., 60.*dtLimits.front())));
		_max_time_step = (unsigned int)(round(fmax(1., 60.*dtLimits.back())));
		if(_min_time_step > _max_time_step)
			std::swap(_min_time_step, _max_time_step);
	} else {
		_min_time_step = 1;
		_max_time_step = _time_step;
	}

	//Check that time step is within its valid range
	if(_time_step < _min_time_step || _time_step > _max_time_step) {
		std::ostringstream os;
		os << "Time step (" << std::fixed << std::setprecision(0) << _time_step
		   << "s) is not within its valid range [" << _min_time_step << "s,"
		   << _max_time_step << "s]. Please modify the value of key TIME_STEP or "
		   << "TIME_STEP::LIMITS in section [" << SFConstants::CONFIG_SECTION_NAME
		   << "]";
		throw InvalidConfigKeyException(os.str(), AT);
	}
}

const mio::Date& Clock::getStartTime() const {
	return _start_time;
}

const mio::Date& Clock::getEndTime() const {
	return _end_time;
}

const mio::Date& Clock::getCurrTime() const {
	return _curr_time;
}

const mio::Date Clock::getNextTime() const {
	return _curr_time + double(_time_step)/86400.0; //< convert from seconds to days
}

const unsigned int& Clock::getTimeStep() const {
	return _time_step;
}

void Clock::updateTimeStep() {
	const double remainingTime(_end_time.getJulian() - _curr_time.getJulian());
	_time_step = std::min(_time_step, (unsigned int)round(86400.*remainingTime));
}

void Clock::advanceInTime() {
	_curr_time = this->getNextTime();
	if(_curr_time > _end_time)
		throw RuntimeException("Simulation end time (" +
				_end_time.toString(mio::Date::ISO) + ") exceeded!", AT);

	this->updateTimeStep();
}

void Clock::reset() {
	_curr_time = _start_time;
	_time_step = _initial_time_step;
}

void Clock::reset(const mio::Date& newStartTime, const mio::Date& newEndTime) {
	if(newStartTime >= newEndTime) {
		std::ostringstream os;
		os << "Cannot reset clock's start time to "
		   << newStartTime.toString(mio::Date::ISO)
		   << " and end time to " << newEndTime.toString(mio::Date::ISO)
		   << ": new start time is posterior to new end time";
		throw mio::InvalidArgumentException(os.str(), AT);
	}
	_start_time = newStartTime;
	_curr_time  = newStartTime;
	_end_time   = newEndTime;
	_time_step  = _initial_time_step;
	this->updateTimeStep();
}

void Clock::addTimeStepLimiter(const TimeStepLimiter& limiter) {
	_time_step_limiters.insert(&limiter);
}

void Clock::removeTimeStepLimiter(const TimeStepLimiter& limiter) {
	_time_step_limiters.erase(&limiter);
}



//-----------------------------------------------------------------------------
// Implementation of class ObservableModel
//-----------------------------------------------------------------------------

ObservableModel::ObservableModel() : _observers() {}

void ObservableModel::reset() {
	for(ModelObserver *obs : _observers)
		obs->reset();
}

void ObservableModel::addObserver(ModelObserver& obs) {
	_observers.insert(&obs);
}

void ObservableModel::removeObserver(ModelObserver& obs) {
	_observers.erase(&obs);
}

void ObservableModel::updateObservers(const mio::Date& currTime, const Watershed& watershed,
		const StreamNetwork& network) {
	for(ModelObserver *obs : _observers) {
		obs->update(currTime, watershed, network);
	}
}

ObservableModel::~ObservableModel() {}


//-----------------------------------------------------------------------------
// Implementation of the global functions in namespace SFUtils
//-----------------------------------------------------------------------------

namespace SFUtils {

double radToDeg(const double& angle) {
	return angle/SFConstants::PI*180.;
}


double degToRad(const double& angle) {
	return angle/180.*SFConstants::PI;
}


std::string trim(std::string str) {
    mio::IOUtils::trim(str);
    return str;
}


bool isSpace(const char& c) {
	return c == '\n' || c == '\t' || c == '\r' || c == ' ' || c == '\v';
}


/**
 * @brief Returns whether the first argument is a multiple of the second one
 * within the given precision
 * @param a Potential multiple of the second argument
 * @param b Divisor
 * @param precision Precision
 * @return Is the first argument a multiple of the second one?
 */
bool isMultiple(const double& a, const double& b, const double& precision) {
	return fabs(remainder(a, b)) < precision;
}


/**
 * @brief This method converts the given time (in seconds) to a string
 * containing the time value followed by its corresponding units.
 * @param time Time value to be converted to seconds
 * @return Time value converted to a string
 */
std::string timeToString(const double time) {
	std::ostringstream os;

	if(time < 60.)
		os << std::fixed << std::setprecision(0) << time << " sec";
	else if(time < 3600.)
		os << std::fixed << std::setprecision(1) << time/60. << " min";
	else if(time < 86400.)
		os << std::fixed << std::setprecision(1) << time/3600. << " h";
	else
		os << std::fixed << std::setprecision(1) << time/86400. << " days";

	return os.str();
}

/**
 * @brief Returns the contents of the input string in the array of double
 * passed as second input argument
 * @param[in] line String corresponding to a collection of numbers separated by
 * the character specified as third input argument
 * @param[out] data Array storing the numbers contained in the input string
 * @param[in] delim Character separating the numbers in the input string
 */
size_t readLineToVec(const std::string& line, std::vector<double>& data,
		const char& delim) {
	data.clear();

	double num;
	std::string word;
	std::istringstream lineToWord(line), wordToNum;
	wordToNum.setf(std::ios::fixed);

	while (getline(lineToWord, word, delim)){
		wordToNum.str(word);
		wordToNum.clear(); //< reset error state flag from eof to good
		wordToNum >> num;
		if (wordToNum.fail()) {
			std::ostringstream os;
			os << "Cannot read column " << data.size() + 1 << " in data line \""
			   << line << "\"";
			throw mio::InvalidFormatException(os.str(), AT);
		}
		data.push_back(num);
	}

	return data.size();
}

} /* namespace SFUtils */

} /* namespace sflow */
