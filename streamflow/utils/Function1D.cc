/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/utils/Function1D.h>

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class Function1D
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class Function1D.
 */
Function1D::Function1D() {}


//-----------------------------------------------------------------------------
// Implementation of class LinearFunction1D
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class LinearFunction1D.
 * @param factor Factor (a)
 * @param offset Offset (b)
 */
LinearFunction1D::LinearFunction1D(const double& factor, const double& offset) :
		Function1D(), _factor(factor), _offset(offset) {}

double LinearFunction1D::getFactor() const {
	return _factor;
}

double LinearFunction1D::getOffset() const {
	return _offset;
}

void LinearFunction1D::setFactor(const double& factor) {
	this->_factor = factor;
}

void LinearFunction1D::setOffset(const double& offset) {
	this->_offset = offset;
}

/**
 * @brief Overload of the operator (), evaluating the value of the function at
 * a given x.
 * @param x Value of x
 * @return Value of y
 */
double LinearFunction1D::operator()(const double& x) const {
	return x*_factor + _offset;
}

/**
 * @brief Overload of the operator (), evaluating the composition of
 * the input function and of the current one. For example, if
 * h = g(f), then h(x) = g(f(x)) for all x.
 * @param fun Function to compose with the current one
 * @return Composition of the two functions
 */
LinearFunction1D LinearFunction1D::operator()(const LinearFunction1D& fun) const {
	return LinearFunction1D(_factor*fun._factor, _offset + _factor*fun._offset);
}

/**
 * @brief Method returning the inverse of the current function: if
 * g = f.inv(), then g(f(x)) = x for all x.
 * @return Inverse of the current function.
 */
LinearFunction1D LinearFunction1D::inv() const {
	if(std::abs(_factor) < 1e-10)
		throw DivisionByZeroException("Cannot invert the function since "
				"multiplicative factor is zero", AT);

	return LinearFunction1D(1/_factor, -_offset/_factor);
}

/**
 * @brief Overload of the operator ==, stating whether two linear
 * functions are identical
 * @param fun Linear function to compare with the current one.
 * @return Are the two functions identical?
 */
bool LinearFunction1D::operator==(const LinearFunction1D& fun) const {
	return (_factor == fun._factor) && (_offset == fun._offset);
}


//-----------------------------------------------------------------------------
// Implementation of class PiecewiseFunction1D
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class PiecewiseFunction1D.
 * @param x x positions of the points defining the piecewise-defined function.
 * This vector must contain unique elements sorted in ascending order.
 * @param y y positions of the points defining the piecewise-defined function.
 */
PiecewiseFunction1D::PiecewiseFunction1D(const std::vector<double>& x,
		const std::vector<double>& y) : Function1D(), _x(x), _y(y),
				_n_points(x.size())
{
	if(!std::is_sorted(x.begin(), x.end()))
		throw mio::InvalidArgumentException("Vector of x positions must be in "
				"ascending order", AT);
	for(size_t ix = 0; ix < _n_points - 1; ++ix) {
		if(x[ix] == x[ix+1])
			throw mio::InvalidArgumentException("Vector of x positions must "
					"contain unique elements", AT);
	}
}

/**
 * @brief Overload of the operator (), evaluating the value of the function at
 * a given x.
 * @param xev Value of x at which the function should be evaluated.
 * @return Value of f(xev), determined as follows:
 *              | y[0],                                 if xev in (-inf, x[0])
 *     f(xev) = | interpolated between y[i] and y[i+1], if xev in [x[i], x[i+1])
 *              | y[n],                                 if xev in [x[n], +inf)
 * where n denotes the number of points in vector x. Interpolation between
 * y[i] and y[i+1] is performed using the private function "interpolate".
 */
double PiecewiseFunction1D::operator()(const double& xev) const {
	const size_t ix = findNextPosIndex(xev);
	if(ix == 0)
		return _y[0];
	else if(ix == _n_points)
		return _y[_n_points - 1];
	else
		return this->interpolate(xev, ix - 1);
}

/**
 * @brief Method returning the integral of the piecewise-defined function
 * between the points x1 and x2.
 * @param x1 Lower limit of the integration range.
 * @param x2 Upper limit of the integration range.
 * @return Value of the function integral between x1 and x2.
 */
double PiecewiseFunction1D::getIntegral(const double& x1, const double& x2) const {
	if(x1 > x2) {
		return -this->getIntegral(x2, x1);
	} else if(x1 == x2) {
		return 0.0;
	}
	const size_t ix1 = findNextPosIndex(x1);
	const size_t ix2 = findNextPosIndex(x2) - 1;
	if(x1 > _x[_n_points-1] || x2 < _x[0] || ix2 < ix1) {
		return this->integratePiece(x1, x2);
	} else {
		double integral = 0.0;
		integral += this->integratePiece(x1, _x[ix1]);
		for(size_t ix = ix1; ix < ix2; ++ix) {
			integral += this->integratePiece(ix);
		}
		integral += this->integratePiece(_x[ix2], x2);
		return integral;
	}
}

/**
 * @brief Method returning the mean value of the piecewise-defined function over
 * the interval [x1, x2).
 * @param x1 Lower limit of the interval.
 * @param x2 Upper limit of the interval
 * @return Mean value of the function over [x1, x2).
 */
double PiecewiseFunction1D::getMeanVal(const double& x1, const double& x2) const {
	return x1 == x2 ? this->operator()(x1) : std::abs(this->getIntegral(x1, x2))/(x2 - x1);
}

/**
 * @brief Function returning the index i of the smallest element in the x vector
 * such that x[i] > xev.
 * @param xev Position along the x axis.
 * @return Index i.
 */
size_t PiecewiseFunction1D::findNextPosIndex(const double& xev) const {
	const std::vector<double>::const_iterator it = std::find_if(
			_x.begin(), _x.end(),	[&](const double& v){return v > xev;});
	return size_t(it - _x.begin());
}

/**
 * @brief Method returning the integral of the piecewise-defined function in
 * the range [x[ix], x[ix+1]).
 * @param ix Index of the element in the x vector corresponding to the lower
 * limit of the integration range. The upper limit of the integration range
 * is x[ix+1].
 * @return Integral of the function in the range [x[ix], x[ix+1]).
 */
double PiecewiseFunction1D::integratePiece(const size_t& ix) const {
	return this->integratePiece(_x[ix], _x[ix+1]);
}


//-----------------------------------------------------------------------------
// Implementation of class PiecewiseLinear1D
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class PiecewiseLinear1D.
 * @param x x positions of the points defining the piecewise linear function.
 * This vector must contain unique elements sorted in ascending order.
 * @param y y positions of the points defining the piecewise linear function.
 */
PiecewiseLinear1D::PiecewiseLinear1D(const std::vector<double>& x,
		const std::vector<double>& y) : PiecewiseFunction1D(x, y) {}

/**
 * @brief Function evaluating the piecewise linear function at point xev,
 * which is in range [x[ix], x[ix+1]).
 * @param xev Value at which the function should be evaluated
 * @param ix Index of the largest element in vector x such that x[ix] < xev.
 * @return Value of f(xev).
 */
double PiecewiseLinear1D::interpolate(const double& xev, const size_t& ix) const {
	return (_y[ix+1] - _y[ix])/(_x[ix+1] - _x[ix])*(xev - _x[ix]) + _y[ix];
}

/**
 * @brief Method computing the integral of the piecewise linear function between
 * x1 and x2, where the entire range [x1, x2) contains only one piece of the
 * function.
 * @param x1 Lower limit of the integration range.
 * @param x2 Upper limit of the integration range.
 * @return Integral of the piecewise linear function over the range [x1, x2).
 */
double PiecewiseLinear1D::integratePiece(const double& x1, const double& x2) const {
	return (this->operator()(x2) + this->operator()(x1))/2.0*(x2 - x1);
}


//-----------------------------------------------------------------------------
// Implementation of class PiecewiseConstant1D
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class PiecewiseConstant1D.
 * @param x x positions of the points defining the piecewise constant function.
 * This vector must contain unique elements sorted in ascending order.
 * @param y y positions of the points defining the piecewise constant function.
 */
PiecewiseConstant1D::PiecewiseConstant1D(const std::vector<double>& x,
		const std::vector<double>& y) : PiecewiseFunction1D(x, y) {}

/**
 * @brief Function evaluating the piecewise linear function at point xev,
 * which is in range [x[ix], x[ix+1]).
 * @param xev Value at which the function should be evaluated
 * @param ix Index of the largest element in vector x such that x[ix] < xev.
 * @return Value of f(xev), which evaluates to y[i] in the range [x[i], x[i+1]),
 * to y[0] in the range (-inf, x[0]) and to y[n] in the range [x[n], +inf),
 * where n denotes the number of elements in x.
 */
double PiecewiseConstant1D::interpolate(const double&, const size_t& ix) const {
	return _y[ix];
}

/**
 * @brief Method computing the integral of the piecewise constant function
 * between x1 and x2, where the entire range [x1, x2) contains only one piece
 * of the function.
 * @param x1 Lower limit of the integration range.
 * @param x2 Upper limit of the integration range.
 * @return Integral of the piecewise constant function over the range [x1, x2).
 */
double PiecewiseConstant1D::integratePiece(const double& x1, const double& x2) const {
	return this->operator()(x1)*(x2 - x1);
}


//-----------------------------------------------------------------------------
// Implementation of class PowerFunction1D
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class PowerFunction1D.
 * @param a Factor (a)
 * @param b Power (b)
 */
PowerFunction1D::PowerFunction1D(const double& a, const double& b) :
		Function1D(), _factor(a), _power(b) {}

/**
 * @brief Overload of the operator (), evaluating the function at a given x.
 * @param x Point at which function should be evaluated
 * @return a*x^b
 */
double PowerFunction1D::operator()(const double& x) const {
	return _factor*pow(x, _power);
}

const double& PowerFunction1D::getFactor() const {
	return _factor;
}

const double& PowerFunction1D::getPower() const {
	return _power;
}

void PowerFunction1D::setFactor(const double& in_factor) {
	_factor = in_factor;
}

void PowerFunction1D::setPower(const double& in_power) {
	_power = in_power;
}

}
