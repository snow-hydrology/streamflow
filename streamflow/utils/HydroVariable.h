/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYDROVARIABLE_H_
#define HYDROVARIABLE_H_

#include <string>
#include <streamflow/utils/Units.h>
#include <streamflow/utils/StreamflowExceptions.h>
#include <meteoio/MeteoIO.h>

namespace sflow {

/**
 * @class HydroVariable
 * @brief This class defines a hydrological variable which can be measured
 * or simulated.
 * @author Aurelien Gallice
 * @date 2015-11-17
 */
class HydroVariable {
	public:
		HydroVariable(const HydroVariable&) = delete;
		HydroVariable& operator=(const HydroVariable&) = delete;
		virtual const std::string& toString() const;
		virtual const Units& getUnits() const;
		virtual bool operator==(const HydroVariable& var) const;
		virtual bool operator!=(const HydroVariable& var) const;
		virtual ~HydroVariable();

	protected:
		const std::string _name;
		const Units _units;

		HydroVariable(const std::string& name, const Units& units); //< private constructor to avoid instantiation

		//These static methods return Units instances which are created using
		//the private constructor of class Units. This avoids using the static
		//Units instances to create new Units and therefore solves the "static
		//initialization order fiasco" (see see https://isocpp.org/wiki/faq/ctors#static-init-order)
		static Units getLengthUnits();
		static Units getPowerUnits();
		static Units getTemperatureUnits();
		static Units getEnergyFluxUnits();
		static Units getDischargeUnits();
};


/**
 * @class StreamVariable
 * @brief This class defines a variable which can be measured or simulated in
 * a stream reach.
 * @author Aurelien Gallice
 * @date 2015-11-17
 */
class StreamVariable : public HydroVariable {
	public:
		static const StreamVariable H, Q, T;
		static const StreamVariable nISWR, nILWR, ELWR, LatentHeatFlux, SensibleHeatFlux;
		static const StreamVariable BedCondHeatFlux, BedFricHeatFlux;
		static const StreamVariable InletHeatFlux, LateralHeatFlux;

		static const StreamVariable& getVariable(std::string name);

	protected:
		static const std::vector<const StreamVariable*> _ALL_VARIABLES; //< contains the list of all stream variables

		StreamVariable(const std::string& name, const Units& units); //< private constructor to avoid instantiation
};


/**
 * @class SubwatershedVariable
 * @brief This class defines a variable which can be measured or simulated in
 * a subwatershed.
 * @author Aurelien Gallice
 * @date 2015-11-17
 */
class SubwatershedVariable : public HydroVariable {
	public:
		static const SubwatershedVariable Q_source, Q_outlet, T_source, T_outlet;

		static const SubwatershedVariable& getVariable(std::string name);

	protected:
		static const std::vector<const SubwatershedVariable*> _ALL_VARIABLES; //< contains the list of all subwatershed variables

		SubwatershedVariable(const std::string& name, const Units& units); //< private constructor to avoid instantiation
};

} /* namespace sflow */

#endif /* HYDROVARIABLE_H_ */
