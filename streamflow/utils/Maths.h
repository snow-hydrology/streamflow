/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MATHS_H_
#define MATHS_H_

#include <meteoio/MeteoIO.h>

namespace sflow {

/**
 * @brief This class implements several algorithms to find the root of a real
 * function. The root of a function f is defined as the value x0 for which
 * f(x0) = 0.
 * @author Aurelien Gallice
 * @date 2015-11-16
 */
class RootFindingAlgorithms {
	public:
		template<typename Lambda>
		static double falsePosition(Lambda&& fun, bool& convergence, const double& xOld,
				const double& xCurr, const double& precision = 1e-10,
				double fOld = mio::IOUtils::nodata,
				double fCurr = mio::IOUtils::nodata, const size_t& iIter = 0);

		template<typename Lambda>
		static double brentMethod(Lambda&& fun, bool& convergence,const double& xmin,
				const double& xmax, const double& precision = 1e-10,
				double fmin = mio::IOUtils::nodata,
				double fmax = mio::IOUtils::nodata, const size_t& iIter = 0);

	protected:
		static const size_t _MAX_NUM_ITERATIONS = 100; //< [-] maximum number of iterations to find the function root
};


/**
 * @brief Implements a variation of the false position method known as the
 * Illinois algorithm used to find the root of a function. The algorithm requires
 * an estimate of the range in which the root is located (see parameters xOld
 * and xNew below).
 *
 * Reference:
 * Dowell, M. and Jarratt, P., A modified regula falsi method for computing the
 * root of an equation, BTI(11), 1971.
 * @param fun Function whose root is to be found
 * @param xOld Lower limit of the range in which the root is to be found
 * @param xCurr Upper limit of the range in which the root is to be found
 * @param precision Absolute precision with which the root is to be found
 * @param fOld Value of fun(xOld), if available. The function will compute
 * it if not provided.
 * @param fCurr Value of fun(xCurr), if available. The function will compute
 * it if not provided.
 * @param iIter Parameter used by the recursive function calls (should not be
 * specified)
 * @return Value of x such that fun(x) = 0.
 */
template<typename Lambda>
double RootFindingAlgorithms::falsePosition(Lambda&& fun, bool& convergence,
		const double& xOld, const double& xCurr, const double& precision, double fOld,
		double fCurr, const size_t& iIter) {
	if(fabs(xCurr - xOld) < precision || fabs(fCurr) < precision)
	{
		convergence=true;
		return xCurr;
	}
	else if(iIter == _MAX_NUM_ITERATIONS)
	{
		convergence=false;
		return xCurr;
	}
		//throw std::runtime_error("Maximum number of iterations reached");

	if(fOld  == mio::IOUtils::nodata) fOld  = fun(xOld);
	if(fCurr == mio::IOUtils::nodata) fCurr = fun(xCurr);
	if(fOld*fCurr > 0.0) { //< handle the case where the zero is not bracketed at first function call
		const double& xmin(std::min(fOld, fCurr));
		const double& xmax(std::max(fOld, fCurr));
		const double x0(std::max(2.*xmin - xmax, 0.));
		const double x1(2.*xmax - xmin);
		return falsePosition(std::forward<Lambda>(fun), x0, x1, precision,
				fun(x0), fun(x1), iIter + 1);
	}

	const double xNew(xCurr - fCurr*(xCurr - xOld)/(fCurr - fOld));
	const double fNew(fun(xNew));
	if(fNew*fCurr < 0.0)
		return falsePosition(std::forward<Lambda>(fun), convergence, xCurr, xNew, precision,
				fCurr, fNew, iIter+1);
	else
		return falsePosition(std::forward<Lambda>(fun), convergence, xOld, xNew, precision,
				fOld/2.0, fNew, iIter+1);
}


/**
 * @brief Implements the simplified version of Brent's method proposed by
 * S.A. Stage (2013). The algorithm requires an estimate of the range in which
 * the root is located (see parameters xmin and xmax below).
 *
 * Reference:
 * Stage, S.A., Comments on an improvement to the Brent's method, IJEA, 4(1),
 * 2013.
 * @param fun Function whose root is to be found
 * @param xmin Lower limit of the range in which the root is to be found
 * @param xmax Upper limit of the range in which the root is to be found
 * @param precision Absolute precision with which the root is to be found
 * @param fmin Value of fun(xmin), if available. The function will compute
 * it if not provided.
 * @param fmax Value of fun(xmax), if available. The function will compute
 * it if not provided.
 * @param iIter Parameter used by the recursive function calls (should not be
 * specified)
 * @return Value of x such that fun(x) = 0.
 */
template<typename Lambda>
double RootFindingAlgorithms::brentMethod(Lambda&& fun, bool& convergence,
		const double& xmin, const double& xmax, const double& precision, double fmin,
		double fmax,const size_t& iIter) {
	if(fabs(xmax - xmin) < precision || fabs(fmax) < precision)
	{
		convergence=true;
		return xmax;
	}
	else if(iIter == _MAX_NUM_ITERATIONS)
	{
		convergence=false;
		return xmax;
	}
		//throw std::runtime_error("Maximum number of iterations reached");

	//Compute the values of the function at xmin and xmax
	if(fmin == mio::IOUtils::nodata) fmin = fun(xmin);
	if(fmax == mio::IOUtils::nodata) fmax = fun(xmax);
	if(fmin*fmax > 0.0) {//< handle the case where the zero is not bracketed at first function call
		const double x0(std::max(2.*xmin - xmax, 0.));
		const double x1(2.*xmax - xmin);
		return brentMethod(std::forward<Lambda>(fun), convergence, x0, x1, precision, fun(x0),
				fun(x1), iIter + 1);
	} else if(xmin > xmax) {//< handle the case where xmin and xmax have to be swapped (do not increment iIter)
		return brentMethod(std::forward<Lambda>(fun), convergence, xmax, xmin, precision,
				fmax, fmin, iIter);
	}

	//Compute the values of the function at two intermediate points xint1 and
	//xint2
	double xint1((xmin + xmax)/2.0);
	double fint1(fun(xint1));
	double xint2;
	if(fmin != fint1 && fmax != fint1) {
		xint2 = xmin*fmax*fint1/((fmin - fmax)*(fmin - fint1))
				+ xmax*fmin*fint1/((fmax - fmin)*(fmax - fint1))
				+ xint1*fmin*fmax/((fint1 - fmin)*(fint1 - fmax));
		const bool isInInterval(xmin < xint2 && xint2 < xmax);
		if(!isInInterval && fmin*fint1 < 0)
			xint2 = (xmin + xint1)/2.0;
		else if(!isInInterval)
			xint2 = (xmax + xint1)/2.0;
	} else if(fmin*fint1 < 0) {
		xint2 = xint1 - fint1*(xint1 - xmin)/(fint1 - fmin);
	} else {
		xint2 = xmax - fmax*(xmax - xint1)/(fmax - fint1);
	}
	double fint2(fun(xint2));

	//Recursively call the function over the new range containing the root
	if(fint1*fint2 <= 0.) {
		return brentMethod(std::forward<Lambda>(fun), convergence, xint1, xint2, precision,
				fint1, fint2, iIter + 1);
	} else {
		if(xint2 < xint1) {
			std::swap(xint1, xint2);
			std::swap(fint1, fint2);
		}
		if(fmin*fint1 < 0.)
			return brentMethod(std::forward<Lambda>(fun), convergence, xmin, xint1, precision,
					fmin, fint1, iIter + 1);
		else
			return brentMethod(std::forward<Lambda>(fun), convergence, xint2, xmax, precision,
					fint2, fmax, iIter + 1);
	}
}

} /* namespace sflow */

#endif /* MATHS_H_ */
