/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LIBSTREAMFLOW_H_
#define LIBSTREAMFLOW_H_

/**
 * @file libstreamflow.h
 * @version 1.0
 * This is the header file to include for projects making use of libstreamflow
 */

#include <streamflow/Watershed.h>
#include <streamflow/StreamNetwork.h>
#include <streamflow/HydrologicalModel.h>
#include <streamflow/StreamflowConfig.h>

#include <streamflow/utils/SFUtils.h>
#include <streamflow/utils/Units.h>
#include <streamflow/utils/Function1D.h>
#include <streamflow/utils/HydroVariable.h>

#include <streamflow/io/WatershedWriter.h>
#include <streamflow/io/NetworkWriter.h>
#include <streamflow/io/OutputWriter.h>
#include <streamflow/io/ReachCellsReader.h>
#include <streamflow/io/IOContainer.h>
#include <streamflow/io/DataContainer.h>
#include <streamflow/io/TSContainer.h>
#include <streamflow/io/TSGrids.h>
#include <streamflow/io/TSReaches.h>
#include <streamflow/io/TSWatersheds.h>
#include <streamflow/io/CalibrationDataContainer.h>
#include <streamflow/io/SimulatedDataContainer.h>



#include <streamflow/calibration/Parameter.h>
#include <streamflow/calibration/ParameterSet.h>
#include <streamflow/calibration/CalibrationAlgorithm.h>
#include <streamflow/calibration/ObjectiveFunction.h>
#include <streamflow/calibration/PerformanceMeasure.h>

#include <streamflow/streamReach/FrictionModel.h>
#include <streamflow/streamReach/HeightModel.h>
#include <streamflow/streamReach/WidthModel.h>
#include <streamflow/streamReach/StreamCell.h>
#include <streamflow/streamReach/StreamReach.h>
#include <streamflow/streamReach/LumpedStreamReach.h>
#include <streamflow/streamReach/LumpedReachDecorator.h>
#include <streamflow/streamReach/DiscretizedStreamReach.h>
#include <streamflow/streamReach/DiscretizedReachDecorator.h>

#include <streamflow/subwatershed/Subwatershed.h>
#include <streamflow/subwatershed/LumpedSubwatershed.h>
#include <streamflow/subwatershed/LumpedSubwatershedDecorator.h>

#endif /* LIBSTREAMFLOW_H_ */
