/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as 
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public 
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STREAMNETWORK_H_
#define STREAMNETWORK_H_

#include <vector>
#include <sstream>
#include <iostream>
#include <meteoio/MeteoIO.h>
#include <streamflow/Watershed.h>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/utils/StreamflowExceptions.h>
#include <streamflow/io/ReachCellsReader.h>
#include <streamflow/io/DataContainer.h>
#include <streamflow/streamReach/StreamReach.h>
#include <streamflow/streamReach/LumpedStreamReach.h>
#include <streamflow/streamReach/LumpedReachDecorator.h>
#include <streamflow/streamReach/DiscretizedStreamReach.h>
#include <streamflow/streamReach/DiscretizedReachDecorator.h>
#include <streamflow/streamReach/HeightModel.h>
#include <streamflow/streamReach/WidthModel.h>
#include <streamflow/streamReach/FrictionModel.h>
#include <streamflow/streamReach/ReachHeatFluxModel.h>
#include <streamflow/subwatershed/Subwatershed.h>

namespace sflow {

class Watershed; //< forward declaration to avoid cyclic includes

/**
 * @class StreamNetwork
 * @brief This class represents a stream network, composed of multiple stream
 * reaches. It provides facilities to handle all reaches at the same time, such
 * as evolving them in time.
 * The stream network parameters correspond to the collection of all the
 * parameters of every stream reach (plus potentially additional parameters).
 * In order to distinguish between the parameters of the different reaches,
 * each parameter name is prepended with the prefix "REACH[i]_", where [i]
 * denotes the index of the stream reach it is associated with. Note that the
 * denominations "reach index" and "watershed index" are used indifferently,
 * both refer to the ID number of the watershed in which the stream reach is
 * located. Additional parameters which are not specific to any stream reach
 * are not renamed.
 * @author Aurelien Gallice
 * @date 2015-08-10
 */
class StreamNetwork: public ParametrizableObject, public TimeEvolvingObject,
		public DiscretizedObject {
	public:
		class const_iterator;

		StreamNetwork(const mio::Config& cfg, Clock& clk, std::shared_ptr<DataContainer> dataContainer);
		virtual bool containsReach(const size_t& subwatershedId) const;
		virtual const StreamReach& getReach(const size_t& subwatershedId) const;
		virtual const std::vector<size_t>& getReachIndices() const;
		virtual StreamNetwork::const_iterator begin() const;
		virtual StreamNetwork::const_iterator end() const;
		virtual const bool& computesTemperature() const;
		virtual Discretization getDiscretization() const;
		virtual ParameterSet getParams() const;
		virtual void setParams(const ParameterSet&);
		virtual void setWatershed(const Watershed& watershed);
		virtual void advanceInTime();
		virtual void reset();
		void initialize();
		virtual void setDisplayInfo(const bool& display);
		virtual bool localizePoint(const mio::Coords& point, size_t& subwatershedId,
				size_t& cellId) const;
		virtual ~StreamNetwork();

	protected:
		std::vector<SharedObject*> _shared_objects;        //< contains the objects which are referenced to by all the stream reaches
		std::vector<ParametrizableObject*> _shared_params; //< points to the objects in _shared_objects which are ParametrizableObject objects
		std::vector<StreamReach*> _reaches;
		std::vector<size_t> _reach_indices;
		const bool _computes_temp;                         //< true if the temperature of the subwatershed's outflow discharge is computed
		bool _is_set;                                      //< becomes true after call to setWatershed
		double _distance_precision;                        //< [m] used to localize points in the stream network

		static const std::string _PARAMETER_PREFIX; //< prefix to prepend to the names of the reach parameters

		virtual void createReaches(const mio::Config& cfg, Clock& clock,
				const ReachCellsReader& cellsReader, std::shared_ptr<DataContainer> dataContainer);
		virtual void createLumpedMuskingumCungeReaches(const mio::Config& cfg,
				Clock& clock, const ReachCellsReader& reader);
		virtual void createDiscretizedMuskingumCungeReaches(const mio::Config& cfg,
				Clock& clock, const ReachCellsReader& reader);
		virtual void createLumpedDirectRoutingReaches(const mio::Config& cfg,
				const ReachCellsReader& reader);
		virtual void createDiscretizedDirectRoutingReaches(const mio::Config& cfg,
				const ReachCellsReader& reader);
		virtual void applyLumpedEnergyBalanceDecorator(const mio::Config& cfg,
				std::shared_ptr<DataContainer> dataContainer);
		virtual void applyDiscretizedEnergyBalanceDecorator(const mio::Config& cfg,
						Clock& clock, std::shared_ptr<DataContainer> dataContainer);
		virtual void connectReaches(const ReachCellsReader& reader);
		virtual StreamHeightModel& constructHeightModel(const mio::Config& cfg);
		virtual StreamWidthModel& constructWidthModel(const mio::Config& cfg);
		virtual StreamBedFrictionModel& constructFrictionModel(const mio::Config& cfg);

		static std::string getTemperatureModelName(const mio::Config& cfg);
		static const ReachCellsReader& getReachCellsReader(const mio::Config& cfg, std::shared_ptr<DataContainer> dataContainer);
		static const Subwatershed& identifySubwatershed(const StreamReach& reach,
				const Watershed& watershed);
};


/**
 * @class StreamNetwork::const_iterator
 * @brief This class behaves as an iterator operating on StreamNetwork objects.
 * It can be used to access the StreamReaches contained in the StreamNetwork
 * in an iterative manner, and allows the use of the range-based for-loop
 * introduced in c++11.
 * @author Aurelien Gallice
 * @date 2015-09-10
 */
class StreamNetwork::const_iterator {
	public:
		const_iterator(const StreamNetwork& network, const size_t& pos);
		const_iterator(const const_iterator& copy);
		virtual const_iterator& operator=(const const_iterator& copy);
		virtual const StreamReach& operator*() const;
		virtual const StreamReach* operator->() const;
		virtual const_iterator& operator++();
		virtual const_iterator operator+(const size_t& offset) const;
		virtual const_iterator operator-(const size_t& offset) const;
		virtual bool operator!=(const const_iterator& other) const;
		virtual ~const_iterator();

	protected:
		const StreamNetwork *_network;
		size_t _curr_pos;
};

} /* namespace sflow */

#endif /* STREAMNETWORK_H_ */
