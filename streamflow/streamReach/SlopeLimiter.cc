/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/streamReach/SlopeLimiter.h>

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class BergerLimiter
//-----------------------------------------------------------------------------

BergerLimiter::BergerLimiter(const double& hPrev, const double& hCurr, const double& hNext) :
		SlopeLimiter(), _a(hPrev/hCurr), _b(hNext/hCurr),
		_fp((1. + _a)/(2. + _a + _b)) {}


double BergerLimiter::operator()(const double& f) const {
	if(f <= _fp)
		return 2.*(1. - _a/(1. + _a)*pow(f/_fp, 1./_a));
	else
		return 2.*(1. - _b/(1. + _b)*pow((1. - f)/(1. - _fp), 1./_b))*(1./f - 1.);
}


BergerLimiter::~BergerLimiter() {}


//-----------------------------------------------------------------------------
// Implementation of class VanAlbadaLimiter
//-----------------------------------------------------------------------------

VanAlbadaLimiter::VanAlbadaLimiter(const double& hPrev, const double& hCurr, const double& hNext) :
		SlopeLimiter(), _A((hPrev + hCurr)/(hCurr + hNext)),
		_B(2.*hCurr/(hCurr + hNext)),
		_k((int)ceil(_B/(2.*fmin(1., _A) - _B)))
{
	if(_k > 10) {
		std::cout << "[w] The space discretization is such that the slope limiter "
		          << "will need to raise the ratio of the temperature difference between "
				  << "consecutive cells to a power larger than 10. Please try to use "
				  << "a different spatial discretization, avoiding as much as possible "
				  << "contiguous spatial elements with very different lengths.\n";
	}
}


double VanAlbadaLimiter::operator()(const double& f) const {
	const double gamma(1./f - 1.); //< corresponds to 1/theta in the nomenclature of Zeng
	const double gammaPowK(SFUtils::fastPow(gamma, _k));
	return _B*(gamma + gammaPowK)/(1. + _A*gammaPowK);
}


VanAlbadaLimiter::~VanAlbadaLimiter() {}

} /* namespace sflow */
