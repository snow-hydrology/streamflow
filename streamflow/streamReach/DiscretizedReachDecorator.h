/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STREAMFLOW_STREAMREACH_DISCRETIZEDREACHDECORATOR_H_
#define STREAMFLOW_STREAMREACH_DISCRETIZEDREACHDECORATOR_H_

#include <sstream>
#include <functional>
#include <streamflow/utils/Maths.h>
#include <streamflow/utils/StreamflowExceptions.h>
#include <streamflow/io/DataContainer.h>
#include <streamflow/calibration/ParameterSet.h>
#include <streamflow/streamReach/SlopeLimiter.h>
#include <streamflow/streamReach/StreamReach.h>
#include <streamflow/streamReach/FrictionModel.h>
#include <streamflow/streamReach/ReachHeatFluxModel.h>

namespace sflow {

//Forward declarations to avoid unnecessary includes
class StreamCell;
class Subwatershed;
class CellInflow;
class StreamVariable;


/**
 * @class DiscretizedReachDecorator
 * @brief This class implements a decorator to class DiscretizedReachInterface.
 * For a description of the decorator pattern,
 * see <a>https://en.wikipedia.org/wiki/Decorator_pattern</a>.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-12-04
 */
class DiscretizedReachDecorator: public DiscretizedReachInterface {
	using DiscretizedReachInterface::get; //< avoids shadowing method get() from base class

	public:
		static const double MIN_TEMPERATURE; //< [K] minimum allowed stream temperature
		static const double MAX_TEMPERATURE; //< [K] maximum allowed stream temperature

		DiscretizedReachDecorator(DiscretizedReachInterface *decoratedObject);
		DiscretizedReachDecorator(const DiscretizedReachDecorator&) = delete;
		DiscretizedReachDecorator& operator=(const DiscretizedReachDecorator&) = delete;
		virtual std::vector<StreamCell>::const_iterator begin() const override;
		virtual std::vector<StreamCell>::const_iterator end() const override;
		virtual bool isHeadReach() const override;
		virtual bool isSet() const override;
		virtual void advanceInTime() override;
		virtual void reset() override;
		virtual void setDisplayInfo(const bool& display) override;
		virtual void setParams(const ParameterSet& params) override;
		virtual void setSubwatershed(const Subwatershed& subwatershed) override;
		virtual void setUpstreamReach(const StreamReach& reach) override;
		virtual void setUpstreamReaches(const StreamReach& reach1,
				const StreamReach& reach2) override;
		virtual void resetUpstreamReaches() override;
		virtual void updateInletBoundaryConditions() override;
		virtual void updateLateralBoundaryConditions() override;
		virtual const CellInflow& getInletBoundaryConditions() const override;
		virtual const std::vector<CellInflow>& getLateralBoundaryConditions() const override;
		virtual ParameterSet getParams() const override;
		virtual size_t getNumCells() const override;
		virtual const std::vector<StreamCell>& getCells() const override;
		virtual const StreamCell& getCell(const size_t& idx) const override;
		virtual double get(const StreamVariable& var, const size_t& iCell) const override;
		virtual std::vector<double> get(const StreamVariable& var) const override;
		virtual unsigned int getStrahlerOrder() const override;
		virtual const Subwatershed& getSubwatershed() const override;
		virtual void initialize() override;
		virtual double getLength() const override;
		virtual double getWidth() const override;
		virtual double getMeanSlope() const override;
		virtual size_t getIndex() const override;
		virtual double getSpecificDrainageArea() const override;
		virtual double getTotalDrainageArea() const override;
		virtual ~DiscretizedReachDecorator() override;

	protected:
		DiscretizedReachInterface *_decorated_object;
};


/**
 * @class DiscretizedReachWithEB
 * @brief This class gives a LumpedStreamReach the possibility to compute stream
 * temperature, based on the energy-balance equation of the stream reach. The
 * origin of the meteorological data used to compute the energy-balance can be
 * specified by the
 * This class differs from class LumpedReachWithReachEB in the sense that it is
 * based on the meteorological variables averaged over the subwatershed area,
 * not those measured over the stream cells.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-12-04
 */
class DiscretizedReachWithEB : public DiscretizedReachDecorator, public TimeStepLimiter {
	public:
		using DiscretizedReachDecorator::get; //< avoids shadowing methods get() from base class

		DiscretizedReachWithEB(DiscretizedReachInterface *decoratedObject,
				const ReachHeatFluxModel& model,
				const StreamBedFrictionModel& fricModel, Clock& clk,  std::shared_ptr<DataContainer> dataCont);
		virtual int getRequiredMaximumTimeStep() const override;
		virtual void updateInletBoundaryConditions() override;
		virtual void advanceInTime() override;
		virtual void reset() override;
		virtual double get(const StreamVariable& var, const size_t& iCell) const override;
		virtual std::vector<double> get(const StreamVariable& var) const override;
		virtual void setDisplayInfo(const bool& display) override;
		virtual void initialize() override;
		virtual ~DiscretizedReachWithEB() override;

	protected:
		const ReachHeatFluxModel& _heat_fluxes;
		std::shared_ptr<DataContainer> dataContainer;
		const StreamBedFrictionModel& _friction_model;
		std::vector<mio::MeteoData> _meteo; //< stores the meteo variables at the current time step for method get()
		std::vector<double> _temperature;   //< [K] stream temperature
		std::vector<double> _air_pressure;  //< [Pa] so as to avoid computing the standard air pressure at each time step
		std::vector<double> _velocity;      //< [m/s] flow velocity
		double _inlet_temperature_old;
		double _inlet_temperature_inc;
		std::vector<SlopeLimiter*> _slope_limiters;
		bool _display_info;      //< should information be displayed in the standard output?
		bool _display_max_iter_warning;     //< should a warning be displayed in case the number of iterations exceeds its maximum allowed value?
		bool _display_max_temp_warning;     //< should a warning be displayed in case stream temperature exceeds its maximum allowed value?

		static const double _COURANT_NUMBER;        //< [-] maximum CFL number at each time step
		static const double _TEMPERATURE_PRECISION; //< [K] precision up to which stream temperature should be determined at each time step

		virtual std::vector<double> computeStandardAirPressure() const;
		virtual std::vector<SlopeLimiter*> createSlopeLimiters() const;
		virtual void updateFlowVelocity(const std::vector<double>& discharge,
				const std::vector<double>& waterHeight);
		virtual void advectTemperature(const double& timeStep);
		virtual void accountForHeatSources(const double& dt,
				const std::vector<double>& newDischarge,
				const std::vector<double>& newWaterHeight);
		virtual std::vector<double> reconstructLeftStates(double phantomTemp) const;
		virtual double ebResidual(const StreamCell& cell, const CellInflow& inflow,
				const double& currTemp, const double& newTemp, const double& dt,
				const double& incomingRad, const double& bedFriction,
				const mio::MeteoData& meteo, const double& surface,
				const double& volume) const;
};

} /* namespace sflow */

#endif /* STREAMFLOW_STREAMREACH_DISCRETIZEDREACHDECORATOR_H_ */
