/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WIDTHMODEL_H_
#define WIDTHMODEL_H_

#include <map>
#include <string>
#include <vector>
#include <numeric>
#include <unordered_map>
#include <meteoio/MeteoIO.h>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/streamReach/StreamReach.h>
#include <streamflow/streamReach/StreamCell.h>
#include <streamflow/calibration/Parameter.h>
#include <streamflow/calibration/ParameterSet.h>

namespace sflow {

/**
 * @class StreamWidthModel
 * @brief Virtual class used to compute the width of a stream reach, based on
 * the area drained by the latter.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-07-20
 */
class StreamWidthModel : public SharedObject, public ParametrizableObject {
	public:
		static const double MIN_STREAM_WIDTH; //< [m] minimum stream width (to avoid zero-width streams)

		StreamWidthModel() : SharedObject(), ParametrizableObject() {}
		virtual double getWidth(const StreamReach& reach) = 0;
		virtual ~StreamWidthModel() {}
};


/**
 * @class AreaPowerWidthModel
 * @brief This class implements the interface StreamWidthModel. It computes the
 * width of the stream reach (in m) as: \f$aA^b\f$, where \f$A\f$ denotes the
 * area drained by the stream reach (in km<sup>2</sup>), and \f$a\f$ and \f$b\f$
 * are two calibration parameters. These parameters should be indicated in the
 * configuration file using the following keys:
 * @code
 * WIDTH_MODEL_FACTOR         = 5
 * WIDTH_MODEL_FACTOR::LIMITS = 0 10
 * WIDTH_MODEL_POWER          = 2
 * WIDTH_MODEL_POWER::LIMITS  = 1 3
 * @endcode
 * The keys WIDTH_MODEL_FACTOR and WIDTH_MODEL_POWER correspond to the
 * parameters a and b, respectively. Their counterparts ending with
 * "\::LIMITS" indicate the minimum and maximum values that each parameter can
 * take, respectively (this information is useful to the optimization algorithm
 * in order to automatically calibrate the model). The keys ending with
 * "\::LIMITS" are optional.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-07-20
 */
class AreaPowerWidthModel : public StreamWidthModel {
	public:
		AreaPowerWidthModel(const mio::Config& cfg);
		virtual double getWidth(const StreamReach& reach);
		virtual ParameterSet getParams() const;
		virtual void setParams(const ParameterSet& params);
		virtual ~AreaPowerWidthModel();

	protected:
		Parameter _factor;
		Parameter _power;
		std::unordered_map<size_t, double> _cache_widths; //< stores the width of each subwatershed
};


/**
 * @class AreaLinearWidthModel
 * @brief This class implements the interface StreamWidthModel. It computes the
 * width of the stream reach (in m) as: \f$aA + b\f$, where \f$A\f$ denotes the
 * area drained by the stream reach (in km<sup>2</sup>), and \f$a\f$ and \f$b\f$
 * are two calibration parameters. These parameters should be indicated in the
 * configuration file using the following keys:
 * @code
 * WIDTH_MODEL_SLOPE           = 5
 * WIDTH_MODEL_SLOPE::LIMITS   = 0 10
 * WIDTH_MODEL_OFFSET          = 2
 * WIDTH_MODEL_OFFSET::LIMITS  = 1 3
 * @endcode
 * The keys WIDTH_MODEL_SLOPE and WIDTH_MODEL_OFFSET correspond to the
 * parameters a and b, respectively. Their counterparts ending with
 * "\::LIMITS" indicate the minimum and maximum values that each parameter can
 * take, respectively (this information is useful to the optimization algorithm
 * in order to automatically calibrate the model). The keys ending with
 * "\::LIMITS" are optional.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-07-20
 */
class AreaLinearWidthModel : public StreamWidthModel {
	public:
		AreaLinearWidthModel(const mio::Config& cfg);
		virtual double getWidth(const StreamReach& reach);
		virtual ParameterSet getParams() const;
		virtual void setParams(const ParameterSet& params);
		virtual ~AreaLinearWidthModel();

	protected:
		Parameter _slope;
		Parameter _offset;
		std::unordered_map<size_t, double> _cache_widths; //< stores the width of each subwatershed
};


/**
 * @class DischargePowerWidthModel
 * @brief This class implements the interface StreamWidthModel. It computes the
 * width of the stream reach (in m) as: \f$aQ^b\f$, where \f$Q\f$ denotes the
 * stream reach discharge (in m<sup>3</sup>/s), and \f$a\f$ and \f$b\f$ are two
 * calibration parameters. These parameters should be indicated in the
 * configuration file using the following keys:
 * @code
 * WIDTH_MODEL_FACTOR         = 5
 * WIDTH_MODEL_FACTOR::LIMITS = 0 10
 * WIDTH_MODEL_POWER          = 2
 * WIDTH_MODEL_POWER::LIMITS  = 1 3
 * @endcode
 * The keys WIDTH_MODEL_FACTOR and WIDTH_MODEL_POWER correspond to the
 * parameters a and b, respectively. Their counterparts ending with
 * "\::LIMITS" indicate the minimum and maximum values that each parameter can
 * take, respectively (this information is useful to the optimization algorithm
 * in order to automatically calibrate the model). The keys ending with
 * "\::LIMITS" are optional.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-10-09
 */
class DischargePowerWidthModel : public StreamWidthModel {
	public:
		DischargePowerWidthModel(const mio::Config& cfg);
		virtual double getWidth(const StreamReach& reach);
		virtual ParameterSet getParams() const;
		virtual void setParams(const ParameterSet& params);
		virtual ~DischargePowerWidthModel();

	protected:
		Parameter _factor;
		Parameter _power;
};


/**
 * @class UniformWidth
 * @brief This class implements the interface StreamWidthModel. It assigns the
 * same value to the width of all the stream reaches, regardless of the area
 * drained by the reach or the stream discharge. The width value (in meters)
 * can be specified in the configuration file using key UNIFORM_WIDTH:
 * @code
 * UNIFORM_WIDTH         = 5
 * UNIFORM_WIDTH::LIMITS = 1 10
 * @endcode
 * Key UNIFORM_WIDTH\::LIMITS indicates the minimum and maximum values that the
 * width can take (this information is useful to the optimization algorithm
 * in order to automatically calibrate the model). The key ending with
 * "\::LIMITS" is optional.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-10-09
 */
class UniformWidth: public StreamWidthModel {
	public:
		UniformWidth(const mio::Config& cfg);
		virtual double getWidth(const StreamReach&);
		virtual ParameterSet getParams() const;
		virtual void setParams(const ParameterSet& params);
		virtual ~UniformWidth();

	protected:
		Parameter _width;
};

} /* namespace sflow */

#endif /* WIDTHMODEL_H_ */
