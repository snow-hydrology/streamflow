/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STREAMCELL_H_
#define STREAMCELL_H_

#include <string>
#include <sstream>
#include <iostream>
#include <meteoio/MeteoIO.h>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/utils/StreamflowExceptions.h>

namespace sflow {

/**
 * @class StreamCell
 * @brief This class represents a stream cell, defined by its middle point
 * coordinates, its length, its slope, its specific drainage area (i.e. the
 * area specifically drained by the point and not by the other points belonging
 * to the same stream reach) and its total drainage area. The cell width is not
 * part of this container class, since all the points belonging to a same
 * stream reach are assumed to have the same width (as such, the width
 * attribute is defined in class StreamReach object and not here). Since
 * adjacent stream cells are rarely aligned along the vertical or horizontal
 * axes, the effective length of the stream segment symbolized by the cell does
 * generally not correspond to the cell edge's length. For example, when three
 * cells are aligned along a diagonal direction, the length of the middle one
 * is equal to sqrt(2)*[edge length]. It is this effective length which is
 * stored in public attribute "length". The class attributes are left public
 * for easier access to them, but the getter and setter methods can be used
 * instead if preferred.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-07-15
 */
class StreamCell {
	public:
		mio::Coords coords;
		double spec_drainage_area; //< [m^2] area drained specifically by the cell (and only by this cell)
		double tot_drainage_area;  //< [m^2] total upstream area drained by the cell
		double length;             //< [m]
		double slope;              //< [-]

		StreamCell();
		StreamCell(const mio::Coords& coords, const mio::DEMObject& dem, const double& specDrainageArea = 0.0,
				const double& totDrainageArea = 0.0, const double& length = 0.0,
				const double& slope = 0.0);
		void setCoords(const mio::Coords& coords);
		void setSpecificDrainageArea(const double& area);
		void setTotalDrainageArea(const double& area);
		void setLength(const double& length);
		void setSlope(const double& slope);
		void set(const mio::Coords& coords,const mio::DEMObject& dem, const double& specDrainageArea = 0.0,
				const double& totDrainageArea = 0.0, const double& length = 0.0,
				const double& slope = 0.0);
		const mio::Coords& getCoords() const;
		double getAltitude() const;
		double getEasting() const;
		double getNorthing() const;
		double getLat() const;
		double getLon() const;
		const double& getSpecificDrainageArea() const;
		const double& getTotalDrainageArea() const;
		const double& getLength() const;
		const double& getSlope() const;
		const std::string toString() const;
		friend std::ostream& operator<<(std::ostream& os, const StreamCell& p);
};


/**
 * @class CellInflow
 * @brief This class is intended to be used as a container for mass and heat
 * lateral fluxes into a given stream cell from the surrounding hillslopes. The
 * class attributes are left public for easier access to them, but the getter
 * and setter methods can be used instead if preferred.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-07-24
 */
class CellInflow {
	public:
		double discharge;   //< in [m^3/s]
		double temperature; //< in [K]

		CellInflow();
		const double& getDischarge() const;
		const double& getTemperature() const;
		double getHeat() const;
		void setDischarge(const double& discharge);
		void setTemperature(const double& temp);
		const std::string toString() const;
		CellInflow& operator+=(const CellInflow& other);
		CellInflow& operator-=(const CellInflow& other);
		CellInflow& operator*=(const double& num);
		CellInflow& operator/=(const double& num);
		friend CellInflow operator+(CellInflow a, const CellInflow& b);
		friend CellInflow operator-(CellInflow a, const CellInflow& b);
		friend CellInflow operator*(CellInflow a, const double& num);
		friend CellInflow operator/(CellInflow a, const double& num);
		friend std::ostream& operator<<(std::ostream& os, const CellInflow& i);
};


/**
 * @class CellIndex
 * @brief This struct is used as a container for the x- and y-indices of a grid
 * cell. Operator < is implemented so that this class can be used as a map key
 * (maps order their key-value pairs according to the key in c++).
 * @ingroup streamReach
 */
class CellIndex {
	public:
		size_t x;
		size_t y;

		CellIndex();
		CellIndex(const size_t& ix, const size_t& iy);
		void set(const mio::Coords& pt);
		void set(const mio::Coords& pt, const mio::Coords& llCorner,
				const double& cellSize);
		void set(const std::string& str);
		void set(const size_t& ix, const size_t& iy);
		friend bool operator==(const CellIndex& a, const CellIndex& b);
		friend bool operator<(const CellIndex& a, const CellIndex& b);
};

} /* namespace sflow */

#endif /* STREAMCELL_H_ */
