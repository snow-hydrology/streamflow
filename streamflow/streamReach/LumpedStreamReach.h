/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LUMPEDSTREAMREACH_H_
#define LUMPEDSTREAMREACH_H_

#include <vector>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/utils/StreamflowExceptions.h>
#include <streamflow/subwatershed/Subwatershed.h>
#include <streamflow/streamReach/StreamReach.h>
#include <streamflow/streamReach/StreamCell.h>
#include <streamflow/streamReach/HeightModel.h>
#include <streamflow/streamReach/WidthModel.h>
#include <streamflow/streamReach/FrictionModel.h>
#include <streamflow/calibration/Parameter.h>
#include <streamflow/calibration/ParameterSet.h>

namespace sflow {

/**
 * @class LumpedStreamReach
 * @brief This class partly implements the interface LumpedReachInterface. It
 * defines the attributes to be used by any lumped stream reach, along with
 * their associated setter and getter methods. The implementation of the time
 * stepping method is delegated to the subclasses.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-07-20
 */
class LumpedStreamReach: public LumpedReachInterface {
  public:
    using LumpedReachInterface::get; //< avoids shadowing methods get() from base class

    LumpedStreamReach(const std::vector<StreamCell>& cells,
        StreamWidthModel& widthModel, const Clock& clk);
    LumpedStreamReach(const LumpedStreamReach&) = delete;
    LumpedStreamReach& operator=(const LumpedStreamReach&) = delete;
    virtual std::vector<StreamCell>::const_iterator begin() const override;
    virtual std::vector<StreamCell>::const_iterator end() const override;
    virtual bool isHeadReach() const override;
    virtual bool isSet() const override;
    virtual void updateInletBoundaryConditions() override;
    virtual void updateLateralBoundaryConditions() override;
    virtual size_t getNumCells() const override;
    virtual const std::vector<StreamCell>& getCells() const override;
    virtual const StreamCell& getCell(const size_t& idx) const override;
    virtual unsigned int getStrahlerOrder() const override;
    virtual const Subwatershed& getSubwatershed() const override;
    virtual double getLength() const override;
    virtual double getWidth() const override;
    virtual double getMeanSlope() const override;
    virtual size_t getIndex() const override;
    virtual double getSpecificDrainageArea() const override;
    virtual double getTotalDrainageArea() const override;
    virtual const CellInflow& getInletBoundaryConditions() const override;
    virtual const CellInflow& getLateralBoundaryConditions() const override;
    virtual void setSubwatershed(const Subwatershed& subwatershed) override;
    virtual void setUpstreamReach(const StreamReach& reach) override;
    virtual void setUpstreamReaches(const StreamReach& reach1,
        const StreamReach& reach2) override;
    virtual void resetUpstreamReaches() override;
    virtual void reset() override;
    virtual double get(const StreamVariable& var) const override;
    virtual double get(const StreamVariable& var, const size_t&) const override;
    virtual void initialize() override;
    virtual ~LumpedStreamReach() override;

  protected:
    const std::vector<StreamCell> _cells;
    const Subwatershed *_subwatershed;
    std::vector<const LumpedReachInterface*> _upstream_reaches;
    StreamWidthModel& _width_model;
    bool _is_head_reach;
    unsigned int _strahler_order;
    const double _spec_drainage_area; //< in [m^2] area drained specifically by the reach (and by no other)
    const double _length;             //< in [m]
    double _width;                    //< in [m]
    double _mean_slope;               //< in [-] (not const since some algorithms may need to modify it to ensure that it is > 0)
    double _discharge;                //< in [m^3/s]
    double _water_height;             //< in [m]
    CellInflow _inlet_bc;             //< inlet boundary conditions
    CellInflow _lateral_bc;           //< lateral boundary conditions

    virtual void setInitialConditions();
    virtual double computeMeanSlope() const;
};


/**
 * @class InstantaneousRoutingReach
 * @brief This class implements a very simple stream reach, which
 * instantaneously advects water received from the upstream reaches and lateral
 * hillslopes to the downstream reach. The computation of the reach water
 * height is delegated to an instance of class StreamHeightModel (a reference
 * to which must be passed as input parameter to the class constructor) so that
 * the same object can be used to compute the water height of all reaches. This
 * allows for the model parameters to be concentrated in a single object,
 * instead of being copied in all instances of class InstantaneousRoutingReach.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-07-21
 */
class InstantaneousRoutingReach : public LumpedStreamReach {
  public:
    InstantaneousRoutingReach(const std::vector<StreamCell>& cells,
        StreamWidthModel& widthModel, const Clock& clk,
        StreamHeightModel& heightModel);
    virtual ParameterSet getParams() const override;
    virtual void setParams(const ParameterSet& params) override;
    virtual void advanceInTime() override;
    virtual void setDisplayInfo(const bool&) override;
    virtual ~InstantaneousRoutingReach() override;

  protected:
    StreamHeightModel& _height_model;
};


/**
 * @class LumpedMuskingumCungeReach
 * @brief This class implements a stream reach whose discharge and water height
 * are computed based on the Muskingum-Cunge method (Cunge, 1969). The method
 * implemented here corresponds to the variant MVPMC3 presented by Ponce and
 * Chaganti (1994). It is more physically-based than the original Muskingum
 * method and corresponds to a numerical solution of the diffusive wave
 * approximation of the shallow water equations. The method is second order
 * accurate in space but only first order accurate in time. The specification of
 * Manning's coefficient is delegated to an instance of class FrictionModel,
 * (a reference to which must be passed as input parameter to the class
 * constructor) so that the same object can be used to compute the friction
 * force in all reaches. This allows for the model parameters to be
 * concentrated in a single object, instead of being copied in all instances of
 * class MuskingumCungeReach.
 *
 * References:
 * Cunge, J.A., On the subject of a flood propagation computation method
 * (Muskingum method), Journal of Hydraulic Research, 2, 1969
 * Ponce, V.M. and Chaganti, P.V., Variable-parameter Muskingum-Cunge method
 * revisited, Journal of Hydrology, 162, 433-439, 1994
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-11-24
 */
class LumpedMuskingumCungeReach : public LumpedStreamReach, public TimeStepLimiter {
  public:
    LumpedMuskingumCungeReach(const std::vector<StreamCell>& cells,
        StreamWidthModel& widthModel, Clock& clk,
        const StreamBedFrictionModel& fricModel);
    virtual int getRequiredMaximumTimeStep() const override;
    virtual ParameterSet getParams() const override;
    virtual void setParams(const ParameterSet&) override;
    virtual void setSubwatershed(const Subwatershed& subwatershed) override;
    virtual void advanceInTime() override;
    virtual void reset() override;
    virtual void setDisplayInfo(const bool& display) override;
    virtual void updateLateralBoundaryConditions() override;
    virtual void updateInletBoundaryConditions() override;
    virtual void initialize() override;
    virtual ~LumpedMuskingumCungeReach() override;

  protected:
    double _inlet_discharge_old;   //< [m^3/s] upstream inflow discharge at the previous time step
    double _lateral_discharge_old; //< [m^3/s] lateral inflow discharge at the previous time step
    const StreamBedFrictionModel& _friction_model;
    bool _display_info;
    void innerAdvanceInTime(const double dt, const double meanDischarge);
    double celerity;
    static const double _MIN_SLOPE; //< [-] minimum mean slope

    virtual void setInitialConditions() override;
};

} /* namespace sflow */

#endif /* LUMPEDSTREAMREACH_H_ */
