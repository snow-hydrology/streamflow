/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <streamflow/streamReach/WidthModel.h>

namespace sflow {

const double StreamWidthModel::MIN_STREAM_WIDTH = 0.1; //< [m] minimum stream width (to avoid zero-width streams)


//-----------------------------------------------------------------------------
// Implementation of class AreaPowerWidthModel
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class AreaPowerWidthModel
 * @param cfg Instance of the Config object holding the model parameters
 */
AreaPowerWidthModel::AreaPowerWidthModel(const mio::Config& cfg) :
		_factor(), _power(), _cache_widths()
{
	const std::string& section = SFConstants::CONFIG_SECTION_NAME;
	_factor.set(cfg, "WIDTH_MODEL_FACTOR", section, Units::NONE, {1e-4, 1},
			"Factor used to compute the width of the stream reach based on the "
			"drainage area of the latter");
	_power.set(cfg, "WIDTH_MODEL_POWER", section, Units::NONE, {0.0, 3},
			"Exponent used to compute the width of the stream reach based on the"
			" drainage area of the latter");
}


/**
 * @brief Method returning the width of the given stream reach. The width
 * (im m) is computed as: a*A^b, where A is the drainage area (in km^2) and a
 * and b are two parameters.
 * @param reach Stream reach whose width must be computed
 * @return Stream width in meters
 */
double AreaPowerWidthModel::getWidth(const StreamReach& reach) {
	double width;
	if(!reach.isSet() || _cache_widths.count(reach.getIndex()) == 0) {
		const std::vector<StreamCell>& cells(reach.getCells());
		const double drainageArea(0.5*(cells.front().tot_drainage_area +
				cells.back().tot_drainage_area)*1e-6); //< in km^2
		width = fmax(_factor.getValue()*pow(drainageArea, _power.getValue()),
				StreamWidthModel::MIN_STREAM_WIDTH);
		if(reach.isSet()) //< ensure that we can get the reach index
			_cache_widths[reach.getIndex()] = width;
	} else {
		width = _cache_widths.at(reach.getIndex());
	}
	return width;
}


/**
 * @brief Method returning the model parameters
 * @return Model parameters
 */
ParameterSet AreaPowerWidthModel::getParams() const {
	ParameterSet params;
	params.add(_factor);
	params.add(_power);
	return params;
}


/**
 * @brief Method setting the model parameters. Not all parameters need to be
 * specified, unknown parameters will be ignored.
 * @param params Set containing the new values of the model parameters
 */
void AreaPowerWidthModel::setParams(const ParameterSet& params) {
	if(_factor.toBeCalibrated() && params.contains(_factor.getName()))
		_factor.setValue(params[_factor.getName()].getValue());

	if(_power.toBeCalibrated() && params.contains(_power.getName()))
		_power.setValue(params[_power.getName()].getValue());

	_cache_widths.clear();
}


/**
 * @brief Destructor of class AreaPowerWidthModel
 */
AreaPowerWidthModel::~AreaPowerWidthModel() {}


//-----------------------------------------------------------------------------
// Implementation of class AreaLinearWidthModel
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class AreaLinearWidthModel
 * @param cfg Instance of the Config object holding the model parameters
 */
AreaLinearWidthModel::AreaLinearWidthModel(const mio::Config& cfg) :
		_slope(), _offset(), _cache_widths()
{
	const std::string& section = SFConstants::CONFIG_SECTION_NAME;
	_slope.set(cfg, "WIDTH_MODEL_SLOPE", section, Units::m/Units::km.pow(2),
			{0., 5.}, "Linear slope used to compute the width of the stream "
			"reach based on the drainage area of the latter");
	_offset.set(cfg, "WIDTH_MODEL_OFFSET", section, Units::m, {0., 1.},
			"Offset used to compute the width of the stream reach based on the"
			" drainage area of the latter");
}


/**
 * @brief Method returning the width of the given stream reach. The width
 * (im m) is computed as: a*A + b, where A is the drainage area (in km^2) and a
 * and b are two parameters.
 * @param reach Stream reach whose width must be computed
 * @return Stream width in meters
 */
double AreaLinearWidthModel::getWidth(const StreamReach& reach) {
	double width;
	if(!reach.isSet() || _cache_widths.count(reach.getIndex()) == 0) {
		const std::vector<StreamCell>& cells(reach.getCells());
		const double drainageArea(0.5*(cells.front().tot_drainage_area +
				cells.back().tot_drainage_area)*1e-6); //< in km^2
		width = fmax(_slope.getValue()*drainageArea + _offset.getValue(),
				StreamWidthModel::MIN_STREAM_WIDTH);
		if(reach.isSet()) //< ensure that we can get the reach index
			_cache_widths[reach.getIndex()] = width;
	} else {
		width = _cache_widths.at(reach.getIndex());
	}
	return width;
}


/**
 * @brief Method returning the model parameters
 * @return Model parameters
 */
ParameterSet AreaLinearWidthModel::getParams() const {
	ParameterSet params;
	params.add(_slope);
	params.add(_offset);
	return params;
}


/**
 * @brief Method setting the model parameters. Not all parameters need to be
 * specified, unknown parameters will be ignored.
 * @param params Set containing the new values of the model parameters
 */
void AreaLinearWidthModel::setParams(const ParameterSet& params) {
	if(_slope.toBeCalibrated() && params.contains(_slope.getName()))
		_slope.setValue(params[_slope.getName()].getValue(_slope.getUnits()));

	if(_offset.toBeCalibrated() && params.contains(_offset.getName()))
		_offset.setValue(params[_offset.getName()].getValue(_offset.getUnits()));

	_cache_widths.clear();
}


/**
 * @brief Destructor of class AreaLinearWidthModel
 */
AreaLinearWidthModel::~AreaLinearWidthModel() {}


//-----------------------------------------------------------------------------
// Implementation of class DischargePowerWidthModel
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class DischargePowerWidthModel
 * @param cfg Instance of the Config object holding the model parameters
 */
DischargePowerWidthModel::DischargePowerWidthModel(const mio::Config& cfg) :
		_factor(), _power()
{
	const std::string& section = SFConstants::CONFIG_SECTION_NAME;
	_factor.set(cfg, "WIDTH_MODEL_FACTOR", section, Units::NONE, {1e-4, 1},
			"Factor used to compute the width of the stream reach based on the "
			"stream discharge");
	_power.set(cfg, "WIDTH_MODEL_POWER", section, Units::NONE, {0.0, 3},
			"Exponent used to compute the width of the stream reach based on the"
			" stream discharge");
}


/**
 * @brief Method returning the width of the given stream reach. The width
 * (im m) is computed as: a*Q^b, where Q is the stream discharge (in m^3/s) and
 * a and b are two parameters.
 * @param reach Stream reach whose width must be computed
 * @return Stream width in meters
 */
double DischargePowerWidthModel::getWidth(const StreamReach& reach) {
	double discharge;
	switch(reach.getDiscretization()) {
	case DiscretizedObject::Discretization::Lumped:
		discharge = static_cast<const LumpedReachInterface&>(reach).get(StreamVariable::Q);
		break;
	case DiscretizedObject::Discretization::Distributed:
		{
		const std::vector<double>& Q(static_cast<const DiscretizedReachInterface&>(reach).get(StreamVariable::Q));
		discharge = std::accumulate(Q.begin(), Q.end(), 0.0)/double(Q.size());
		break;
		}
	default:
		std::ostringstream os;
		os << "Error with the discretization retuened by reach.getDiscretization(). Unknown value";
		throw RuntimeException(os.str(), AT);
	}
	return fmax(_factor.getValue()*mio::Optim::fastPow(discharge, _power.getValue()),
			StreamWidthModel::MIN_STREAM_WIDTH);
}


/**
 * @brief Method returning the model parameters
 * @return Model parameters
 */
ParameterSet DischargePowerWidthModel::getParams() const {
	ParameterSet params;
	params.add(_factor);
	params.add(_power);
	return params;
}


/**
 * @brief Method setting the model parameters. Not all parameters need to be
 * specified, unknown parameters will be ignored.
 * @param params Set containing the new values of the model parameters
 */
void DischargePowerWidthModel::setParams(const ParameterSet& params) {
	if(_factor.toBeCalibrated() && params.contains(_factor.getName()))
		_factor.setValue(params[_factor.getName()].getValue());

	if(_power.toBeCalibrated() && params.contains(_power.getName()))
		_power.setValue(params[_power.getName()].getValue());
}


/**
 * @brief Destructor of class DischargePowerWidthModel
 */
DischargePowerWidthModel::~DischargePowerWidthModel() {}


//-----------------------------------------------------------------------------
// Implementation of class UniformWidth
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class UniformWidth
 * @param cfg Instance of the Config object holding the model parameters
 */
UniformWidth::UniformWidth(const mio::Config& cfg) :
		_width(cfg, "UNIFORM_WIDTH", SFConstants::CONFIG_SECTION_NAME, Units::m,
				{0.1, 100.}, "Width of all the stream reaches (regardless of "
				"their drainage area or discharge)") {}


/**
 * @brief Method returning the stream reach width, which is the same for all the
 * reaches
 * @return Stream width in meters
 */
double UniformWidth::getWidth(const StreamReach&) {
	return _width.getValue();
}


/**
 * @brief Method returning the model parameters
 * @return Model parameters
 */
ParameterSet UniformWidth::getParams() const {
	ParameterSet params;
	params.add(_width);
	return params;
}


/**
 * @brief Method setting the model parameters. Not all parameters need to be
 * specified, unknown parameters will be ignored.
 * @param params Set containing the new values of the model parameters
 */
void UniformWidth::setParams(const ParameterSet& params) {
	if(_width.toBeCalibrated() && params.contains(_width.getName()))
		_width.setValue(params[_width.getName()].getValue(_width.getUnits()));
}


/**
 * @brief Destructor of class UniformWidth
 */
UniformWidth::~UniformWidth() {}

} /* namespace sflow */
