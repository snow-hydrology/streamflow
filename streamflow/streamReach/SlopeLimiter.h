/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STREAMFLOW_STREAMREACH_SLOPELIMITER_H_
#define STREAMFLOW_STREAMREACH_SLOPELIMITER_H_

#include <cmath>
#include <iostream>
#include <streamflow/utils/SFUtils.h>

namespace sflow {

/**
 * @brief This class defines the interface that must be implemented by all slope
 * limiter functions. The latter are used in the finite-volume methods in order
 * to limit numerical diffusion at the shocks and wet-dry fronts. They serve to
 * reconstruct the left state of any variable \f$y(x)\f$ at each cell interface
 * using the following formula:
 * 		\f[y(x_{i+1/2,L}) = y(x_i) + \frac{1}{2} \phi [y(x_i) - y(x_{i-1})],\f]
 * where \f$\phi\f$ denotes the limiter.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2016-01-11
 */
class SlopeLimiter {
	public:
		SlopeLimiter() {}
		virtual double operator()(const double& theta) const = 0;
		virtual ~SlopeLimiter() {}
};


/**
 * @class BergerLimiter
 * @brief This class implements the enhanced slope limiter developed by Berger
 * et al. (2005). The formula of the limiter was adapted so as to conform
 * with the limiter definition stated in the documentation header of the parent
 * class. The nomenclature of the model parameters corresponds to the
 * one used by Berger et al. in their paper.
 *
 * Reference: Berger, M., Aftosmis, M.J., and Murman, S.M., Analysis of slope
 * limiters on irregular grids, 43rd AIAA Aerospace Science Meeting, 2005.
 * @ingroup streamReach
 */
class BergerLimiter : public SlopeLimiter {
	public:
		BergerLimiter(const double& hPrev, const double& hCurr, const double& hNext);
		virtual double operator()(const double& f) const;
		virtual ~BergerLimiter();

	protected:
		double _a;
		double _b;
		double _fp;
};


/**
 * @class VanAlbadaLimiter
 * @brief This class implements the enhanced van Albada slope limiter developed
 * by X. Zeng (2014). The formula of the limiter was adapted so as to conform
 * with the limiter definition stated in the documentation header of the parent
 * class. The nomenclature of the model parameters corresponds to the
 * one used by X. Zeng in his paper.
 *
 * Reference: Xianyi Zeng, A general approach to enhance slope limiters on
 * non-uniform rectilinear grids, 2014 (not published).
 * @ingroup streamReach
 */
class VanAlbadaLimiter : public SlopeLimiter {
	public:
		VanAlbadaLimiter(const double& hPrev, const double& hCurr, const double& hNext);
		virtual double operator()(const double& f) const;
		virtual ~VanAlbadaLimiter();

	protected:
		double _A;
		double _B;
		int _k;
};

} /* namespace sflow */

#endif /* STREAMFLOW_STREAMREACH_SLOPELIMITER_H_ */
