/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/streamReach/LumpedReachDecorator.h>

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class LumpedReachDecorator
//-----------------------------------------------------------------------------

const double LumpedReachDecorator::MIN_TEMPERATURE(273.15); //< [K]
const double LumpedReachDecorator::MAX_TEMPERATURE(308.15); //< [K]


/**
 * @brief Constructor of class LumpedReachDecorator
 * @param decoratedObject Pointer to the object to be decorated
 */
LumpedReachDecorator::LumpedReachDecorator(LumpedReachInterface *decoratedObject) :
				LumpedReachInterface(decoratedObject->getClock()),
				_decorated_object(decoratedObject) {}

std::vector<StreamCell>::const_iterator LumpedReachDecorator::begin() const {
	return _decorated_object->begin();
}

std::vector<StreamCell>::const_iterator LumpedReachDecorator::end() const {
	return _decorated_object->end();
}

bool LumpedReachDecorator::isHeadReach() const {
	return _decorated_object->isHeadReach();
}

bool LumpedReachDecorator::isSet() const {
	return _decorated_object->isSet();
}

ParameterSet LumpedReachDecorator::getParams() const {
	return _decorated_object->getParams();
}

void LumpedReachDecorator::setParams(const ParameterSet& params) {
	_decorated_object->setParams(params);
}

void LumpedReachDecorator::advanceInTime() {
	_decorated_object->advanceInTime();
}

void LumpedReachDecorator::reset() {
	_decorated_object->reset();
}

void LumpedReachDecorator::setDisplayInfo(const bool& display) {
	_decorated_object->setDisplayInfo(display);
}

void LumpedReachDecorator::setUpstreamReach(const StreamReach& reach) {
	_decorated_object->setUpstreamReach(reach);
}

void LumpedReachDecorator::setUpstreamReaches(const StreamReach& reach1,
		const StreamReach& reach2) {
	_decorated_object->setUpstreamReaches(reach1, reach2);
}

void LumpedReachDecorator::resetUpstreamReaches() {
	_decorated_object->resetUpstreamReaches();
}

void LumpedReachDecorator::setSubwatershed(const Subwatershed& subwatershed) {
	_decorated_object->setSubwatershed(subwatershed);
}

void LumpedReachDecorator::updateInletBoundaryConditions() {
	_decorated_object->updateInletBoundaryConditions();
}

void LumpedReachDecorator::updateLateralBoundaryConditions() {
	_decorated_object->updateLateralBoundaryConditions();
}

size_t LumpedReachDecorator::getNumCells() const {
	return _decorated_object->getNumCells();
}

const std::vector<StreamCell>& LumpedReachDecorator::getCells() const {
	return _decorated_object->getCells();
}

const StreamCell& LumpedReachDecorator::getCell(const size_t& idx) const {
	return _decorated_object->getCell(idx);
}

unsigned int LumpedReachDecorator::getStrahlerOrder() const {
	return _decorated_object->getStrahlerOrder();
}

const Subwatershed& LumpedReachDecorator::getSubwatershed() const {
	return _decorated_object->getSubwatershed();
}

void LumpedReachDecorator::initialize() {
	_decorated_object->initialize();
}


double LumpedReachDecorator::getLength() const {
	return _decorated_object->getLength();
}

double LumpedReachDecorator::getWidth() const {
	return _decorated_object->getWidth();
}

double LumpedReachDecorator::getMeanSlope() const {
	return _decorated_object->getMeanSlope();
}

size_t LumpedReachDecorator::getIndex() const {
	return _decorated_object->getIndex();
}

double LumpedReachDecorator::getSpecificDrainageArea() const {
	return _decorated_object->getSpecificDrainageArea();
}

double LumpedReachDecorator::getTotalDrainageArea() const {
	return _decorated_object->getTotalDrainageArea();
}

const CellInflow& LumpedReachDecorator::getInletBoundaryConditions() const {
	return _decorated_object->getInletBoundaryConditions();
}

const CellInflow& LumpedReachDecorator::getLateralBoundaryConditions() const {
	return _decorated_object->getLateralBoundaryConditions();
}

double LumpedReachDecorator::get(const StreamVariable& var) const {
	return _decorated_object->get(var);
}

double LumpedReachDecorator::get(const StreamVariable& var, const size_t&) const {
	return this->get(var);
}

LumpedReachDecorator::~LumpedReachDecorator() {
	delete _decorated_object;
}


//-----------------------------------------------------------------------------
// Implementation of class LumpedReachWithEB
//-----------------------------------------------------------------------------

const double LumpedReachWithEB::_TEMPERATURE_PRECISION(0.0001); //< [K]


LumpedReachWithEB::LumpedReachWithEB(LumpedReachInterface *decoratedObject,
		const ReachHeatFluxModel& model, std::shared_ptr<DataContainer> dataCont) :
				LumpedReachDecorator(decoratedObject),
				_heat_fluxes(model), dataContainer(dataCont),
				_middle_cell(this->getCell(this->getNumCells()/2)),
				_air_pressure(SFLaws::standardAirPressure(_middle_cell.getAltitude(), false)),
				_temperature(278.15), _meteo(), _display_info(true)
{
}

void LumpedReachWithEB::initialize()
{
	LumpedReachDecorator::initialize();
	dataContainer->getLumpedMeteoData(_clock.getCurrTime(), *this, _meteo);
}

void LumpedReachWithEB::updateLateralBoundaryConditions() {
	LumpedReachDecorator::updateLateralBoundaryConditions();
	dataContainer->getLumpedMeteoData(_clock.getNextTime(), *this, _meteo);
	_meteo(mio::MeteoData::P) = _air_pressure;
}


void LumpedReachWithEB::advanceInTime() {
	LumpedReachDecorator::advanceInTime();


	const double dt(double(_clock.getTimeStep()));
	const mio::Date nextTime(_clock.getNextTime());
	const CellInflow& inletInflow(this->getInletBoundaryConditions());
	const CellInflow& lateralInflow(this->getLateralBoundaryConditions());
	const double width(this->getWidth());
	const double volume(this->get(StreamVariable::H)*width*this->getLength());

	double advFlux(0.), inflowDischarge(0.);
	if(inletInflow.discharge > 0. && inletInflow.temperature != mio::IOUtils::nodata) {
		advFlux         += inletInflow.discharge*inletInflow.temperature;
		inflowDischarge += inletInflow.discharge;
	}
	if(lateralInflow.discharge > 0. && lateralInflow.temperature != mio::IOUtils::nodata) {
		advFlux         += lateralInflow.discharge*lateralInflow.temperature;
		inflowDischarge += lateralInflow.discharge;
	}

	const double friction(_heat_fluxes.getBedFrictionHeatFlux(this->getMeanSlope(),
			this->get(StreamVariable::Q), width));
	const double incomingRad(_heat_fluxes.getAbsorbedLWRadiation(_meteo) +
			_heat_fluxes.getAbsorbedSWRadiation(_middle_cell, nextTime, _meteo));

	const double deltaTemp(fmax(2./3600.*dt, 2*_TEMPERATURE_PRECISION)); //< [K] range around the old temperature value in which the new temperature value should be searched for
	try {
		bool convergence = true;
		_temperature = RootFindingAlgorithms::brentMethod([&](const double& Twater){
			return this->ebResidual(dt, Twater, incomingRad, friction, advFlux,
					inflowDischarge, volume, width);},
				convergence, fmax(_temperature - deltaTemp, 273.15), _temperature + deltaTemp,
				_TEMPERATURE_PRECISION);
		if(_display_info && this->isSet() && !convergence) {
			std::cout << "[w] Maximum number of iterations reached while computing "
								<< "temperature of stream reach in subwatershed " << this->getIndex()
								<< " at date " << _clock.getNextTime().toString(mio::Date::ISO) << ". "
								<< "Proceeding with the simulation but results might be "
								<< "incorrect\n";
		}
	} catch(const std::exception& e) {
		std::ostringstream os;
		os << "Error while computing temperature of stream reach in subwatershed "
		   << this->getIndex() << " at date " << nextTime.toString(mio::Date::ISO)
		   << ": " << e.what();
		throw RuntimeException(os.str(), AT);
	}

	_temperature = fmax(_temperature, MIN_TEMPERATURE);
	if(_temperature > MAX_TEMPERATURE) {
		_temperature = MAX_TEMPERATURE;
		if(_display_info) {
			std::cout << "[w] Temperature of stream reach in subwatershed "
			          << this->getIndex() << " exceeds " << MAX_TEMPERATURE
			          << "K at date " << nextTime.toString(mio::Date::ISO) << "\n";
		}
	}
}


void LumpedReachWithEB::reset() {
	LumpedReachDecorator::reset();
	_temperature = 278.15;
	dataContainer->getLumpedMeteoData(_clock.getCurrTime(), *this, _meteo); //< HACK: requires clock to have been reset before!
}


void LumpedReachWithEB::setDisplayInfo(const bool& display) {
	LumpedReachDecorator::setDisplayInfo(display);
	_display_info = display;
}


double LumpedReachWithEB::get(const StreamVariable& var) const {
	if(var == StreamVariable::T) {
		return _temperature;
	} else if(var == StreamVariable::nISWR) {
		return _heat_fluxes.getAbsorbedSWRadiation(this->getCell(0),
				_clock.getCurrTime(), _meteo);
	} else if(var == StreamVariable::nILWR) {
		return _heat_fluxes.getAbsorbedLWRadiation(_meteo);
	} else if(var == StreamVariable::ELWR) {
		return _heat_fluxes.getEmittedLWRadiation(_temperature);
	} else if(var == StreamVariable::LatentHeatFlux) {
		return _heat_fluxes.getLatentHeatFlux(this->getCell(0), _meteo,
				_temperature);
	} else if(var == StreamVariable::SensibleHeatFlux) {
		return _heat_fluxes.getSensibleHeatFlux(this->getCell(0), _meteo,
				_temperature);
	} else if(var == StreamVariable::BedCondHeatFlux) {
		return _heat_fluxes.getBedConductionHeatFlux(_temperature, _meteo("TSOIL"));
	} else if(var == StreamVariable::BedFricHeatFlux) {
		return _heat_fluxes.getBedFrictionHeatFlux(this->getMeanSlope(),
				this->get(StreamVariable::Q), this->getWidth());
	} else if(var == StreamVariable::InletHeatFlux) {
		return this->getInletBoundaryConditions().getHeat();
	} else if (var == StreamVariable::LateralHeatFlux) {
		return this->getLateralBoundaryConditions().getHeat();
	} else {
		return LumpedReachDecorator::get(var);
	}
}


LumpedReachWithEB::~LumpedReachWithEB() {}


double LumpedReachWithEB::ebResidual(const double& dt, const double& streamTemp,
		const double& incomingRadiation, const double& frictionFlux,
		double advectedFlux, const double& inflowDischarge,	const double& volume,
		const double& width) const {
	const double elwr(_heat_fluxes.getEmittedLWRadiation(streamTemp));
	const double latentFlux(_heat_fluxes.getLatentHeatFlux(_middle_cell, _meteo,
			streamTemp));
	const double sensibleFlux(_heat_fluxes.getSensibleHeatFlux(_middle_cell,
			_meteo, streamTemp));
	const double bedCond(_heat_fluxes.getBedConductionHeatFlux(streamTemp,
			_meteo("TSOIL")));
	const double totHeatFlow((incomingRadiation + elwr + latentFlux + sensibleFlux +
			frictionFlux + bedCond)*width*this->getLength());

	if(inflowDischarge > .0)
		advectedFlux -= inflowDischarge*streamTemp;

	const double rhoCp(SFConstants::WATER_MASS_DENSITY*SFConstants::WATER_HEAT_CAPACITY);
	const double streamTempInc(dt/volume*(advectedFlux + totHeatFlow/rhoCp));
	return streamTemp - _temperature - streamTempInc;
}

} /* namespace sflow */
