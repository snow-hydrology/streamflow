/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <streamflow/streamReach/FrictionModel.h>

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class ManningFrictionModel
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class ManningFrictionModel
 * @param cfg Reference to the Config object holding the simulation parameters
 */
ManningFrictionModel::ManningFrictionModel(const mio::Config& cfg) :
		StreamBedFrictionModel(), _friction_coeff(cfg, "MANNING_COEFF",
				SFConstants::CONFIG_SECTION_NAME, Units::NONE, {0.02, 0.07},
				"Manning's roughness coefficient") {}


ParameterSet ManningFrictionModel::getParams() const {
	ParameterSet output;
	output.add(_friction_coeff);
	return output;
}


void ManningFrictionModel::setParams(const ParameterSet& params) {
	if(_friction_coeff.toBeCalibrated() && params.contains(_friction_coeff.getName()))
		_friction_coeff.setValue(params[_friction_coeff.getName()].getValue());
}


/**
 * @brief Returns the hydraulic head loss according to Manning's formula. The
 * hydraulic head loss corresponds to the slope of the energy line.
 * @param width Stream width in meters
 * @param waterHeight Water depth in meters
 * @param discharge Stream discharge in m^3/s
 * @return Hydraulic head loss (dimensionless)
 */
double ManningFrictionModel::getHydraulicHeadLoss(const double& width, const double& waterHeight,
		const double& discharge) const {
	const double area(waterHeight*width);
	const double coeff(_friction_coeff.getValue()*mio::Optim::fastPow(width, 2./3.)/
			mio::Optim::fastPow(area, 5./3.));
	return mio::Optim::pow2(coeff*discharge);
}


/**
 * @brief Returns the celerity, which corresponds to the speed at which
 * information travels down the stream reach. From a mathematical point of view,
 * this corresponds to the derivative of discharge with respect to the
 * cross-sectional area of the stream reach. Note that celerity is not equal to
 * the water velocity, which is defined as the ratio of discharge over the
 * cross-sectional area of the stream reach.
 *
 * This function approximates the hydraulic head loss with the stream bed slope.
 * @param width Stream width in meters
 * @param slope Stream bed slope (dimensionless)
 * @param discharge Stream discharge in m^3/s
 * @return Celerity in m/s
 */
double ManningFrictionModel::getCelerity(const double& width, const double& slope,
		const double& discharge) const {
	const double coeff2(_friction_coeff.getValue()*_friction_coeff.getValue());
	return 5./3.*mio::Optim::fastPow(slope/coeff2, 3./10.)*
			mio::Optim::fastPow(discharge/width, 2./5.);
}


/**
 * @brief Returns the stream water depth computed from the discharge according
 * to Manning's formula.
 *
 * This function approximates the hydraulic head loss with the stream bed slope.
 * @param width Stream width in meters
 * @param slope Stream bed slope (dimensionless)
 * @param discharge Stream discharge in m^3/s
 * @return Stream water depth in meters
 */
double ManningFrictionModel::getWaterHeight(const double& width, const double& slope,
		const double& discharge) const {
	const double tmp(_friction_coeff.getValue()*discharge/(width*sqrt(slope)));
	return fmax(mio::Optim::fastPow(tmp, 3./5.), StreamReach::MIN_WATER_HEIGHT);
}


/**
 * @brief Destructor of class ManningFrictionModel
 */
ManningFrictionModel::~ManningFrictionModel() {}


//-----------------------------------------------------------------------------
// Implementation of class ManningFrictionModel
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class ChezyFrictionModel
 * @param cfg Reference to the Config object holding the simulation parameters
 */
ChezyFrictionModel::ChezyFrictionModel(const mio::Config& cfg) :
		StreamBedFrictionModel(), _friction_coeff(cfg, "CHEZY_COEFF",
				SFConstants::CONFIG_SECTION_NAME, Units::NONE, {4., 80.},
				"Chezy's roughness coefficient") {}


ParameterSet ChezyFrictionModel::getParams() const {
	ParameterSet output;
	output.add(_friction_coeff);
	return output;
}


void ChezyFrictionModel::setParams(const ParameterSet& params) {
	if(_friction_coeff.toBeCalibrated() && params.contains(_friction_coeff.getName()))
		_friction_coeff.setValue(params[_friction_coeff.getName()].getValue());
}


/**
 * @brief Returns the hydraulic head loss according to Chezy's formula. The
 * hydraulic head loss corresponds to the slope of the energy line.
 * @param width Stream width in meters
 * @param waterHeight Water depth in meters
 * @param discharge Stream discharge in m^3/s
 * @return Hydraulic head loss (dimensionless)
 */
double ChezyFrictionModel::getHydraulicHeadLoss(const double& width, const double& waterHeight,
		const double& discharge) const {
	const double factor(discharge/(width*_friction_coeff.getValue()));
	return mio::Optim::pow2(factor)/mio::Optim::pow3(waterHeight);
}


/**
 * @brief Returns the celerity, which corresponds to the speed at which
 * information travels down the stream reach. From a mathematical point of view,
 * this corresponds to the derivative of discharge with respect to the
 * cross-sectional area of the stream reach. Note that celerity is not equal to
 * the water velocity, which is defined as the ratio of discharge over the
 * cross-sectional area of the stream reach.
 *
 * This function approximates the hydraulic head loss with the stream bed slope.
 * @param width Stream width in meters
 * @param slope Stream bed slope (dimensionless)
 * @param discharge Stream discharge in m^3/s
 * @return Celerity in m/s
 */
double ChezyFrictionModel::getCelerity(const double& width, const double& slope,
		const double& discharge) const {
	const double coeff2(mio::Optim::pow2(_friction_coeff.getValue()));
	return 1.5*cbrt(coeff2*slope*discharge/width);
}


/**
 * @brief Returns the stream water depth computed from the discharge according
 * to Chezy's formula.
 *
 * This function approximates the hydraulic head loss with the stream bed slope.
 * @param width Stream width in meters
 * @param slope Stream bed slope (dimensionless)
 * @param discharge Stream discharge in m^3/s
 * @return Stream water depth in meters
 */
double ChezyFrictionModel::getWaterHeight(const double& width, const double& slope,
		const double& discharge) const {
	const double tmp(discharge/(width*_friction_coeff.getValue()*sqrt(slope)));
	return fmax(pow(tmp, 2./3.), StreamReach::MIN_WATER_HEIGHT);
}


/**
 * @brief Destructor of class ChezyFrictionModel
 */
ChezyFrictionModel::~ChezyFrictionModel() {}

} /* namespace sflow */
