/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REACHHEATFLUXMODEL_H_
#define REACHHEATFLUXMODEL_H_

#include <streamflow/utils/SFUtils.h>
#include <streamflow/calibration/Parameter.h>
#include <streamflow/calibration/ParameterSet.h>
#include <streamflow/streamReach/StreamCell.h>
#include <meteoio/MeteoIO.h>

namespace sflow {

/**
 * @class ReachHeatFluxModel
 * @brief This class is used to compute the heat fluxes of a stream reach. Each
 * method of the class exists in two versions: one for computing the heat fluxes
 * over a vector of stream cells, and one for computing the heat fluxes of a
 * single stream cell.
 *
 * The latent heat flux is computed using a formula based on a wind function.
 * The two parameters of the wind function must be specified in the
 * configuration file using keys WIND_FUNCTION_FACTOR and WIND_FUNCTION_OFFSET.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-11-09
 */
class ReachHeatFluxModel : public SharedObject, public ParametrizableObject {
	public:
		ReachHeatFluxModel(const mio::Config& cfg);
		virtual ParameterSet getParams() const;
		virtual void setParams(const ParameterSet& params);
		virtual double getAbsorbedSWRadiation(const StreamCell& cell,
				const mio::Date& date, const mio::MeteoData& meteo) const;
		virtual double getAbsorbedLWRadiation(const mio::MeteoData& meteo) const;
		virtual double getEmittedLWRadiation(const double& streamTemp) const;
		virtual double getLatentHeatFlux(const StreamCell& cell,
				const mio::MeteoData& meteo, const double& streamTemp) const;
		virtual double getSensibleHeatFlux(const StreamCell& cell,
				const mio::MeteoData& meteo, const double& streamTemp) const;
		virtual double getBedConductionHeatFlux(const double& streamTemp,
				const double& bedTemp) const;
		virtual double getBedFrictionHeatFlux(const double& slope,
				const double& discharge, const double& width) const;
		virtual ~ReachHeatFluxModel();

	protected:
		Parameter _wind_factor;  //< [-]
		Parameter _wind_offset;  //< [m/s]
		Parameter _bed_heat_transfer_coeff; //< [W/(m^2*K)]
};


} /* namespace sflow */

#endif /* _REACHHEATFLUXMODEL_H_ */
