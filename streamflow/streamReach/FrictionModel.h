/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FRICTIONMODEL_H_
#define FRICTIONMODEL_H_

#include <cmath>
#include <streamflow/utils/Units.h>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/calibration/Parameter.h>
#include <streamflow/calibration/ParameterSet.h>
#include <streamflow/streamReach/StreamReach.h>
#include <meteoio/MeteoIO.h>

namespace sflow {

/**
 * @class StreamBedFrictionModel
 * @brief This class defines the interface to be implemented by the classes
 * describing stream bed friction.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-11-24
 */
class StreamBedFrictionModel: public ParametrizableObject, public SharedObject {
	public:
		StreamBedFrictionModel() : ParametrizableObject(), SharedObject() {}
		virtual double getHydraulicHeadLoss(const double& width, const double& waterHeight,
				const double& discharge) const = 0;
		virtual double getCelerity(const double& width, const double& slope,
				const double& discharge) const = 0;
		virtual double getWaterHeight(const double& width, const double& slope,
				const double& discharge) const = 0;
		virtual ~StreamBedFrictionModel() {}
};


/**
 * @class ManningFrictionModel
 * @brief This class implements Manning's friction model (see
 * <a href="https://en.wikipedia.org/wiki/Manning_formula">description</a>).
 * The class assumes a wide rectangular channel, so that the hydraulic radius
 * can be approximated with the water depth.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-11-24
 */
class ManningFrictionModel: public StreamBedFrictionModel {
	public:
		ManningFrictionModel(const mio::Config& cfg);
		virtual ParameterSet getParams() const;
		virtual void setParams(const ParameterSet& params);
		virtual double getHydraulicHeadLoss(const double& width, const double& waterHeight,
				const double& discharge) const;
		virtual double getCelerity(const double& width, const double& slope,
				const double& discharge) const;
		virtual double getWaterHeight(const double& width, const double& slope,
				const double& discharge) const;
		virtual ~ManningFrictionModel();

	protected:
		Parameter _friction_coeff; //< [s/m^(1/3)]
};


/**
 * @class ChezyFrictionModel
 * @brief This class implements Chezy's friction model (see
 * <a href="https://en.wikipedia.org/wiki/Ch%C3%A9zy_formula">description</a>).
 * The class assumes a wide rectangular channel, so that the hydraulic radius
 * can be approximated with the water depth.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-11-24
 */
class ChezyFrictionModel: public StreamBedFrictionModel {
	public:
		ChezyFrictionModel(const mio::Config& cfg);
		virtual ParameterSet getParams() const;
		virtual void setParams(const ParameterSet& params);
		virtual double getHydraulicHeadLoss(const double& width, const double& waterHeight,
				const double& discharge) const;
		virtual double getCelerity(const double& width, const double& slope,
				const double& discharge) const;
		virtual double getWaterHeight(const double& width, const double& slope,
				const double& discharge) const;
		virtual ~ChezyFrictionModel();

	protected:
		Parameter _friction_coeff; //< [m^0.5/s]
};

} /* namespace sflow */

#endif /* FRICTIONMODEL_H_ */
