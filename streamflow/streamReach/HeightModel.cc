/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as 
   published by the Free Software Foundation, either version 3 of the 
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public 
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <streamflow/streamReach/HeightModel.h>

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class DischargePowerHeightModel
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class DischargePowerHeightModel
 * @param cfg Instance of the Config object holding the model parameters
 */
DischargePowerHeightModel::DischargePowerHeightModel(const mio::Config& cfg) : _factor(),
		_power() {
	const std::string& section = SFConstants::CONFIG_SECTION_NAME;
	_factor.set(cfg, "HEIGHT_MODEL_FACTOR", section, Units::NONE, {1e-4, 2},
			"Factor used to compute the stream water height based on discharge");
	_power.set(cfg, "HEIGHT_MODEL_POWER", section, Units::NONE, {0.0, 3},
			"Exponent used to compute the stream water height based on discharge");
}


/**
 * @brief Method returning the stream water height (in m) computed as: a*Q^b,
 * where Q is the stream discharge (in km^2) and a and b are two parameters. This
 * method should be used to compute the water height of lumped stream reaches.
 * @param reach Stream reach  whose water height should be returned
 * @return Water height in meters
 */
double DischargePowerHeightModel::getHeight(const LumpedReachInterface& reach) {
	return fmax(_factor.getValue()*mio::Optim::fastPow(reach.get(StreamVariable::Q),
			_power.getValue()), StreamReach::MIN_WATER_HEIGHT);
}


/**
 * @brief Method returning the stream water height (in m) computed as: a*Q^b,
 * where Q is the stream discharge (in km^2) and a and b are two parameters. This
 * method should be used to compute the water height of discretized stream
 * reaches.
 * @param[in] reach Stream reach whose water height should be returned
 * @param[out] height Value of water height (in meters) in each reach cell
 */
void DischargePowerHeightModel::getHeight(const DiscretizedReachInterface& reach,
		std::vector<double>& height) {
	const std::vector<double> discharge(reach.get(StreamVariable::Q));
	const size_t nCells(reach.getNumCells());
	height.resize(nCells);
	for(size_t iCell(0); iCell < nCells; ++iCell) {
		height[iCell] = fmax(_factor.getValue()*mio::Optim::fastPow(discharge[iCell],
				_power.getValue()), StreamReach::MIN_WATER_HEIGHT);
	}
}


/**
 * @brief Method returning the model parameters
 * @return Model parameters
 */
ParameterSet DischargePowerHeightModel::getParams() const {
	ParameterSet params;
	params.add(_factor);
	params.add(_power);
	return params;
}


/**
 * @brief Method setting the model parameters. Not all parameters need to be
 * specified, unknown parameters will be ignored.
 * @param params Set containing the new values of the model parameters
 */
void DischargePowerHeightModel::setParams(const ParameterSet& params) {
	if(_factor.toBeCalibrated() && params.contains(_factor.getName()))
		_factor.setValue(params[_factor.getName()].getValue());

	if(_power.toBeCalibrated() && params.contains(_power.getName()))
		_power.setValue(params[_power.getName()].getValue());
}


/**
 * @brief Destructor of class DischargePowerHeightModel
 */
DischargePowerHeightModel::~DischargePowerHeightModel() {}


//-----------------------------------------------------------------------------
// Implementation of class PrescribedVelocityModel
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class PrescribedVelocityModel
 * @param cfg Instance of the Config object holding the model parameters
 */
PrescribedVelocityModel::PrescribedVelocityModel(const mio::Config& cfg) :
		_velocity(cfg, "HEIGHT_MODEL_VELOCITY", SFConstants::CONFIG_SECTION_NAME,
				Units::m/Units::s, {1e-4, 2}, "Prescribed flow velocity used to "
				"compute the stream water height based on discharge and stream "
				"width") {}


/**
 * @brief Method returning the stream water height (in m) computed as: Q/(w*v),
 * where Q is the stream discharge (in km^2), w is the stream width (in m) and
 * v is the flow velocity (in m/s). This method should be used to compute the
 * water height of lumepd stream reaches.
 * @param reach Stream reach  whose water height should be returned
 * @return Water height in meters
 */
double PrescribedVelocityModel::getHeight(const LumpedReachInterface& reach) {
	const double h(reach.get(StreamVariable::Q)/(reach.getWidth()*_velocity.getValue()));
	return fmax(h, StreamReach::MIN_WATER_HEIGHT);
}


/**
 * @brief Method returning the stream water height (in m) computed as: Q/(w*v),
 * where Q is the stream discharge (in km^2), w is the stream width (in m) and
 * v is the flow velocity (in m/s). This method should be used to compute the
 * water height of discretized stream reaches.
 * @param[in] reach Stream reach  whose water height should be returned
 * @param[out] height Value of water height (in meters) in each reach cell
 */
void PrescribedVelocityModel::getHeight(const DiscretizedReachInterface& reach,
		std::vector<double>& height) {
	const std::vector<double> discharge(reach.get(StreamVariable::Q));
	const size_t nCells(reach.getNumCells());
	const double width(reach.getWidth());
	height.resize(nCells);
	for(size_t iCell(0); iCell < nCells; ++iCell) {
		height[iCell] = fmax(discharge[iCell]/(width*_velocity.getValue()),
				StreamReach::MIN_WATER_HEIGHT);
	}
}


/**
 * @brief Method returning the model parameters
 * @return Model parameters
 */
ParameterSet PrescribedVelocityModel::getParams() const {
	ParameterSet params;
	params.add(_velocity);
	return params;
}


/**
 * @brief Method setting the model parameters. Not all parameters need to be
 * specified, unknown parameters will be ignored.
 * @param params Set containing the new values of the model parameters
 */
void PrescribedVelocityModel::setParams(const ParameterSet& params) {
	if(_velocity.toBeCalibrated() && params.contains(_velocity.getName()))
		_velocity.setValue(params[_velocity.getName()].getValue(_velocity.getUnits()));
}


/**
 * @brief Destructor of class PrescribedVelocityModel
 */
PrescribedVelocityModel::~PrescribedVelocityModel() {}

} /* namespace sflow */

