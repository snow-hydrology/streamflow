/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/streamReach/ReachHeatFluxModel.h>

namespace sflow {

/**
 * @brief Constructor of class ReachHeatFluxModel
 * @param cfg Reference to the config object holding the model parameters
 */
ReachHeatFluxModel::ReachHeatFluxModel(const mio::Config& cfg) :
		SharedObject(), ParametrizableObject(), _wind_factor(), _wind_offset(),
		_bed_heat_transfer_coeff()
{
	const std::string& section = SFConstants::CONFIG_SECTION_NAME;
	_wind_factor.set(cfg, "WIND_FUNCTION_FACTOR", section, Units::NONE,
			{1e-3, 3e-3}, "Factor of the wind function used to compute the latent "
			"heat flux");
	_wind_offset.set(cfg, "WIND_FUNCTION_OFFSET", section, Units::m/Units::s,
			{1e-3, 3e-3}, "Offset of the wind function used to compute the latent "
			"heat flux");
	_bed_heat_transfer_coeff.set(cfg, "BED_HEAT_TRANSFER_COEFF", section,
			Units::W/(Units::m.pow(2)*Units::degK), {0., 50.},
			"Heat transfer coefficient between water and the stream bed");
}


ParameterSet ReachHeatFluxModel::getParams() const {
	ParameterSet params;
	params.add(_wind_factor);
	params.add(_wind_offset);
	params.add(_bed_heat_transfer_coeff);
	return params;
}


void ReachHeatFluxModel::setParams(const ParameterSet& params) {
	std::string paramName(_wind_factor.getName());
	if(_wind_factor.toBeCalibrated() && params.contains(paramName))
		_wind_factor.setValue(params[paramName].getValue(_wind_factor.getUnits()));

	paramName = _wind_offset.getName();
	if(_wind_offset.toBeCalibrated() && params.contains(paramName))
		_wind_offset.setValue(params[paramName].getValue());

	paramName = _bed_heat_transfer_coeff.getName();
	if(_bed_heat_transfer_coeff.toBeCalibrated() && params.contains(paramName))
		_bed_heat_transfer_coeff.setValue(params[paramName].getValue(_bed_heat_transfer_coeff.getUnits()));
}


/**
 * @brief Returns the net short-wave radiation absorbed by the stream
 * @param cell Stream cell at which the net short-wave radiation should be
 * returned
 * @param date Date at which the net short-wave radiation should be returned
 * (used to compute the water albedo)
 * @param meteo Object containing the values of the meteo variables over the
 * stream cell of interest
 * @return Net short-wave radiation in W/m^2
 */
double ReachHeatFluxModel::getAbsorbedSWRadiation(const StreamCell& cell,
		const mio::Date& date, const mio::MeteoData& meteo) const {
	const double cosTheta(SFLaws::cosSolarZenithAngle(cell.coords, date));
	return (1. - SFLaws::waterAlbedo(cosTheta))*meteo(mio::MeteoData::ISWR);
}


/**
 * @brief Returns the atmospheric long wave radiation absorbed by the stream
 * @param meteo Object containing the values of the meteo variables over the
 * stream cell of interest
 * @return Absorbed atmospheric long-wave radiation in W/m^2
 */
double ReachHeatFluxModel::getAbsorbedLWRadiation(const mio::MeteoData& meteo) const {
	return SFConstants::WATER_EMISSIVITY*meteo(mio::MeteoData::ILWR);
}


/**
 * @brief Returns the long wave radiation emitted by the stream. The returned
 * value is negative, since it represents a heat loss for the stream.
 * @param streamTemp Stream temperature in K
 * @return Emitted long-wave radiation in W/m^2
 */
double ReachHeatFluxModel::getEmittedLWRadiation(const double& streamTemp) const {
	const double T4(mio::Optim::pow4(streamTemp));
	return -SFConstants::WATER_EMISSIVITY*SFConstants::STEFAN_BOLTZMANN*T4;
}


/**
 * @brief Returns the latent heat flux over the given cell. A positive value
 * indicates that the heat flux flows into the stream (energy gain) while a
 * negative value indicates that the heat flux leaves the stream (energy loss).
 * The latent heat flux is computed according to the Penman-Montheith equation
 * (see Beven, K., Rainfall-runoff modeling: the primer, 2nd ed., Wiley & Sons).
 * @param cell Stream cell at which the latent heat flux should be computed
 * @param meteo Object containing the values of the meteo variables over the
 * stream cell of interest
 * @param streamTemp Stream temperature in the reach cell (in K).
 * @return Latent heat flux over the given cell in W/m^2
 */
double ReachHeatFluxModel::getLatentHeatFlux(const StreamCell& cell,
		const mio::MeteoData& meteo, const double& streamTemp) const {
	const double& Tair(meteo(mio::MeteoData::TA));
	double Pair(meteo(mio::MeteoData::P));
	if(Pair == mio::IOUtils::nodata)
		Pair = SFLaws::standardAirPressure(cell.getAltitude());

	const double gamma(SFLaws::psychrometricConstant(Pair));
	const double es_a(SFLaws::waterVaporPressure(Tair)*meteo(mio::MeteoData::RH));
	const double es_w(SFLaws::waterVaporPressure(streamTemp)); //< saturation vapor pressure at the water temperature
	const double windFun(_wind_factor.getValue()*meteo(mio::MeteoData::VW) +
			_wind_offset.getValue());
	const double rhoCp(SFConstants::AIR_HEAT_CAPACITY*SFLaws::airMassDensity(Tair, Pair)); //< [J/(K*m^3)]
	return -rhoCp*windFun*(es_w - es_a)/gamma;
}


/**
 * @brief Returns the sensible heat flux over the given cell. A positive value
 * indicates that the heat flux flows into the stream (energy gain) while a
 * negative value indicates that the heat flux leaves the stream (energy loss).
 * The sensible heat flux is computed according to the Penman-Montheith equation
 * (see Beven, K., Rainfall-runoff modeling: the primer, 2nd ed., Wiley & Sons).
 * @param cell Stream cell at which the latent heat flux should be computed
 * @param meteo Object containing the values of the meteo variables over the
 * stream cell of interest
 * @param streamTemp Stream temperature in the reach cell (in K).
 * @return Sensible heat flux over the given cell in W/m^2
 */
double ReachHeatFluxModel::getSensibleHeatFlux(const StreamCell& cell,
		const mio::MeteoData& meteo, const double& streamTemp) const {
	const double& Tair(meteo(mio::MeteoData::TA));
	double Pair(meteo(mio::MeteoData::P));
	if(Pair == mio::IOUtils::nodata)
		Pair = SFLaws::standardAirPressure(cell.getAltitude());

	const double windFun(_wind_factor.getValue()*meteo(mio::MeteoData::VW) +
			_wind_offset.getValue());
	const double rhoCp(SFConstants::AIR_HEAT_CAPACITY*SFLaws::airMassDensity(Tair, Pair)); //< [J/(K*m^3)]
	return -rhoCp*windFun*(streamTemp - Tair);
}


/**
 * @brief Returns the conduction heat flux between water and the stream bed. A
 * positive value indicates that the heat flux enters the stream (energy gain)
 * while a negative value indicates that the heat flux leaves the stream (energy
 * loss).
 * @param streamTemp Stream temperature in K
 * @param bedTemp Stream bed temperature in K
 * @return Conduction heat flux at the stream bed in W/m^2
 */
double ReachHeatFluxModel::getBedConductionHeatFlux(const double& streamTemp,
		const double& bedTemp) const {
	return _bed_heat_transfer_coeff.getValue()*(bedTemp - streamTemp);
}


/**
 * @brief Returns the heat flux generated by the friction of water over the
 * stream bed. The friction heat flux is computed by assuming that the
 * acceleration of water due to gravity is entirely compensated by the friction
 * over the stream bed (in other words, it is assumed that the water head loss
 * equals the local slope).
 * @param slope Tangent of the bed slope (dimensionless)
 * @param discharge Stream discharge in m^3/s
 * @param width Stream width in m
 * @return Friction heat flux in W/m^2
 */
double ReachHeatFluxModel::getBedFrictionHeatFlux(const double& slope,
		const double& discharge, const double& width) const {
	return SFConstants::WATER_MASS_DENSITY*SFConstants::GRAVIT_ACCELERATION*
			fabs(slope)*discharge/width;
}


/**
 * @brief Destructor of class ReachHeatFluxModel
 */
ReachHeatFluxModel::~ReachHeatFluxModel() {}

} /* namespace sflow */
