/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/streamReach/StreamCell.h>

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class StreamCell
//-----------------------------------------------------------------------------

/**
 * @brief Default constructor of class StreamCell
 */
StreamCell::StreamCell() : coords(), spec_drainage_area(0.0),
		tot_drainage_area(0.0), length(0.0), slope(0.0) {}

/**
 * @brief Constructor of class StreamCell
 * @param in_coords Coordinates of the cell's middle point
 * @param in_specDrainageArea Specific area drained by the cell in [m^2]
 * @param in_totDrainageArea Total area drained by the cell in [m^2]
 * @param in_length Length of the stream segment symbolized by the cell in [m]
 * @param in_slope Cell slope (tangent of the cell slope angle)
 */
StreamCell::StreamCell(const mio::Coords& in_coords,  const mio::DEMObject& dem, const double& in_specDrainageArea,
		const double& in_totDrainageArea, const double& in_length, const double& in_slope) :
				coords(in_coords), spec_drainage_area(in_specDrainageArea),
				tot_drainage_area(in_totDrainageArea), length(in_length),
				slope(in_slope) {
					dem.gridify(coords);
					}

/**
 * @brief Sets the coordinates of the cell's center point
 * @param in_coords New coordinates of the cell's center point
 */
void StreamCell::setCoords(const mio::Coords& in_coords) {
	coords = in_coords;
}

/**
 * @brief Sets the total area drained by the cell
 * @param area New value of the total area drained by the cell in [m^2]
 */
void StreamCell::setTotalDrainageArea(const double& area) {
	tot_drainage_area = area;
}

/**
 * @brief Sets the specific area drained by the cell
 * @param area New value of the specific area drained by the cell in [m^2]
 */
void StreamCell::setSpecificDrainageArea(const double& area) {
	spec_drainage_area = area;
}

/**
 * @brief Sets the length of the stream segment symbolized by the cell
 * @param in_length New value of the cell length in [m]
 */
void StreamCell::setLength(const double& in_length) {
	length = in_length;
}

/**
 * @brief Sets the slope of the stream segment symbolized by the cell
 * @param in_slope New value of the cell slope in [-]
 */
void StreamCell::setSlope(const double& in_slope) {
	slope = in_slope;
}

/**
 * @brief Sets all cell attributes in one go
 * @param in_coords New coordinates of the cell's center point
 * @param in_specDrainageArea New value of the specific area drained by the
 * cell in [m^2]
 * @param in_totDrainageArea New value of the total area drained by the cell
 * in [m^2]
 * @param in_length New value of the cell length in [m]
 * @param in_slope New value of the cell slope in [-]
 */
void StreamCell::set(const mio::Coords& in_coords,  const mio::DEMObject& dem, const double& in_specDrainageArea,
		const double& in_totDrainageArea, const double& in_length, const double& in_slope) {
	coords = in_coords;
	dem.gridify(coords);
	spec_drainage_area = in_specDrainageArea;
	tot_drainage_area  = in_totDrainageArea;
	length = in_length;
	slope  = in_slope;
}

/**
 * @brief Returns the coordinates of the cell's middle point
 * @return Middle point coordinates
 */
const mio::Coords& StreamCell::getCoords() const {
	return coords;
}

/**
 * @brief Returns the cell altitude above sea level in [m]
 * @return Cell altitude
 */
double StreamCell::getAltitude() const {
	return coords.getAltitude();
}

/**
 * @brief Returns the East coordinate of the cell's middle point in the
 * configured projection system in [m]
 * @return Easting
 */
double StreamCell::getEasting() const {
	return coords.getEasting();
}

/**
 * @brief Returns the North coordinate of the cell's middle point in the
 * configured projection system in [m]
 * @return Northing
 */
double StreamCell::getNorthing() const {
	return coords.getNorthing();
}

/**
* @brief Returns the Latitude of the cell's middle point in the configured
* projection system
* @return Latitude
*/
double StreamCell::getLat() const {
	return coords.getLat();
}

/**
* @brief Returns the Longitude of the cell's middle point in the configured
* projection system
* @return Longitude
 */
double StreamCell::getLon() const {
	return coords.getLon();
}

/**
 * @brief Returns the total area drained by the cell in [m^2]
 * @return Total area drained by the point
 */
const double& StreamCell::getTotalDrainageArea() const {
	return tot_drainage_area;
}

/**
 * @brief Returns the specific area drained by the cell in [m^2]
 * @return Specific area drained by the point
 */
const double& StreamCell::getSpecificDrainageArea() const {
	return spec_drainage_area;
}

/**
 * @brief Returns the length of the stream segment symbolized by the cell in [m]
 * @return Cell length
 */
const double& StreamCell::getLength() const {
	return length;
}

/**
 * @brief Returns the slope of the stream cell (i.e. the tangent of the cell
 * slope angle)
 * @return Cell slope
 */
const double& StreamCell::getSlope() const {
	return slope;
}

/**
 * @brief Method used to convert the object to a string.
 * @return String
 */
const std::string StreamCell::toString() const {
	std::ostringstream os;
	os << "<stream cell>\n";
	os << "coordinates:\t"  << this->getLat() << "N, " << coords.getLon() << "E\n";
	os << "altitude asl:\t" << this->getAltitude() << " m\n";
	os << "length:\t\t" << length << " m\n";
	os << "slope:\t\t" << slope << "\n";
	os << "spec. drainage area:\t" << spec_drainage_area << " m^2\n";
	os << "tot. drainage area:\t" << tot_drainage_area << " m^2\n";
	os << "</stream cell>\n";
	return os.str();
}

std::ostream& operator<<(std::ostream& os, const StreamCell& p) {
	os << p.toString();
	return os;
}


//-----------------------------------------------------------------------------
// Implementation of class CellLateralInflow
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class CellLateralInflow
 */
CellInflow::CellInflow() : discharge(0.0),
		temperature(mio::IOUtils::nodata) {}

/**
 * @brief Returns the lateral discharge inflow into the stream cell
 * @return Lateral discharge inflow in [m^3/s]
 */
const double& CellInflow::getDischarge() const {
	return discharge;
}

/**
 * @brief Returns the temperature of the lateral inflow into the stream cell
 * @return Lateral inflow temperature in [K]
 */
const double& CellInflow::getTemperature() const {
	return temperature;
}

/**
 * @brief Returns the lateral heat inflow into the stream cell
 * @return Lateral heat inflow in [W]
 */
double CellInflow::getHeat() const {
	return SFConstants::WATER_MASS_DENSITY*SFConstants::WATER_HEAT_CAPACITY*
			discharge*temperature;
}

/**
 * @brief Sets the lateral discharge inflow into the stream cell
 * @param inflow New value of the lateral discharge inflow in [m^3/s]
 */
void CellInflow::setDischarge(const double& inflow) {
	discharge = inflow;
}

/**
 * @brief Sets the temperature of the lateral inflow into the stream cell
 * @param temp New value of the lateral inflow temperature in [K]
 */
void CellInflow::setTemperature(const double& temp) {
	temperature = temp;
}


/**
 * @brief Adds the respective discharge and temperature values of the input
 * object to their associated counterparts of the current object.
 * @param other Instance whose values of discharge and temperature should be
 * added to those of the current object
 * @return Reference to the modified current object.
 */
CellInflow& CellInflow::operator+=(const CellInflow& other) {
	discharge   += other.discharge;
	temperature += other.temperature;
	return *this;
}


/**
 * @brief Subtracts the respective discharge and temperature values of the input
 * object from their associated counterparts of the current object.
 * @param other Instance whose values of discharge and temperature should be
 * subtracted from those of the current object
 * @return Reference to the modified current object.
 */
CellInflow& CellInflow::operator-=(const CellInflow& other) {
	discharge   -= other.discharge;
	temperature -= other.temperature;
	return *this;
}


/**
 * @brief Multiplies the discharge and temperature values of the current object
 * with the number specified as input argument.
 * @param num Number to multiply the discharge and temperature values of the
 * current object with
 * @return Reference to the modified current object.
 */
CellInflow& CellInflow::operator*=(const double& num) {
	discharge   *= num;
	temperature *= num;
	return *this;
}


/**
 * @brief Divides the discharge and temperature values of the current object
 * with the number specified as input argument.
 * @param num Number to divide the discharge and temperature values of the
 * current object with
 * @return Reference to the modified current object.
 */
CellInflow& CellInflow::operator/=(const double& num) {
	discharge   /= num;
	temperature /= num;
	return *this;
}


/**
 * @brief Adds the values of discharge and temperature of two CellInflow objects
 * @param a First CellInflow object
 * @param b Second CellInflow object
 * @return CellInflow instance whose values of discharge and temperature
 * correspond to the sums of the respective values contained in the input objects
 */
CellInflow operator+(CellInflow a, const CellInflow& b) {
	a += b;
	return a;
}


/**
 * @brief Subtracts the values of discharge and temperature of the second object
 * from those of the first object
 * @param a CellInflow object from which the discharge and temperature values
 * of the second input argument should be subtracted
 * @param b CellInflow object whose discharge and temperature values should be
 * subtracted from those of the first object
 * @return a - b
 */
CellInflow operator-(CellInflow a, const CellInflow& b) {
	a -= b;
	return a;
}


/**
 * @brief Multiplies the values of discharge and temperature of the CellInflow
 * instance with the number passed as second input argument
 * @param a CellInflow object whose discharge and temperature values should be
 * multiplied by the second input argument
 * @param num Number to multiply the discharge and temperature values of the
 * first input argument with
 * @return a*num
 */
CellInflow operator*(CellInflow a, const double& num) {
	a *= num;
	return a;
}


/**
 * @brief Divides the values of discharge and temperature of the CellInflow
 * instance with the number passed as second input argument
 * @param a CellInflow object whose discharge and temperature values should be
 * divided by the second input argument
 * @param num Number to divide the discharge and temperature values of the
 * first input argument with
 * @return a/num
 */
CellInflow operator/(CellInflow a, const double& num) {
	a /= num;
	return a;
}


/**
 * @brief Method used to convert the object to a string.
 * @return String
 */
const std::string CellInflow::toString() const {
	std::ostringstream os;
	os << "<lateral inflow>\n";
	os << "discharge:\t" << discharge << " m^3/s\n";
	os << "temperature:\t" << temperature << " K\n";
	os << "heat:\t" << this->getHeat() << " W\n";
	os << "</lateral inflow>\n";
	return os.str();
}

std::ostream& operator<<(std::ostream& os, const CellInflow& s) {
	os << s.toString();
	return os;
}



//-----------------------------------------------------------------------------
// Implementation of class CellIndex
//-----------------------------------------------------------------------------

/**
 * @brief Default constructor of class CellIndex.
 */
CellIndex::CellIndex() : x(), y() {}


/**
 * @brief Constructor of class CellIndex initializing the cell indices
 * @param ix x-index
 * @param iy y-index
 */
CellIndex::CellIndex(const size_t& ix, const size_t& iy) : x(ix), y(iy) {}


/**
 * @brief Sets the cell indices to those of the point passed as input argument.
 * An error is thrown if the point indices are not valid.
 * @param pt Point whose indices should be copied in the current CellIndex
 * instance
 */
void CellIndex::set(const mio::Coords& pt) {
	int ix(pt.getGridI()), iy(pt.getGridJ());
	if(ix < 0 || iy < 0)
		throw InvalidIndexException("Point indices cannot be negative", AT);

	x = size_t(ix);
	y = size_t(iy);
}


/**
 * @brief Sets the cell indices to those of the point passed as input argument.
 * The point indices are computed with respect to the grid whose cell size and
 * lower-left corner position are passed as input arguments.
 * @param pt Point whose indices should be copied in the current CellIndex
 * instance
 * @param llCorner Lower-left corner of the grid with respect to which the point
 * indices should be determined
 * @param cellSize Size of the cells of the grid with respect to which the point
 * indices should be determined
 */
void CellIndex::set(const mio::Coords& pt, const mio::Coords& llCorner,
		const double& cellSize) {
	const double xShift = pt.getEasting()  - llCorner.getEasting();
	const double yShift = pt.getNorthing() - llCorner.getNorthing();
	if(xShift < 0. || yShift < 0.) {
		std::ostringstream os;
		os << "Point (" << pt.getEasting() << ", " << pt.getNorthing() << ") "
		   << "is located outside of the DEM that was used in the Alpine3D "
		   << "simulation";
		throw mio::InvalidArgumentException(os.str(), AT);
	}

	x = size_t(floor(xShift/cellSize));
	y = size_t(floor(yShift/cellSize));
}


/**
 * @brief Sets the cell indices based on the string passed as input argument.
 * The string should be formatted according to the following pattern:
 *     [any alphabetic character(s)]ix_iy[_ or . followed by any character(s)]
 * for example:
 *     "13_45"                    (valid example)
 *     "13_45_dischma"            (valid example)
 *     "dischma_13_45.smet"       (valid example)
 *     "dischma_13_45_meteo.smet" (valid example)
 *     "1_dischma_13_45"          (invalid example because numeric character
 *                                 precedes the cell indices)
 * @param str String from which the cell indices should be extracted.
 */
void CellIndex::set(const std::string& str) {
	const size_t begPos = str.find_first_of("0123456789");
	const size_t sepPos = str.find("_", begPos);
	const size_t endPos = std::min(str.find_first_of("_.", sepPos+1), str.size());

	if( begPos == std::string::npos || sepPos == std::string::npos ||
	    !mio::IOUtils::convertString(x, str.substr(begPos, sepPos-begPos)) ||
	    !mio::IOUtils::convertString(y, str.substr(sepPos+1, endPos-sepPos-1)))
		throw mio::InvalidArgumentException("Cannot extract the x and y indices "
				"of the cell from string \"" + str + "\"", AT);
}


/**
 * @brief Sets the cell indices
 * @param ix x-index
 * @param iy y-index
 */
void CellIndex::set(const size_t& ix, const size_t& iy) {
	x = ix;
	y = iy;
}


/**
 * @brief Overload of operator == for objects of type CellIndex
 * @param a First object to compare
 * @param b Second object to compare with the first one
 * @return Are the two sets of indices equal?
 */
bool operator==(const CellIndex& a, const CellIndex& b) {
	return (a.x == b.x) && (a.y == b.y);
}


/**
 * @brief Overload of operator < for objects of type CellIndex. This operator
 * does admittedly not make a lot of sense, but allows CellIndex objects to be
 * used as map keys.
 * @param a First object to compare
 * @param b Second object to compare with the first one
 * @return Is first object smaller than the second one?
 */
bool operator<(const CellIndex& a, const CellIndex& b) {
	return (a.x < b.x) || (a.x == b.x && a.y < b.y);
}
}
