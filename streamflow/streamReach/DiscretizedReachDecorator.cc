/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/streamReach/DiscretizedReachDecorator.h>

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class DiscretizedReachDecorator
//-----------------------------------------------------------------------------

const double DiscretizedReachDecorator::MIN_TEMPERATURE(273.15); //< [K]
const double DiscretizedReachDecorator::MAX_TEMPERATURE(308.15); //< [K]

/**
 * @brief Constructor of class DiscretizedReachDecorator
 * @param decoratedObject Pointer to the object to be decorated
 */
DiscretizedReachDecorator::DiscretizedReachDecorator(DiscretizedReachInterface *decoratedObject) :
				DiscretizedReachInterface(decoratedObject->getClock()),
				_decorated_object(decoratedObject) {}

std::vector<StreamCell>::const_iterator DiscretizedReachDecorator::begin() const {
	return _decorated_object->begin();
}

std::vector<StreamCell>::const_iterator DiscretizedReachDecorator::end() const {
	return _decorated_object->end();
}

bool DiscretizedReachDecorator::isHeadReach() const {
	return _decorated_object->isHeadReach();
}

bool DiscretizedReachDecorator::isSet() const {
	return _decorated_object->isSet();
}

ParameterSet DiscretizedReachDecorator::getParams() const {
	return _decorated_object->getParams();
}

void DiscretizedReachDecorator::setParams(const ParameterSet& params) {
	_decorated_object->setParams(params);
}

void DiscretizedReachDecorator::advanceInTime() {
	_decorated_object->advanceInTime();
}

void DiscretizedReachDecorator::reset() {
	_decorated_object->reset();
}

void DiscretizedReachDecorator::setDisplayInfo(const bool& display) {
	_decorated_object->setDisplayInfo(display);
}

void DiscretizedReachDecorator::setUpstreamReach(const StreamReach& reach) {
	_decorated_object->setUpstreamReach(reach);
}

void DiscretizedReachDecorator::setUpstreamReaches(const StreamReach& reach1,
		const StreamReach& reach2) {
	_decorated_object->setUpstreamReaches(reach1, reach2);
}

void DiscretizedReachDecorator::resetUpstreamReaches() {
	_decorated_object->resetUpstreamReaches();
}

void DiscretizedReachDecorator::setSubwatershed(const Subwatershed& subwatershed) {
	_decorated_object->setSubwatershed(subwatershed);
}

void DiscretizedReachDecorator::updateInletBoundaryConditions() {
	_decorated_object->updateInletBoundaryConditions();
}

void DiscretizedReachDecorator::updateLateralBoundaryConditions() {
	_decorated_object->updateLateralBoundaryConditions();
}

size_t DiscretizedReachDecorator::getNumCells() const {
	return _decorated_object->getNumCells();
}

const std::vector<StreamCell>& DiscretizedReachDecorator::getCells() const {
	return _decorated_object->getCells();
}

const StreamCell& DiscretizedReachDecorator::getCell(const size_t& idx) const {
	return _decorated_object->getCell(idx);
}

unsigned int DiscretizedReachDecorator::getStrahlerOrder() const {
	return _decorated_object->getStrahlerOrder();
}

const Subwatershed& DiscretizedReachDecorator::getSubwatershed() const {
	return _decorated_object->getSubwatershed();
}

void DiscretizedReachDecorator::initialize() {
	_decorated_object->initialize();
}


double DiscretizedReachDecorator::getLength() const {
	return _decorated_object->getLength();
}

double DiscretizedReachDecorator::getWidth() const {
	return _decorated_object->getWidth();
}

double DiscretizedReachDecorator::getMeanSlope() const {
	return _decorated_object->getMeanSlope();
}

size_t DiscretizedReachDecorator::getIndex() const {
	return _decorated_object->getIndex();
}

double DiscretizedReachDecorator::getSpecificDrainageArea() const {
	return _decorated_object->getSpecificDrainageArea();
}

double DiscretizedReachDecorator::getTotalDrainageArea() const {
	return _decorated_object->getTotalDrainageArea();
}

const CellInflow& DiscretizedReachDecorator::getInletBoundaryConditions() const {
	return _decorated_object->getInletBoundaryConditions();
}

const std::vector<CellInflow>& DiscretizedReachDecorator::getLateralBoundaryConditions() const {
	return _decorated_object->getLateralBoundaryConditions();
}

std::vector<double> DiscretizedReachDecorator::get(const StreamVariable& var) const {
	return _decorated_object->get(var);
}

double DiscretizedReachDecorator::get(const StreamVariable& var, const size_t& iCell) const {
	return _decorated_object->get(var, iCell);
}

DiscretizedReachDecorator::~DiscretizedReachDecorator() {
	delete _decorated_object;
}


//-----------------------------------------------------------------------------
// Implementation of class DiscretizedReachWithEB
//-----------------------------------------------------------------------------

const double DiscretizedReachWithEB::_COURANT_NUMBER(0.9);           //< [-]
const double DiscretizedReachWithEB::_TEMPERATURE_PRECISION(0.0001); //< [K]


DiscretizedReachWithEB::DiscretizedReachWithEB(DiscretizedReachInterface *decoratedObject,
		const ReachHeatFluxModel& model,
		const StreamBedFrictionModel& fricModel, Clock& clk,  std::shared_ptr<DataContainer> dataCont) :
				DiscretizedReachDecorator(decoratedObject), TimeStepLimiter(clk),
				_heat_fluxes(model), dataContainer(dataCont),
				_friction_model(fricModel), _meteo(),
				_temperature(decoratedObject->getNumCells(), 278.15),
				_air_pressure(this->computeStandardAirPressure()),
				_velocity(decoratedObject->getNumCells()),
				_inlet_temperature_old(278.15), _inlet_temperature_inc(0.),
				_slope_limiters(this->createSlopeLimiters()),
				_display_info(true), _display_max_iter_warning(true),
				_display_max_temp_warning(true)
{

}

void DiscretizedReachWithEB::initialize()
{
	DiscretizedReachDecorator::initialize();
	dataContainer->initializeMeteo(*this, _meteo);
	dataContainer->getMeteoData(_clock.getCurrTime(), *this, _meteo);
	this->updateFlowVelocity(this->get(StreamVariable::Q), this->get(StreamVariable::H));
}


int DiscretizedReachWithEB::getRequiredMaximumTimeStep() const {
	double dt(86400.); //< 1 day in [s]
	for(size_t iCell(0); iCell < _velocity.size(); ++iCell)
		dt = fmin(this->getCell(iCell).length/_velocity[iCell]*_COURANT_NUMBER, dt);

	if(_display_info && dt < 1. && this->isSet()) {
		std::cout << "[w] Time step needs to be smaller than 1 second in order "
				<< "to compute stream temperature in reach " << this->getIndex()
				<< " at date " << _clock.getCurrTime().toString(mio::Date::ISO)
				<< ". Proceeding with the simulation, but results might be "
				<< "incorrect.\n";
		dt = 1.;
	}

  return fmax(1,floor(dt + 0.1));
}


void DiscretizedReachWithEB::updateInletBoundaryConditions() {
	_inlet_temperature_old = this->getInletBoundaryConditions().temperature;

	DiscretizedReachDecorator::updateInletBoundaryConditions();

	const double newInletTemp(this->getInletBoundaryConditions().temperature);
	_inlet_temperature_inc = newInletTemp - _inlet_temperature_old;
}


void DiscretizedReachWithEB::advanceInTime() {
	_display_max_iter_warning = true;
	_display_max_temp_warning = true;

	DiscretizedReachDecorator::advanceInTime();

	const std::vector<double> newDischarge(this->get(StreamVariable::Q));
	const std::vector<double> newWaterHeight(this->get(StreamVariable::H));

	//1. Advect temperature as if no heat source was present (= pure advection)
	const double stepDuration(_clock.getTimeStep());
	this->advectTemperature(stepDuration);

	//2. Account for the heat sources as if water was static
	dataContainer->getMeteoData(_clock.getNextTime(), *this, _meteo);
	this->accountForHeatSources(stepDuration, newDischarge, newWaterHeight);

	//3. Update flow velocity for function getRequiredMaximumTimeStep()
	this->updateFlowVelocity(newDischarge, newWaterHeight);

}


void DiscretizedReachWithEB::reset() {
	DiscretizedReachDecorator::reset();

	_temperature.assign(this->getNumCells(), 278.15);
	dataContainer->getMeteoData(_clock.getCurrTime(), *this, _meteo); //< HACK: requires Clock to have been reset previously!
	this->updateFlowVelocity(this->get(StreamVariable::Q), this->get(StreamVariable::H));
	_inlet_temperature_old = 278.15;
	_inlet_temperature_inc = 0.;
}


double DiscretizedReachWithEB::get(const StreamVariable& var, const size_t& iCell) const {
	if(iCell > this->getNumCells()) {
		std::ostringstream os;
		os << "Unknown reach cell " << iCell;
		if(this->isSet()) os << " in stream reach " << this->getIndex();
		throw mio::IndexOutOfBoundsException(os.str(), AT);
	}

	if(var == StreamVariable::T) {
		return _temperature[iCell];
	} else if(var == StreamVariable::nISWR) {
		return _heat_fluxes.getAbsorbedSWRadiation(this->getCell(iCell),
				_clock.getCurrTime(), _meteo[iCell]);
	} else if(var == StreamVariable::nILWR) {
		return _heat_fluxes.getAbsorbedLWRadiation(_meteo[iCell]);
	} else if(var == StreamVariable::ELWR) {
		return _heat_fluxes.getEmittedLWRadiation(_temperature[iCell]);
	} else if(var == StreamVariable::LatentHeatFlux) {
		return _heat_fluxes.getLatentHeatFlux(this->getCell(iCell), _meteo[iCell],
				_temperature[iCell]);
	} else if(var == StreamVariable::SensibleHeatFlux) {
		return _heat_fluxes.getSensibleHeatFlux(this->getCell(iCell), _meteo[iCell],
				_temperature[iCell]);
	} else if(var == StreamVariable::BedCondHeatFlux) {
		return _heat_fluxes.getBedConductionHeatFlux(_temperature[iCell],
				_meteo[iCell]("TSOIL"));
	} else if(var == StreamVariable::BedFricHeatFlux) {
		return _heat_fluxes.getBedFrictionHeatFlux(this->getCell(iCell).slope,
				this->get(StreamVariable::Q, iCell), this->getWidth());
	} else if(var == StreamVariable::InletHeatFlux) {
		return this->getInletBoundaryConditions().getHeat();
	} else if (var == StreamVariable::LateralHeatFlux) {
		return this->getLateralBoundaryConditions()[iCell].getHeat();
	} else {
		return DiscretizedReachDecorator::get(var, iCell);
	}
}


std::vector<double> DiscretizedReachWithEB::get(const StreamVariable& var) const {
	if(var == StreamVariable::T) {
		return _temperature;
	} else if(var == StreamVariable::nISWR          ||
			var == StreamVariable::nILWR            ||
			var == StreamVariable::ELWR             ||
			var == StreamVariable::LatentHeatFlux   ||
			var == StreamVariable::SensibleHeatFlux ||
			var == StreamVariable::BedCondHeatFlux  ||
			var == StreamVariable::BedFricHeatFlux  ||
			var == StreamVariable::InletHeatFlux    ||
			var == StreamVariable::LateralHeatFlux) {
		const size_t nCells(this->getNumCells());
		std::vector<double> output(nCells);
		for(size_t iCell(0); iCell < nCells; ++iCell)
			output[iCell] = this->get(var, iCell);

		return output;
	} else {
		return DiscretizedReachDecorator::get(var);
	}
}


void DiscretizedReachWithEB::setDisplayInfo(const bool& display) {
	DiscretizedReachDecorator::setDisplayInfo(display);

	_display_info = display;
}


DiscretizedReachWithEB::~DiscretizedReachWithEB() {
	for(SlopeLimiter* limiter : _slope_limiters)
		delete limiter;
}


std::vector<double> DiscretizedReachWithEB::computeStandardAirPressure() const {
	const std::vector<StreamCell>& cells(this->getCells());
	std::vector<double> output;
	output.reserve(cells.size());
	for(const StreamCell& cell : cells) {
		output.push_back(SFLaws::standardAirPressure(cell.getAltitude(), false));
	}
	return output;
}


std::vector<SlopeLimiter*> DiscretizedReachWithEB::createSlopeLimiters() const {
	const size_t nCells(this->getNumCells());
	const std::vector<StreamCell>& cells(this->getCells());

	std::vector<SlopeLimiter*> output;
	output.reserve(nCells);

	for(size_t iCell(0); iCell < nCells; ++iCell) {
		const double& hCurr(cells[iCell].length);
		const double hPrev(iCell > 0 ? cells[iCell-1].length : hCurr);
		const double hNext(iCell < nCells-1 ? cells[iCell+1].length : hCurr);
		output.push_back(new VanAlbadaLimiter(hPrev, hCurr, hNext));
	}

	return output;
}


void DiscretizedReachWithEB::updateFlowVelocity(const std::vector<double>& discharge,
		const std::vector<double>& waterHeight) {
	const size_t nCells(this->getNumCells());
	const double width(this->getWidth());

	_velocity.resize(nCells);
	for(size_t iCell(0); iCell < nCells; ++iCell) {
		_velocity[iCell] = fmax(discharge[iCell]/(width*waterHeight[iCell]),
				MIN_VELOCITY);
	}
}


void DiscretizedReachWithEB::advectTemperature(const double& timeStep) {
	//Compute the stream temperature in the lhs ghost cell
	const double travelTime(fmin(timeStep, 0.5*this->getCell(0).length/_velocity.front()));
	double phantomTemp(_inlet_temperature_old + travelTime/timeStep*_inlet_temperature_inc);

	//Advect temperature in each reach cell and for each time step
	const std::vector<double> leftStates(this->reconstructLeftStates(phantomTemp));
	for(size_t iCell(0); iCell < this->getNumCells(); ++iCell) {
		const double& cellLen(this->getCell(iCell).length);
		const double& prevLState(leftStates[iCell]);
		const double& nextLState(leftStates[iCell+1]);
		const double& v(_velocity[iCell]);
		_temperature[iCell] -= timeStep*(v/cellLen*(nextLState - prevLState));
	}
}


void DiscretizedReachWithEB::accountForHeatSources(const double& dt,
		const std::vector<double>& newDischarge,
		const std::vector<double>& newWaterHeight) {
	const double width(this->getWidth());
	const std::vector<CellInflow>& newLatInflow(this->getLateralBoundaryConditions());

	for(size_t iCell(0); iCell < this->getNumCells(); ++iCell) {
		double& currTemp(_temperature[iCell]);
		const StreamCell& currCell(this->getCell(iCell));
		mio::MeteoData& newMeteo(_meteo[iCell]);
		newMeteo(mio::MeteoData::P) = _air_pressure[iCell];
		const double cellSurface(width*currCell.length);
		const double cellVolume(cellSurface*newWaterHeight[iCell]);

		//Compute the terms of the energy-balance which do not depend on stream
		//temperature
		const double incomingRadiation(_heat_fluxes.getAbsorbedLWRadiation(newMeteo) +
				_heat_fluxes.getAbsorbedSWRadiation(currCell, _clock.getNextTime(), newMeteo));
		const double bedFriction(_heat_fluxes.getBedFrictionHeatFlux(currCell.slope,
				newDischarge[iCell], width));

		//Compute the new temperature at the current cell using backward Euler
		const double deltaTemp(fmax(2./3600.*dt, 2.*_TEMPERATURE_PRECISION)); //< [K] range around the old temperature value in which the new temperature value should be searched
		try {
			bool convergence = true;
			currTemp = RootFindingAlgorithms::brentMethod([&](const double& Twater){
				return this->ebResidual(currCell, newLatInflow[iCell], currTemp,
						Twater, dt, incomingRadiation, bedFriction, newMeteo,
						cellSurface, cellVolume);},
						convergence, fmax(currTemp - deltaTemp, 273.15), currTemp + deltaTemp,
						_TEMPERATURE_PRECISION);
			if(_display_info && this->isSet() && _display_max_iter_warning && !convergence) {
				std::cout << "[w] Maximum number of iterations reached while computing "
									<< "temperature of stream reach in subwatershed " << this->getIndex()
									<< " at date " << _clock.getNextTime().toString(mio::Date::ISO) << ". "
									<< "Proceeding with the simulation but results might be "
									<< "incorrect\n";
				_display_max_iter_warning = false;
			}
		} catch(const std::exception& e) {
			std::ostringstream os;
			os << "Error while computing temperature";
			if(this->isSet())
				os << " of stream reach in subwatershed " << this->getIndex();
			else
				os << " in a stream reach";
			os << " at date " << _clock.getNextTime().toString(mio::Date::ISO)
			   << ": " << e.what();
			throw RuntimeException(os.str(), AT);
		}

		currTemp = fmax(currTemp, MIN_TEMPERATURE);
		if(currTemp > MAX_TEMPERATURE) {
			currTemp = MAX_TEMPERATURE;
			if(_display_info && this->isSet() && _display_max_temp_warning) {
				std::cout << "[w] Temperature of stream reach in subwatershed "
				          << this->getIndex() << " exceeds " << MAX_TEMPERATURE
				          << "K at date "
				          << _clock.getCurrTime().toString(mio::Date::ISO) << "\n";
				_display_max_temp_warning = false;
			}
		}
	}
}


std::vector<double> DiscretizedReachWithEB::reconstructLeftStates(double phantomTemp) const {
	const size_t nCells(this->getNumCells());
	std::vector<double> output(nCells + 1);

	if(phantomTemp == mio::IOUtils::nodata)
		phantomTemp = _temperature.front();

	if(_inlet_temperature_old != mio::IOUtils::nodata)
		output.front() = _inlet_temperature_old;
	else
		output.front() = _temperature.front();

	for(size_t iCell(0); iCell < nCells; ++iCell) {
		const double& currTemp(_temperature[iCell]);
		const double prevTemp(iCell > 0 ? _temperature[iCell-1] : phantomTemp);
		const double nextTemp(iCell < nCells-1 ? _temperature[iCell+1] : currTemp);

		double leftState(currTemp);
		if(currTemp != prevTemp) {
			double limitFrac(1.0);
			if(prevTemp != nextTemp && currTemp >= fmin(nextTemp, prevTemp) &&
					currTemp <= fmax(nextTemp, prevTemp)) {
				const double ratio((currTemp - prevTemp)/(nextTemp - prevTemp));
				limitFrac = (*_slope_limiters[iCell])(ratio);
			}
			leftState += 0.5*limitFrac*(currTemp - prevTemp);
		}
		output[iCell+1] = leftState;
	}

	return output;
}


double DiscretizedReachWithEB::ebResidual(const StreamCell& cell, const CellInflow& inflow,
		const double& currTemp, const double& newTemp, const double& dt,
		const double& incomingRad, const double& bedFriction, const mio::MeteoData& meteo,
		const double& surface, const double& volume) const {
	//Compute cell heat inflow due to energy fluxes from the atmosphere and the
	//stream bed
	const double emittedLW(_heat_fluxes.getEmittedLWRadiation(newTemp));
	const double latentHF(_heat_fluxes.getLatentHeatFlux(cell, meteo, newTemp));
	const double sensibleHF(_heat_fluxes.getSensibleHeatFlux(cell, meteo, newTemp));
	const double bedCondHF(_heat_fluxes.getBedConductionHeatFlux(newTemp,
			meteo("TSOIL")));
	const double totHeatFlow((incomingRad + emittedLW + latentHF + sensibleHF +
			bedCondHF + bedFriction)*surface);

	//Compute the cell heat inflow due to the lateral advection of ground water
	double lateralHeatFlow(0.);
	if(inflow.discharge > 0. && inflow.temperature != mio::IOUtils::nodata) {
		const double tempDiff(inflow.temperature - newTemp);
		lateralHeatFlow = inflow.discharge*tempDiff;
	}

	const double rhoCp(SFConstants::WATER_MASS_DENSITY*SFConstants::WATER_HEAT_CAPACITY);
	return newTemp - currTemp - dt*(totHeatFlow/rhoCp + lateralHeatFlow)/volume;
}

} /* namespace sflow */
