/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LUMPEDREACHDECORATOR_H_
#define LUMPEDREACHDECORATOR_H_

#include <sstream>
#include <iostream>
#include <streamflow/utils/Maths.h>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/utils/HydroVariable.h>
#include <streamflow/utils/StreamflowExceptions.h>
#include <streamflow/io/DataContainer.h>
#include <streamflow/calibration/Parameter.h>
#include <streamflow/calibration/ParameterSet.h>
#include <streamflow/streamReach/StreamCell.h>
#include <streamflow/streamReach/StreamReach.h>
#include <streamflow/streamReach/ReachHeatFluxModel.h>
#include <meteoio/MeteoIO.h>

namespace sflow {

class Subwatershed; //< forward declaration to avoid unnecessary includes


/**
 * @class LumpedReachDecorator
 * @brief This class implements a decorator to class LumpedReachInterface.
 * For a description of the decorator pattern,
 * see <a>https://en.wikipedia.org/wiki/Decorator_pattern</a>.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-07-20
 */
class LumpedReachDecorator: public LumpedReachInterface {
	public:
		using LumpedReachInterface::get; //< avoids shadowing methods get() from base class

		static const double MIN_TEMPERATURE; //< [K] minimum allowed stream temperature
		static const double MAX_TEMPERATURE; //< [K] maximum allowed stream temperature

		LumpedReachDecorator(LumpedReachInterface *decoratedObject);
		LumpedReachDecorator(const LumpedReachDecorator&) = delete;
		LumpedReachDecorator& operator=(const LumpedReachDecorator&) = delete;
		virtual std::vector<StreamCell>::const_iterator begin() const override;
		virtual std::vector<StreamCell>::const_iterator end() const override;
		virtual bool isHeadReach() const override;
		virtual bool isSet() const override;
		virtual ParameterSet getParams() const override;
		virtual void setParams(const ParameterSet&) override;
		virtual void advanceInTime() override;
		virtual void reset() override;
		virtual void setDisplayInfo(const bool& display) override;
		virtual void setUpstreamReach(const StreamReach& reach) override;
		virtual void setUpstreamReaches(const StreamReach& reach1,
				const StreamReach& reach2) override;
		virtual void resetUpstreamReaches() override;
		virtual void setSubwatershed(const Subwatershed& subwatershed) override;
		virtual void updateInletBoundaryConditions() override;
		virtual void updateLateralBoundaryConditions() override;
		virtual size_t getNumCells() const override;
		virtual const std::vector<StreamCell>& getCells() const override;
		virtual void initialize() override;
		virtual const StreamCell& getCell(const size_t& idx) const override;
		virtual unsigned int getStrahlerOrder() const override;
		virtual const Subwatershed& getSubwatershed() const override;
		virtual double getLength() const override;
		virtual double getWidth() const override;
		virtual double getMeanSlope() const override;
		virtual size_t getIndex() const override;
		virtual double getSpecificDrainageArea() const override;
		virtual double getTotalDrainageArea() const override;
		virtual const CellInflow& getInletBoundaryConditions() const override;
		virtual const CellInflow& getLateralBoundaryConditions() const override;
		virtual double get(const StreamVariable& var) const override;
		virtual double get(const StreamVariable& var, const size_t& iCell) const override;
		virtual ~LumpedReachDecorator() override;

	protected:
		LumpedReachInterface *_decorated_object;
};


/**
 * @class LumpedReachWithEB
 * @brief This class gives a LumpedStreamReach the possibility to compute stream
 * temperature, based on the energy-balance equation of the stream reach. The
 * origin of the meteorological data used to compute the energy-balance can be
 * specified by the
 * This class differs from class LumpedReachWithReachEB in the sense that it is
 * based on the meteorological variables averaged over the subwatershed area,
 * not those measured over the stream cells.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-11-05
 */
class LumpedReachWithEB : public LumpedReachDecorator {
	public:
		using LumpedReachDecorator::get; //< avoids shadowing methods get() from base class

		LumpedReachWithEB(LumpedReachInterface *decoratedObject,
				const ReachHeatFluxModel& model, std::shared_ptr<DataContainer> dataCont);
		virtual void updateLateralBoundaryConditions() override;
		virtual void advanceInTime() override;
		virtual void reset() override;
		virtual void setDisplayInfo(const bool& display) override;
		virtual double get(const StreamVariable& var) const override;
		virtual void initialize() override;
		virtual ~LumpedReachWithEB() override;

	protected:
		const ReachHeatFluxModel& _heat_fluxes;
		std::shared_ptr<DataContainer> dataContainer;
		const StreamCell& _middle_cell; //< cell representative for the whole reach (located in the middle of the reach)
		const double _air_pressure;     //< [Pa] air pressure over the stream reach
		double _temperature;            //< [K] stream temperature
		mio::MeteoData _meteo;          //< meteorological variables above the stream
		bool _display_info;             //< should information be displayed on the screen?

		static const double _TEMPERATURE_PRECISION; //< [K] absolute stream temperature precision to be reached at each time step

		virtual double ebResidual(const double& dt, const double& streamTemp,
				const double& incomingRadiation, const double& frictionFlux,
				double advectedFlux, const double& inflowDischarge,
				const double& volume, const double& width) const;
};

} /* namespace sflow */

#endif /* LUMPEDREACHDECORATOR_H_ */
