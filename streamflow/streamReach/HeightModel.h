/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HEIGHTMODEL_H_
#define HEIGHTMODEL_H_

#include <map>
#include <string>
#include <vector>
#include <numeric>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/utils/HydroVariable.h>
#include <streamflow/streamReach/StreamReach.h>
#include <streamflow/streamReach/StreamCell.h>
#include <streamflow/calibration/Parameter.h>
#include <streamflow/calibration/ParameterSet.h>
#include <meteoio/MeteoIO.h>

namespace sflow {

/**
 * @class StreamHeightModel
 * @brief Virtual class used to compute the stream water height at a given
 * point, based on the stream discharge and stream width at the same point.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-07-20
 */
class StreamHeightModel : public SharedObject, public ParametrizableObject {
	public:
		StreamHeightModel() : SharedObject(), ParametrizableObject() {}
		virtual double getHeight(const LumpedReachInterface& reach) = 0;
		virtual void getHeight(const DiscretizedReachInterface& reach,
				std::vector<double>& height) = 0;
		virtual ~StreamHeightModel() {}
};


/**
 * @class DischargePowerHeightModel
 * @brief This class implements the interface StreamHeightModel. It computes
 * the stream water height (in m) as: \f$a Q^b\f$, where \f$Q\f$ denotes the
 * stream discharge (in m<sup>3</sup>/s), and \f$a\f$ and \f$b\f$ are two
 * calibration parameters. These parameters should be indicated in the
 * configuration file using the following keys:
 * @code
 * HEIGHT_MODEL_FACTOR         = 5
 * HEIGHT_MODEL_FACTOR::LIMITS = 0 10
 * HEIGHT_MODEL_POWER          = 2
 * HEIGHT_MODEL_POWER::LIMITS  = 1 3
 * @endcode
 * The keys HEIGHT_MODEL_FACTOR::VALUE and HEIGHT_MODEL_POWER::VALUE correspond
 * to the parameters a and b, respectively. Their counterparts ending with
 * "\::LIMITS" indicate the minimum and maximum values that each parameter can
 * take, respectively (his information is useful to the optimization algorithm
 * in order to automatically calibrate the model). The keys ending with
 * "\::LIMITS" are optional.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-07-20
 */
class DischargePowerHeightModel : public StreamHeightModel {
	using StreamHeightModel::getHeight; //< import all overloaded methods getHeight() from base class

	public:
		DischargePowerHeightModel(const mio::Config& cfg);
		virtual double getHeight(const LumpedReachInterface& reach);
		virtual void getHeight(const DiscretizedReachInterface& reach,
				std::vector<double>& height);
		virtual ParameterSet getParams() const;
		virtual void setParams(const ParameterSet& params);
		virtual ~DischargePowerHeightModel();

	protected:
		Parameter _factor;
		Parameter _power;
};


/**
 * @class PrescribedVelocityModel
 * @brief This class implements the interface StreamHeightModel. It computes
 * the stream water height (in m) as: \f$Q/(w \cdot v)\f$, where \f$Q\f$ denotes
 * the stream discharge (in m<sup>3</sup>/s), \f$w\f$ the stream width (in m)
 * and \f$v\f$ the prescribed stream velocity (in m/s). The prescribed velocity
 * should be specified in the configuration file using the following syntax:
 * @code
 * HEIGHT_MODEL_VELOCITY         = 0.5
 * HEIGHT_MODEL_VELOCITY::LIMITS = 0.1 1
 * @endcode
 * The upper line specifies the value to be used for velocity in a single model
 * run, whereas the second line indicates the minimum and maximum values that
 * can be assigned to the velocity by the optimization module when automatically
 * calibrating the model (second line is optional).
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-07-20
 */
class PrescribedVelocityModel : public StreamHeightModel {
	using StreamHeightModel::getHeight; //< import all overloaded methods getHeight() from base class

	public:
		PrescribedVelocityModel(const mio::Config& cfg);
		virtual double getHeight(const LumpedReachInterface& reach);
		virtual void getHeight(const DiscretizedReachInterface& reach,
				std::vector<double>& height);
		virtual ParameterSet getParams() const;
		virtual void setParams(const ParameterSet& params);
		virtual ~PrescribedVelocityModel();

	protected:
		Parameter _velocity; //< in [m/s]
};

} /* namespace sflow */

#endif /* HEIGHTMODEL_H_ */
