/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/streamReach/DiscretizedStreamReach.h>

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class DiscretizedStreamReach
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class DiscretizedStreamReach
 * @param cells Cells composing the stream reach. The cells must be ordered
 * streamwise, i.e. the first cell should correspond to the upstream end point
 * and the last one to the downstream end point of the reach
 * @param widthModel Model used to compute the width of the stream reach
 * @param clk Reference to the external Clock instance which holds the current
 * simulation time
 */
DiscretizedStreamReach::DiscretizedStreamReach(const std::vector<StreamCell>& cells,
		StreamWidthModel& widthModel, const Clock& clk) :
				DiscretizedReachInterface(clk), _cells(cells),
				_n_cells(cells.size()), _subwatershed(nullptr),
				_upstream_reaches(), _width_model(widthModel),
				_is_head_reach(true), _strahler_order(1),
				_spec_drainage_area(computeSpecificDrainageArea(cells)),
				_length(computeLength(cells)),
				_width(StreamWidthModel::MIN_STREAM_WIDTH),
				_mean_slope(this->computeMeanSlope()),
				_discharge(_n_cells, 0.), _water_height(_n_cells, 0.5),
				_inlet_bc(), _lateral_bc(_n_cells) {}


void DiscretizedStreamReach::initialize()
{
	this->setInitialConditions();
	this->updateLateralBoundaryConditions();
}


/**
 * @brief Returns an iterator on the cell located at the upstream end of the
 * stream reach.
 * @return Const iterator on the most upstream cell
 */
std::vector<StreamCell>::const_iterator DiscretizedStreamReach::begin() const {
	return _cells.begin();
}


/**
 * @brief Returns an iterator on the cell located at the downstream end of the
 * stream reach.
 * @return Const iterator on the most downstream cell
 */
std::vector<StreamCell>::const_iterator DiscretizedStreamReach::end() const {
	return _cells.end();
}


/**
 * @brief Can be used to know whether the stream reach is located in a
 * headwater subwatershed
 * @return Is stream reach in a headwater subwatershed?
 */
bool DiscretizedStreamReach::isHeadReach() const {
	return _is_head_reach;
}


/**
 * @brief Has the stream reach already been associated with its corresponding
 * subwatershed?
 * @return Is the stream reach associated with a subwatershed?
 */
bool DiscretizedStreamReach::isSet() const {
	return _subwatershed;
}


/**
 * @brief This method sets the inlet boundary conditions for the stream reach
 */
void DiscretizedStreamReach::updateInletBoundaryConditions() {
	if(!_subwatershed)
		throw RuntimeException("The subwatershed associated with the current "
				"stream reach has not yet been defined yet", AT);

	_inlet_bc.discharge = _subwatershed->get(SubwatershedVariable::Q_source);
	const double inflowTemperature(_subwatershed->get(SubwatershedVariable::T_source));
	double inflowEnergy(_inlet_bc.discharge < StreamReach::MIN_DISCHARGE ?
			0. : inflowTemperature*_inlet_bc.discharge);

	for(const DiscretizedReachInterface* reach : _upstream_reaches) {
		const double upstreamDischarge(reach->get(StreamVariable::Q).back());
		_inlet_bc.discharge += upstreamDischarge;
		inflowEnergy += upstreamDischarge*reach->get(StreamVariable::T).back();
	}

	_inlet_bc.temperature = _inlet_bc.discharge < StreamReach::MIN_DISCHARGE ?
			mio::IOUtils::nodata : inflowEnergy/_inlet_bc.discharge;
}


/**
 * @brief This method sets the lateral boundary conditions for the stream reach
 */
void DiscretizedStreamReach::updateLateralBoundaryConditions() {
	if(!_subwatershed)
		throw RuntimeException("The subwatershed associated with the current "
				"stream reach has not yet been defined yet", AT);

	_subwatershed->getStreamLateralInflow(_lateral_bc);
}


/**
 * @brief Returns the number of cells composing the stream reach
 * @return Number of stream cells
 */
size_t DiscretizedStreamReach::getNumCells() const {
	return _n_cells;
}


/**
 * @brief Returns all the cells defining the stream reach, ordered
 * streamwise (first cell is the upstream reach end, last cell is the
 * downstream reach end)
 * @return Stream cells
 */
const std::vector<StreamCell>& DiscretizedStreamReach::getCells() const {
	return _cells;
}


/**
 * @brief Returns the (i+1)-th cell composing the stream reach. The index
 * i starts from 0.
 * @param idx Index of the cell to be returned. The cells are numbered in
 * increasing order in the streamflow direction, starting at 0 for the cell
 * located at the upstream end of the reach.
 * @return (idx+1)-th stream cell
 */
const StreamCell& DiscretizedStreamReach::getCell(const size_t& idx) const {
	if(idx >= this->getNumCells()) {
		std::ostringstream os;
		os << "Unknown cell " << idx;
		if(this->isSet()) os << " in reach " << this->getIndex();
		throw mio::IndexOutOfBoundsException(os.str(), AT);
	}
	return _cells[idx];
}


/**
 * @brief Returns the Strahler order of the stream reach
 * @return Strahler order
 */
unsigned int DiscretizedStreamReach::getStrahlerOrder() const {
	return _strahler_order;
}


/**
 * @brief Returns the subwatershed in which the reach is located
 * @return Parent subwatershed
 */
const Subwatershed& DiscretizedStreamReach::getSubwatershed() const {
	if(!_subwatershed)
		throw RuntimeException("The subwatershed associated with the current "
				"stream reach has not yet been defined yet", AT);

	return *_subwatershed;
}


/**
 * @brief Returns the reach length in meters
 * @return Reach length
 */
double DiscretizedStreamReach::getLength() const {
	return _length;
}


/**
 * @brief Returns the stream width in meters
 * @return Stream width
 */
double DiscretizedStreamReach::getWidth() const {
	return _width;
}


/**
 * @brief Returns the mean slope of the stream bed
 * @return Mean bed slope (dimensionless)
 */
double DiscretizedStreamReach::getMeanSlope() const {
	return _mean_slope;
}


/**
 * @brief Returns the reach index (which corresponds to the index of the
 * subwatershed in which the reach is located)
 * @return Reach index
 */
size_t DiscretizedStreamReach::getIndex() const {
	if(!_subwatershed)
		throw RuntimeException("The subwatershed associated with the current "
				"stream reach has not been defined yet", AT);

	return _subwatershed->getIndex();
}


/**
 * @brief Returns the specific drainage area of the stream reach (i.e. the area
 * drained specifically by the cells composing the stream reach)
 * @return specific drainage area in [m^2]
 */
double DiscretizedStreamReach::getSpecificDrainageArea() const {
	return _spec_drainage_area;
}


/**
 * @brief Returns the total drainage area of the stream reach (i.e. the entire
 * area drained by all the cells composing the stream reach)
 * @return specific drainage area in [m^2]
 */
double DiscretizedStreamReach::getTotalDrainageArea() const {
	return _cells.back().tot_drainage_area;
}


/**
 * @brief Returns the discharge and temperature of the water flowing into the
 * stream reach through its upstream end point
 * @return Inlet boundary conditions
 */
const CellInflow& DiscretizedStreamReach::getInletBoundaryConditions() const {
	return _inlet_bc;
}


/**
 * @brief Returns the discharge and temperature of the water flowing into the
 * stream reach through its lateral sides
 * @return Lateral boundary conditions
 */
const std::vector<CellInflow>& DiscretizedStreamReach::getLateralBoundaryConditions() const {
	return _lateral_bc;
}


/**
 * @brief Sets the subwatershed in which the current stream reach is located
 * @param subwatershed Reference to the subwatershed containing the current
 * stream reach
 */
void DiscretizedStreamReach::setSubwatershed(const Subwatershed& subwatershed) {
	_subwatershed = &subwatershed;
}


/**
 * @brief This method tells the stream reach that the boundary conditions at
 * its upstream end are defined by the outlet of the specified upstream reach.
 * This function should be used in case the stream reach is fed by only one
 * reach upstream.
 * @param reach Reference to the reach located directly upstream of the current
 * one
 */
void DiscretizedStreamReach::setUpstreamReach(const StreamReach& reach) {
	const DiscretizedReachInterface *tmp(dynamic_cast<const DiscretizedReachInterface*>(&reach));
	if(!tmp)
		throw mio::InvalidArgumentException("The function only accepts an input "
				"argument of type DiscretizedReachInterface", AT);

	_upstream_reaches.clear();
	_upstream_reaches.push_back(tmp);
	_is_head_reach = false;
	_strahler_order = reach.getStrahlerOrder();
	if(this->isSet()) this->setInitialConditions();
}


/**
 * @brief This method tells the stream reach that the boundary conditions at
 * its upstream end are defined by the respective outlets of the specified
 * upstream reaches. This function should be used in case the stream reach is
 * fed by two reaches upstream.
 * @param reach1 Reference to one of the two reaches located directly
 * upstream of the current one
 * @param reach2 Reference to the other reach located directly upstream of the
 * current one
 */
void DiscretizedStreamReach::setUpstreamReaches(const StreamReach& reach1,
		const StreamReach& reach2) {
	const DiscretizedReachInterface *tmp1(dynamic_cast<const DiscretizedReachInterface*>(&reach1));
	const DiscretizedReachInterface *tmp2(dynamic_cast<const DiscretizedReachInterface*>(&reach2));
	if(!tmp1 || !tmp2)
		throw mio::InvalidArgumentException("The function only accepts input "
				"arguments of type DiscretizedReachInterface", AT);

	_upstream_reaches.clear();
	_upstream_reaches.push_back(tmp1);
	_upstream_reaches.push_back(tmp2);
	_is_head_reach = false;
	const unsigned int orderReach1(reach1.getStrahlerOrder());
	const unsigned int orderReach2(reach2.getStrahlerOrder());
	if(orderReach1 != orderReach2)
		_strahler_order = std::max(orderReach1, orderReach2);
	else
		_strahler_order = orderReach1 + 1;

	if(this->isSet()) this->setInitialConditions();
}


/**
 * @brief This method resets the stream reach to its original state regarding
 * its connectivity with other upstream reaches. After calling this method,
 * the stream reach will be as if located in a headwater subwatershed, i.e. not
 * connected to any upstream reaches.
 */
void DiscretizedStreamReach::resetUpstreamReaches() {
	_upstream_reaches.clear();
	_is_head_reach  = true;
	_strahler_order = 1;
	if(this->isSet()) this->setInitialConditions();
}


/**
 * @brief Resets the stream reach to its original state.
 *
 * WARNING: the subwatershed associated with the stream reach must have been
 * reset prior to calling this function, otherwise original state may not be
 * restored correctly.
 */
void DiscretizedStreamReach::reset() {
	this->updateLateralBoundaryConditions(); //< HACK: requires subwatershed to have been reset before!
	this->setInitialConditions();
}


/**
 * @brief Returns the value of the specified stream reach variable in each cell
 * belonging to the stream reach. Water height is returned in meters, discharge
 * in m^3/s, temperature in K and heat fluxes in W/m^2.
 * @param var Requested variable
 * @return Value of the requested variable
 */
std::vector<double> DiscretizedStreamReach::get(const StreamVariable& var) const {
	if(var == StreamVariable::H)
		return _water_height;
	else if(var == StreamVariable::Q)
		return _discharge;
	else
		return std::vector<double>(_n_cells, mio::IOUtils::nodata);
}


/**
 * @brief Returns the value of the specified stream reach variable in the
 * specified cell of the stream reach.
 * @param var Requested reach variable
 * @param iCell Index of the reach cell of interest. The cells are numbered in
 * increasing order in the streamflow direction, starting at 0 for the cell
 * located at the upstream end of the reach.
 * @return Value of the requested reach variable in the specified cell
 */
double DiscretizedStreamReach::get(const StreamVariable& var, const size_t& iCell) const {
	if(iCell >= _n_cells) {
		std::ostringstream os;
		os << "Unknown reach cell " << iCell;
		throw mio::IndexOutOfBoundsException(os.str(), AT);
	}

	if(var == StreamVariable::H)
		return _water_height.at(iCell);
	else if(var == StreamVariable::Q)
		return _discharge.at(iCell);
	else
		return mio::IOUtils::nodata;
}


/**
 * @brief Destructor of class DiscretizedStreamReach
 */
DiscretizedStreamReach::~DiscretizedStreamReach() {}


/**
 * @brief Sets the initial values of the stream reach variables (water
 * height and discharge)
 */
void DiscretizedStreamReach::setInitialConditions() {
	if(!_subwatershed)
		throw RuntimeException("The subwatershed associated with the current "
				"stream reach has not yet been defined yet", AT);

	this->updateInletBoundaryConditions();
	_water_height.assign(_n_cells, fmax(0.5, MIN_WATER_HEIGHT));
	_discharge.assign(_n_cells, fmax(_inlet_bc.discharge, MIN_DISCHARGE));
	_width = _width_model.getWidth(*this);
}


/**
 * @brief Method used to compute the mean slope of the stream reach
 * @return Mean bed slope (dimensionless)
 */
double DiscretizedStreamReach::computeMeanSlope() const {
	if(_cells.size() > 1) {
		return fmax(_cells.front().getAltitude() - _cells.back().getAltitude(), 0.)/
				_length;
	} else {
		return _cells.front().slope;
	}
}


//-----------------------------------------------------------------------------
// Implementation of class DirectRoutingReach
//-----------------------------------------------------------------------------

DirectRoutingReach::DirectRoutingReach(const std::vector<StreamCell>& cells,
		StreamWidthModel& widthModel, const Clock& clk,
		StreamHeightModel& heightModel) :
				DiscretizedStreamReach(cells, widthModel, clk),
				_height_model(heightModel) {}


ParameterSet DirectRoutingReach::getParams() const {
	ParameterSet empty;
	return empty;
}


void DirectRoutingReach::setParams(const ParameterSet&) {
	//do nothing
}


void DirectRoutingReach::advanceInTime() {
	_discharge.front() = _inlet_bc.discharge + _lateral_bc.front().discharge;
	_discharge.front() = fmax(_discharge.front(), MIN_DISCHARGE);
	for(size_t iCell(1); iCell < _n_cells; ++iCell) {
		const double currDischarge(_discharge[iCell-1] + _lateral_bc[iCell].discharge);
		_discharge[iCell] = fmax(currDischarge, MIN_DISCHARGE);
	}
	_height_model.getHeight(*this, _water_height);
	_width = _width_model.getWidth(*this);
}


void DirectRoutingReach::setDisplayInfo(const bool&) {
	//do nothing
}


DirectRoutingReach::~DirectRoutingReach() {}


//-----------------------------------------------------------------------------
// Implementation of class DiscrMuskingumCungeReach
//-----------------------------------------------------------------------------


const double DiscrMuskingumCungeReach::_MIN_SLOPE(1e-6); //< [-]


/**
 * Constructor of class MuskingumCungeReach
 * @param cells Cells composing the stream reach. The cells must be ordered
 * streamwise, i.e. the first cell should correspond to the upstream end point
 * and the last one to the downstream end point of the reach.
 * @param widthModel Model used to compute the width of the stream reach.
 * @param clk Reference to the Clock instance holding the current simulation
 * time
 * @param fricModel Reference to a StreamBedFrictionModel object used to
 * compute the effects of stream bed friction
 */
DiscrMuskingumCungeReach::DiscrMuskingumCungeReach(const std::vector<StreamCell>& cells,
		StreamWidthModel& widthModel, Clock& clk,
		const StreamBedFrictionModel& fricModel) :
				DiscretizedStreamReach(cells, widthModel, clk), TimeStepLimiter(clk),
				_inlet_discharge_old(_inlet_bc.discharge),
				_lateral_discharge_old(_n_cells), _friction_model(fricModel),
				_display_info(true)
{
	this->setOldLateralDischarge();
	_mean_slope = fmax(_MIN_SLOPE, _mean_slope); //< ensure that mean slope is not zero
	for(StreamCell& cell : _cells)
		cell.slope = fmax(cell.slope, _MIN_SLOPE); //< ensure that cell slope is not zero
}




int DiscrMuskingumCungeReach::getRequiredMaximumTimeStep() const {
	double maxTimeStep(86400.); //< [s]
	for(size_t iCell(0); iCell < _n_cells; ++iCell) {
		const StreamCell& currCell(_cells[iCell]);
		const double currDischarge(fmax(_discharge[iCell], MIN_DISCHARGE));
		const double celerity(_friction_model.getCelerity(_width, currCell.slope,
				currDischarge));
		const double k(currCell.length/celerity);
		const double x(0.5*fmin(1., 1. - currDischarge/(_width*currCell.slope*
				currCell.length*celerity))); //< HACK: constrain x to the range (-inf, 0.5]
		maxTimeStep = fmin(maxTimeStep, 2.*k*(1. - x));
	}

	if(_display_info && this->isSet() && maxTimeStep < 1.) {
		std::cout << "[w] Time step needs to be lower than 1 second "
							<< "in order to ensure convergence of the Muskingum-Cunge "
							<< "method in reach " << std::fixed << std::setprecision(0)
							<< this->getIndex() << " at date"
							<< _clock.getCurrTime().toString(mio::Date::ISO)
							<< ". Advancing the simulation with a time step of 1s, "
							<< "results might be incorrect\n";
	}

	return fmax(1,floor(maxTimeStep + 0.1));
}


ParameterSet DiscrMuskingumCungeReach::getParams() const {
	ParameterSet empty;
	return empty;
}


void DiscrMuskingumCungeReach::setParams(const ParameterSet&) {
	//do nothing
}


/**
 * @brief Sets the subwatershed in which the current stream reach is located
 * @param subwatershed Reference to the subwatershed containing the current
 * stream reach
 */
void DiscrMuskingumCungeReach::setSubwatershed(const Subwatershed& subwatershed) {
	DiscretizedStreamReach::setSubwatershed(subwatershed);
	this->setOldLateralDischarge();
}


void DiscrMuskingumCungeReach::advanceInTime() {
	int remaining (_clock.getTimeStep());
	while(remaining>0){
		double dt=0;
		const int dt_loc(getRequiredMaximumTimeStep());
		if(dt_loc>=remaining)
		{
			dt=remaining;
		}
		else if(dt_loc>remaining/2.){
			dt=remaining/2.;
		}
		else{
			dt=dt_loc;
		}
		dt=dt<remaining?dt:remaining;
		innerAdvanceInTime(dt);
		remaining-=static_cast<int>(dt);
	}
}

/**
 * @brief Method used to compute the values of the outlet discharge and water
 * height of the stream reach at time t+dt, where t denotes the current time
 * and dt corresponds to the time step
 */
void DiscrMuskingumCungeReach::innerAdvanceInTime(const double dt) {
	//Process first cell
	double inletDischargeOld(_inlet_discharge_old + _lateral_discharge_old.front());
	double inletDischargeNew(_inlet_bc.discharge + _lateral_bc.front().discharge);
	double newUpstreamDischarge(this->computeNewDischargeValue(dt, inletDischargeOld,
			inletDischargeNew, _discharge[0], _cells[0]));

	//Process remaining cells
	for(size_t iCell(1); iCell < _n_cells; ++iCell) {
		inletDischargeOld = _discharge[iCell - 1] + _lateral_discharge_old[iCell];
		inletDischargeNew = newUpstreamDischarge + _lateral_bc[iCell].discharge;
		const double newDischarge(this->computeNewDischargeValue(dt, inletDischargeOld,
				inletDischargeNew, _discharge[iCell], _cells[iCell]));
		_discharge[iCell-1]  = fmax(newUpstreamDischarge, MIN_DISCHARGE);
		newUpstreamDischarge = newDischarge;

	}
	_discharge.back() = fmax(newUpstreamDischarge, MIN_DISCHARGE);

	//Compute new water height values
	for(size_t iCell(0); iCell < _n_cells; ++iCell) {
		_water_height[iCell] = _friction_model.getWaterHeight(_width,
				_mean_slope, _discharge.at(iCell));
	}
	_width = _width_model.getWidth(*this);
}


void DiscrMuskingumCungeReach::reset() {
	DiscretizedStreamReach::reset();
	this->setOldLateralDischarge();
}


void DiscrMuskingumCungeReach::setDisplayInfo(const bool& display) {
	_display_info = display;
}


void DiscrMuskingumCungeReach::updateLateralBoundaryConditions() {
	this->setOldLateralDischarge();
	DiscretizedStreamReach::updateLateralBoundaryConditions();
}


void DiscrMuskingumCungeReach::updateInletBoundaryConditions() {
	_inlet_discharge_old = _inlet_bc.discharge;
	DiscretizedStreamReach::updateInletBoundaryConditions();
}


/**
 * @brief Destructor of class MuskingumCungeReach
 */
DiscrMuskingumCungeReach::~DiscrMuskingumCungeReach() {}


/**
 * @brief Sets the initial values of the stream reach variables (water
 * height and discharge)
 */
void DiscrMuskingumCungeReach::setInitialConditions() {
	DiscretizedStreamReach::setInitialConditions();
	_inlet_discharge_old = _inlet_bc.discharge;
}


void DiscrMuskingumCungeReach::setOldLateralDischarge() {
	_lateral_discharge_old.resize(_n_cells);
	for(size_t iCell(0); iCell < _n_cells; ++iCell)
		_lateral_discharge_old[iCell] = _lateral_bc[iCell].discharge;
}


double DiscrMuskingumCungeReach::computeNewDischargeValue(const double& dt,
		const double& oldInDischarge, const double& newInDischarge,
		const double& oldOutDischarge, const StreamCell& cell) {
	//Compute the 3-point average discharge
	double meanDischarge((oldInDischarge + newInDischarge + oldOutDischarge)/3.0);
	meanDischarge = fmax(meanDischarge, MIN_DISCHARGE);

	//Compute the physically-based values of the parameters k and x
	const double celerity(_friction_model.getCelerity(_width, cell.slope, meanDischarge));
	const double k(cell.length/celerity);
	const double x(0.5*fmin(1. - meanDischarge/(_width*cell.slope*cell.length*celerity), 1.)); //< HACK: constrain x to the range (-inf, 0.5]

	//Compute the weighting factors c1, c2 and c3
	const double denom(k*(1. - x) + 0.5*dt);
	const double c1((0.5*dt + k*x)/denom);
	const double c2((0.5*dt - k*x)/denom);
	const double c3(1. - dt/denom);

	//Return the new outlet discharge
	return fmax(c1*oldInDischarge + c2*newInDischarge + c3*oldOutDischarge, 0.);
}

} /* namespace sflow */
