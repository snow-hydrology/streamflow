/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as 
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public 
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/streamReach/StreamReach.h>

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class StreamReach
//-----------------------------------------------------------------------------

const double StreamReach::MIN_WATER_HEIGHT(1e-3); //< [m]
const double StreamReach::MIN_DISCHARGE(1e-4);    //< [m^3/s]
const double StreamReach::MIN_VELOCITY(1e-3);     //< [m/s]


/**
 * @brief Method used to compute the length of a stream reach
 * @param cells Cells composing the stream reach
 * @return Length of the stream reach in meters
 */
double StreamReach::computeLength(const std::vector<StreamCell>& cells) {
	double length(0.);
	for(const StreamCell& cell : cells) {
		length += cell.length;
	}
	return length;
}


/**
 * @brief Method used to compute the specific area drained by a stream reach
 * @param cells Cells composing the stream reach
 * @return Specific drainage area of the stream in [m^2]
 */
double StreamReach::computeSpecificDrainageArea(const std::vector<StreamCell>& cells) {
	double drainageArea(0.);
	for(const StreamCell& cell : cells) {
		drainageArea += cell.spec_drainage_area;
	}
	return drainageArea;
}


//-----------------------------------------------------------------------------
// Implementation of class LumpedReachInterface
//-----------------------------------------------------------------------------

DiscretizedObject::Discretization LumpedReachInterface::getDiscretization() const {
	return Discretization::Lumped;
}


//-----------------------------------------------------------------------------
// Implementation of class DiscretizedReachInterface
//-----------------------------------------------------------------------------

DiscretizedObject::Discretization DiscretizedReachInterface::getDiscretization() const {
	return Discretization::Distributed;
}

} /* namespace sflow */
