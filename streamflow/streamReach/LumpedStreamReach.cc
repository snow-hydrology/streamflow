/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/streamReach/LumpedStreamReach.h>

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class LumpedStreamReach
//-----------------------------------------------------------------------------

/**
 * @brief Constructor of class LumpedStreamReach, which does not initialize the
 * subwatershed in which the current reach is located
 * @param cells Cells composing the stream reach. The cells must be ordered
 * streamwise, i.e. the first cell should correspond to the upstream end point
 * and the last one to the downstream end point of the reach
 * @param widthModel Model used to compute the width of the stream reach
 * @param clk Reference to the external Clock instance which holds the current
 * simulation time
 */
LumpedStreamReach::LumpedStreamReach(const std::vector<StreamCell>& cells,
    StreamWidthModel& widthModel, const Clock& clk) :
        LumpedReachInterface(clk), _cells(cells), _subwatershed(nullptr),
        _upstream_reaches(), _width_model(widthModel),
        _is_head_reach(true), _strahler_order(1),
        _spec_drainage_area(computeSpecificDrainageArea(cells)),
        _length(computeLength(cells)),
        _width(StreamWidthModel::MIN_STREAM_WIDTH),
        _mean_slope(this->computeMeanSlope()),
        _discharge(), _water_height(), _inlet_bc(), _lateral_bc() {


}

void LumpedStreamReach::initialize()
{
  this->setInitialConditions();
  this->updateLateralBoundaryConditions();
}


/**
 * @brief Returns an iterator on the cell located at the upstream end of the
 * stream reach.
 * @return Const iterator on the most upstream cell
 */
std::vector<StreamCell>::const_iterator LumpedStreamReach::begin() const {
  return _cells.begin();
}


/**
 * @brief Returns an iterator on the cell located at the downstream end of the
 * stream reach.
 * @return Const iterator on the most downstream cell
 */
std::vector<StreamCell>::const_iterator LumpedStreamReach::end() const {
  return _cells.end();
}


/**
 * @brief Can be used to know whether the stream reach is located in a
 * headwater subwatershed
 * @return Is stream reach in a headwater subwatershed?
 */
bool LumpedStreamReach::isHeadReach() const {
  return _is_head_reach;
}


/**
 * @brief Has the stream reach already been associated with its corresponding
 * subwatershed?
 * @return Is the stream reach associated with a subwatershed?
 */
bool LumpedStreamReach::isSet() const {
  return _subwatershed;
}


/**
 * @brief This method sets the inlet boundary conditions for the stream reach
 */
void LumpedStreamReach::updateInletBoundaryConditions() {
  if(!_subwatershed)
    throw RuntimeException("The subwatershed associated with the current "
        "stream reach has not yet been defined yet", AT);

  _inlet_bc.discharge = _subwatershed->get(SubwatershedVariable::Q_source);
  const double inflowTemperature(_subwatershed->get(SubwatershedVariable::T_source));
  double inflowEnergy(_inlet_bc.discharge < StreamReach::MIN_DISCHARGE ?
      0. : inflowTemperature*_inlet_bc.discharge);

  for(const LumpedReachInterface* reach : _upstream_reaches) {
    const double upstreamDischarge(reach->get(StreamVariable::Q));
    if(upstreamDischarge > 0.) {
      _inlet_bc.discharge += upstreamDischarge;
      inflowEnergy += upstreamDischarge*reach->get(StreamVariable::T);
    }
  }

  _inlet_bc.temperature = _inlet_bc.discharge < StreamReach::MIN_DISCHARGE ?
      mio::IOUtils::nodata : inflowEnergy/_inlet_bc.discharge;
}


/**
 * @brief This method sets the lateral boundary conditions for the stream reach
 */
void LumpedStreamReach::updateLateralBoundaryConditions() {
  if(!_subwatershed)
    throw RuntimeException("The subwatershed associated with the current "
        "stream reach has not yet been defined yet", AT);
  _subwatershed->getStreamLateralInflow(_lateral_bc);
}


/**
 * @brief Returns the number of cells composing the stream reach
 * @return Number of stream cells
 */
size_t LumpedStreamReach::getNumCells() const {
  return _cells.size();
}


/**
 * @brief Returns all the cells defining the stream reach, ordered
 * streamwise (first cell is the upstream reach end, last cell is the
 * downstream reach end)
 * @return Stream cells
 */
const std::vector<StreamCell>& LumpedStreamReach::getCells() const {
  return _cells;
}


/**
 * @brief Returns the (i+1)-th cell composing the stream reach. The index
 * i starts from 0.
 * @param idx Index of the cell to be returned (starts from 0)
 * @return (idx+1)-th stream cell
 */
const StreamCell& LumpedStreamReach::getCell(const size_t& idx) const {
  if(idx >= this->getNumCells()) {
    std::ostringstream os;
    os << "Unknown reach cell " << idx;
    if(this->isSet()) os << " in reach " << this->getIndex();
    throw mio::IndexOutOfBoundsException(os.str(), AT);
  }

  return _cells[idx];
}


/**
 * @brief Returns the Strahler order of the stream reach
 * @return Strahler order
 */
unsigned int LumpedStreamReach::getStrahlerOrder() const {
  return _strahler_order;
}


/**
 * @brief Returns watershed in which the reach is located
 * @return Watershed
 */
const Subwatershed& LumpedStreamReach::getSubwatershed() const {
  if(!_subwatershed)
    throw RuntimeException("The subwatershed associated with the current "
        "stream reach has not yet been defined yet", AT);

  return *_subwatershed;
}


/**
 * @brief Returns the reach length in meters
 * @return Reach length
 */
double LumpedStreamReach::getLength() const {
  return _length;
}


/**
 * @brief Returns the stream width in meters
 * @return Stream width
 */
double LumpedStreamReach::getWidth() const {
  return _width;
}


/**
 * @brief Returns the mean slope of the stream bed
 * @return Mean bed slope (dimensionless)
 */
double LumpedStreamReach::getMeanSlope() const {
  return _mean_slope;
}


/**
 * @brief Returns the reach index (which corresponds to the index of the
 * subwatershed in which the reach is located)
 * @return Reach index
 */
size_t LumpedStreamReach::getIndex() const {
  if(!_subwatershed)
    throw RuntimeException("The subwatershed associated with the current "
        "stream reach has not yet been defined yet", AT);

  return _subwatershed->getIndex();
}


/**
 * @brief Returns the specific drainage area of the stream reach (i.e. the area
 * drained specifically by the cells composing the stream reach)
 * @return specific drainage area in [m^2]
 */
double LumpedStreamReach::getSpecificDrainageArea() const {
  return _spec_drainage_area;
}


/**
 * @brief Returns the total drainage area of the stream reach (i.e. the entire
 * area drained by all the cells composing the stream reach)
 * @return specific drainage area in [m^2]
 */
double LumpedStreamReach::getTotalDrainageArea() const {
  return _cells.back().tot_drainage_area;
}


/**
 * @brief Returns the discharge and temperature of the water flowing into the
 * stream reach through its upstream end point
 * @return Inlet boundary conditions
 */
const CellInflow& LumpedStreamReach::getInletBoundaryConditions() const {
  return _inlet_bc;
}


/**
 * @brief Returns the discharge and temperature of the water flowing into the
 * stream reach through its lateral sides
 * @return Lateral boundary conditions
 */
const CellInflow& LumpedStreamReach::getLateralBoundaryConditions() const {
  return _lateral_bc;
}


/**
 * @brief Sets the subwatershed in which the current stream reach is located
 * @param subwatershed Reference to the subwatershed containing the current
 * stream reach
 */
void LumpedStreamReach::setSubwatershed(const Subwatershed& subwatershed) {
  _subwatershed = &subwatershed;
}


/**
 * @brief This method tells the stream reach that the boundary conditions at
 * its upstream end are defined by the outlet of the specified upstream reach.
 * This function should be used in case the stream reach is fed by only one
 * reach upstream.
 * @param reach Reference to the reach located directly upstream of the current
 * one
 */
void LumpedStreamReach::setUpstreamReach(const StreamReach& reach) {
  const LumpedReachInterface *tmp(dynamic_cast<const LumpedReachInterface*>(&reach));
  if(!tmp)
    throw mio::InvalidArgumentException("The function only accepts an input "
        "argument of type LumpedReachInterface", AT);

  _upstream_reaches.clear();
  _upstream_reaches.push_back(tmp);
  _is_head_reach = false;
  _strahler_order = reach.getStrahlerOrder();
  if(this->isSet()) this->setInitialConditions();
}


/**
 * @brief This method tells the stream reach that the boundary conditions at
 * its upstream end are defined by the respective outlets of the specified
 * upstream reaches. This function should be used in case the stream reach is
 * fed by two reaches upstream.
 * @param reach1 Reference to one of the two reaches located directly
 * upstream of the current one
 * @param reach2 Reference to the other reach located directly upstream of the
 * current one
 */
void LumpedStreamReach::setUpstreamReaches(const StreamReach& reach1,
    const StreamReach& reach2) {
  const LumpedReachInterface *tmp1(dynamic_cast<const LumpedReachInterface*>(&reach1));
  const LumpedReachInterface *tmp2(dynamic_cast<const LumpedReachInterface*>(&reach2));
  if(!tmp1 || !tmp2)
    throw mio::InvalidArgumentException("The function only accepts input "
        "arguments of type LumpedReachInterface", AT);

  _upstream_reaches.clear();
  _upstream_reaches.push_back(tmp1);
  _upstream_reaches.push_back(tmp2);
  _is_head_reach = false;
  const unsigned int orderReach1(reach1.getStrahlerOrder());
  const unsigned int orderReach2(reach2.getStrahlerOrder());
  if(orderReach1 != orderReach2)
    _strahler_order = std::max(orderReach1, orderReach2);
  else
    _strahler_order = orderReach1 + 1;

  if(this->isSet()) this->setInitialConditions();
}


/**
 * @brief This method resets the stream reach to its original state regarding
 * its connectivity with other upstream reaches. After calling this method,
 * the stream reach will be as if located in a headwater subwatershed, i.e. not
 * connected to any upstream reaches.
 */
void LumpedStreamReach::resetUpstreamReaches() {
  _upstream_reaches.clear();
  _is_head_reach  = true;
  _strahler_order = 1;
  if(this->isSet()) this->setInitialConditions();
}


/**
 * @brief Resets the stream reach to its original state.
 *
 * WARNING: the subwatershed associated with the stream reach must have been
 * reset prior to calling this function, otherwise original state may not be
 * restored correctly.
 */
void LumpedStreamReach::reset() {
  this->updateLateralBoundaryConditions(); //< HACK: requires subwatershed to have been reset before!
  this->setInitialConditions();
}


/**
 * @brief Returns the value of the specified stream reach variable. Water height
 * is returned in meters, discharge in m^3/s, temperature in K and heat fluxes
 * in W/m^2.
 * @param var Requested variable
 * @return Value of the requested variable
 */
double LumpedStreamReach::get(const StreamVariable& var) const {
  if(var == StreamVariable::H)
    return _water_height;
  else if(var == StreamVariable::Q)
    return _discharge;
  else
    return mio::IOUtils::nodata;
}


/**
 * @brief Returns the value of the specified stream reach variable in the
 * specified reach cell. Since a lumped reach is treated as a single point, the
 * value of the variable does not depend on the cell (i.e. the second input
 * argument is not used)
 * @param var Requested reach variable
 * @return Value of the requested reach variable
 */
double LumpedStreamReach::get(const StreamVariable& var, const size_t&) const {
  return this->get(var);
}


/**
 * @brief Destructor of class LumpedStreamReach
 */
LumpedStreamReach::~LumpedStreamReach() {}


/**
 * @brief Sets the initial values of the stream reach variables (water
 * height and discharge)
 */
void LumpedStreamReach::setInitialConditions() {
  if(!_subwatershed)
    throw RuntimeException("The subwatershed associated with the current "
        "stream reach has not yet been defined yet", AT);

  this->updateInletBoundaryConditions();
  _water_height = fmax(0.5, MIN_WATER_HEIGHT);
  _discharge = fmax(_inlet_bc.discharge, MIN_DISCHARGE);
  _width = _width_model.getWidth(*this);
}


/**
 * @brief Method used to compute the mean slope of the stream reach
 * @return Mean bed slope (dimensionless)
 */
double LumpedStreamReach::computeMeanSlope() const {
  if(_cells.size() > 1) {
    return fmax(_cells.front().getAltitude() - _cells.back().getAltitude(), 0.)/
        _length;
  } else {
    return _cells.front().slope;
  }
}


//-----------------------------------------------------------------------------
// Implementation of class InstantaneousRoutingReach
//-----------------------------------------------------------------------------

/**
 * Constructor of class InstantaneousRoutingReach which does not set the
 * subwatershed in which the stream reach is located
 * @param cells Cells composing the stream reach. The cells must be ordered
 * streamwise, i.e. the first cell should correspond to the upstream end point
 * and the last one to the downstream end point of the reach.
 * @param widthModel Model used to compute the width of the stream reach.
 * @param clk Reference to the Clock instance holding the current simulation
 * time
 * @param heightModel Reference to a StreamHeightModel object used to compute
 * the stream height based on the stream discharge and stream width
 */
InstantaneousRoutingReach::InstantaneousRoutingReach(const std::vector<StreamCell>& cells,
    StreamWidthModel& widthModel, const Clock& clk, StreamHeightModel& heightModel) :
        LumpedStreamReach(cells, widthModel, clk), _height_model(heightModel) {}


ParameterSet InstantaneousRoutingReach::getParams() const {
  ParameterSet empty;
  return empty;
}


void InstantaneousRoutingReach::setParams(const ParameterSet&) {
  //do nothing
}


/**
 * @brief Method used to compute the values of the outlet discharge and water
 * height of the stream reach at time t+dt, where t denotes the current time
 * and dt corresponds to the time step
 */
void InstantaneousRoutingReach::advanceInTime() {
  _discharge = fmax(_inlet_bc.discharge + _lateral_bc.discharge, MIN_DISCHARGE);
  _water_height = _height_model.getHeight(*this);
  _width = _width_model.getWidth(*this);
}


void InstantaneousRoutingReach::setDisplayInfo(const bool&) {
  //do nothing
}


/**
 * @brief Destructor of class InstantaneousRoutingReach
 */
InstantaneousRoutingReach::~InstantaneousRoutingReach() {}


//-----------------------------------------------------------------------------
// Implementation of class LumpedMuskingumCungeReach
//-----------------------------------------------------------------------------

const double LumpedMuskingumCungeReach::_MIN_SLOPE(1e-6); //< [-]


/**
 * Constructor of class MuskingumCungeReach
 * @param cells Cells composing the stream reach. The cells must be ordered
 * streamwise, i.e. the first cell should correspond to the upstream end point
 * and the last one to the downstream end point of the reach.
 * @param widthModel Model used to compute the width of the stream reach.
 * @param clk Reference to the Clock instance holding the current simulation
 * time
 * @param fricModel Reference to a StreamBedFrictionModel object used to
 * compute the effect of friction on stream flow
 */
LumpedMuskingumCungeReach::LumpedMuskingumCungeReach(const std::vector<StreamCell>& cells,
    StreamWidthModel& widthModel, Clock& clk, const StreamBedFrictionModel& fricModel) :
        LumpedStreamReach(cells, widthModel, clk), TimeStepLimiter(clk),
        _inlet_discharge_old(_inlet_bc.discharge),
        _lateral_discharge_old(_lateral_bc.discharge),
        _friction_model(fricModel), _display_info(true),celerity()
{
  _mean_slope = fmax(_MIN_SLOPE, _mean_slope); //< ensure that mean slope is not zero
}

void LumpedMuskingumCungeReach::initialize()
{
  LumpedStreamReach::initialize();
  _lateral_discharge_old = _lateral_bc.discharge;
}



int LumpedMuskingumCungeReach::getRequiredMaximumTimeStep() const {
  const double inletDischargeOld(_inlet_discharge_old + _lateral_discharge_old);
  const double inletDischargeNew(_inlet_bc.discharge + _lateral_bc.discharge);
  const double meanDischarge((inletDischargeOld + inletDischargeNew + _discharge)/3.0);

  const double k(_length/celerity);
  const double x(0.5*fmin(1., 1. - meanDischarge/(_width*_mean_slope*_length*celerity))); //< HACK: constrain x to the range (-inf, 0.5]
  const double maxTimeStep(2.*k*(1. - x));

  if(_display_info && this->isSet() && maxTimeStep < 1.) {
    std::cout << "[w] Time step needs to be lower than 1 second "
              << "in order to ensure convergence of the Muskingum-Cunge "
              << "method in reach " << std::fixed << std::setprecision(0)
              << this->getIndex() << " at date"
              << _clock.getCurrTime().toString(mio::Date::ISO)
              << ". Advancing the simulation with a time step of 1s, "
              << "results might be incorrect" << std::endl;
  }

  return fmax(1,floor(maxTimeStep + 0.1));
}


ParameterSet LumpedMuskingumCungeReach::getParams() const {
  ParameterSet empty;
  return empty;
}


void LumpedMuskingumCungeReach::setParams(const ParameterSet&) {
  //do nothing
}


/**
 * @brief Sets the subwatershed in which the current stream reach is located
 * @param subwatershed Reference to the subwatershed containing the current
 * stream reach
 */
void LumpedMuskingumCungeReach::setSubwatershed(const Subwatershed& subwatershed) {
  LumpedStreamReach::setSubwatershed(subwatershed);
}


/**
 * @brief Method used to compute the values of the outlet discharge and water
 * height of the stream reach at time t+dt, where t denotes the current time
 * and dt corresponds to the time step
 */
void LumpedMuskingumCungeReach::advanceInTime() {
  int remaining(_clock.getTimeStep());
  while(remaining>0){
    double dt=0;

    const double inletDischargeOld(_inlet_discharge_old + _lateral_discharge_old);
    const double inletDischargeNew(_inlet_bc.discharge + _lateral_bc.discharge);
    //Compute the 3-point average discharge
    double meanDischarge((inletDischargeOld + inletDischargeNew + _discharge)/3.0);
    if(meanDischarge < MIN_DISCHARGE) { //< set discharge to min. discharge if stream reach is dry
      _discharge = MIN_DISCHARGE;
      _water_height = _friction_model.getWaterHeight(_width, _mean_slope, _discharge);
      _width = _width_model.getWidth(*this);
      return;
    }

    celerity=_friction_model.getCelerity(_width, _mean_slope, meanDischarge);

    const int dt_loc(getRequiredMaximumTimeStep());
    if(dt_loc>=remaining)
    {
      dt=remaining;
    }
    else if(dt_loc>remaining/2.){
      dt=remaining/2.;
  }
    else{
      dt=dt_loc;
    }
    dt=dt<remaining?dt:remaining;
    innerAdvanceInTime(dt,meanDischarge);
    remaining-=static_cast<int>(dt);
  }
}

void LumpedMuskingumCungeReach::innerAdvanceInTime(const double dt, const double meanDischarge) {

  const double inletDischargeOld(_inlet_discharge_old + _lateral_discharge_old);
  const double inletDischargeNew(_inlet_bc.discharge + _lateral_bc.discharge);
  // //Compute the physically-based values of the parameters k and x
  // const double celerity(_friction_model.getCelerity(_width, _mean_slope, meanDischarge));
  const double k(_length/celerity);
  const double x(fmin(0.5*(1. - meanDischarge/(_width*_mean_slope*_length*celerity)),
      0.5)); //< HACK: constrain x to the range (-inf, 0.5]

  //Compute the weighting factors c1, c2 and c3
  const double denom(k*(1. - x) + 0.5*dt);
  const double c1((0.5*dt + k*x)/denom);
  const double c2((0.5*dt - k*x)/denom);
  const double c3(1. - dt/denom);

  //Compute the new outlet discharge, water height and stream width
  _discharge = fmax(c1*inletDischargeOld + c2*inletDischargeNew + c3*_discharge,
      MIN_DISCHARGE);
  const double newMeanDischarge = 0.5*(inletDischargeNew + _discharge);
  _water_height = _friction_model.getWaterHeight(_width, _mean_slope, newMeanDischarge);
  _width = _width_model.getWidth(*this);
}


void LumpedMuskingumCungeReach::reset() {
  LumpedStreamReach::reset();
  _lateral_discharge_old = _lateral_bc.discharge;
}


void LumpedMuskingumCungeReach::setDisplayInfo(const bool& display) {
  _display_info = display;
}


void LumpedMuskingumCungeReach::updateLateralBoundaryConditions() {
  _lateral_discharge_old = _lateral_bc.discharge;
  LumpedStreamReach::updateLateralBoundaryConditions();
}


void LumpedMuskingumCungeReach::updateInletBoundaryConditions() {
  _inlet_discharge_old = _inlet_bc.discharge;
  LumpedStreamReach::updateInletBoundaryConditions();
}


/**
 * @brief Destructor of class MuskingumCungeReach
 */
LumpedMuskingumCungeReach::~LumpedMuskingumCungeReach() {}


/**
 * @brief Sets the initial values of the stream reach variables (water
 * height and discharge)
 */
void LumpedMuskingumCungeReach::setInitialConditions() {
  LumpedStreamReach::setInitialConditions();
  _inlet_discharge_old = _inlet_bc.discharge;
}

} /* namespace sflow */
