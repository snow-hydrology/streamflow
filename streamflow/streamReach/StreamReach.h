/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STREAMREACH_H_
#define STREAMREACH_H_

#include <vector>
#include <streamflow/utils/SFUtils.h>
#include <streamflow/utils/HydroVariable.h>
#include <streamflow/streamReach/StreamCell.h>

namespace sflow {

//Forward declarations in order to avoid unnecessary includes
class CellInflow;
class Subwatershed;


/**
 * @class StreamReach
 * @brief This class implements a stream reach, defined by a set of points
 * linked together by straight segments. The width of a stream reach is supposed
 * to be homogeneous. This class is used by the hydrological model to advect
 * water along the stream network.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-07-17
 */
class StreamReach: public ParametrizableObject, public TimeEvolvingObject,
		public DiscretizedObject {
	public:
		static const double MIN_WATER_HEIGHT; //< [m] minimum water height in the stream reach
		static const double MIN_DISCHARGE;    //< [m^3/s] minimum discharge in the stream reach
		static const double MIN_VELOCITY;     //< [m/s] minimum flow velocity in the stream reach

		StreamReach(const Clock& clk) : ParametrizableObject(),
				TimeEvolvingObject(clk), DiscretizedObject() {}
		virtual std::vector<StreamCell>::const_iterator begin() const = 0;
		virtual std::vector<StreamCell>::const_iterator end() const = 0;
		virtual bool isHeadReach() const = 0;
		virtual bool isSet() const = 0;
		virtual size_t getNumCells() const = 0;
		virtual const std::vector<StreamCell>& getCells() const = 0;
		virtual const StreamCell& getCell(const size_t& idx) const = 0;
		virtual double get(const StreamVariable& var, const size_t& iCell) const = 0;
		virtual unsigned int getStrahlerOrder() const = 0;
		virtual const Subwatershed& getSubwatershed() const = 0;
		virtual double getLength() const = 0;
		virtual double getWidth() const = 0;
		virtual double getMeanSlope() const = 0;
		virtual size_t getIndex() const = 0;
		virtual double getSpecificDrainageArea() const = 0;
		virtual double getTotalDrainageArea() const = 0;
		virtual const CellInflow& getInletBoundaryConditions() const = 0;
		virtual void setSubwatershed(const Subwatershed& subwatershed) = 0;
		virtual void setUpstreamReach(const StreamReach& reach) = 0;
		virtual void setUpstreamReaches(const StreamReach& reach1,
				const StreamReach& reach2) = 0;
		virtual void resetUpstreamReaches() = 0;
		virtual void updateInletBoundaryConditions() = 0;
		virtual void updateLateralBoundaryConditions() = 0;
		virtual void initialize() = 0;

		virtual ~StreamReach() {}

	protected:
		static double computeLength(const std::vector<StreamCell>& cells);
		static double computeSpecificDrainageArea(const std::vector<StreamCell>& cells);
};


/**
 * @class LumpedReachInterface
 * @brief This class represents a lumped stream reach, i.e. a reach whose
 * water height, discharge and temperature are uniform over its length.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-07-17
 */
class LumpedReachInterface : public StreamReach {
	public:
		using StreamReach::get; //< avoids shadowing method get() from base class

		LumpedReachInterface(const Clock& clk) : StreamReach(clk) {}
		virtual const CellInflow& getLateralBoundaryConditions() const = 0;
		virtual double get(const StreamVariable& var) const = 0;
		virtual Discretization getDiscretization() const;
		virtual ~LumpedReachInterface() {}
};


/**
 * @class DiscretizedReachInterface
 * @brief This class represents a spatially discretized stream reach. The reach
 * discretization is defined by the stream points.
 * @ingroup streamReach
 * @author Aurelien Gallice
 * @date 2015-07-17
 */
class DiscretizedReachInterface : public StreamReach {
	public:
		using StreamReach::get; //< avoids shadowing method get() from base class

		DiscretizedReachInterface(const Clock& clk) : StreamReach(clk) {}
		virtual const std::vector<CellInflow>& getLateralBoundaryConditions() const = 0;
		virtual std::vector<double> get(const StreamVariable& var) const = 0;
		virtual Discretization getDiscretization() const;
		virtual ~DiscretizedReachInterface() {}
};

} /* namespace sflow */

#endif /* STREAMREACH_H_ */
