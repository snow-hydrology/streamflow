/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
 StreamFlow is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as 
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.

 StreamFlow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public 
 License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
 */

#include <streamflow/StreamNetwork.h>

namespace sflow {

//-----------------------------------------------------------------------------
// Implementation of class StreamNetwork
//-----------------------------------------------------------------------------

const std::string StreamNetwork::_PARAMETER_PREFIX = "REACH";

/**
 * @brief Constructor of class StreamNetwork
 * @param cfg Reference to the Config instance holding the model parameters
 * @param clk Reference to the instance of class Clock which holds the
 * current simulation time
 * @param inputReader Reference to the InputReader object used to read the input
 * values of the stream reaches (meteo variables, soil temperature, etc.)
 */
StreamNetwork::StreamNetwork(const mio::Config& cfg, Clock& clk, std::shared_ptr<DataContainer> dataContainer) :
		ParametrizableObject(), TimeEvolvingObject(clk), DiscretizedObject(),
		_shared_objects(), _shared_params(), _reaches(), _reach_indices(),
		_computes_temp(getTemperatureModelName(cfg) != "NONE"), _is_set(false),
		_distance_precision(0.0)
{
	const ReachCellsReader& cellsReader(getReachCellsReader(cfg,dataContainer));
	_reach_indices = cellsReader.getSortedReachIndices();
	this->createReaches(cfg, clk, cellsReader, dataContainer);
	this->connectReaches(cellsReader);
	delete &cellsReader;
}

void StreamNetwork::initialize()
{
	for (StreamReach* reach : _reaches)
	{
		reach->initialize();
	}	
}

/**
 * @brief Checks whether the input number corresponds to the index of a
 * subwatershed containing one of the network's reaches.
 * @param subwatershedId Subwatershed index to be checked
 * @return Does the specified number correspond to the ID number of an existing
 * subwatershed?
 */
bool StreamNetwork::containsReach(const size_t& subwatershedId) const {
	return std::find(_reach_indices.begin(), _reach_indices.end(), subwatershedId) !=
			_reach_indices.end();
}


/**
 * @brief Returns a reference to the specified stream reach
 * @param subwatershedId Index of the stream reach to be returned
 * @return Requested StreamReach instance
 */
const StreamReach& StreamNetwork::getReach(const size_t& subwatershedId) const {
	std::vector<size_t>::const_iterator it;
	it = std::find(_reach_indices.begin(), _reach_indices.end(), subwatershedId);
	if(it == _reach_indices.end()) {
		std::ostringstream os;
		os << "Subwatershed with ID number " << subwatershedId
		   << " does not exist, or its associated stream reach could not be found";
		throw InvalidIndexException(os.str(), AT);
	}
	return *(_reaches.at(std::distance(_reach_indices.begin(), it)));
}


/**
 * @brief This method returns the indices of the subwatersheds in which the
 * stream reaches are located.
 * @return Indices of the subwatersheds containing the stream reaches
 */
const std::vector<size_t>& StreamNetwork::getReachIndices() const {
	return _reach_indices;
}


/**
 * @brief Returns a begin iterator on the stream reaches contained in the
 * network.
 * @return Begin iterator
 */
StreamNetwork::const_iterator StreamNetwork::begin() const {
	StreamNetwork::const_iterator it(*this, 0);
	return it;
}


/**
 * @brief Returns an end iterator on the stream reaches contained in the
 * network.
 * @return End iterator
 */
StreamNetwork::const_iterator StreamNetwork::end() const {
	StreamNetwork::const_iterator it(*this, _reaches.size());
	return it;
}


/**
 * @brief Returns whether water temperature is computed in the network reaches
 * @return Is water temperature computed?
 */
const bool& StreamNetwork::computesTemperature() const {
	return _computes_temp;
}


/**
 * @brief Returns the discretization level of the stream reaches contained in
 * the network
 * @return Discretization level of the stream reaches
 */
DiscretizedObject::Discretization StreamNetwork::getDiscretization() const {
	return _reaches.front()->getDiscretization();
}


/**
 * @brief Returns the network parameters. As explained in the class description,
 * the parameters which are specific to a particular stream reach are prepended
 * with the prefix "REACH[i]_", where [i] denotes the index of the stream reach.
 * In this way, parameters with identical names in different stream reaches can
 * still be distinguished.
 * @return Set of watershed parameters
 */
ParameterSet StreamNetwork::getParams() const {
	ParameterSet params, tmp;

	//Get the parameters of the stream reaches
	std::ostringstream os;
	for(StreamReach *reach : _reaches) {
		os << _PARAMETER_PREFIX << reach->getIndex() << "_";
		tmp = reach->getParams();
		tmp.prependNames(os.str());
		params.add(tmp);
		os.str("");
		os.clear();
	}

	//Get the parameters of the objects referenced to by all the reaches
	for(ParametrizableObject *sharedObject : _shared_params)
		params.add(sharedObject->getParams());

	return params;
}


/**
 * @brief Sets the network parameters. The parameters which are specific to
 * stream reach [i] should be prepended with the prefix "REACH[i]_". Not all
 * parameters need be specified, only those contained in the input argument will
 * be modified in the StreamNetwork instance. Unknown parameters will be ignored.
 * @param params New values of the watershed parameters.
 */
void StreamNetwork::setParams(const ParameterSet& params) {
	ParameterSet tmp;
	std::string prefix;
	std::ostringstream os;

	//Set the parameters of the individual stream reaches
	for(StreamReach *reach : _reaches) {
		os << _PARAMETER_PREFIX << reach->getIndex() << "_";
		tmp = params.getSubset(os.str());
		tmp.ltrimNames(os.str());
		reach->setParams(tmp);
		os.str("");
		os.clear();
	}

	//Set the parameters of the objects referenced to by all the reaches
	for(ParametrizableObject *sharedObject : _shared_params)
		sharedObject->setParams(params);
}


/**
 * @brief Sets the watershed associated with the current stream network. This
 * function associates each stream reach with its corresponding subwatershed.
 * It also identifies the subwatersheds which are not headwater catchments.
 * @param watershed Reference to the Watershed object containing the
 * subwatersheds
 */
void StreamNetwork::setWatershed(const Watershed& watershed) {
	if(_computes_temp && !watershed.computesTemperature())
		std::runtime_error("You required stream temperature to be computed, "
				"but you did not specify any model to compute the temperature of "
				"subsurface water originating from the hillslopes. Please indicate "
				"a model different than NONE in key SUBWATERSHED_TEMPERATURE_MODEL "
				"in section [" + SFConstants::CONFIG_SECTION_NAME +
				"] of the configuration file");

	for(StreamReach *reach : _reaches) {
		reach->setSubwatershed(identifySubwatershed(*reach, watershed));
	}

	_distance_precision = watershed.begin()->getMask().cellsize/2.0;
	_is_set = true;
}


void StreamNetwork::advanceInTime() {
	if(!_is_set)
		throw RuntimeException("Method setWatershed must be called before "
				"advancing stream network in time", AT);
	for(StreamReach *reach : _reaches) {
		reach->updateInletBoundaryConditions();
		reach->updateLateralBoundaryConditions();
		reach->advanceInTime();
	}

}


void StreamNetwork::reset() {
	for(StreamReach *reach : _reaches)
		reach->reset();
}


void StreamNetwork::setDisplayInfo(const bool& display) {
	for(StreamReach* reach : _reaches)
		reach->setDisplayInfo(display);
}


/**
 * @brief Returns the ID number of the subwatershed in which the given point
 * is located, as well as the index of the stream reach's cell it belongs to
 * @param[in] point Point to localize
 * @param[out] subwatershedId ID number of the subwatershed in which the point
 * is located. In case the point is not located in any subwatershed, the value
 * of this argument is undefined.
 * @param[out] cellId Index (starting at 0) of the stream reach's cell in
 * which the point is located
 * @return True if point could be localized successfully, false otherwise
 */
bool StreamNetwork::localizePoint(const mio::Coords& point, size_t& subwatershedId,
		size_t& cellId) const {
	if(!_is_set)
		throw RuntimeException("Method setWatershed must be called before "
				"localizing points", AT);
	mio::Coords cellCoords;
	for(StreamReach *reach : _reaches) {
		for(size_t iCell = 0; iCell < reach->getNumCells(); ++iCell) {
			cellCoords = reach->getCell(iCell).coords;
			if( fabs(point.getEasting() - cellCoords.getEasting()) < _distance_precision &&
			    fabs(point.getNorthing() - cellCoords.getNorthing()) < _distance_precision) {
				subwatershedId = reach->getIndex();
				cellId = iCell;
				return true;
			}
		}
	}
	return false;
}


/**
 * @brief Destructor of class StreamNetwork
 */
StreamNetwork::~StreamNetwork() {
	for(StreamReach *reach : _reaches)
		delete reach;

	for(SharedObject *object : _shared_objects)
		delete object;
}


/**
 * @brief Constructs the stream reaches
 * @param cfg Reference to the Config object holding the simulation parameters
 * @param clock Reference to the Clock object computing the simulation time step
 * @param cellsReader Reference to the object used to read the coordinates of the
 * reach cells
 * @param inputReader Reference to the InputReader object used to read the input
 * values of the stream reaches (meteo variables, soil temperature, etc.)
 */
void StreamNetwork::createReaches(const mio::Config& cfg, Clock& clock,
		const ReachCellsReader& cellsReader, std::shared_ptr<DataContainer> dataContainer) {
	_reaches.reserve(_reach_indices.size());

	//Instantiate the proper class of stream reaches
	std::string name("LUMPED_DIRECT_ROUTING");
	cfg.getValue("STREAM_CLASS", SFConstants::CONFIG_SECTION_NAME, name,
			mio::IOUtils::nothrow);
	mio::IOUtils::toUpper(name);
	std::cout << "[i] Selected stream type: " << name << std::endl;
	if(name == "LUMPED_MUSKINGUM_CUNGE")
		this->createLumpedMuskingumCungeReaches(cfg, clock, cellsReader);
	else if(name == "DISCRETIZED_MUSKINGUM_CUNGE")
		this->createDiscretizedMuskingumCungeReaches(cfg, clock, cellsReader);
	else if(name == "LUMPED_DIRECT_ROUTING")
		this->createLumpedDirectRoutingReaches(cfg, cellsReader);
	else if(name == "DISCRETIZED_DIRECT_ROUTING")
		this->createDiscretizedDirectRoutingReaches(cfg, cellsReader);
	else
		throw InvalidConfigKeyException("Key STREAM_CLASS in section [" +
				SFConstants::CONFIG_SECTION_NAME + "] of the configuration "
				"file can only take the following values: LUMPED_MUSKINGUM_CUNGE, "
				"DISCRETIZED_MUSKINGUM_CUNGE, LUMPED_DIRECT_ROUTING or "
				"DISCRETIZED_DIRECT_ROUTING", AT);

	//If the water temperature in the stream reaches should be computed, apply
	//the appropriate decorator to the stream reach objects
	if(_computes_temp) {
		const std::string modelType(getTemperatureModelName(cfg));
		std::cout << "[i] Selected stream temperature model: "
		          << modelType << std::endl;
		if(modelType == "ENERGY_BALANCE")
			if(_reaches.front()->getDiscretization() == Discretization::Lumped)
				this->applyLumpedEnergyBalanceDecorator(cfg, dataContainer);
			else
				this->applyDiscretizedEnergyBalanceDecorator(cfg, clock, dataContainer);
		else
			throw InvalidConfigKeyException("Key STREAM_TEMPERATURE_MODEL in "
					"section [" + SFConstants::CONFIG_SECTION_NAME + "] of the "
					"configuration file can only take the following values: "
					"NONE or ENERGY_BALANCE", AT);
	}
}


/**
 * @brief This method fills attribute _reaches with instances of class
 * LumpedMuskingumReach
 * @param cfg Reference to the Config object containing the simulation
 * parameters
 * @param clock Reference to the Clock object computing the simulation time step
 * @param reader Reference to the object used to read the coordinates of the
 * reach cells
 */
void StreamNetwork::createLumpedMuskingumCungeReaches(const mio::Config& cfg,
		Clock& clock, const ReachCellsReader& reader) {
	//Instantiate the object used to model the stream bed friction
	StreamBedFrictionModel& fricModel(this->constructFrictionModel(cfg));

	//Construct the model used to compute the reach widths
	StreamWidthModel& widthModel(this->constructWidthModel(cfg));

	//Instantiate the stream reaches
	StreamReach *tmp(nullptr);
	for(const size_t& index : _reach_indices) {
		tmp = new LumpedMuskingumCungeReach(reader.getReachCells(index), widthModel,
				clock, fricModel);
		_reaches.push_back(tmp);
	}
}


/**
 * @brief This method fills attribute _reaches with instances of class
 * DiscrMuskingumCungeReach
 * @param cfg Reference to the Config object containing the simulation
 * parameters
 * @param clock Reference to the Clock object computing the simulation time step
 * @param reader Reference to the object used to read the coordinates of the
 * reach cells
 */
void StreamNetwork::createDiscretizedMuskingumCungeReaches(const mio::Config& cfg,
		Clock& clock, const ReachCellsReader& reader) {
	//Instantiate the object used to model the stream bed friction
	StreamBedFrictionModel& fricModel(this->constructFrictionModel(cfg));

	//Construct the model used to compute the reach widths
	StreamWidthModel& widthModel(this->constructWidthModel(cfg));

	//Instantiate the stream reaches
	StreamReach *tmp(nullptr);
	for(const size_t& index : _reach_indices) {
		tmp = new DiscrMuskingumCungeReach(reader.getReachCells(index), widthModel,
				clock, fricModel);
		_reaches.push_back(tmp);
	}
}


/**
 * @brief This method fills attribute _reaches with instances of class
 * InstantaneousRoutingReach
 * @param cfg Reference to the Config object containing the simulation
 * parameters
 * @param reader Reference to the object used to read the coordinates of the
 * reach cells
 */
void StreamNetwork::createLumpedDirectRoutingReaches(const mio::Config& cfg,
			const ReachCellsReader& reader) {
	//Instantiate the object used to compute the water depth
	StreamHeightModel& heightModel(this->constructHeightModel(cfg));

	//Construct the model used to compute the reach widths
	StreamWidthModel& widthModel(constructWidthModel(cfg));

	//Instantiate the stream reaches
	StreamReach *tmp(nullptr);
	for(const size_t& index : _reach_indices) {
		tmp = new InstantaneousRoutingReach(reader.getReachCells(index), widthModel,
				_clock, heightModel);
		_reaches.push_back(tmp);
	}
}


/**
 * @brief This method fills attribute _reaches with instances of class
 * DirectRoutingReach
 * @param cfg Reference to the Config object containing the simulation
 * parameters
 * @param reader Reference to the object used to read the coordinates of the
 * reach cells
 */
void StreamNetwork::createDiscretizedDirectRoutingReaches(const mio::Config& cfg,
			const ReachCellsReader& reader) {
	//Instantiate the object used to compute the water depth
	StreamHeightModel& heightModel(this->constructHeightModel(cfg));

	//Construct the model used to compute the reach widths
	StreamWidthModel& widthModel(constructWidthModel(cfg));

	//Instantiate the stream reaches
	StreamReach *tmp(nullptr);
	for(const size_t& index : _reach_indices) {
		tmp = new DirectRoutingReach(reader.getReachCells(index), widthModel,
				_clock, heightModel);
		_reaches.push_back(tmp);
	}
}


void StreamNetwork::applyLumpedEnergyBalanceDecorator(const mio::Config& cfg,  std::shared_ptr<DataContainer> dataContainer) {
	
	dataContainer->addLoadInstruction(Variables::TSOIL,DataType::NETWORK_POINTS);
	dataContainer->addLoadInstruction(Variables::TA,DataType::NETWORK_POINTS);
	dataContainer->addLoadInstruction(Variables::ISWR,DataType::NETWORK_POINTS);
	dataContainer->addLoadInstruction(Variables::ILWR,DataType::NETWORK_POINTS);
	dataContainer->addLoadInstruction(Variables::RH,DataType::NETWORK_POINTS);
	dataContainer->addLoadInstruction(Variables::VW,DataType::NETWORK_POINTS);


	//Create the object used to compute the energy fluxes
	ReachHeatFluxModel *fluxModel(new ReachHeatFluxModel(cfg));
	_shared_objects.push_back(fluxModel);
	_shared_params.push_back(fluxModel);

	//Decorate the StreamReach objects
	LumpedReachInterface *tmp(nullptr);
	for(StreamReach*& reach : _reaches) { //< we copy the pointers by reference!
		tmp = dynamic_cast<LumpedReachInterface*>(reach);
		reach = new LumpedReachWithEB(tmp, *fluxModel, dataContainer);
	}

}


void StreamNetwork::applyDiscretizedEnergyBalanceDecorator(const mio::Config& cfg,
		Clock& clock, std::shared_ptr<DataContainer> dataContainer) {
			
	dataContainer->addLoadInstruction(Variables::TSOIL,DataType::WATERSHED_AVG);
	dataContainer->addLoadInstruction(Variables::TSOIL,DataType::NETWORK_POINTS);
	dataContainer->addLoadInstruction(Variables::TA,DataType::NETWORK_POINTS);
	dataContainer->addLoadInstruction(Variables::ISWR,DataType::NETWORK_POINTS);
	dataContainer->addLoadInstruction(Variables::ILWR,DataType::NETWORK_POINTS);
	dataContainer->addLoadInstruction(Variables::RH,DataType::NETWORK_POINTS);
	dataContainer->addLoadInstruction(Variables::VW,DataType::NETWORK_POINTS);
	
	//Create the object used to compute the energy fluxes
	ReachHeatFluxModel *fluxModel(new ReachHeatFluxModel(cfg));
	_shared_objects.push_back(fluxModel);
	_shared_params.push_back(fluxModel);

	//Retrieve the stream bed friction model in the vector containing the shared
	//objects, or construct a new one if none was found
	StreamBedFrictionModel *fricModel(nullptr);
	for(ParametrizableObject* obj : _shared_params) {
		fricModel = dynamic_cast<StreamBedFrictionModel*>(obj);
		if(fricModel) break; //< break loop if stream bed friction model was found
	}
	if(!fricModel)
		fricModel = &this->constructFrictionModel(cfg);

	//Decorate the StreamReach objects
	DiscretizedReachInterface *tmp(nullptr);
	for(StreamReach*& reach : _reaches) { //< we copy the pointers by reference!
		tmp = dynamic_cast<DiscretizedReachInterface*>(reach);
		reach = new DiscretizedReachWithEB(tmp, *fluxModel, *fricModel, clock,  dataContainer);
	}
}


/**
 * @brief This method connects each stream reach with those located directly
 * upstream (if any)
 * @param reader Reference to the object used to read the coordinates of the
 * reach cells and the indices of the upstream reaches
 */
void StreamNetwork::connectReaches(const ReachCellsReader& reader) {
	auto getReachPtr = [&](const size_t& index) {
		return _reaches.at(std::find(_reach_indices.begin(), _reach_indices.end(), index)
				- _reach_indices.begin());
	};

	std::vector<size_t> upstreamIndices;
	for(size_t iIndex = 0; iIndex < _reach_indices.size(); ++iIndex) {
		upstreamIndices = reader.getUpstreamReaches(_reach_indices.at(iIndex));
		if(upstreamIndices.size() == 1)
			_reaches[iIndex]->setUpstreamReach(*getReachPtr(upstreamIndices.front()));
		else if(upstreamIndices.size() == 2)
			_reaches[iIndex]->setUpstreamReaches(*getReachPtr(upstreamIndices.front()),
					*getReachPtr(upstreamIndices.back()));
	}
}


/**
 * @brief Returns an instance of the correct StreamHeightModel subclass,
 * constructed according to the information contained in the configuration file
 * @param cfg Reference to the Config object holding the simulation parameters
 * @return Reference to the correct StreamHeightModel
 */
StreamHeightModel& StreamNetwork::constructHeightModel(const mio::Config& cfg) {
	StreamHeightModel *heightModel(nullptr);
	std::string type("DISCHARGE_POWER");
	cfg.getValue("STREAM_HEIGHT_MODEL", SFConstants::CONFIG_SECTION_NAME, type,
			mio::IOUtils::nothrow);
	mio::IOUtils::toUpper(type);
	std::cout << "[i] Selected water depth model: " << type << std::endl;
	if(type == "DISCHARGE_POWER")
		heightModel = new DischargePowerHeightModel(cfg);
	else if(type == "CONST_VELOCITY")
		heightModel = new PrescribedVelocityModel(cfg);
	else
		throw InvalidConfigKeyException("Key STREAM_HEIGHT_MODEL in section ["
				+ SFConstants::CONFIG_SECTION_NAME + "] of the configuration "
				"file can only take the following values: DISCHARGE_POWER or "
				"CONST_VELOCITY", AT);

	_shared_objects.push_back(heightModel);
	_shared_params.push_back(heightModel);

	return *heightModel;
}


/**
 * @brief Returns an instance of the correct StreamWidthModel subclass,
 * constructed according to the information contained in the configuration file
 * @param cfg Reference to the Config object holding the simulation parameters
 * @return Reference to the correct StreamWidthModel
 */
StreamWidthModel& StreamNetwork::constructWidthModel(const mio::Config& cfg) {
	StreamWidthModel *model;
	std::string type("AREA_POWER");
	cfg.getValue("STREAM_WIDTH_MODEL", SFConstants::CONFIG_SECTION_NAME, type,
			mio::IOUtils::nothrow);
	mio::IOUtils::toUpper(type);
	std::cout << "[i] Selected stream width model: " << type << std::endl;

	if(type == "AREA_POWER")
		model = new AreaPowerWidthModel(cfg);
	else if(type == "AREA_LINEAR")
		model = new AreaLinearWidthModel(cfg);
	else if(type == "DISCHARGE_POWER")
		model = new DischargePowerWidthModel(cfg);
	else
		throw InvalidConfigKeyException("Key STREAM_WIDTH_MODEL in section [" +
				SFConstants::CONFIG_SECTION_NAME + "] of the configuration file "
				"can only take the following values: AREA_POWER, AREA_LINEAR or "
				"DISCHARGE_POWER", AT);

	_shared_objects.push_back(model);
	_shared_params.push_back(model);

	return *model;
}


/**
 * @brief Constructs an object used to compute the effect of bed friction on the
 * stream flow based on the information contained in the Config file
 * @param cfg Reference to the Config object holding the simulation parameters
 * @return Reference to the correct StreamBedFrictionModel
 */
StreamBedFrictionModel& StreamNetwork::constructFrictionModel(const mio::Config& cfg) {
	StreamBedFrictionModel *fricModel(nullptr);
	std::string name("MANNING");
	cfg.getValue("STREAM_BED_FRICTION", SFConstants::CONFIG_SECTION_NAME,
			name, mio::IOUtils::nothrow);
	mio::IOUtils::toUpper(name);
	std::cout << "[i] Selected model to compute stream bed friction: "
	          << name << std::endl;
	if(name == "MANNING")
		fricModel = new ManningFrictionModel(cfg);
	else if(name == "CHEZY")
		fricModel = new ChezyFrictionModel(cfg);
	else
		throw InvalidConfigKeyException("Key STREAM_BED_FRICTION in section ["
				+ SFConstants::CONFIG_SECTION_NAME + "] of the configuration "
				"file can only take the following values: MANNING or CHEZY", AT);

	_shared_objects.push_back(fricModel);
	_shared_params.push_back(fricModel);

	return *fricModel;
}


/**
 * @brief Returns the name of the model to be used in order to compute water
 * temperature in the stream reaches
 * @param cfg Reference to the Config instance holding the model parameters
 * @return Name of the temperature model to be used by the stream network
 */
std::string StreamNetwork::getTemperatureModelName(const mio::Config& cfg) {
	std::string name("None");
	cfg.getValue("STREAM_TEMPERATURE_MODEL", SFConstants::CONFIG_SECTION_NAME,
			name, mio::IOUtils::nothrow);
	mio::IOUtils::toUpper(name);
	return name;
}


/**
 * @brief Returns a reference to the correct instance of class ReachCellsReader
 * to be used to read the coordinates of the cells composing the stream reaches
 * @param cfg Reference to the Config object holding the simulation parameters
 * @return Reference to the correct instance of class ReachCellsReader
 */
const ReachCellsReader& StreamNetwork::getReachCellsReader(const mio::Config& cfg, std::shared_ptr<DataContainer> dataContainer) {
	const ReachCellsReader *reader(nullptr);

	std::string type("TAUDEM");
	cfg.getValue("REACH_CELLS_READER", SFConstants::CONFIG_SECTION_NAME,
			type, mio::IOUtils::nothrow);
	mio::IOUtils::toUpper(type);
	std::cout << "[i] Coordinates of the stream cells will be read using an "
	          "object of type: " << type << std::endl;

	if(type == "TAUDEM")
		reader = new TaudemReachFilesReader(cfg, dataContainer);
	else
		throw InvalidConfigKeyException("Key REACH_CELLS_READER in section [" +
				SFConstants::CONFIG_SECTION_NAME + "] of the configuration file "
				"can only take the following value: TAUDEM", AT);

	return *reader;
}


/**
 * @brief Method returning a reference to the subwatershed containing the
 * stream reach passed as input argument
 * @param reach Stream reach whose associated subwatershed is to be found
 * @param watershed Reference to the Watershed object containing all the
 * subwatersheds
 * @return Subwatershed associated with the input stream reach
 */
const Subwatershed& StreamNetwork::identifySubwatershed(const StreamReach& reach,
		const Watershed& watershed) {
	const Subwatershed *subwatershed(nullptr);
	for(const Subwatershed& sub : watershed) {
		if(sub.contains(reach, true)) {
			subwatershed = &sub;
			break;
		}
	}
	if(!subwatershed)
		throw RuntimeException("Could not find the subwatershed associated "
				"with the given stream reach", AT);
	return *subwatershed;
}


//-----------------------------------------------------------------------------
// Implementation of class NetworkIterator
//-----------------------------------------------------------------------------

StreamNetwork::const_iterator::const_iterator(const StreamNetwork& network, const size_t& pos) :
		_network(&network), _curr_pos(std::min(pos, network._reaches.size()))
{}

StreamNetwork::const_iterator::const_iterator(const const_iterator& copy) :
		_network(copy._network), _curr_pos(copy._curr_pos) {}

StreamNetwork::const_iterator& StreamNetwork::const_iterator::operator=(const const_iterator& copy) {
	_network = copy._network;
	_curr_pos = copy._curr_pos;
	return *this;
}

const StreamReach& StreamNetwork::const_iterator::operator*() const {
	return *(this->operator->());
}

const StreamReach* StreamNetwork::const_iterator::operator->() const {
	if(_curr_pos == _network->_reaches.size())
		throw InvalidIteratorException("StreamNetwork iterator points on "
				"last-plus-one element", AT);

	return _network->_reaches.at(_curr_pos);
}

StreamNetwork::const_iterator& StreamNetwork::const_iterator::operator++() {
	if(_curr_pos != _network->_reaches.size())
		++_curr_pos;

	return *this;
}

StreamNetwork::const_iterator StreamNetwork::const_iterator::operator+(const size_t& offset) const {
	size_t newPos = std::min(_curr_pos + offset, _network->_reaches.size());
	const_iterator it(*_network, newPos);
	return it;
}

StreamNetwork::const_iterator StreamNetwork::const_iterator::operator-(const size_t& offset) const {
	size_t newPos = std::max(size_t(0), _curr_pos - offset);
	const_iterator it(*_network, newPos);
	return it;
}

bool StreamNetwork::const_iterator::operator!=(const StreamNetwork::const_iterator& other) const {
	return (_network != other._network) || (_curr_pos != other._curr_pos);
}

StreamNetwork::const_iterator::~const_iterator() {}

} /* namespace sflow */
