/*****************************************************************************/
/* Copyright 2015 (CRYOS Laboratory, EPFL, Switzerland)                      */
/*****************************************************************************/
/* This file is part of StreamFlow.
   StreamFlow is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   StreamFlow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with StreamFlow. If not, see <http://www.gnu.org/licenses/>.
*/

#include <list>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <streamflow/libstreamflow.h>
#include <meteoio/MeteoIO.h>
//-----------------------------------------------------------------------------
// Forward declarations (implementations are located below main function)
//-----------------------------------------------------------------------------
struct ProgramOptions {
	ProgramOptions() : cfg_file(), start_date(), end_date(), warmup_start_date(),
			warmup_end_date(), warmup_simulation(false), run_simulation(true),
			calibrate_model(false), exit_program(false) {}
	std::string cfg_file;
	mio::Date   start_date;
	mio::Date   end_date;
	mio::Date   warmup_start_date;
	mio::Date   warmup_end_date;
	bool        warmup_simulation;
	bool        run_simulation;
	bool        calibrate_model;
	bool        exit_program;
};
void realMain(int argc, char *argv[]);
void displayProgamInfo(int argc, char *argv[]);
ProgramOptions getProgramOptions(int argc, char *argv[]);
void validateProgramOptions(ProgramOptions& opts, const sflow::StreamflowConfig& cfg);
void checkAndCorrectConfig(sflow::StreamflowConfig& cfg);
std::string getOptionArgument(const std::string& option);
void usage(const std::string progName);
//void getMeasurementDateRange(const sflow::StreamflowConfig& cfg,
	//	mio::Date& startDate, mio::Date& endDate);


//-----------------------------------------------------------------------------
// Main function called on program execution
//-----------------------------------------------------------------------------
int main(int argc, char *argv[]) {
	try {
		realMain(argc, argv);
	} catch (const std::exception &e) {
		std::cerr << "ERROR " << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}


//-----------------------------------------------------------------------------
// Implementation of the above-declared functions
//-----------------------------------------------------------------------------
void realMain(int argc, char *argv[]) {

	ProgramOptions progOpts(getProgramOptions(argc, argv));
	if(progOpts.exit_program) return;

	displayProgamInfo(argc, argv);

	sflow::StreamflowConfig cfg(progOpts.cfg_file);

	validateProgramOptions(progOpts,cfg);

  checkAndCorrectConfig(cfg);

	if(progOpts.warmup_simulation) {
		std::cout << "[i] Warmup start date: "
		          << progOpts.warmup_start_date.toString(mio::Date::ISO) << std::endl;
		std::cout << "[i] Warmup end date:   "
				  << progOpts.warmup_end_date.toString(mio::Date::ISO)
				  << std::endl;
	}
	std::cout << "[i] Simulation start date: "
	          << progOpts.start_date.toString(mio::Date::ISO) << std::endl;
	std::cout << "[i] Simulation end date:   "
			  << progOpts.end_date.toString(mio::Date::ISO)
			  << std::endl;
	std::cout << "[i] Initializing model..." << std::endl;



	mio::Date startData = progOpts.start_date;
	mio::Date endData = progOpts.end_date;
	if(progOpts.warmup_simulation){
		startData=std::min(progOpts.start_date, progOpts.warmup_start_date);
		endData=std::max(progOpts.end_date, progOpts.warmup_end_date);
	}

	sflow::HydrologicalModel model(cfg, progOpts.start_date, progOpts.end_date,startData,endData);

	std::cout << "[i] Setting warmup phase" << std::endl;
	if(progOpts.warmup_simulation){
		model.setWarmupPhase(progOpts.warmup_start_date, progOpts.warmup_end_date);
	}
	std::cout << "[i] Model initialization complete" << std::endl;

	if(progOpts.calibrate_model && progOpts.run_simulation) {
		sflow::ModelCalibration::calibrate(model, cfg,progOpts.start_date,progOpts.end_date);
	}
	else if(progOpts.run_simulation) {
		std::cout << "[i] Creating writer" << std::endl;
		sflow::OutputWriter writer(cfg, model.getClock());
		std::cout << "[i] Adding observer" << std::endl;
		model.addObserver(writer);
		std::cout << "[i] Running" << std::endl;
		model.run();
	}
}


void displayProgamInfo(int argc, char *argv[]) {
	std::cout << "\nLibstreamflow " << sflow::getLibVersion() << std::endl;
	std::cout << "MeteoIO " << mio::getLibVersion() << std::endl;
	std::cout << "Command line:";
	for(int iArg=0; iArg < argc; iArg++)
		std::cout << " " << argv[iArg];
	std::cout << std::endl << std::endl;
}


ProgramOptions getProgramOptions(int argc, char *argv[]) {
	ProgramOptions opts;

	if(argc == 1) {
		std::cout << "Type " << argv[0] << " --help to get more information\n";
		opts.exit_program = true;
		return opts;
	}

	std::string arg(argv[1]);
	if(arg == "-h" || arg == "--help") {
		usage(argv[0]);
		opts.exit_program = true;
		return opts;
	}

	for(int iArg = 1; iArg < argc; ++iArg) {
		arg = std::string(argv[iArg]);
		if(arg == "--no-compute") {
			opts.run_simulation = false;
		} else if(arg == "-c" || arg == "-calibrate") {
			opts.calibrate_model = true;
		} else if(arg.substr(0, 2) == "-i" || arg.substr(0, 8) == "--iofile") {
			opts.cfg_file = getOptionArgument(arg);
		} else if(arg.substr(0, 2) == "-a" || arg.substr(0, 11) == "--startdate") {
			if(!mio::IOUtils::convertString(opts.start_date, getOptionArgument(arg), 0)) //< assume UTC, correct afterwards in validateProgramOptions()
				throw std::invalid_argument("Start date must be expressed as "
						"YYYY-MM-DDTHH:MM (e.g.:2008-08-11T09:00)");
		} else if(arg.substr(0, 2) == "-z" || arg.substr(0, 9) == "--enddate") {
			if(!mio::IOUtils::convertString(opts.end_date, getOptionArgument(arg), 0)) //< assume UTC, correct afterwards in validateProgramOptions()
				throw std::invalid_argument("End date must be expressed as "
						"YYYY-MM-DDTHH:MM (e.g.:2008-08-11T09:00)");
		} else if(arg.substr(0, 18) == "--warmup-startdate") {
			opts.warmup_simulation = true;
			if(!mio::IOUtils::convertString(opts.warmup_start_date, getOptionArgument(arg), 0)) //< assume UTC, correct afterwards in validateProgramOptions()
				throw std::invalid_argument("Warmup start date must be expressed as "
						"YYYY-MM-DDTHH:MM (e.g.:2008-08-11T09:00)");
		} else if(arg.substr(0, 16) == "--warmup-enddate") {
			opts.warmup_simulation = true;
			if(!mio::IOUtils::convertString(opts.warmup_end_date, getOptionArgument(arg), 0)) //< assume UTC, correct afterwards in validateProgramOptions()
				throw std::invalid_argument("End date must be expressed as "
						"YYYY-MM-DDTHH:MM (e.g.:2008-08-11T09:00)");
		} else if(arg == "-h" || arg == "--help") {
			std::cout << "Option -h [--help] should be used alone\n";
			opts.exit_program = true;
			return opts;
		} else {
			throw std::invalid_argument("Unknown option \"" + arg + "\"");
		}
	}

	return opts;
}


void validateProgramOptions(ProgramOptions& opts, const sflow::StreamflowConfig& cfg) {
	//mio::Date startDate, endDate;
	//getMeasurementDateRange(cfg, startDate, endDate);
	/*
	const double timeZone(cfg.get("TIME_ZONE", "INPUT")); //< in hours
	const double timeZoneInDays(timeZone/24.0);

	if(opts.start_date.isUndef() && startDate.isUndef()) {
		throw std::runtime_error("Cannot determine the simulation start time "
				"based on the input data, please specify it using option -a "
				"while calling the executable");
	} else if(opts.start_date.isUndef()) {
		opts.start_date = startDate;
	} else {
		opts.start_date.setTimeZone(timeZone);
		opts.start_date -= timeZoneInDays;
		if(!startDate.isUndef() && opts.start_date < startDate)
			throw std::runtime_error("The specified simulation start date is "
					"anterior to the first available input data, please provide "
					"a latter date in option -a");
	}

	if(opts.end_date.isUndef() && endDate.isUndef()) {
		throw std::runtime_error("Cannot determine the simulation end time "
				"based on the input data, please specify it using option -z "
				"while calling the executable");
	} else if(opts.end_date.isUndef()) {
		opts.end_date = endDate;
	} else {
		opts.end_date.setTimeZone(timeZone);
		opts.end_date -= timeZoneInDays;
		if(!endDate.isUndef() && opts.end_date > endDate)
			throw std::runtime_error("The specified simulation end date is "
					"posterior to the last available input data, please provide "
					"an earlier date in option -z");
	}
	*/
	const double timeZone(cfg.get("TIME_ZONE", "INPUT")); //< in hours
	const double timeZoneInDays(timeZone/24.0);
	opts.start_date.setTimeZone(timeZone);
	opts.start_date -= timeZoneInDays;
	opts.end_date.setTimeZone(timeZone);
	opts.end_date -= timeZoneInDays;

	if(opts.end_date <= opts.start_date)
		throw std::runtime_error("The simulation end date must be posterior to "
				"the start date");

	if(opts.warmup_simulation && opts.warmup_start_date.isUndef())
		throw std::runtime_error("Option --warmup-enddate is provided, but "
				"option --warmup-startdate is missing");

	if(opts.warmup_simulation && opts.warmup_end_date.isUndef())
		throw std::runtime_error("Option --warmup-startdate is provided, but "
				"option --warmup-enddate is missing");
	/*
	if(opts.warmup_simulation) {
		opts.warmup_start_date.setTimeZone(timeZone);
		opts.warmup_start_date -= timeZoneInDays;
		opts.warmup_end_date.setTimeZone(timeZone);
		opts.warmup_end_date -= timeZoneInDays;
		if(!startDate.isUndef() && opts.warmup_start_date < startDate) {
			throw std::runtime_error("The specified warmup start date is "
					"anterior to the first available input data, please provide "
					"a latter date in option --warmup-startdate");
		} else if(!endDate.isUndef() && opts.warmup_end_date > endDate) {
			throw std::runtime_error("The specified warmup end date is "
					"posterior to the last available input data, please provide "
					"an earlier date in option --warmup-enddate");
		} else if(opts.warmup_start_date >= opts.warmup_end_date) {
			throw std::runtime_error("The end date of the warmup phase must be "
					"posterior to the start date");
		}*/
}


std::string getOptionArgument(const std::string& option) {
	size_t pos(option.find("="));
	if(pos == std::string::npos)
		throw std::invalid_argument("Option argument must be provided with a "
				"\"=\" sign (e.g. -a=2015-10-01T12:00)");

	return option.substr(pos + 1);
}


void usage(const std::string prog)
{
	const std::string indent("    ");
	const std::string doubleIndent(indent + indent);
	std::cout << prog << ": runs a hydrological simulation using the model StreamFlow "
	          << "(part of the Alpine3D suite)" << std::endl << std::endl;
	std::cout << "Usage:\n" << indent << prog << " [OPTION]\n\n";
	std::cout << "Available options:" << std::endl;
	std::cout << indent << "-a=DATE, --startdate=DATE\n";
	std::cout << doubleIndent << "Simulation start date, with DATE expressed "
	          << "as YYYY-MM-DDTHH:MM\n"
			  << doubleIndent << "[default: date of first available input data]\n\n";
	std::cout << indent << "-z=DATE, --enddate=DATE\n";
	std::cout << doubleIndent << "Simulation end date, with DATE expressed "
	          << "as in option -a\n"
			  << doubleIndent << "[default: date of last available input data]\n\n";
	std::cout << indent << "-i=FILE, --iofile=FILE\n";
	std::cout << doubleIndent << "Configuration file holding the model "
	          << "parameters [default: ./io.ini]\n\n";
	std::cout << indent << "--warmup-startdate=DATE\n";
	std::cout << doubleIndent << "Start date of the warmup phase, with DATE "
	          << "expressed as in option -a \n"
			  << doubleIndent << "Note: this option requires --warmup-enddate "
			  << "to be specified\n"
			  << doubleIndent << "[default: no warmup phase]\n\n";
	std::cout << indent << "--warmup-enddate=DATE\n";
	std::cout << doubleIndent << "End date of the warmup phase, with DATE "
	          << "expressed as in option -a\n"
			  << doubleIndent << "Note: this option requires --warmup-startdate "
			  << "to be specified\n"
			  << doubleIndent << "[default: no warmup phase]\n\n";
	std::cout << indent << "-c, --calibrate\n";
	std::cout << doubleIndent << "Calibrate model parameters [default: off]\n\n";
	std::cout << indent << "--no-compute\n";
	std::cout << doubleIndent << "Do not compute the simulation, only parse "
	          << "and validate the inputs\n"
	          << doubleIndent << "[default: off]\n\n";
	std::cout << indent << "-h, --help\n";
	std::cout << doubleIndent << "Print help message and version information\n";
}



void checkAndCorrectConfig(sflow::StreamflowConfig& cfg){
  if(cfg.keyExists("GRID2D", "INPUT"))
  {
    if(!cfg.keyExists("GRID2DPATH","INPUT"))
    {
      std::cout << "[W] Key GRID2D specified in INPUT section but no GRID2DPATH specified,\n \
                    data wont be loaded from grids." << std::endl;
    }
    const std::string grid_type=(cfg.get("GRID2D", "INPUT"));
    if(grid_type=="NETCDF"){
      if(!cfg.keyExists("GRID2DFILE","INPUT"))
      {
        std::cout << "[W] Key GRID2D specified as NETCDF in INPUT section but no GRID2DFILE specified,\n \
                      data wont be loaded from NETCDF grids." << std::endl;
      }
      if(cfg.keyExists("SOIL_GRIDS_NAME", "INPUT"))
      {
        cfg.addKey("NETCDF_VAR::TSOIL","INPUT",cfg.get("SOIL_GRIDS_NAME", "INPUT"));//< mandatory key
      }
    }
  }
}


/*
void getMeasurementDateRange(const sflow::StreamflowConfig& cfg,
		mio::Date& startDate, mio::Date& endDate) {
	std::string inputFolder;
	cfg.getValue("CATCHMENTS_PATH", "INPUTFROMA3D", inputFolder,
			mio::IOUtils::nothrow);

	if(!inputFolder.empty()) {
		std::list<std::string> files(mio::FileUtils::readDirectory(inputFolder, "catch"));
		if(files.empty())
			throw std::runtime_error("Cannot read subwatershed inflow values: "
					"folder " + inputFolder + " does not contain any file named " +
					"'catch*'. Please modify value of key CATCHMENTS_PATH in "
					"section [InputFromA3D] of the configuration file");

		sflow::SmetFilePreParser range(inputFolder, files.begin(), files.end());
		startDate = range.getStartDate();
		endDate   = range.getEndDate();
		return;
	}

	cfg.getValue("METEOPATH", "INPUTFROMA3D", inputFolder, mio::IOUtils::nothrow);

	if(!inputFolder.empty()) {
		const std::string exp = cfg.get("EXPERIMENT", "INPUTFROMA3D");
		std::list<std::string> files(mio::FileUtils::readDirectory(inputFolder,
				exp + ".smet"));
		if(files.empty())
			throw std::runtime_error("Cannot read meteo values over the stream "
					"cells: folder " + inputFolder + " does not contain any "
					"file named '*" + exp + ".smet'. Please modify value of keys "
					"METEOPATH and/or EXPERIMENT in section [InputFromA3D] of "
					"the configuration file");

		sflow::SmetFilePreParser range(inputFolder, files.begin(), files.end());
		startDate = range.getStartDate();
		endDate   = range.getEndDate();
	}
}*/
